from __future__ import absolute_import, unicode_literals

# This will make sure the app is always imported when
# Django starts so that shared_task will use this app.
from .celery import app as celery_app

__all__ = ('celery_app',)


emprered_logo = """
 _____                        ______         _
|  ___|                       | ___ \       | |
| |__ _ __ ___  _ __  _ __ ___| |_/ /___  __| |
|  __| '_ ` _ \| '_ \| '__/ _ \    // _ \/ _` |
| |__| | | | | | |_) | | |  __/ |\ \  __/ (_| |
\____/_| |_| |_| .__/|_|  \___\_| \_\___|\__,_|
               | |
               |_|
"""


print('-'*49)
print(emprered_logo)
print()
print("Made with a bunch of <3 by some nerds at UTN FRC.")
print('-'*49)
