from __future__ import absolute_import, unicode_literals
import os
from celery import Celery
from celery.utils.log import get_task_logger
from celery.schedules import crontab

# set the default Django settings module for the 'celery' program.
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'Emprered.settings')

app = Celery('Emprered')
logger = get_task_logger(__name__)

# Using a string here means the worker doesn't have to serialize
# the configuration object to child processes.
# - namespace='CELERY' means all celery-related configuration keys
#   should have a `CELERY_` prefix.
app.config_from_object('django.conf:settings', namespace='CELERY')

# Load task modules from all registered Django app configs.
app.autodiscover_tasks()


@app.on_after_configure.connect
def setup_periodic_tasks(sender, **kwargs):

    sender.add_periodic_task(60.0, check_events.s())
    sender.add_periodic_task(60.0, delete_old_notifications_t.s(7))

    # Calls test('world') every 30 seconds
    # sender.add_periodic_task(30.0, test.s('world'), expires=10)
    #
    # # Executes every Monday morning at 7:30 a.m.
    # sender.add_periodic_task(
    #     crontab(hour=7, minute=30, day_of_week=1),
    #     test.s('Happy Mondays!'),
    # )


@app.task(bind=True)
def debug_task(self):
    print('Request: {0!r}'.format(self.request))

@app.task
def add(x, y):
    return x + y

@app.task
def test(arg):
    print(arg)

@app.task
def check_events():
    logger.info("Chequeando eventos")
    from .server.views.eventos_view import EventosView
    EventosView.check_upcoming_events()

@app.task
def delete_old_notifications_t(days):
    logger.info("Borrando notificaciones viejas")
    from .server.views.notificaciones_view import NotificacionesView
    NotificacionesView.delete_old_notifications(days)
