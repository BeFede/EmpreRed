import { GET_CATALOGO, POST_CATALOGO, GET_DETALLE_CATALOGO, POST_DETALLE_CATALOGO, POST_COMENTARIO, DELETE_DETALLE_CATALOGO } from './types';
import { apiRoutes } from '../config/configs';
import * as axiosManager from 'axios';

const axios = axiosManager.create()
axios.defaults.headers = {
   Authorization: localStorage.getItem('token')
}

export const postCatalogo = (catalogo) => dispatch => {
    (catalogo===null) ? catalogo = {} : catalogo= catalogo;
    const urlPost = `${apiRoutes.urlCatalogos}`;
        return axios.post(urlPost, catalogo)
        .then(({data}) => {
            dispatch({
                type: POST_CATALOGO,
                payload: data.mensaje
            });
            // La peticion me devuelve un datos.id_catalogo
            axios.get(urlPost)
            .then(({data}) => {
            dispatch({
                type: GET_CATALOGO,
                payload: data.datos
                });
            })
            .catch(err => {
                console.log(err);
                dispatch({
                    type: GET_CATALOGO,
                    payload: err.response.data
                });
            });
            return { datos: data.datos, estado: data.estado };
        })
        .catch(err => {
            console.log(err);
            dispatch({
                type: POST_CATALOGO,
                payload: { estado: err.response.data.estado }
            });
            return { estado: err.response.data.estado, mensaje: err.response.data.mensaje };
        });
}

export const getCatalogo = (query) => dispatch => {

    let urlFinal;
    if(query)
    {
        urlFinal = `${apiRoutes.urlCatalogos}?titulo=${query.titulo}&precio_desde=${query.precio_desde}&precio_hasta=${query.precio_hasta}`;
    }
    else{
        urlFinal = `${apiRoutes.urlCatalogos}`;
    }
        return axios.get(urlFinal)
        .then(({data}) => {
            dispatch({
                type: GET_CATALOGO,
                payload: data.datos.catalogo
            });
            return { datos: data.datos.catalogo, estado: data.estado };
            })
            .catch(err => {
                console.log(err);
                dispatch({
                    type: GET_CATALOGO,
                    payload: err.response.data
                });
                return { estado: err.response.data.estado, mensaje: err.response.data.mensaje };
            });
}



export const getDetalleCatalogo = (id) => dispatch => {
    const urlFinal = `${apiRoutes.urlDetalleCatalogo}/${id}/`;
        return axios.get(urlFinal)
        .then(({data}) => {
            dispatch({
                type: GET_DETALLE_CATALOGO,
                payload: data.datos.detalle
            });
            
            return { datos: data.datos.detalle, estado: data.estado };
            })
            .catch(err => {
                console.log(err);
                dispatch({
                    type: GET_DETALLE_CATALOGO,
                    payload: err.response.data
                });
                return { estado: err.response.data.estado, mensaje: err.response.data.mensaje };
            });
}

export const postDetalleCatalogo = (detalle) => dispatch => {
    
    let urlPost = '';
    // SI viene con id es un update, sino un post
    if(detalle.id) {
        urlPost = `${apiRoutes.urlDetalleCatalogo}/${detalle.id}`;
    } else {
        urlPost = `${apiRoutes.urlDetalleCatalogo}`;
    }

    console.log(detalle)
    var detalleFormData = new FormData();
    Object.keys(detalle).map(function(key, index) {        
        if (key != 'nuevasFotos'){
            detalleFormData.append(key, detalle[key])
        }
        else {
            //detalleFormData.append('fotosNuevas', detalle[key])
            detalle[key].forEach(foto => {                         
                detalleFormData.append('fotosNuevas', foto)
            });
        }
    });

    let axiosConfig = {
        headers: {
            "Content-Type": "multipart/form-data",
        }
    };

    return axios.post(urlPost, detalleFormData, axiosConfig)
        .then(({data}) => {
            dispatch({
                type: POST_DETALLE_CATALOGO,
                payload: data.mensaje
            });
            
            return { estado: data.estado };
        })
        .catch(err => {
            console.log(err);
            dispatch({
                type: POST_DETALLE_CATALOGO,
                payload: { estado: err.response.data.estado }
            });
            return { estado: err.response.data.estado, mensaje: err.response.data.mensaje };
        });
}

export const deleteDetalleCatalogo = (detalles, idDetalle) => dispatch => {

    const urlDelete = `${apiRoutes.urlDetalleCatalogo}/${idDetalle}`;

    //Actualizamos el estado de la aplicacion
    for (var i = 0; i < detalles.length; i++) {
        if (idDetalle === detalles[i].id){
            detalles.splice(i,1);
        }
    }
        return axios.delete(urlDelete)
        .then(({data}) => {
            dispatch({
                type: DELETE_DETALLE_CATALOGO,
                payload: data.mensaje
            });
            
            return { estado: data.estado };
        })
        .catch(err => {
            console.log(err);
            dispatch({
                type: DELETE_DETALLE_CATALOGO,
                payload: { estado: err.response.data.estado }
            });
            return { estado: err.response.data.estado, mensaje: err.response.data.mensaje };
        });
}


export const postComentario = (comentario) => dispatch => {
    const urlPost = `${apiRoutes.urlPostComentario}`;
    const urlGetDetalle = `${apiRoutes.urlDetalleCatalogo}/${comentario.id_detalle}`;
        return axios.post(urlPost, comentario)
        .then(({data}) => {
            dispatch({
                type: POST_COMENTARIO,
                payload: data.mensaje
            });
            // Actualizo la variable del detalle de redux para poder actualizar los comentarios en tiempo real ahre
            axios.get(urlGetDetalle)
            .then(({data}) => {
                dispatch({
                    type: GET_DETALLE_CATALOGO,
                    payload: data.datos.detalle
                    });
                    // return { datos: data.datos.detalle, estado: data.estado };
            })
            .catch(err => {
                console.log(err);
                dispatch({
                    type: GET_DETALLE_CATALOGO,
                    payload: err.response.data
                });
                // return { estado: err.response.data.estado, mensaje: err.response.data.mensaje };
            });
            return { estado: data.estado };
        })
        .catch(err => {
            console.log(err);
            dispatch({
                type: POST_COMENTARIO,
                payload: { estado: err.response.data.estado }
            });
            return { estado: err.response.data.estado, mensaje: err.response.data.mensaje };
        });
}
