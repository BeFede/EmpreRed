import { REGISTRAR_CLIENTE_FAIL, REGISTRAR_CLIENTE_SUCCESS, GET_CLIENTES, GET_CLIENTE, ACTUALIZAR_CLIENTE } from './types';
import * as axiosManager from 'axios';
import { apiRoutes } from '../config/configs';

const axios = axiosManager.create()
axios.defaults.headers = {
   Authorization: localStorage.getItem('token')
}

export const getCliente = () => dispatch => {
    // Esta sintaxis se llama "curried functions", busquenla
    // si no entienden que hace con el dispatch    
            // Aca hacemos la peticion a axios
           return  axios.get(apiRoutes.urlCliente)
            .then(({data}) => {
                dispatch({
                    type: GET_CLIENTE,
                    payload: data.datos.clientes
                })
                return data;
            })
            .catch(err => {
                console.log(err);
            });
    
    }
    


export const getClientes = () => dispatch => {
// Esta sintaxis se llama "curried functions", busquenla
// si no entienden que hace con el dispatch
        // Aca hacemos la peticion a axios
       return  axios.get(apiRoutes.urlClientes)
        .then(({data}) => {
            dispatch({
                type: GET_CLIENTES,
                payload: data.datos.clientes
            })
            return data;
        })
        .catch(err => {
            console.log(err);
        });

}

export const putDatosC = (datos) => dispatch => {

    const urlFinal = `${apiRoutes.urlClientes}`;
   
        return axios.put(urlFinal, datos)
        .then(({data}) => {
            dispatch({
                type: ACTUALIZAR_CLIENTE,
                payload: { estado: data.estado }
            });
            return { estado: data.estado };
        })
        .catch(err => {
            console.log(err);
            dispatch({
                type: ACTUALIZAR_CLIENTE,
                payload: { estado: err.response.data.estado }
            });
            return { estado: err.response.data.estado, mensaje: err.response.data.mensaje };
        });
    }

export const registrarCliente = (cliente) => dispatch => {
    let urlFinal = apiRoutes.urlClientes;
    // Aca hacemos la peticion a axios
        return axios.post(urlFinal, cliente,
            {
                headers: {
                    Authorization: localStorage.getItem('token')
                 }
            }
            )
        .then(({data}) => {
            if(data.estado === 'OK') {
                if (!data.mensaje){
                  localStorage.setItem('token', data.datos.token)
                  localStorage.setItem('usuario', JSON.stringify(data.datos.usuario))
            dispatch({
                type: REGISTRAR_CLIENTE_SUCCESS,
                payload: data
            })
        }}

            return data;
        
        })
        .catch(err => {
            console.log(err);
            dispatch({
                type: REGISTRAR_CLIENTE_FAIL,
                payload: err.response.data
            })
        
            return err.response.data;
        });
}
