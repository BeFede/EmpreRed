import { COMPRA_MP_SUCESS , COMPRA_MP_FAILED, GET_COMPRAS, GET_COMPRA } from './types';
import { apiRoutes } from '../config/configs';
import * as axiosManager from 'axios';

const axios = axiosManager.create()
axios.defaults.headers = {
   Authorization: localStorage.getItem('token')
}

export const postCompra = (compra) => dispatch => {
    const urlPost = `${apiRoutes.urlCompras}`;
        return axios.post(urlPost, compra)
        .then(({data}) => {
            dispatch({
                type: COMPRA_MP_SUCESS,
                payload: data.mensaje
            });
            return { estado: data.estado };
            })
            .catch(err => {
                console.log(err);
                dispatch({
                    type: COMPRA_MP_FAILED,
                    payload: { estado: err.response.data.estado }
                });
                return { estado: err.response.data.estado, mensaje: err.response.data.mensaje };
            });
}

export const getCompras = (query) => dispatch => {

    let urlFinal;

    if(query){
        urlFinal = `${apiRoutes.urlCompras}?producto=${query.producto}&materia_prima=${query.materia_prima}` +
        `&fecha_desde=${query.fecha_desde}&fecha_hasta=${query.fecha_hasta}&proveedor=${query.proveedor}&comprobante=${query.comprobante}`;    
    }
    else{
        urlFinal = `${apiRoutes.urlCompras}`;
    }
        return axios.get(urlFinal)
        .then(({data}) => {
            dispatch({
                type: GET_COMPRAS,
                payload: data.datos
            });
            return { datos: data.datos, estado: data.estado };
            })
            .catch(err => {
                console.log(err);
                dispatch({
                    type: GET_COMPRAS,
                    payload: err.response.data
                });
                return { estado: err.response.data.estado, mensaje: err.response.data.mensaje };
            });
}

export const getCompra = (id) => dispatch => {
    const urlFinal = `${apiRoutes.urlCompras}${id}/`;
        return axios.get(urlFinal)
        .then(({data}) => {
            dispatch({
                type: GET_COMPRA,
                payload: data.datos.compra
            });
            return { datos: data.datos.compra, estado: data.estado };
            })
            .catch(err => {
                console.log(err);
                dispatch({
                    type: GET_COMPRA,
                    payload: err.response.data
                });
                return { estado: err.response.data.estado, mensaje: err.response.data.mensaje };
            });
}

