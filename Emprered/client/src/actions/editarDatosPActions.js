import { EDITAR_DATOS_PERSONALES_SUCCESS, EDITAR_DATOS_PERSONALES_FAIL } from './types';
import { apiRoutes } from '../config/configs';
import * as axiosManager from 'axios';

const axios = axiosManager.create()
axios.defaults.headers = {
   Authorization: localStorage.getItem('token')
}

export const getDatosP = () => dispatch => {

    const urlFinal = `${apiRoutes.urlDatosP}`;

        return axios.get(urlFinal)
        .then(({data}) => {
            dispatch({
                type: EDITAR_DATOS_PERSONALES_SUCCESS,
                payload: {estado: data.estado, datos: data.datos }
            })
            return {estado: data.estado, datos: data.datos };
        })
        .catch(err => {
            console.log(err);
            dispatch({
                type: EDITAR_DATOS_PERSONALES_FAIL,
                payload: {estado:err.response.data.estado, datos: null, mensaje: err.response.data.mensaje }
            })
            return {estado:err.response.data.estado, datos: null, mensaje: err.response.data.mensaje };
        });
}

 export const postDatosP = (datos) => dispatch => {

    const urlFinal = `${apiRoutes.allEmprendedores}`;
    // var datosFormData = new FormData();
    // Object.keys(datos).map(function (key, index) {
    //     datosFormData.append(key, datos[key])
    // });

    // let axiosConfig = {
    //     "Content-Type": "multipart/form-data"
    // }
        // Aca hacemos la peticion a axios
        // return axios.put(urlFinal, datosFormData, axiosConfig)
        return axios.put(urlFinal, datos)
        .then(({data}) => {
            dispatch({
                type: EDITAR_DATOS_PERSONALES_SUCCESS,
                payload: { estado: data.estado }
            });
            return { estado: data.estado };
        })
        .catch(err => {
            console.log(err);
            dispatch({
                type: EDITAR_DATOS_PERSONALES_FAIL,
                payload: { estado: err.response.data.estado }
            });
            return { estado: err.response.data.estado, mensaje: err.response.data.mensaje };
        });
    }
