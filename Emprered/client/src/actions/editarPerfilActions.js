import { EDIT_USUARIO_SUCCESS, EDIT_USUARIO_FAIL } from './types';
import { apiRoutes } from '../config/configs';
import * as axiosManager from 'axios';

const axios = axiosManager.create()
axios.defaults.headers = {
   Authorization: localStorage.getItem('token')
}

export const editarUsuario = (usuario) => dispatch => { 

    var usuarioFormData = new FormData();
    Object.keys(usuario).map(function (key, index) {
        usuarioFormData.append(key, usuario[key])
    });

    let axiosConfig = {
        "Content-Type": "multipart/form-data"
    }

    return axios.post(apiRoutes.urlEditarUsuario, usuarioFormData, axiosConfig)
        .then(({data})=> {
        
        localStorage.setItem('token', data.datos.token)
        localStorage.setItem('usuario', JSON.stringify(data.datos.usuario))

        dispatch({
            type: EDIT_USUARIO_SUCCESS,
            payload: data
        })
        return data;
    })
    .catch(err => {
        console.log(err);
        dispatch({
            type: EDIT_USUARIO_FAIL,
            payload: err.response.data
        })
        
        return err.response.data
    });
}

