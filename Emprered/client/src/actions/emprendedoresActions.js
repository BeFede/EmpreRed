import { GET_TODOS_EMPRENDEDORES, REGISTRAR_EMPRENDEDOR_SUCCESS, REGISTRAR_EMPRENDEDOR_FAIL } from './types';
import * as axiosManager from 'axios';
import { apiRoutes } from '../config/configs';

const axios = axiosManager.create()
axios.defaults.headers = {
   Authorization: localStorage.getItem('token')
}



export const getTodosEmprendedores = () => dispatch => {
// Esta sintaxis se llama "curried functions", busquenla
// si no entienden que hace con el dispatch

        // Aca hacemos la peticion a axios
        axios.get(apiRoutes.allEmprendedores)
        .then(({data}) => {
            dispatch({
                type: GET_TODOS_EMPRENDEDORES,
                payload: data.datos.emprendedores
            })
            
        })
        .catch(err => {
            console.log(err);
        });

}

export const registrarEmprendedor = (emprendedor) => dispatch => {

    // Si es update uso la ruta con idMateriaPrima, si es insert uso la sin
    let urlFinal = apiRoutes.allEmprendedores;
        // Aca hacemos la peticion a axios
        return axios.post(urlFinal, emprendedor, {
            headers : {
               Authorization: localStorage.getItem('token')
            }
        })
        .then(({data}) => {
            console.log(data);
            if(data.estado == 'OK') {


          //localStorage.setItem('token', data.datos.token)
          localStorage.setItem('usuario', JSON.stringify(data.datos.usuario))

            dispatch({
                type: REGISTRAR_EMPRENDEDOR_SUCCESS,
                payload: data
            })
        }
            return data;
        })
        .catch(err => {
            console.log(err);
            dispatch({
                type: REGISTRAR_EMPRENDEDOR_FAIL,
                payload: err.response.data
            })
            
            return err.response.data;
        });

}
