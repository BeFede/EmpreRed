import { GET_EVENTOS, POST_EVENTO, DEL_EVENTO } from './types';
import { apiRoutes } from '../config/configs';
import * as axiosManager from 'axios';

const axios = axiosManager.create()
axios.defaults.headers = {
   Authorization: localStorage.getItem('token')
}

const dividirEventos = (eventos) => {
    let eventosPendientes = [];
    let eventosVencidos = [];

    for(let evento of eventos) {
        // Acá al estar el server en timezone de argentina, viene la fecha en UTC con la Z al ultimo y se transforma bien (-3)
        const fecha = new Date(`${evento.eventos_meta[0].repeat_start}`);
        const fechaActual = new Date();
        if(fecha > fechaActual) {
            eventosPendientes.push(evento);
        } else {
            eventosVencidos.push(evento);
        }
    }

    return { eventosPendientes: eventosPendientes, eventosVencidos: eventosVencidos};
}

export const getEventos = () => dispatch => {
    const urlFinal = `${apiRoutes.urlEventos}`;
    let eventos = {};
        return axios.get(urlFinal)
        .then(({data}) => {
            // Hago la división entre eventos vencidos y no vencidos
            if(data.datos.eventos.length !== 0) {
                eventos = dividirEventos(data.datos.eventos);
            } else {
                eventos = { eventosPendientes: [], eventosVencidos: []};
            }

            dispatch({
                type: GET_EVENTOS,
                payload: eventos
            });
            
            return { datos: eventos, estado: data.estado };
            })
            .catch(err => {
                console.log(err);
                dispatch({
                    type: GET_EVENTOS,
                    payload: err.response.data
                });
                return { estado: err.response.data.estado, mensaje: err.response.data.mensaje };
            });
}

export const postEvento = (evento) => dispatch => {

    const urlGet = `${apiRoutes.urlEventos}`;
    let urlPost = `${apiRoutes.urlEventos}`

    // Si es modificacion la data va a venir con un id
    if(evento.id) {
        urlPost = `${apiRoutes.urlEventos}${evento.id}`;
    }

    let eventos = [];
        return axios.post(urlPost, evento)
        .then(({data}) => {
            dispatch({
                type: POST_EVENTO,
                payload: data
            });
            // Actualizo la variable de eventos de redux para poder actualizar la lista en tiempo real
            axios.get(urlGet)
            .then(({data}) => {
                // Hago la división entre eventos vencidas y no vencidas
                if(data.datos.eventos.length !== 0) {
                    eventos = dividirEventos(data.datos.eventos);
                } else {
                    eventos = { eventosPendientes: [], eventosVencidos: []};
                }
                dispatch({
                    type: GET_EVENTOS,
                    payload: eventos
                    });
            })
            .catch(err => {
                console.log(err);
                dispatch({
                    type: GET_EVENTOS,
                    payload: err.response.data
                });
            });
            return { estado: data.estado };
        })
        .catch(err => {
            console.log(err);
            dispatch({
                type: POST_EVENTO,
                payload: { estado: err.response.data.estado }
            });
            return { estado: err.response.data.estado, mensaje: err.response.data.mensaje };
        });
}

export const deleteEvento = (eventos, idEventoABorrar) => dispatch => {
    const urlFinal = `${apiRoutes.urlEventos}${idEventoABorrar}`

    //Actualizamos el estado de la aplicacion
    for (var i = 0; i < eventos.length; i++) {
      if (idEventoABorrar === eventos[i].id){
          eventos.splice(i,1);
      }
    }

        // Aca hacemos la peticion a axios
        return axios.delete(urlFinal)
        .then(({data}) => {
            dispatch({
                type: DEL_EVENTO,
                payload: eventos
            });
            return data
        })
        .catch(err => {
            console.log(err);
            return err.response.data;
        });
}