import { GET_MARCAS, POST_MARCA } from './types';
import { apiRoutes } from '../config/configs';
import * as axiosManager from 'axios';

const axios = axiosManager.create()
axios.defaults.headers = {
   Authorization: localStorage.getItem('token')
}

export const getMarcas = () => dispatch => {

    const urlFinal = `${apiRoutes.urlMarcas}`;

        return axios.get(urlFinal)
        .then(({data}) => {
            dispatch({
                type: GET_MARCAS,
                payload: {estado: data.estado, datos: data.datos }
            })
            return {estado: data.estado, datos: data.datos };
        })
        .catch(err => {
            console.log(err);
            dispatch({
                type: GET_MARCAS,
                payload: {estado:err.response.data.estado, datos: null, mensaje: err.response.data.mensaje }
            })
            return {estado:err.response.data.estado, datos: null, mensaje: err.response.data.mensaje };
        });
}

 export const postMarca = (marca) => dispatch => {

    const urlFinal = `${apiRoutes.urlMarcas}`;
    var marcaFormData = new FormData();
    Object.keys(marca).map(function (key, index) {
        marcaFormData.append(key, marca[key])
    });

    let axiosConfig = {
        "Content-Type": "multipart/form-data"
    }

        // Aca hacemos la peticion a axios
        return axios.post(urlFinal, marcaFormData, axiosConfig)
        .then(({data}) => {
            dispatch({
                type: POST_MARCA,
                payload: { estado: data.estado }
            });
            return { estado: data.estado };
        })
        .catch(err => {
            console.log(err);
            dispatch({
                type: POST_MARCA,
                payload: { estado: err.response.data.estado }
            });
            return { estado: err.response.data.estado, mensaje: err.response.data.mensaje };
        });

}