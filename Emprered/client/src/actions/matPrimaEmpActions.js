import { GET_TODAS_MAT_PRIMAS_EMP, PUT_MATERIA_PRIMA_EMP, DEL_MATERIA_PRIMA_EMP } from './types';
import { apiRoutes } from '../config/configs';
import * as axiosManager from 'axios';


const axios = axiosManager.create()
axios.defaults.headers = {
   Authorization: localStorage.getItem('token')
}

 export const getTodasMPEmp = (query) => dispatch => {
    // Esta sintaxis se llama "curried functions", busquenla
    // si no entienden que hace con el dispatch

    let urlFinal;

    if(query === undefined) {
        urlFinal = `${apiRoutes.allMatPrimasEmp.urlEmps}${apiRoutes.allMatPrimasEmp.urlMP}`;
    } else {
        urlFinal = `${apiRoutes.allMatPrimasEmp.urlEmps}${apiRoutes.allMatPrimasEmp.urlMP}?nombre=${query.nombre}&unidad_medida=${query.unidad_medida}` +
            `&costo_desde=${query.costo_desde}&costo_hasta=${query.costo_hasta}&stock_desde=${query.stock_desde}&stock_hasta=${query.stock_hasta}&stock_bajo=${query.stock_bajo}`;
    }
    
        // Aca hacemos la peticion a axios
        return axios.get(urlFinal)
        .then(({data}) => {
            dispatch({
                type: GET_TODAS_MAT_PRIMAS_EMP,
                payload: data.datos.materias_primas
            })
            return data.datos.materias_primas;
        })
        .catch(err => {
            console.log(err);
        });
}

 export const putMateriaPrimaEmp = (materiaPrima) => dispatch => {
    // Esta sintaxis se llama "curried functions", busquenla
    // si no entienden que hace con el dispatch
    const idMateriaPrima = materiaPrima.id;
    if (materiaPrima.alicuota_iva == undefined) materiaPrima.alicuota_iva = 1;
    if (materiaPrima.unidad_medida == undefined) materiaPrima.unidad_medida = 1;
    const urlGet = `${apiRoutes.allMatPrimasEmp.urlEmps}${apiRoutes.allMatPrimasEmp.urlMP}`;
    // Si es update uso la ruta con idMateriaPrima, si es insert uso la sin
    let urlFinal = (idMateriaPrima) ?
                        `${apiRoutes.allMatPrimasEmp.urlEmps}${apiRoutes.allMatPrimasEmp.urlMP}${idMateriaPrima}`  + '/'
                        : `${apiRoutes.allMatPrimasEmp.urlEmps}${apiRoutes.allMatPrimasEmp.urlMP}`;


        // Aca hacemos la peticion a axios
        return axios.put(urlFinal, materiaPrima)
        .then(({data}) => {
            dispatch({
                type: PUT_MATERIA_PRIMA_EMP,
                payload: data.mensaje
            });
            axios.get(urlGet, {
              headers: { Authorization: localStorage.getItem('token')}
              })
            .then(({data}) => {
                dispatch({
                    type: GET_TODAS_MAT_PRIMAS_EMP,
                    payload: data.datos.materias_primas
                })
                return data.datos.materias_primas;
            })
            .catch(err => {
                console.log(err);
            });
        })
        .catch(err => {
            console.log(err);
        });

}

 export const delMateriaPrimaEmp = (materias_primas, idMateriaPrimaABorrar) => dispatch => {
    // Esta sintaxis se llama "curried functions", busquenla
    // si no entienden que hace con el dispatch

    // Si es update uso la ruta con idMateriaPrima, si es insert uso la sin
    const urlFinal = `${apiRoutes.allMatPrimasEmp.urlEmps}${apiRoutes.allMatPrimasEmp.urlMP}${idMateriaPrimaABorrar}` + '/';
    const urlGet = `${apiRoutes.allMatPrimasEmp.urlEmps}${apiRoutes.allMatPrimasEmp.urlMP}`;


    //Actualizamos el estado de la aplicacion
    for (var i = 0; i < materias_primas.length; i++) {
      if (idMateriaPrimaABorrar === materias_primas[i].id){
          materias_primas.splice(i,1);
      }
    }

        // Aca hacemos la peticion a axios
        return axios.delete(urlFinal)
        .then(({data}) => {
            dispatch({
                type: DEL_MATERIA_PRIMA_EMP,
                payload: materias_primas
            });
            return materias_primas;
        })
        .catch(err => {
            console.log(err);
        });
}
