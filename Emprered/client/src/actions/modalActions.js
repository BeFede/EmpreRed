import { MODAL_OPEN, MODAL_CLOSE } from "./types";

// Aca esperamos recibir los headers, el body, y todo lo propio de cada modal
export function openModal(modalProps) {
    return {
    type: MODAL_OPEN,
    payload: modalProps
  };
}

// Esta la usamos solo para cerrar el modal
export function closeModal() {
  return {
    type: MODAL_CLOSE 
  };
}