import { SELECT_MP, SELECT_PROD, REMOVE_PROD, REMOVE_MP } from './types';

export const selectMP = (item) => dispatch => {
    dispatch({
        type: SELECT_MP,
        payload: item
    })
}

export const selectProd = (item) => dispatch => {
    dispatch({
        type: SELECT_PROD,
        payload: item
    })
}

export const removeProd = () => dispatch => {
    dispatch({
        type: REMOVE_PROD,
        payload: null
    })
}

export const removeMP = () => dispatch => {
    dispatch({
        type: REMOVE_MP,
        payload: null
    })
}