import { GET_PLANES_PRODUCCION, POST_PLAN_PRODUCCION} from './types';
import { apiRoutes } from '../config/configs';
import * as axiosManager from 'axios';

const axios = axiosManager.create()
axios.defaults.headers = {
   Authorization: localStorage.getItem('token')
}

export const getPlanesProduccion = (idProducto) => dispatch => {

    const urlFinal = `${apiRoutes.allEmprendedores}productos/${idProducto}/plan_produccion/`;

        return axios.get(urlFinal)
        .then(({data}) => {
            console.log(data);
            dispatch({
                type: GET_PLANES_PRODUCCION,
                payload: {estado: data.estado, datos: data.datos.plan_produccion }
            })
            return {estado: data.estado, datos: data.datos.plan_produccion };
        })
        .catch(err => {
            console.log(err);
            dispatch({
                type: GET_PLANES_PRODUCCION,
                payload: {estado:err.response.data.estado, datos: null }
            })
            return {estado:err.response.data.estado, datos: null };
        });
}

 export const postPlanProduccion = (planProduccion) => dispatch => {

    const urlFinal = `${apiRoutes.urlPostPlanesProduccion}`;

        // Aca hacemos la peticion a axios
        return axios.post(urlFinal, planProduccion)
        .then(({data}) => {
            dispatch({
                type: POST_PLAN_PRODUCCION,
                payload: { estado: data.estado }
            });
            return { estado: data.estado };
        })
        .catch(err => {
            console.log(err);
            dispatch({
                type: POST_PLAN_PRODUCCION,
                payload: { estado: err.response.data.estado }
            });
            return { estado: err.response.data.estado, mensaje: err.response.data.mensaje };
        });

}