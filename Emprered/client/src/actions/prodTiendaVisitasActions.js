import { GET_PRODUCTOS_VISITAS } from './types';
import { apiRoutes } from '../config/configs';
import * as axiosManager from 'axios';

const axios = axiosManager.create()
axios.defaults.headers = {
   Authorization: localStorage.getItem('token')
}

export const getProductosVisita = (pagina_actual) => dispatch => {
    
    const urlFinal = `${apiRoutes.urlTiendaProdVisitas}&tamano_pagina=5&pagina_actual=${pagina_actual}`;

        return axios.get(urlFinal)
        .then(({data}) => {
            dispatch({
                type: GET_PRODUCTOS_VISITAS,
                payload: {estado: data.estado, datos: data.datos }
            })
            return {estado: data.estado, datos: data.datos };
        })
        .catch(err => {
            console.log(err);
            dispatch({
                type: GET_PRODUCTOS_VISITAS,
                payload: {estado:err.response.data.estado, datos: null, mensaje: err.response.data.mensaje }
            })
            return {estado:err.response.data.estado, datos: null, mensaje: err.response.data.mensaje };
        });
}
