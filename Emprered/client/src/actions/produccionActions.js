import { PRODUCCION_SUCESS , PRODUCCION_FAILED, GET_PRODUCCIONES, GET_PRODUCCION } from './types';
import { apiRoutes } from '../config/configs';
import * as axiosManager from 'axios';

const axios = axiosManager.create()
axios.defaults.headers = {
   Authorization: localStorage.getItem('token')
}

export const postProduccion = (produccion) => dispatch => {
    const urlPost = `${apiRoutes.urlProducciones}`;
        return axios.post(urlPost, produccion)
        .then(({data}) => {
            dispatch({
                type: PRODUCCION_SUCESS,
                payload: data.mensaje
            });
            // Llamo a la action de get para que me actualice los datos de redux
            axios.get(urlPost)
            .then(({data}) => {
            dispatch({
                type: GET_PRODUCCIONES,
                payload: data.datos.producciones
                });
            })
            .catch(err => {
                console.log(err);
                dispatch({
                    type: GET_PRODUCCIONES,
                    payload: err.response.data
                });
            });
            return { estado: data.estado };
        })
        .catch(err => {
            console.log(err);
            dispatch({
                type: PRODUCCION_FAILED,
                payload: { estado: err.response.data.estado }
            });
            return { estado: err.response.data.estado, mensaje: err.response.data.mensaje };
        });
}

export const getProducciones = (query) => dispatch => {

    let urlFinal;

    if(query)
    {
        urlFinal = `${apiRoutes.urlProducciones}?producto=${query.producto}&materia_prima=${query.materia_prima}` +
        `&fecha_desde=${query.fecha_desde}&fecha_hasta=${query.fecha_hasta}&estado=${query.estado}&descripcion=${query.descripcion}`;
    }
    else
    {
        urlFinal = `${apiRoutes.urlProducciones}`;
    }

        return axios.get(urlFinal)
        .then(({data}) => {
            dispatch({
                type: GET_PRODUCCIONES,
                payload: data.datos.producciones
            });
            
            return { datos: data.datos.producciones, estado: data.estado };
            })
            .catch(err => {
                console.log(err);
                dispatch({
                    type: GET_PRODUCCIONES,
                    payload: err.response.data
                });
                return { estado: err.response.data.estado, mensaje: err.response.data.mensaje };
            });
}

export const getProduccion = (id) => dispatch => {
    const urlFinal = `${apiRoutes.urlProducciones}${id}`;
        return axios.get(urlFinal)
        .then(({data}) => {
            dispatch({
                type: GET_PRODUCCION,
                payload: data.datos.produccion
            });
            console.log(data.datos.produccion.detalles);
            return { datos: data.datos.produccion, estado: data.estado };
            })
            .catch(err => {
                console.log(err);
                dispatch({
                    type: GET_PRODUCCION,
                    payload: err.response.data
                });
                return { estado: err.response.data.estado, mensaje: err.response.data.mensaje };
            });
}

export const descargarReporte = (id_produccion, reporte, documentTarget) => dispatch => {
    const urlFinal = `${apiRoutes.urlReporteProducciones}?id_produccion=${ id_produccion }&listado=${reporte}`;
        axios.get(urlFinal)
        .then(({ data }) => {
            
            // Genero el link del pdf descargado. La magia wacho
            var blob = new Blob([data]);
            console.log(blob);
            var link = documentTarget.createElement('a');
            link.href = window.URL.createObjectURL(blob);            
            link.setAttribute('download', 'produccion_'+ id_produccion + '.pdf');
            documentTarget.body.appendChild(link);
            link.click();
            
        })
        .catch(err => {
            console.log(err);            
        });
    }
        


