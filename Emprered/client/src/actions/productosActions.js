import { GET_PRODUCTOS, PUT_PRODUCTO, DEL_PRODUCTO } from './types';
import { apiRoutes } from '../config/configs';
import * as axiosManager from 'axios';

const axios = axiosManager.create()
axios.defaults.headers = {
   Authorization: localStorage.getItem('token')
}


export const getProductos = (query) => dispatch => {
    // Esta sintaxis se llama "curried functions", busquenla
    // si no entienden que hace con el dispatch

    let urlFinal;

    if(query === undefined) {
        urlFinal = `${apiRoutes.allProductos.urlEmps}${apiRoutes.allProductos.urlProd}`;
    } else {

        urlFinal = `${apiRoutes.allProductos.urlEmps}${apiRoutes.allProductos.urlProd}?nombre=${query.nombre}&unidad_medida=${query.unidad_medida}` +
            `&costo_desde=${query.costo_desde}&costo_hasta=${query.costo_hasta}&stock_desde=${query.stock_desde}&stock_hasta=${query.stock_hasta}` +
            `&stock_bajo=${query.stock_bajo}&categoria=${query.categoria}&subcategoria=${query.subcategoria}`;
    }
    

        // Aca hacemos la peticion a axios
        return axios.get(urlFinal)
        .then(({data}) => {
            dispatch({
                type: GET_PRODUCTOS,
                payload: data.datos.productos
            })
            return data.datos.productos;
        })
        .catch(err => {
            console.log(err);
            dispatch({
                type: GET_PRODUCTOS,
                payload: err.response.data.datos
            })
            return err.response.data.datos;
        });
    }



export const putProducto = ( producto) => dispatch => {
  const idProducto = producto.id;

  const urlGet = `${apiRoutes.allProductos.urlEmps}${apiRoutes.allProductos.urlProd}`;
  // Si es update uso la ruta con idMateriaPrima, si es insert uso la sin
  let urlFinal = (idProducto) ?
                      `${apiRoutes.allProductos.urlEmps}${apiRoutes.allProductos.urlProd}${idProducto}`  + '/'
                      : `${apiRoutes.allProductos.urlEmps}${apiRoutes.allProductos.urlProd}`;

      let  axiosConfig = {
        headers: {
            "Content-Type": "multipart/form-data",
        }
    };

    var productoFormData = new FormData();
    Object.keys(producto).map(function(key, index) {
       productoFormData.append(key, producto[key])
    });

        axios.post(urlFinal, productoFormData, axiosConfig)
        .then(({data}) => {
            dispatch({
                type: PUT_PRODUCTO,
                payload: data.mensaje
            });
            axios.get(urlGet,  {
              headers: { Authorization: localStorage.getItem('token') }
              })
            .then(({data}) => {
                dispatch({
                    type: GET_PRODUCTOS,
                    payload: data.datos.productos
                })
            })
            .catch(err => {
                console.log(err);
            });
        })
        .catch(err => {
            console.log(err);
        });
}

export const delProducto = (productos, idProductoABorrar) => dispatch => {
    // Esta sintaxis se llama "curried functions", busquenla
    // si no entienden que hace con el dispatch

    // Si es update uso la ruta con idMateriaPrima, si es insert uso la sin
    const urlFinal = `${apiRoutes.allProductos.urlEmps}${apiRoutes.allProductos.urlProd}${idProductoABorrar}`  + '/'

    //Actualizamos el estado de la aplicacion
    for (var i = 0; i < productos.length; i++) {
      if (idProductoABorrar === productos[i].id){
          productos.splice(i,1);
      }
    }


        // Aca hacemos la peticion a axios
        return axios.delete(urlFinal)
        .then(({data}) => {
            dispatch({
                type: DEL_PRODUCTO,
                payload: productos
            });
            return data
        })
        .catch(err => {
            console.log(err);
            return err.response.data;
        });
}
