import { GET_PROVEEDORES, POST_PROVEEDOR, DELETE_PROVEEDOR } from './types';
import { apiRoutes } from '../config/configs';
import * as axiosManager from 'axios';

const axios = axiosManager.create()
axios.defaults.headers = {
   Authorization: localStorage.getItem('token')
}

export const getProveedores = () => dispatch => {

    const urlFinal = `${apiRoutes.urlProveedores}`;

    return axios.get(urlFinal)
    .then(({data}) => {
        dispatch({
            type: GET_PROVEEDORES,
            payload: data.datos.proveedores
        })
        return data.datos.proveedores;
    })
    .catch(err => {
        console.log(err);
        dispatch({
            type: GET_PROVEEDORES,
            payload: err.response.data.datos
        })
        return err.response.data.datos;
    });
}

export const postProveedor = (proveedor) => dispatch => {
    const idProveedor = proveedor.id;
  
    const urlGet = `${apiRoutes.urlProveedores}`;
    // Si es update uso la ruta con idProveedor, sino no
    let urlFinal = (idProveedor) ?
                        `${apiRoutes.urlProveedores}${idProveedor}`  + '/'
                        : `${apiRoutes.urlProveedores}`;
  
          return axios.post(urlFinal, proveedor)
          .then(({data}) => {
              dispatch({
                  type: POST_PROVEEDOR,
                  payload: data.mensaje
              });
              axios.get(urlGet)
              .then(({data}) => {
                  dispatch({
                      type: GET_PROVEEDORES,
                      payload: data.datos.proveedores
                  })
                  
              })
              .catch(err => {
                  console.log(err);
              });
              return data.mensaje;
          })
          .catch(err => {
              console.log(err);
              return err.response.data;
          });
}

export const delProveedor = (proveedores, idProveedorABorrar) => dispatch => {

    const urlFinal = `${apiRoutes.urlProveedores}${idProveedorABorrar}`  + '/'

    const urlGet = `${apiRoutes.urlProveedores}`;

    //Actualizamos el estado de la aplicacion
    for (var i = 0; i < proveedores.length; i++) {
        if (idProveedorABorrar === proveedores[i].id){
            proveedores.splice(i,1);
        }
      }

        // Aca hacemos la peticion a axios
        return axios.delete(urlFinal)
        .then(({data}) => {
            dispatch({
                type: DELETE_PROVEEDOR,
                payload: data.mensaje
            });
            return data.mensaje
        })
        .catch(err => {
            console.log(err);
            return err.response.data;
        });
}