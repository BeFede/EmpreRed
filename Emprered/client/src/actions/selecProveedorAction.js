import { SELEC_PROVEEDOR, REMOVE_SELEC_PROVEEDOR} from './types';

export const selecProveedor = (item) => dispatch => {
    dispatch({
        type: SELEC_PROVEEDOR,
        payload: item
    })
}

export const removeSelecProveedor = () => dispatch => {
    dispatch({
        type: REMOVE_SELEC_PROVEEDOR,
        payload: null
    })
}