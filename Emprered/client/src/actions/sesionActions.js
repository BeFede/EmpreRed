import { OBTENER_SESION, LOGIN } from './types';
import * as axios from 'axios';
import { apiRoutes } from '../config/configs';


export const obtenerSesion = () => dispatch => {

    dispatch({
        type: OBTENER_SESION,
        payload: {
            rol: 'emprendedor',
            id: 6
        }
    });

}

export const login = (username, password) => dispatch => {

    return axios.post(apiRoutes.urlLogin, {
        username: username, 
        password: password,
        tipo_autenticacion: 'emprered'
    })
    .then(({data}) => {
        if(data.estado === 'OK') {
            if (!data.mensaje){
              localStorage.setItem('token', data.datos.token)
              localStorage.setItem('usuario', JSON.stringify(data.datos.usuario))


            dispatch({
                type: LOGIN,
                payload: { datos: data.datos }
            })

            return data;

          }

        } else {
            dispatch({
                type: LOGIN,
                payload: { mensaje: data.mensaje }
            })
        }

        })
        .catch(err => {
            console.log(err);
            dispatch({
                type: LOGIN,
                payload: { mensaje: err.response.data.mensaje }
            })
            return err.response.data;
        });


}


export const loginGoogle = (token) => dispatch => {
    
    console.log(token);
    let config = { 'Authorization': "Bearer " + token };
    return axios.post(apiRoutes.urlLogin, { "tipo_autenticacion": 'google'}, {headers: config})
        .then(({ data }) => {
            if (data.estado === 'OK') {
                //document.cookie = `autenticacion = ${data.datos.token}`
                if (!data.mensaje) {
                    localStorage.setItem('token', data.datos.token)
                    localStorage.setItem('usuario', JSON.stringify(data.datos.usuario))


                    dispatch({
                        type: LOGIN,
                        payload: { datos: data.datos }
                    })  
                }
                else {
                    alert(data.mensaje)
                }
                return data;

            } else {
                dispatch({
                    type: LOGIN,
                    payload: { mensaje: data.mensaje }
                })
                return data;
            }

        })
        .catch(err => {
            console.log(err);
            dispatch({
                type: LOGIN,
                payload: { mensaje: err.response.data.mensaje }
            })
            return err.response.data;
        });


}

