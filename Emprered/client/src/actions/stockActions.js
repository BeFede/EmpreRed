import { GET_MOVIMIENTOS_STOCK, POST_MOVIMIENTOS_STOCK, GET_TODAS_MAT_PRIMAS_EMP, GET_PRODUCTOS } from './types';
import { apiRoutes } from '../config/configs';
import * as axiosManager from 'axios';

const axios = axiosManager.create()
axios.defaults.headers = {
   Authorization: localStorage.getItem('token')
}

export const postMovimientos = (stock) => dispatch => {
    const urlPost = `${apiRoutes.urlMovimientos}`;
    const urlGetMP = `${apiRoutes.allMatPrimasEmp.urlEmps}${apiRoutes.allMatPrimasEmp.urlMP}`;
    const urlGetProd =  `${apiRoutes.allMatPrimasEmp.urlEmps}${apiRoutes.allProductos.urlProd}`;
        return axios.post(urlPost, stock)
        .then(({data}) => {
            dispatch({
                type: POST_MOVIMIENTOS_STOCK,
                payload: data.mensaje
            });
            // Para actualizar la tabla de MP
            
            axios.get(urlGetMP, {
                headers: { Authorization: localStorage.getItem('token')}
                })
              .then(({data}) => {
                  dispatch({
                      type: GET_TODAS_MAT_PRIMAS_EMP,
                      payload: data.datos.materias_primas
                  })
                  return data.datos.materias_primas;
              })
              .catch(err => {
                  console.log(err);
            });

            axios.get(urlGetProd, {
                headers: { Authorization: localStorage.getItem('token')}
                })
              .then(({data}) => {
                  dispatch({
                    type: GET_PRODUCTOS,
                    payload: data.datos.productos
                  })
                  return data.datos.productos;
              })
              .catch(err => {
                  console.log(err);
            });
            return { estado: data.estado };
            })
            .catch(err => {
                console.log(err);
                dispatch({
                    type: POST_MOVIMIENTOS_STOCK,
                    payload: { estado: err.response.data.estado }
                });
                return { estado: err.response.data.estado, mensaje: err.response.data.mensaje };
            });
}

export const getMovimientos = (query) => dispatch => {

    let urlFinal = '';

    if(query) {
        urlFinal = `${apiRoutes.urlMovimientos}?producto=${query.producto}&materia_prima=${query.materia_prima}` +
        `&motivo=${query.motivo}&tipo_movimiento=${query.tipo_movimiento}&fecha_desde=${query.fecha_desde}&fecha_hasta=${query.fecha_hasta}`;
    } else {
        urlFinal = `${apiRoutes.urlMovimientos}`;
    }

        return axios.get(urlFinal)
        .then(({data}) => {
            console.log(data.datos)
            dispatch({
                type: GET_MOVIMIENTOS_STOCK,
                payload: data.datos
            });
            return { datos: data.datos, estado: data.estado };
            })
            .catch(err => {
                console.log(err);
                dispatch({
                    type: GET_MOVIMIENTOS_STOCK,
                    payload: err.response.data
                });
                return { estado: err.response.data.estado, mensaje: err.response.data.mensaje };
            });
}

export const descargarReporteMovimientos = (documentTarget) => dispatch => {
    const urlFinal = `${apiRoutes.urlReporteMovimientos}?listado=movimientos_stock&fin=20`;
        axios.get(urlFinal)
        .then(({ data }) => {
            
            // Genero el link del pdf descargado. La magia wacho
            var blob = new Blob([data]);
            console.log(blob);
            var link = documentTarget.createElement('a');
            link.href = window.URL.createObjectURL(blob);            
            link.setAttribute('download', 'UltimosMovimientos' + '.pdf');
            documentTarget.body.appendChild(link);
            link.click();
            
        })
        .catch(err => {
            console.log(err);            
        });
    }
