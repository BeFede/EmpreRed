import { INSERT_ITEM, REMOVE_ITEM, INSERT_ITEM_BISEXUAL, REMOVE_ITEM_BISEXUAL } from './types';

export const insertItem = (item) => dispatch => {
    dispatch({
        type: INSERT_ITEM,
        payload: item
    })
}

export const removeItem = (item) => dispatch => {
    dispatch({
        type: REMOVE_ITEM,
        payload: item
    })
}

// Para las mp y productos que se insertan juntitos bien homo
export const insertItemBisexual = (item) => dispatch => {
    dispatch({
        type: INSERT_ITEM_BISEXUAL,
        payload: item
    })
}

export const removeItemBisexual = (item) => dispatch => {
    dispatch({
        type: REMOVE_ITEM_BISEXUAL,
        payload: item
    })
}