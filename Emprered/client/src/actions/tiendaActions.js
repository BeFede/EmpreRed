import { GET_CATALOGO, GET_DETALLE_CATALOGO, POST_COMENTARIO, GET_DETALLES_CATALOGO, GET_LINK_MERCADOPAGO, VALORAR_DETALLE, GET_VALORACION_DETALLE } from './types';
import { apiRoutes } from '../config/configs';
import * as axiosManager from 'axios';

const axios = axiosManager.create()
axios.defaults.headers = {
   Authorization: localStorage.getItem('token')
}

export const getCatalogoTienda = (id, numeroPagina) => dispatch => {
    let urlFinal = '';
    if(id != null) {
        if(numeroPagina != null) {
            urlFinal = `${apiRoutes.urlTienda}/catalogos?id=${id}&tamano_pagina=5&pagina_actual=${numeroPagina}`;
        } else {
            urlFinal = `${apiRoutes.urlTienda}/catalogos?id=${id}&tamano_pagina=5`;
        }
    }

        return axios.get(urlFinal)
        .then(({data}) => {
            dispatch({
                type: GET_CATALOGO,
                payload: data.datos.catalogo
            });
            
            return { datos: data.datos.catalogo, estado: data.estado };
            })
            .catch(err => {
                console.log(err);
                dispatch({
                    type: GET_CATALOGO,
                    payload: err.response.data
                });
                return { estado: err.response.data.estado, mensaje: err.response.data.mensaje };
            });
}

export const getTodosCatalogosTienda = (numeroPagina) => dispatch => { 
    // Por defecto le pongo tamaño de pagina 10
    let urlFinal = '';
    if(numeroPagina != null) {
        urlFinal = `${apiRoutes.urlTienda}/catalogos?tamano_pagina=10&pagina_actual=${numeroPagina}`;
    } else {
        urlFinal = `${apiRoutes.urlTienda}/catalogos?tamano_pagina=10`;
    }  
        return axios.get(urlFinal)
        .then(({data}) => {
            dispatch({
                type: GET_CATALOGO,
                payload: data.datos.catalogos
            });
            
            return { datos: data.datos.catalogos, estado: data.estado };
            })
            .catch(err => {
                console.log(err);
                dispatch({
                    type: GET_CATALOGO,
                    payload: err.response.data
                });
                return { estado: err.response.data.estado, mensaje: err.response.data.mensaje };
            });
}

export const getDetalleCatalogoTienda = (id) => dispatch => {
    const urlFinal = `${apiRoutes.urlTienda}/detalles_catalogo?id=${id}`;
        return axios.get(urlFinal)
        .then(({data}) => {
            dispatch({
                type: GET_DETALLE_CATALOGO,
                payload: data.datos.detalle_catalogo
            });
            
            return { datos: data.datos.detalle_catalogo, estado: data.estado };
            })
            .catch(err => {
                console.log(err);
                dispatch({
                    type: GET_DETALLE_CATALOGO,
                    payload: err.response.data
                });
                return { estado: err.response.data.estado, mensaje: err.response.data.mensaje };
            });
}

export const getTitulosPublicaciones = () => dispatch => {
    const urlFinal = `${apiRoutes.urlTienda}/detalles_catalogo`;
        return axios.get(urlFinal)
        .then(({data}) => {
            dispatch({
                type: GET_DETALLES_CATALOGO,
                payload: data.datos.detalles_catalogo
            });
            
            return { datos: data.datos.detalles_catalogo, estado: data.estado };
            })
            .catch(err => {
                console.log(err);
                dispatch({
                    type: GET_DETALLES_CATALOGO,
                    payload: err.response.data
                });
                return { estado: err.response.data.estado, mensaje: err.response.data.mensaje };
            });
}

export const getPublicacionesPorFrase = (frase, numeroPagina) => dispatch => {
    const urlFinal = `${apiRoutes.urlTienda}/productos?orden=visitas&nombre=${frase}&tamano_pagina=10&pagina_actual=${numeroPagina}`;
        return axios.get(urlFinal)
        .then(({data}) => {
            dispatch({
                type: GET_DETALLES_CATALOGO,
                payload: data.datos.productos
            });
            
            return { datos: data.datos.productos, estado: data.estado };
            })
            .catch(err => {
                console.log(err);
                dispatch({
                    type: GET_DETALLES_CATALOGO,
                    payload: err.response.data
                });
                return { estado: err.response.data.estado, mensaje: err.response.data.mensaje };
            });
}

export const getPublicacionesPorFiltros = (frase, numeroPagina, filtros) => dispatch => {

    const categoria = filtros.categorias[0] ? filtros.categorias[0].id : '';
    const urlFinal = `${apiRoutes.urlTienda}/productos?orden=visitas&nombre=${frase}&tamano_pagina=10&pagina_actual=${numeroPagina}`+
    `&categorias=${categoria}&precio_desde=${filtros.precio_desde}&precio_hasta=${filtros.precio_hasta}`;
        return axios.get(urlFinal)
        .then(({data}) => {
            dispatch({
                type: GET_DETALLES_CATALOGO,
                payload: data.datos.productos
            });
            return { datos: data.datos.productos, estado: data.estado };
            })
            .catch(err => {
                console.log(err);
                dispatch({
                    type: GET_DETALLES_CATALOGO,
                    payload: err.response.data
                });
                return { estado: err.response.data.estado, mensaje: err.response.data.mensaje };
            });
}

export const getPublicacionesPorSubcategoria = (subcategorias, numeroPagina) => dispatch => {

    //Parseo el array de subcategorias que me viene para transformarlo en un string
    let stringSubcategorias = '';

    for(const [index, subcat] of subcategorias.entries()) {
        if(index === subcategorias.length - 1) {
            stringSubcategorias = stringSubcategorias + subcat;
        } else {
            stringSubcategorias = stringSubcategorias + subcat + ',';
        }
    }

    const urlFinal = `${apiRoutes.urlTienda}/productos?subcategorias=${stringSubcategorias}&tamano_pagina=10&pagina_actual=${numeroPagina}`;
        return axios.get(urlFinal)
        .then(({data}) => {
            dispatch({
                type: GET_DETALLES_CATALOGO,
                payload: data.datos.productos
            });
            
            return { datos: data.datos.productos, estado: data.estado };
            })
            .catch(err => {
                console.log(err);
                dispatch({
                    type: GET_DETALLES_CATALOGO,
                    payload: err.response.data
                });
                return { estado: err.response.data.estado, mensaje: err.response.data.mensaje };
            });
}

export const postComentarioTienda = (comentario) => dispatch => {
    const urlPost = `${apiRoutes.urlPostComentario}`;
    const urlGetDetalle = `${apiRoutes.urlTienda}/detalles_catalogo?id=${comentario.id_detalle}`;
        return axios.post(urlPost, comentario)
        .then(({data}) => {
            dispatch({
                type: POST_COMENTARIO,
                payload: data.mensaje
            });
            // Actualizo la variable del detalle de redux para poder actualizar los comentarios en tiempo real ahre
            axios.get(urlGetDetalle)
            .then(({data}) => {
                dispatch({
                    type: GET_DETALLE_CATALOGO,
                    payload: data.datos.detalle_catalogo
                    });
            })
            .catch(err => {
                console.log(err);
                dispatch({
                    type: GET_DETALLE_CATALOGO,
                    payload: err.response.data
                });
            });
            return { estado: data.estado };
        })
        .catch(err => {
            console.log(err);
            dispatch({
                type: POST_COMENTARIO,
                payload: { estado: err.response.data.estado }
            });
            return { estado: err.response.data.estado, mensaje: err.response.data.mensaje };
        });
}

export const obtenerLinkMercadoPago = (idDetalle, cantidad) => dispatch => {
    const urlFinal = `${apiRoutes.urlRegistrarVenta}`;

    const body = {
        id_detalle_catalogo: idDetalle,
        cantidad: cantidad,
        url_exito: apiRoutes.urlCompraExito,
        url_error: apiRoutes.urlCompraError
    }

    return axios.post(urlFinal, body)
    .then(({data}) => {
        dispatch({
            type: GET_LINK_MERCADOPAGO,
            payload: data.datos.link_de_pago
        });
        return { datos: data.datos.link_de_pago, estado: data.estado };
    })
    .catch(err => {
        console.log(err);
        dispatch({
            type: GET_LINK_MERCADOPAGO,
            payload: { estado: err.response.data.estado }
        });
        return { estado: err.response.data.estado, mensaje: err.response.data.mensaje };
    });
}

export const valorarDetalle = (idDetalle, puntaje) => dispatch => {
    const urlFinal = `${apiRoutes.urlValorarDetalle}`;

    const body = {
        id_detalle_catalogo: idDetalle,
        puntaje: puntaje,
    }

    return axios.post(urlFinal, body)
    .then(({data}) => {
        dispatch({
            type: VALORAR_DETALLE,
            payload: data.mensaje
        });
        return { mensaje: data.mensaje, estado: data.estado };
    })
    .catch(err => {
        console.log(err);
        dispatch({
            type: VALORAR_DETALLE,
            payload: { estado: err.response.data.estado }
        });
        return { estado: err.response.data.estado, mensaje: err.response.data.mensaje };
    });
}

export const getValoracionDetalle = (idDetalle) => dispatch => {
    const urlFinal = `${apiRoutes.urlValorarDetalle}?id_detalle_catalogo=${idDetalle}`;

    return axios.get(urlFinal)
    .then(({data}) => {
        dispatch({
            type: GET_VALORACION_DETALLE,
            payload: data.datos.valoracion
        });
        return { datos: data.datos.valoracion, estado: data.estado };
    })
    .catch(err => {
        console.log(err);
        dispatch({
            type: GET_VALORACION_DETALLE,
            payload: { estado: err.response.data.estado }
        });
        return { estado: err.response.data.estado, mensaje: err.response.data.mensaje };
    });
}
