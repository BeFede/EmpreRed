import { GET_ALICUOTAS_IVA, GET_CATEGORIAS_PRODUCTO, GET_SUBCATEGORIA, GET_SUBCATEGORIAS, GET_UNIDADES_MEDIDA, GET_CONDICIONES_FRENTE_IVA, GET_TIPOS_DOCUMENTO, GET_ETIQUETAS } from './types';
import * as axios from 'axios';
import { apiRoutes } from '../config/configs';

export const getCategoriasProducto = () => dispatch => {

    const urlFinal = `${apiRoutes.urlCategorias}`;

        // Aca hacemos la peticion a axios
    return axios.get(urlFinal, {withCredentials: false})
    .then(({data}) => {
        dispatch({
            type: GET_CATEGORIAS_PRODUCTO,
            payload: data.datos.categorias
        })
        return data.datos.categorias
    })
    .catch(err => {
        console.log(err);
        
    });
}

export const getSubcategoriasDeCategoria = (idCategoria) => dispatch => {

    const urlFinal = `${apiRoutes.urlCategoria}${idCategoria}`;

        // Aca hacemos la peticion a axios
    return axios.get(urlFinal, {withCredentials: false})
    .then(({data}) => {
        dispatch({
            type: GET_SUBCATEGORIAS,
            payload: data.datos.categoria.subcategorias
        })
        return data.datos.categoria.subcategorias
    })
    .catch(err => {
        console.log(err);
        
    });
}

export const getInfoSubcategoria = (idSubcategoria) => dispatch => {

    const urlFinal = `${apiRoutes.urlSubcategoria}${idSubcategoria}`;

        // Aca hacemos la peticion a axios
    return axios.get(urlFinal, {withCredentials: false})
    .then(({data}) => {
        dispatch({
            type: GET_SUBCATEGORIA,
            payload: data.datos.subcategoria
        })
        return data.datos.subcategoria
    })
    .catch(err => {
        console.log(err);
        
    });
}

export const getAlicuotas = () => dispatch => {
    // Esta sintaxis se llama "curried functions", busquenla
    // si no entienden que hace con el dispatch

    const urlFinal = `${apiRoutes.urlAlicuotas}`;

        // Aca hacemos la peticion a axios
    return axios.get(urlFinal)
    .then(({data}) => {
        dispatch({
            type: GET_ALICUOTAS_IVA,
            payload: data.datos.alicuotas_iva
        })
        return data.datos.alicuotas_iva
    })
    .catch(err => {
        console.log(err);
    });
}

export const getUnidadesMedida = () => dispatch => {
    // Esta sintaxis se llama "curried functions", busquenla
    // si no entienden que hace con el dispatch

    const urlFinal = `${apiRoutes.urlUnidadesMedida}`;

        // Aca hacemos la peticion a axios
    return axios.get(urlFinal)
    .then(({data}) => {
        dispatch({
            type: GET_UNIDADES_MEDIDA,
            payload: data.datos.unidades_medida
        })
        return data.datos.unidades_medida
    })
    .catch(err => {
        console.log(err);
    });
}

export const getCondicionesFrenteAIVA = () => dispatch => {

    const urlFinal = `${apiRoutes.urlCondicionesIVA}`;

        // Aca hacemos la peticion a axios
    return axios.get(urlFinal)
    .then(({data}) => {
        dispatch({
            type: GET_CONDICIONES_FRENTE_IVA,
            payload: data.datos.condiciones_frente_iva
        })
        return data.datos.condiciones_frente_iva
    })
    .catch(err => {
        console.log(err);
    });
}




export const getTiposDocumento = () => dispatch => {

    const urlFinal = `${apiRoutes.urlTiposDocumento}`;
    
        // Aca hacemos la peticion a axios
    return axios.get(urlFinal)
    .then(({data}) => {
        dispatch({
            type: GET_TIPOS_DOCUMENTO,
            payload: data.datos.tipos_documento
        })
        return data.datos.tipos_documento
    })
    .catch(err => {
        console.log(err);
    });
}

export const getEtiquetas = () => dispatch => {

    const urlFinal = `${apiRoutes.urlEtiquetas}`;
    
        // Aca hacemos la peticion a axios
    return axios.get(urlFinal)
    .then(({data}) => {
        dispatch({
            type: GET_ETIQUETAS,
            payload: data.datos.etiquetas
        })
        return data.datos.etiquetas
    })
    .catch(err => {
        console.log(err);
    });
}