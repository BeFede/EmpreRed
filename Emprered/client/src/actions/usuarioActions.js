import { REGISTRAR_USUARIO_SUCCESS, REGISTRAR_USUARIO_FAIL } from './types';
import { apiRoutes } from '../config/configs';
import * as axiosManager from 'axios';

const axios = axiosManager.create()
axios.defaults.headers = {
   Authorization: localStorage.getItem('token')
}


export const registrarUsuario = (usuario) => dispatch => {

        return axios.post(apiRoutes.urlRegistroUsuario, {"usuario":usuario,"tipo_autenticacion":"emprered" }, {
            "Content-Type": "application/json",
        })
        .then(({data}) => {
            dispatch({
                type: REGISTRAR_USUARIO_SUCCESS,
                payload: data
            })

            return data;
        })
        .catch(err => {
            console.log(err);
            dispatch({
                type: REGISTRAR_USUARIO_FAIL,
                payload: err.response.data
            })
            
            return err.response.data;
        });
    }


export const registroGoogle = (token) => dispatch => {

    let config = { 
        "Content-Type": "application/json",    
        'Authorization': "Bearer " + token 
    }
    return axios.post(apiRoutes.urlRegistroUsuario, {"tipo_autenticacion": "google" }, {headers: config})
        .then(({ data }) => {
            dispatch({
                type: REGISTRAR_USUARIO_SUCCESS,
                payload: data
            })
            
            return data;
        })
        .catch(err => {
            console.log(err);
            dispatch({
                type: REGISTRAR_USUARIO_FAIL,
                payload: err.response.data
            })
            
            return err.response.data;
        });
}
