import { GET_VENTAS, GET_COMPRAS_CLIENTE } from './types';
import { apiRoutes } from '../config/configs';
import * as axiosManager from 'axios';

const axios = axiosManager.create()
axios.defaults.headers = {
   Authorization: localStorage.getItem('token')
}

export const getVentas = (numeroPagina, query) => dispatch => { 
    // Por defecto le pongo tamaño de pagina 200
    let urlFinal = '';
    if(numeroPagina != null) {
        urlFinal = `${apiRoutes.urlMisVentas}?tamano_pagina=200&pagina_actual=${numeroPagina}`;
    } else {
        urlFinal = `${apiRoutes.urlMisVentas}?tamano_pagina=200`;
    }  

    if(query) {
        urlFinal = urlFinal + `&emprendedor_apellido=${query.emprendedor_apellido}&comprador_nombre=${query.comprador_nombre}` +
        `&fecha_desde=${query.fecha_desde}&fecha_hasta=${query.fecha_hasta}&estado=${query.estado}&monto_desde=${query.monto_desde}&monto_hasta=${query.monto_hasta}`
    }
        return axios.get(urlFinal)
        .then(({data}) => {
            dispatch({
                type: GET_VENTAS,
                payload: data.datos.ventas
            });
            
            return { datos: data.datos.ventas, estado: data.estado };
            })
            .catch(err => {
                console.log(err);
                dispatch({
                    type: GET_VENTAS,
                    payload: err.response.data
                });
                return { estado: err.response.data.estado, mensaje: err.response.data.mensaje };
            });
}

export const getVentasCliente = (numeroPagina, query) => dispatch => { 
    // Por defecto le pongo tamaño de pagina 200
    let urlFinal = '';
    if(numeroPagina != null) {
        urlFinal = `${apiRoutes.urlMisVentasCliente}?tamano_pagina=200&pagina_actual=${numeroPagina}`;
    } else {
        urlFinal = `${apiRoutes.urlMisVentasCliente}?tamano_pagina=200`;
    } 

    if(query) {
        urlFinal = urlFinal + `&emprendedor_apellido=${query.emprendedor_apellido}&comprador_nombre=${query.comprador_nombre}` +
        `&fecha_desde=${query.fecha_desde}&fecha_hasta=${query.fecha_hasta}&estado=${query.estado}&monto_desde=${query.monto_desde}&monto_hasta=${query.monto_hasta}`
    }
        return axios.get(urlFinal)
        .then(({data}) => {
            dispatch({
                type: GET_COMPRAS_CLIENTE,
                payload: data.datos.ventas
            });
            return { datos: data.datos.ventas, estado: data.estado };
            })
            .catch(err => {
                console.log(err);
                dispatch({
                    type: GET_COMPRAS_CLIENTE,
                    payload: err.response.data
                });
                return { estado: err.response.data.estado, mensaje: err.response.data.mensaje };
            });
}