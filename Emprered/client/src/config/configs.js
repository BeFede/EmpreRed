const api = {

  url: "http://localhost:8000/server/",
  urlCliente: "http://localhost:3000/"

}
export const apiRoutes = {
    allEmprendedores: api.url + 'emprendedores/',
    allMatPrimasEmp: {
      urlEmps: api.url + 'emprendedores/',
      urlMP: 'materias_primas/'
    },
    allProductos:{
      urlEmps: api.url + 'emprendedores/',
      urlProd:'productos/'
    },
    urlCategorias: api.url + 'categorias',
    urlCategoria: api.url + 'categoria/',
    urlSubcategoria: api.url + 'subcategoria/',
    urlUnidadesMedida: api.url + 'unidades_medida/',
    urlAlicuotas: api.url + 'alicuotas_iva/',
    urlCondicionesIVA: api.url + 'condiciones_frente_iva/',
    urlTiposDocumento: api.url + 'tipos_documento/',
    urlEtiquetas: api.url + 'etiquetas/',

    urlLogin: api.url + 'login/',
    urlRegistroUsuario: api.url + 'registro/',
    
    urlPostPlanesProduccion: api.url + 'emprendedores/productos/plan_produccion/',

    urlEditarUsuario: api.url + 'editar_perfil/',

    urlMarcas: api.url + 'marcas/',

    urlDatosP: api.url + 'emprendedores/datos/perfil/',

    urlMovimientos: api.url + 'movimientos_stock/',
    
    urlCompras: api.url + 'compras/',
    urlProveedores: api.url + 'proveedores/',

    urlProducciones: api.url + 'producciones/',

    urlReporteProducciones: api.url + 'reportes_produccion/',
    urlReporteMovimientos: api.url + 'reportes_movimientos_stock/',

    urlClientes: api.url + 'clientes/',
    urlCliente: api.url + 'clientes/datos/perfil',
    urlCatalogos: api.url + 'catalogos_emprendedor/',
    urlDetalleCatalogo : api.url + 'catalogos_emprendedor/administrar_detalle',
    urlPostComentario: api.url + 'catalogos_emprendedor/administrar_detalle/agregar_comentario',

    urlTiendaProdVisitas: api.url + 'tienda/productos?criterio=visitas',
    urlTienda: api.url + 'tienda',
    urlBusquedaTienda: api.url + 'buscar',
    urlRegistrarVenta: api.url + 'registrar_venta',
    urlCompraExito: api.urlCliente + 'post_compra',
    urlCompraError: api.urlCliente + 'post_compra_error',

    urlMisVentas: api.url + 'ventas/vendedor',
    urlMisVentasCliente: api.url + 'ventas/comprador',

    urlValorarDetalle: api.url + 'valorar_detalle_catalogo/',

    urlEventos: api.url + 'eventos/'
};

export const getUser = () => JSON.parse(localStorage.getItem('usuario'));

export const urlMedia = "http://localhost:8000/server/media/";

export const envProduccion = false;
