import React from 'react';
// Al ReactDOM hay que importarlo solo así, sino tira error
import ReactDOM from 'react-dom';
import './index.css';

import registerServiceWorker from './registerServiceWorker';
import { Provider } from 'react-redux'

// Routing
import { BrowserRouter } from 'react-router-dom';
import { Route, Switch} from 'react-router';

// Store
import store from './store';

// Componentes
import App from './web-components/App';
import Entry from './web-components/Entry';
import MatPrimaConsulta from './web-components/MatPrimaConsulta';
import ProductosConsulta from './web-components/ProductosConsulta';
import Catalogo from './web-components/Catalogo';
import Ingreso from './web-components/Ingreso';
import RegUser from './web-components/RegistrarUsuario';
import MisCompras from './web-components/MisCompras';
import RegMicroemprendimiento from './web-components/RegMicroemprendimiento';
import Produccion from './web-components/Produccion';
import HistorialCompras from './web-components/HistorialCompras';
import PlanDeProduccion from './web-components/PlanDeProduccion';
import Movimientos from './web-components/MovimientoStock';
import MostrarMovimientos from './web-components/MostrarMovimientos';
import MiCuentaCliente from './web-components/MiCuentaCliente'
import MiCuentaEmprendedor from './web-components/MiCuentaEmprendedor'
import DetalleCatalogo from './web-components/DetalleCatalogo';
import Tienda from './web-components/tienda/Tienda.js';
import PerfilEmprendedor from './web-components/tienda/PerfilEmprendedor';
import Publicacion from './web-components/tienda/Publicacion.js';
import ResultPage from './web-components/tienda/ResultPage.js';
import PostCompraError from './web-components/tienda/PostCompraError';
import PostCompra from './web-components/tienda/PostCompra';
import ClienteCompras from './web-components/ClienteCompras';
import MisVentas from './web-components/MisVentas';
import MisTareas from './web-components/eventos/MisTareas';
import Notificaciones from './web-components/eventos/Notificaciones';
// Sincronizamos el browserHistory de React Router con el Store

ReactDOM.render(
  <Provider store={store}>
    {/*le decimos al Router que use nuestro history sincronizado*/}
    <BrowserRouter>
      {/*armamos las rutas de nuestra aplicación*/}
      <Switch>
        {/*Con esto el "exact" es como el index, ademas sirve
        para cuando no queremos que lea el resto de la ruta*/}
        <Route exact path="/" component={App}/>
        <Route path="/entry" component={Entry} />
        <Route path="/matprimas" component={MatPrimaConsulta} />
        <Route path="/productos" component={ProductosConsulta}/>
        <Route path="/catalogo" component={Catalogo}/>
        <Route path="/ingreso" component={Ingreso}/>
        <Route path="/regUser" component={RegUser}/>
        <Route path="/compras" component={MisCompras}/>
        <Route path="/planproduccion" component={PlanDeProduccion}/>
        <Route path="/microemprendimiento" component={RegMicroemprendimiento}/>
        <Route path="/produccion" component={Produccion} />
        <Route path="/historialCompras" component={HistorialCompras}/>
        <Route path="/movimientos" component={Movimientos}/>
        <Route path="/mismovimientos" component={MostrarMovimientos}/>
        <Route path="/micuentac" component={MiCuentaCliente}/>
        <Route path="/micuentae" component={MiCuentaEmprendedor}/>
        <Route path="/detallecatalogo" component={DetalleCatalogo}/>
        <Route path="/tienda" component={Tienda}/>
        <Route path="/perfil" component={PerfilEmprendedor}/>
        <Route path="/publicacion" component={Publicacion}/>
        <Route path="/resultado_busqueda" component={ResultPage}/>
        <Route path="/post_compra_error" component={PostCompraError}/>
        <Route path="/post_compra" component={PostCompra}/>
        <Route path="/compras_cliente" component={ClienteCompras}/>
        <Route path="/ventas" component={MisVentas}/>
        <Route path="/tareas" component={MisTareas}/>
        <Route path="/notificaciones" component={Notificaciones}/>
      </Switch>
    </BrowserRouter>
  </Provider>,
  document.getElementById('root')
);
registerServiceWorker();
