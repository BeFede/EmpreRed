import React, { Component } from 'react';
import { Text, View, Image, Button,  StyleSheet } from 'react-native';


import { Actions } from 'react-native-router-flux';

//var prueba = require('../ctrls/PruebaCtrl.js');

const Menu = () => {
  return (
    <View style={{flex:0.7, flexDirection: 'column'}}>
      <View style={styles.title} >
        <Text style={{fontSize: 30, fontWeight: 'bold'}}>¡Bienvenidos!</Text>
        <Text style={{fontSize: 20, fontWeight: 'bold'}}>Red de emprendedores</Text>
      </View>
      <View style={{flex: 2, marginBottom:10, flexDirection: 'row'}}>

      </View>
      <View style={{flex: 1, alignItems: 'center'}}>
        <Text style={{fontSize: 20}}>UTN - FRC</Text>
        <Text style={{fontSize: 18}}>EmpreRed</Text>
      </View>
    </View>
  );
};

var styles = StyleSheet.create({
  title: {
    flex: 1,
    alignItems: 'center',
    marginRight: 28
  }
});

export default Menu;
