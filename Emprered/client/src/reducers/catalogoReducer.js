import { GET_CATALOGO, POST_CATALOGO, GET_DETALLE_CATALOGO, POST_DETALLE_CATALOGO, DELETE_DETALLE_CATALOGO } from '../actions/types';

const initialState = {
    items: [],
}

export default (state = initialState, action) => {
    switch (action.type) {
        case GET_CATALOGO:
            return {
                ...state,
                resultCatalogo: action.payload
            }
        case POST_CATALOGO:
            return {
                ...state,
                resultPostCatalogo: action.payload
            }
        case GET_DETALLE_CATALOGO:
            return {
                ...state,
                resultDetalleCatalogo: action.payload
            }
        case POST_DETALLE_CATALOGO:
            return {
                ...state,
                resultPostDetalleCatalogo: action.payload
            }
        case DELETE_DETALLE_CATALOGO:
            return {
                ...state,
                resultDeleteDetalleCatalogo: action.payload
            }
        default:
            return state;
    }
}