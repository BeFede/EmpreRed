import { GET_CLIENTES, REGISTRAR_CLIENTE_SUCCESS, REGISTRAR_CLIENTE_FAIL } from '../actions/types';

const initialState = {
    items: [],
}

export default (state = initialState, action) => {
    switch (action.type) {
        case GET_CLIENTES:           
            return {
                ...state,
                clientes: action.payload
            }
        case REGISTRAR_CLIENTE_SUCCESS:           
            return {
                ...state,
                mensaje: action.payload
            }
        case REGISTRAR_CLIENTE_FAIL:           
            return {
                ...state,
                mensaje: action.payload
            }
        default: 
            return state;
    }
}