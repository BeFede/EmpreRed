import { GET_COMPRAS, GET_COMPRA, COMPRA_MP_FAILED, COMPRA_MP_SUCESS } from '../actions/types';

const initialState = {
    items: [],
}

export default (state = initialState, action) => {
    switch (action.type) {
        case GET_COMPRAS:
            return {
                ...state,
                resultCompras: action.payload
            }
        case GET_COMPRA:
            return {
                ...state,
                resultCompra: action.payload
            }
        case COMPRA_MP_FAILED:
            return {
                ...state,
                postCompra: action.payload
            }
        case COMPRA_MP_SUCESS:
            return {
                ...state,
                postCompra: action.payload
            }
        default:
            return state;
    }
}