import { EDITAR_DATOS_PERSONALES_SUCCESS, EDITAR_DATOS_PERSONALES_FAIL } from '../actions/types';

const initialState = {
    items: [],
}

export default (state = initialState, action) => {
    switch (action.type){
        case EDITAR_DATOS_PERSONALES_SUCCESS:
            return {
                ...state,
                datosP: action.payload
            }
        case EDITAR_DATOS_PERSONALES_FAIL:
            return {
                ...state,
                datosP: action.payload
            }
        default:
            return state;
    }
}