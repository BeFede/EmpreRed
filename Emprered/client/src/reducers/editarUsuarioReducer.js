import { EDIT_USUARIO_SUCCESS, EDIT_USUARIO_FAIL } from './types';

export default (state = [], action) => {
  switch (action.type) {
    case EDIT_USUARIO_SUCCESS:
      return { 
        mensaje: action.payload
      }
    case EDIT_USUARIO_FAIL:
      return { 
        mensaje: action.payload
      }  
    default:
      return state;

  }
}