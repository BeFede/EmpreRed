import { GET_TODOS_EMPRENDEDORES, REGISTRAR_EMPRENDEDOR_FAIL, REGISTRAR_EMPRENDEDOR_SUCCESS } from '../actions/types';

const initialState = {
    items: [],
}

export default (state = initialState, action) => {
    switch (action.type) {
        case GET_TODOS_EMPRENDEDORES:           
            return {
                ...state,
                items: action.payload
            }
        case REGISTRAR_EMPRENDEDOR_SUCCESS:           
            return {
                ...state,
                mensaje: action.payload
            }
        case REGISTRAR_EMPRENDEDOR_FAIL:           
            return {
                ...state,
                mensaje: action.payload
            }
        default: 
            return state;
    }
}