import { GET_EVENTOS, POST_EVENTO, DEL_EVENTO } from '../actions/types';

const initialState = {
    items: [],
}

export default (state = initialState, action) => {
    switch (action.type) {
        case GET_EVENTOS:
            return {
                ...state,
                resultEventos: action.payload
            }
        case POST_EVENTO:
            return {
                ...state,
                resultPostEvento: action.payload
            }
        case DEL_EVENTO:
            return {
                ...state,
                resultDeleteEvento: action.payload
            }
        default:
            return state;
    }
}