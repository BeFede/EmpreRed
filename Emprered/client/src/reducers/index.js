import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import usuariosReducers from './usuariosReducers';
import empReducer from './empReducer';
import matPrimaReducer from './matPrimaReducer';
import modalReducer from './modalReducer';
import sesionReducer from './sesionReducer';
import productosReducer from './productosReducer';
import tipificacionesReducer from './tipificacionesReducer';
import tableFillReducer from './tableFillReducer';
import planDeProduccionReducer from './planDeProduccionReducer';
import marcasReducer from './marcasReducer';
import comprasReducer from './comprasReducer';
import produccionesReducer from './produccionesReducer';
import datosPersonalesReducer  from './datosPersonalesReducer';
import movimientosStockReducer from './movimientosStockReducer';
import datosClientesReducer from './clientesReducer';
import catalogoReducer from './catalogoReducer';
import prodTiendaVisitReducer from './prodTiendaVisitReducer'
import tiendaReducer from './tiendaReducer';
import mpOrProdSelectReducer from './mpOrProdSelectReducer';
import proveedoresReducer from './proveedoresReducer';
import selecProveedorReducer from './selecProveedorReducer';
import ventasReducer from './ventasReducer';
import eventosReducer from './eventosReducer';

// Aca hay que agregar todos los reducers que hayan
export default combineReducers({
  usuarios: usuariosReducers,
  emps: empReducer,
  mpsEmp: matPrimaReducer,
  modals: modalReducer,
  sesion: sesionReducer,
  productos: productosReducer,
  tipificaciones: tipificacionesReducer,
  // Reducer para routing
  routing: routerReducer,
  tableData: tableFillReducer,
  planProduccion: planDeProduccionReducer,
  marcas: marcasReducer,
  datosPersonales: datosPersonalesReducer,
  compras: comprasReducer,
  producciones : produccionesReducer,

  stock: movimientosStockReducer,
  datosClientes: datosClientesReducer,

  catalogo: catalogoReducer,
  prodVisitasTienda: prodTiendaVisitReducer,
  tienda: tiendaReducer,
  eventos: eventosReducer,

  mpOrProd: mpOrProdSelectReducer,
  proveedores: proveedoresReducer,
  proveedorSeleccionado: selecProveedorReducer,
  ventasYComprasCliente: ventasReducer
});
