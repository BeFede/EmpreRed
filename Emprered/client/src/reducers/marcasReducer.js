import { GET_MARCAS, POST_MARCA } from '../actions/types';

const initialState = {
    items: [],
}

export default (state = initialState, action) => {
    switch (action.type){
        case GET_MARCAS:
            return {
                ...state,
                resultMarcas: action.payload
            }
        case POST_MARCA:
            return {
                ...state,
                resultMarca: action.payload
            }
        default:
            return state;
    }
}