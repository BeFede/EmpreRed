import { GET_TODAS_MAT_PRIMAS_EMP, PUT_MATERIA_PRIMA_EMP, DEL_MATERIA_PRIMA_EMP } from '../actions/types';

const initialState = {
    items: [],
}

export default (state = initialState, action) => {
    switch (action.type) {
        case GET_TODAS_MAT_PRIMAS_EMP:
            return {
                ...state,
                // Estos "..." se llaman "spread operator" de JS
                items: action.payload
            }
        case PUT_MATERIA_PRIMA_EMP:
            return {
                ...state,
                messagePutMateriaPrima: action.payload
            }

        case DEL_MATERIA_PRIMA_EMP:
            return {
                ...state,
                items: action.payload
            }
        default:
            return state;
    }
}
