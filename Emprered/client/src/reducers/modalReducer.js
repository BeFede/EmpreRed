const initialState = null;

export default (state = initialState, action = {}) => {
  switch (action.type) {
    case "MODAL_OPEN": {
      return { modalProps: action.payload };
    }

    case "MODAL_CLOSE": {
      return null;
    }

    default:
      return state;
  }
};
