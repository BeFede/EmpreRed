import { GET_MOVIMIENTOS_STOCK, POST_MOVIMIENTOS_STOCK } from '../actions/types';

const initialState = {
    items: [],
}

export default (state = initialState, action) => {
    switch (action.type){
        case GET_MOVIMIENTOS_STOCK:
            return {
                ...state,
                resultStock: action.payload
            }
        case POST_MOVIMIENTOS_STOCK:
            return {
                ...state,
                resultStock: action.payload
            }
        default:
            return state;
    }
}