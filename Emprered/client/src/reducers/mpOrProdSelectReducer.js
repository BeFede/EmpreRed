import { SELECT_MP, SELECT_PROD, REMOVE_PROD, REMOVE_MP  } from '../actions/types';

const initialState = {
    items: [],
}

export default (state = initialState, action) => {

    switch(action.type){
         
        case SELECT_MP:
        return {
            ...state,
            itemMP: action.payload,
            itemProd: ''
        }
        case SELECT_PROD:
        return{
            ...state,
            itemProd: action.payload,
            itemMP: ''
        }    
        case REMOVE_PROD:
        return{
            ...state,
            itemProd: action.payload,
            itemMP: null
        }    
        case REMOVE_MP:
        return{
            ...state,
            itemProd: null,
            itemMP: action.payload
        }    
        default:
            return state;
    }
}