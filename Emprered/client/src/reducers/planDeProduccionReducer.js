import { GET_PLANES_PRODUCCION, POST_PLAN_PRODUCCION } from '../actions/types';

const initialState = {
    items: [],
}

export default (state = initialState, action) => {
    switch (action.type) {
        case GET_PLANES_PRODUCCION:
            return {
                ...state,
                resultPlan: action.payload
            }
        case POST_PLAN_PRODUCCION:
            return {
                ...state,
                resultPlan: action.payload
            }
        default:
            return state;
    }
}