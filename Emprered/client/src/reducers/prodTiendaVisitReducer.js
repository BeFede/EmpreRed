import { GET_PRODUCTOS_VISITAS } from '../actions/types';

const initialState = {
    items: [],
}

export default (state = initialState, action) => {
    switch (action.type) {
        case GET_PRODUCTOS_VISITAS:
            return {
                ...state,
                prodVisitasTienda: action.payload
            }
        default:
            return state;
    }
}