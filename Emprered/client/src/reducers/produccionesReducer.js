import { GET_PRODUCCIONES, GET_PRODUCCION, PRODUCCION_FAILED, PRODUCCION_SUCESS } from '../actions/types';

const initialState = {
    items: [],
}

export default (state = initialState, action) => {
    switch (action.type) {
        case GET_PRODUCCIONES:
            return {
                ...state,
                resultProducciones: action.payload
            }
        case GET_PRODUCCION:
            return {
                ...state,
                resultProduccion: action.payload
            }
        case PRODUCCION_FAILED:
            return {
                ...state,
                postProduccion: action.payload
            }
        case PRODUCCION_SUCESS:
            return {
                ...state,
                postProduccion: action.payload
            }
        default:
            return state;
    }
}