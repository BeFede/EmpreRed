import { GET_PRODUCTOS, PUT_PRODUCTO, DEL_PRODUCTO } from '../actions/types';

const initialState = {
    items: [],
}

export default (state = initialState, action) => {
    switch (action.type) {
        case GET_PRODUCTOS:
            return {
                ...state,
                // Estos "..." se llaman "spread operator" de JS
                items: action.payload
            }
        case PUT_PRODUCTO:
            return {
                ...state,
                messagePutProducto: action.payload
            }

        case DEL_PRODUCTO:
            return {
                ...state,
                items: action.payload
            }
        default:
            return state;
    }
}
