import { GET_PROVEEDORES, POST_PROVEEDOR, DELETE_PROVEEDOR } from '../actions/types';

const initialState = {
    items: [],
}

export default (state = initialState, action) => {
    switch (action.type) {
        case GET_PROVEEDORES:
            return {
                ...state,
                // Estos "..." se llaman "spread operator" de JS
                proveedores: action.payload
            }
        case POST_PROVEEDOR:
            return {
                ...state,
                mensaje_post_proveedor: action.payload
            }

        case DELETE_PROVEEDOR:
            return {
                ...state,
                mensaje_delete_proveedor: action.payload
            }
        default:
            return state;
    }
}