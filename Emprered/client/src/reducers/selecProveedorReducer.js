import { SELEC_PROVEEDOR, REMOVE_SELEC_PROVEEDOR  } from '../actions/types';

const initialState = {
    items: [],
}

export default (state = initialState, action) => {

    switch(action.type){
         
        case SELEC_PROVEEDOR:
        return {
            ...state,
            item: action.payload
        }    
        case REMOVE_SELEC_PROVEEDOR:
        return{
            ...state,
            item: action.payload
        }   
        default:
            return state;
    }
}