import { LOGIN} from '../actions/types';

const initialState = {
    sesion: [],
}

export default (state = initialState, action) => {
    switch (action.type) {
        case LOGIN:
        // Si payload tiene datos entonces autenticó bien
            if(action.payload.datos) {
                return {
                    sesion: action.payload.datos
                }
            } else {
                return {
                    sesion: action.payload.mensaje
                }
            }
            
        default:
            return state;
    }
}
