import { INSERT_ITEM, REMOVE_ITEM, INSERT_ITEM_BISEXUAL, REMOVE_ITEM_BISEXUAL } from '../actions/types';

const initialState = {
    items: [],
}

export default (state = initialState, action) => {
    switch (action.type) {
        case INSERT_ITEM:
            // Si ya lo insertó no puede volver a insertarlo
            // Para esto chequeo el id del objeto, por ende todos los obj deberian tener un id
            if(state.items.map(e => e.id).indexOf(action.payload.id) !== -1) {
                return { ...state }
            }
            return {
                ...state,
                items: [...state.items, action.payload]
            }
        case REMOVE_ITEM:
            return {
                ...state,
                items: state.items.filter(({id}) => id !== action.payload.id)
            }
        case INSERT_ITEM_BISEXUAL:
        // Si ya lo insertó no puede volver a insertarlo
        // Para esto chequeo el id del objeto, por ende todos los obj deberian tener un id
        let encontroItemIgual = false;
        state.items.map(e => {
            if(e.id === action.payload.id && e.tipo === action.payload.tipo) {
                encontroItemIgual = true;
            }
        });
        if(encontroItemIgual) {
            return { ...state }
        }
        return {
            ...state,
            items: [...state.items, action.payload]
        }
        case REMOVE_ITEM_BISEXUAL:
        return {
            ...state,
            items: state.items.filter((v,i) => {
                        return (v["id"] !== action.payload.id || v["tipo"] !== action.payload.tipo);
                    })
        }
        default:
            return state;
    }
}
