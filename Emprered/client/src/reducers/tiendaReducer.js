import { GET_CATALOGO, GET_DETALLE_CATALOGO, GET_DETALLES_CATALOGO, GET_LINK_MERCADOPAGO, VALORAR_DETALLE, GET_VALORACION_DETALLE} from '../actions/types';

const initialState = {
    items: [],
}

export default (state = initialState, action) => {
    switch (action.type) {
        case GET_CATALOGO:
            return {
                ...state,
                resultCatalogo: action.payload
            }
        case GET_DETALLE_CATALOGO:
            return {
                ...state,
                resultDetalleCatalogo: action.payload
            }
        case GET_DETALLES_CATALOGO:
            return {
                ...state,
                resultDetallesCatalogo: action.payload
            }
        case GET_LINK_MERCADOPAGO:
            return {
                ...state,
                link: action.payload
            }
        case VALORAR_DETALLE:
            return {
                ...state,
                mensajeValoracion: action.payload
            }
        case GET_VALORACION_DETALLE:
            return {
                ...state,
                valoracionDetalle: action.payload
            }
        default:
            return state;
    }
}