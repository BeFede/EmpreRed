import { GET_ALICUOTAS_IVA, GET_UNIDADES_MEDIDA, GET_CATEGORIAS_PRODUCTO, GET_SUBCATEGORIA, GET_SUBCATEGORIAS, GET_CONDICIONES_FRENTE_IVA, GET_TIPOS_DOCUMENTO, GET_ETIQUETAS } from '../actions/types';

const initialState = {}

export default (state = initialState, action) => {
    switch (action.type) {
        case GET_UNIDADES_MEDIDA:
            return {
                ...state,
                // Estos "..." se llaman "spread operator" de JS
                unidadesMedida: action.payload
            }
        case GET_CATEGORIAS_PRODUCTO:
            return {
                ...state,
                categoriasProducto: action.payload
            }
        case GET_SUBCATEGORIA:
            return {
                ...state,
                subcategoria: action.payload
            }
        case GET_SUBCATEGORIAS:
            return {
                ...state,
                subcategorias: action.payload
            }
        case GET_ALICUOTAS_IVA:
            return {
                ...state,
                alicuotas: action.payload
            }
        case GET_CONDICIONES_FRENTE_IVA:
            return {
                ...state,
                condiciones_iva: action.payload
            }
        case GET_TIPOS_DOCUMENTO:            
            return {
                ...state,
                tipos_documento: action.payload
            }
        case GET_ETIQUETAS:            
            return {
                ...state,
                etiquetas: action.payload
            }
        default:
            return state;
    }
}
