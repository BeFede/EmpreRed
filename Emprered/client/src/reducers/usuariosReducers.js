import { REGISTRAR_USUARIO_SUCCESS, REGISTRAR_USUARIO_FAIL } from '../actions/types';

export default (state = [], action) => {
  switch (action.type) {
    case REGISTRAR_USUARIO_SUCCESS:
      return { 
        mensaje: action.payload
      }
    case REGISTRAR_USUARIO_FAIL:
      return { 
        mensaje: action.payload
      }  
    default:
      return state;

  }
}
