import { GET_VENTAS, GET_COMPRAS_CLIENTE } from '../actions/types';

const initialState = {
    items: [],
}

export default (state = initialState, action) => {
    switch (action.type) {
        case GET_VENTAS:
            return {
                ...state,
                resultVentas: action.payload
            }
        case GET_COMPRAS_CLIENTE:
            return {
                ...state,
                resultComprasCliente: action.payload
            }
        default:
            return state;
    }
}