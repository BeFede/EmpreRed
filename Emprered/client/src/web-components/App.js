import React, { Component } from 'react';
import '../App.css';
import { Link } from 'react-router-dom';
import { Button, Header, Icon, Image, Menu, Segment, Sidebar, Input } from 'semantic-ui-react'
import empreRed from './../EmprenRed5.png';
import { Container } from 'semantic-ui-react';
import { Divider } from 'semantic-ui-react';
import MenuPrincipal from './MenuPrincipal';
import MenuEmprendedor from './MenuEmprendedor';
import MenuCliente from './MenuCliente';
import { getUser } from '../config/configs';
import Footer from './Footer.js'
//import './SloganLetter.js'
import Emp2 from '../imagenInicioPiola.jpg'

import Logo from '../assets/EmpreRedRectangular.png'


class ReturnMenu extends Component{
  constructor(props) {
    super(props);
    this.usuario =  props.user;
  }
  render(){
    if(this.usuario.rol === 'Emprendedor'){
    return(
    <MenuEmprendedor />
    )
    }
    else{
      if(this.usuario.rol === 'Cliente')
      {
      return(
        <MenuCliente/>
      )
      }
      return(
        null
      )
    }
  }
}


class App extends Component {


  componentWillMount() {
    //this.props.obtenerSesion()
    this.userLogeado = (JSON.parse(localStorage.getItem('usuario'))) ? JSON.parse(localStorage.getItem('usuario')) : null;
  }

  render() {
    return (
      <div className='conteiner-div'>
      
        <MenuPrincipal />
          <div id="MenuLateral">
            {this.userLogeado ? <ReturnMenu user={this.userLogeado}/> : null}
          </div>

          <div className="App">

            <div className="logo1">
              <center >
                <Image src={Logo} />
              </center>
             
            </div>
            
          </div>

          <div className="conteiner-foot">    
            <Footer />
          </div>
      </div>

       );
     }
   }



   export default App;
