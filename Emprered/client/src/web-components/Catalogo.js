import React, { Component } from 'react'
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { Button, Header, Icon, Image, Confirm, Grid, Table, Radio, Modal, Message, Divider, Dimmer, Loader, GridColumn, Segment } from 'semantic-ui-react'
import MenuPrincipal from './MenuPrincipal';
import MenuEmprendedor from './MenuEmprendedor';
import {getUser} from '../config/configs';
import { getCatalogo, postCatalogo, deleteDetalleCatalogo } from '../actions/catalogoActions';
import { urlMedia } from '../config/configs';
import FiltroCatalogo from './FiltroCatalogo';

class Catalogo extends Component
{

    constructor(props){
        super(props);
        this.state = { cargado: false, catalogo: null, open: false, messageOpen: false, messageInfo: null, tieneCatalogo: false, catalogoVacio: true, detalleABorrar: null, setFiltro: false };
        this.userLogeado = (getUser()) ? getUser() : null;
        this.handleChange = this.handleChange.bind(this);
        this.ordenFiltros = { productoAsc: true, stockAsc: true, unidadAsc: true, precioAsc: true }
    }

    componentWillMount() {
        this.itemsRendered = null;
        this.idCatalogo = null;
        this.obtenerCatalogo();
    }

    showConfirmDialog = (detalle) => this.setState({open: true, detalleABorrar: detalle})
    handleCancel = () => this.setState({open: false})

    handleMessageConfirm = () => {
        this.setState({messageOpen: false});
    }

    handleConfirm = () => {
        this.setState({open: false});
        this.deleteDetalle(this.state.detalleABorrar);       
    }

    renderMensajeBorrado = (info, detalle) => {
        let mensaje = '';
        if(info.estado === 'OK') {
            mensaje = 'Producto quitado del catálogo';
        } else {
            mensaje = info.mensaje
        }
        this.setState({messageInfo: mensaje, messageOpen: true, estadoResponse:info.estado});
    }

    handleChange = (e, {name, detalleSeleccionado, value, checked}) => {
            // Si lo que cambia son los detalles:
            // Lo trackeo en los detalles a enviar y le agrego lo que haya escrito el usuario
            const index = this.dataToSend.detalles.map(e => e.idProducto).indexOf(detalleSeleccionado.idProducto);

            this.dataToSend.detalles[index] = {
                ...this.dataToSend.detalles[index],
                [name]: (value) ? value : checked
            }
    };

    deleteDetalle(detalle) {
        this.props.deleteDetalleCatalogo(this.props.catalogo.detalles_catalogo, detalle.id)
        .then(data => {            
            this.renderMensajeBorrado(data, detalle);        
        }, err => {
            console.log(err);
        });
    }

    obtenerCatalogo() {

    this.props.getCatalogo()
    .then(data => {
        
        this.setState({cargado: true});

        // Si tiene catalogo lo cargo
        // El data.datos es el datos.catalogo de la api
        if(data.datos) {
            this.idCatalogo = data.datos.id_catalogo;
            this.setState( { catalogo: data.datos, tieneCatalogo : true});
            if(data.datos.detalles_catalogo.length > 0) {
                this.setState( {catalogoVacio : false});
            } else {
                this.setState( {catalogoVacio : true});
            }

        } else {
            //Si no tiene catálogo cargo la variable en false
            this.setState( {tieneCatalogo : false});
        }

    }, err => {
      console.log(err);
    }) ;
    }

    crearCatalogo(){
        // Aca el this.state.catalogo va a valer null
        this.props.postCatalogo(this.state.catalogo).
        then(data => {
            if(data.estado === 'OK') {
                this.idCatalogo = data.datos.id_catalogo;
                this.setState({tieneCatalogo: true, catalogoVacio: true});
            }
        }, err => {
            console.log(err);
        }) 
    }

    routeDetalleCatalogo(idDetalle) {  
        this.props.history.push(
            {
            pathname: '/detalleCatalogo',
            state: {catalogo: this.idCatalogo, idDetalle: idDetalle}
            }
        );
    }

    renderItem(detalle)
    {

      let foto = '/static/media/EmprenRed5.f4d3b35d.png';
      if (detalle.producto.foto){
        foto = urlMedia + detalle.producto.foto
      }

      return(
        <Table.Row  key={detalle.titulo}>
        <Table.Cell>
          <Header as='h4'>
            <Image src={foto} rounded size='mini' />
            <Header.Content>
              {detalle.titulo}
            </Header.Content>
            <Header.Subheader>
                {detalle.producto.nombre}
            </Header.Subheader>
          </Header>
        </Table.Cell>
        <Table.Cell textAlign='center'>{(detalle.producto.stock <= 0) ? 'Sin stock' : detalle.producto.stock}</Table.Cell>
        <Table.Cell textAlign='center'>{detalle.producto.unidad_medida}</Table.Cell>
        <Table.Cell textAlign='center'>${detalle.precio_venta}</Table.Cell>

        <Table.Cell textAlign='center'>
            <Radio disabled = {true} toggle defaultChecked={(detalle.estado === "Visible")} onChange={this.handleChange}/>  
        </Table.Cell>

        <Table.Cell textAlign='center' content>
            <Button color="orange" circular icon="eye"
                onClick={() => {
                    this.routeDetalleCatalogo(detalle.id);
                }}
            />
            <Button negative circular icon="times"
                onClick = {() => {
                    this.showConfirmDialog(detalle);
                }}
            />
        </Table.Cell>
      </Table.Row>
      )
    }

    render()
    {   
        
        if(this.props.catalogo)
        {

            this.itemsRendered = this.props.catalogo.detalles_catalogo.map(detalle => (
                this.renderItem(detalle)
            ));

        }

        return (
            <div className="DivPrincipal" style={{overflow:'auto'}}>
                <div id="MenuTop">
                    <MenuPrincipal/>
                </div>
                <div className = "row">
                    <div id="MenuLateral">
                    {(this.userLogeado) ? <MenuEmprendedor/> : null}
                    </div>

                    <div style={{"paddingTop":"61px", marginLeft:"210px"}}>

                        {
                        (this.state.cargado) ?
                        (<Grid centered>
                            {
                            (!this.state.tieneCatalogo) ?
                            <Grid.Row>
                                <Grid.Column width={16}>
                                    <center>
                                        <Message warning>
                                            <Message.Header>No posee ningún Catálogo creado actualmente</Message.Header>
                                            <p>Para crearlo haz click en Crear Catálogo</p>
                                        </Message>
                                        <Button circular color='green' icon labelPosition='left' onClick={() => this.crearCatalogo()}>
                                            <Icon name='file alternate' />
                                            Crear catálogo
                                        </Button>
                                    </center>
                                </Grid.Column>
                            </Grid.Row>
                            :
                            <Grid.Row>
                                <Grid.Column width={16}>
                                    <Grid>
                                        <Grid.Row>
                                            <Grid.Column width={16}>
                                                <Grid>
                                                    <Grid.Row>
                                                    <Grid.Column floated='left' width={5}>
                                                    <Header as='h2'> <Icon name='list' size='tiny'/>Productos del Catálogo</Header>
                                                    </Grid.Column>
                                                    <Grid.Column floated='right' width={5} >
                                                        <div align='right'>
                                                        <Button style={{marginBottom:'5px'}} color='green'
                                                        icon labelPosition='left'
                                                        onClick={() => this.routeDetalleCatalogo(null)}>
                                                        <Icon name='add circle' />
                                                        Agregar Producto
                                                        </Button>
                                                        </div>
                                                    </Grid.Column>
                                                    </Grid.Row>
                                                </Grid>
                                            </Grid.Column>
                                        </Grid.Row>
                                        <Grid.Row centered>
                                            <Grid.Column width={14}>
                                                <Grid>
                                                    <Grid.Row>
                                                            <Grid.Column width={16}>
                                                                {
                                                                (this.state.catalogoVacio) ? 
                                                                <center>
                                                                    <h3>No posee produtos cargados en su catálogo</h3>
                                                                </center>
                                                                :
                                                                (
                                                                <Grid>
                                                                    <Grid.Row centered>
                                                                        <Grid.Column width={16}>
                                                                            <FiltroCatalogo/>
                                                                        </Grid.Column>
                                                                    </Grid.Row>
                                                                    <Divider/>
                                                                    {this.renderTable()}
                                                                </Grid>
                                                                )
                                                                }
                                                            </Grid.Column>
                                                    </Grid.Row>
                                                </Grid>
                                            </Grid.Column>
                                        </Grid.Row>
                                    </Grid>
                                </Grid.Column>
                            </Grid.Row>

                            }
                            
                        </Grid>)
                        :
                        <center>
                            <Dimmer active inverted>
                                <Loader active inline='centered'/>
                            </Dimmer>
                        </center>
                        }
                    </div>
                </div>
                
                <Confirm
                  open={this.state.open}
                  header='Quitar producto de catálogo'
                  content='¿Está seguro de que desea quitar este producto de su catálogo?'
                  cancelButton='No'
                  confirmButton='Sí'
                  onCancel={this.handleCancel}
                  onConfirm={this.handleConfirm}
                />

                <Modal
                    open={this.state.messageOpen}
                    onClose={this.handleMessageConfirm}
                    size='small'
                >
                    <Header icon='browser' content='Aviso' />
                    <Modal.Content>
                    <h3>{this.state.messageInfo}</h3>
                    </Modal.Content>
                    <Modal.Actions>
                    <Button color='green' onClick={this.handleMessageConfirm} inverted>
                        <Icon name='checkmark' /> Aceptar
                    </Button>
                    </Modal.Actions>
              </Modal>

            </div>
        )
    }

    ordenar(atributo) {
        switch(atributo) {
         
          case 'producto': 
            if(this.ordenFiltros.productoAsc) {
              this.props.catalogo.detalles_catalogo.sort(function(a, b){
                return a.titulo.localeCompare(b.titulo);
              });
            }
            else {
              this.props.catalogo.detalles_catalogo.sort(function(a, b){
                return b.titulo.localeCompare(a.titulo);
              })
            }
            this.ordenFiltros.productoAsc = !this.ordenFiltros.productoAsc;
            this.setState({setFiltro: !this.state.setFiltro});
            break;

            case 'stock': 
            if(this.ordenFiltros.stockAsc) {
              this.props.catalogo.detalles_catalogo.sort(function(a, b){
                return a.producto.stock - b.producto.stock;
              });
            } else {
              this.props.catalogo.detalles_catalogo.sort(function(a, b){
                return b.producto.stock - a.producto.stock;
              });
            }
            this.ordenFiltros.stockAsc = !this.ordenFiltros.stockAsc;
            this.setState({setFiltro: !this.state.setFiltro});
            break;

            case 'unidad':
            if(this.ordenFiltros.unidadAsc) {
                this.props.catalogo.detalles_catalogo.sort(function(a, b){
                  return a.producto.unidad_medida.localeCompare(b.producto.unidad_medida);
                });
              }
              else {
                this.props.catalogo.detalles_catalogo.sort(function(a, b){
                  return b.producto.unidad_medida.localeCompare(a.producto.unidad_medida);
                })
              }
              this.ordenFiltros.unidadAsc = !this.ordenFiltros.unidadAsc;
              this.setState({setFiltro: !this.state.setFiltro});
            break;

            case 'precio': 
            if(this.ordenFiltros.precioAsc) {
              this.props.catalogo.detalles_catalogo.sort(function(a, b){
                return a.precio_venta - b.precio_venta;
              });
            } else {
              this.props.catalogo.detalles_catalogo.sort(function(a, b){
                return b.precio_venta - a.precio_venta;
              });
            }
            this.ordenFiltros.precioAsc = !this.ordenFiltros.precioAsc;
            this.setState({setFiltro: !this.state.setFiltro});
            break;
        }
      }
      

    renderTable() {
        return(
                            
            <Grid.Row centered>
                <Grid.Column width={16}>
                      <Table celled>
                          <Table.Header>
                              <Table.Row>
                                  <Table.HeaderCell>Producto<Icon link name='sort' onClick={()=>this.ordenar('producto')}/></Table.HeaderCell>
                                  <Table.HeaderCell textAlign='center'>Stock<Icon link name='sort' onClick={()=>this.ordenar('stock')}/></Table.HeaderCell>
                                  <Table.HeaderCell textAlign='center'>Unidad de medida<Icon link name='sort' onClick={()=>this.ordenar('unidad')}/></Table.HeaderCell>
                                  <Table.HeaderCell textAlign='center'>Precio<Icon link name='sort' onClick={()=>this.ordenar('precio')}/></Table.HeaderCell>
                                  <Table.HeaderCell textAlign='center'>Visible en tienda</Table.HeaderCell>
                                  <Table.HeaderCell textAlign='center'>Acciones</Table.HeaderCell>
                              </Table.Row>
                          </Table.Header>
                                  <Table.Body>
                                      {this.itemsRendered}
                                  </Table.Body>
                       </Table>    
               </Grid.Column>
            </Grid.Row>
        )
    }
}

const mapStateToProps = state => ({
    catalogo: state.catalogo.resultCatalogo
});

export default withRouter(connect(mapStateToProps, { getCatalogo, postCatalogo, deleteDetalleCatalogo })(Catalogo));

