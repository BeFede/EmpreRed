import React, { Component } from 'react'
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { Button, Header, Grid, Table, Message, Divider, Icon, Popup, Rating, Label, Segment } from 'semantic-ui-react'
import MenuPrincipal from './MenuPrincipal';
import MenuCliente from './MenuCliente';
import {getUser} from '../config/configs';
import { openModal } from '../actions/modalActions';
import { getVentasCliente } from '../actions/ventasActions';
import ModalManager from './ModalManager';
import { ModalClienteCompras } from './ModalClienteCompras';
import MenuEmprendedor from './MenuEmprendedor';
import FiltroVentas from './FiltroVentas';
import { getValoracionDetalle, valorarDetalle } from '../actions/tiendaActions';

class ClienteCompras extends Component
{

    constructor(props){
        super(props);
        this.state = { cargado: false, compras: null, messageOpen: false, messageInfo: null,
             tieneCompras: false, paginaActual: 1, setFiltro: true, detalleSiendoValorado: { id: null, esValorado: false, cargandoValoracion: true }  };
        this.userLogeado = (getUser()) ? getUser() : null;
        this.ordenFiltros = { idAsc: false, fechaAsc: true }
    }

    componentWillMount() {
        this.itemsRendered = [];
        this.compras = [];
        this.obtenerCompras();
    }

    asignarColor(estado_id) {
        if(estado_id === 2) {
            return 'green';
        } else if(estado_id === 3) {
            return 'red';
        } else {
            return 'yellow'
        }
    }

    asignarIcon(estado_id) {
        if(estado_id === 2) {
            return 'check circle';
        } else if(estado_id === 3) {
            return 'times circle';
        } else {
            return 'exclamation circle'
        }
    }

    valorarDetalle(id) {
        // Al principio seteo en true el cargandoValoracion, en false el valorado y el id del detalle correspondiente
        this.setState({detalleSiendoValorado: {id: id, cargandoValoracion: true, esValorado: false, valoracion: ''}});

        // Luego llamo al post, cuando se cargue muestro que ya está valorado
        this.props.getValoracionDetalle(id)
        .then(data => {
            this.setState({detalleSiendoValorado: {id: id, cargandoValoracion: false, esValorado: data.datos.es_valorada, valoracion: data.datos.puntaje}});
        }, err => {
            console.log(err);
        })
    }
    
    handleRate = (e, { rating, maxRating }) => {
        this.props.valorarDetalle(this.state.detalleSiendoValorado.id, rating)
        .then(data => {
            this.setState({detalleSiendoValorado:{ id: 1, esValorado:true, cargandoValoracion:false}});
        }, err => {
            console.log(err);
        })
    };

    obtenerCompras() {

        this.props.getVentasCliente(this.state.paginaActual)
        .then(data => {
            console.log(data)
            this.setState({cargado: true});
            // Si tiene compras
            if(data.datos.data) {

                for(let item of data.datos.data) {
                        this.compras.push(item);
                }

                this.setState( { compras: this.compras, tieneCompras : true});
            }  
            
        }, err => {
        console.log(err);
        }) ;
    }

    renderItem(compra)
    {

        const date = new Date(compra.fecha_hora);
        const fechaFormateada = date.toLocaleString();

        return(
            <Table.Row  key={compra.id}>
            <Table.Cell>
            <Header as='h4'>
                <Header.Content>
                {compra.id}
                </Header.Content>
            </Header>
            </Table.Cell>
            <Table.Cell textAlign='center'>{fechaFormateada}</Table.Cell>
                <Table.Cell textAlign='center'>{compra.detalles[0].producto_nombre}</Table.Cell>
                <Table.Cell textAlign='center'>{compra.detalles[0].cantidad}</Table.Cell>
                <Table.Cell textAlign='center'>${compra.pagos.monto}</Table.Cell>
                <Table.Cell textAlign='center'>{compra.emprendedor.nombre}</Table.Cell>
                <Table.Cell textAlign='center'>                
                        <Icon name={this.asignarIcon(compra.estado_id)} color={this.asignarColor(compra.estado_id)} size='big' />
                </Table.Cell>

                <Table.Cell textAlign='center' content>
                    <Button color="orange" circular icon="eye"
                        onClick={() => 
                        this.props.openModal({
                            header: `Compra número ${compra.id}`,
                            content: <ModalClienteCompras data={compra}/>
                        })}
                    />
                    <Popup
                        trigger={<Button circular color='blue' icon='star' content='Valorar' onClick={() => this.valorarDetalle(compra.detalles[0].id_publicacion)}/>}
                        content={
                                (!this.state.detalleSiendoValorado.cargandoValoracion)
                                ?
                                    (!this.state.detalleSiendoValorado.esValorado)
                                    ?
                                    <Rating icon='star' size='large' name='valoracion' defaultRating={0} maxRating={5} onRate={this.handleRate}></Rating>
                                    :
                                    <Label color='green'>
                                        <Icon name='check' /> Producto valorado
                                    </Label>
                                :
                                <Button basic loading content='Cargando' />
                                }
                        on='click'
                        position='top center'
                    />
                </Table.Cell>
        </Table.Row>
        )
    }

    timeout(ms) {
        return new Promise(resolve => setTimeout(resolve, ms));
      }
    
    async sleep(id, valorado) {
        await this.timeout(3000);
        this.setState({detalleSiendoValorado:{ id: id, esValorado:valorado, cargandoValoracion:false}});
    }

    render()
    {   
        // Si tiene compras
        if(this.state.cargado && this.props.comprasCliente) {
            // Si no tiene compras viene un vector vacio en this.props.comprasCliente, si tiene compras viene un "data" que es un vector con las mismas
            if(this.props.comprasCliente.data) {
                this.itemsRendered = this.props.comprasCliente.data.map(compra => {
                
                    return this.renderItem(compra);
                })
            } else {
                this.itemsRendered = this.props.comprasCliente.map(compra => {
                
                    return this.renderItem(compra);
                })
            }
            
        }

        return (
            <div className="DivPrincipal" style={{overflow:'auto'}}>
                <div id="MenuTop">
                    <MenuPrincipal/>
                </div>
                <div className = "row">
                    <div id="MenuLateral">
                    {(this.userLogeado) ? (this.userLogeado.rol === 'Emprendedor' ? <MenuEmprendedor/> : <MenuCliente/>) : null}
                    </div>

                    <div style={{"paddingTop":"61px", marginLeft:"210px"}}>
                        <Grid centered>
                            <Grid.Row>
                                <Grid.Column width={12}>
                                    <Segment loading={!this.state.cargado}>
                                        {
                                        (!this.state.tieneCompras) ?
                                        <Grid centered>
                                            <Grid.Row centered>
                                                <Grid.Column width={10}>
                                                    <center>
                                                        <Message warning size='massive'>
                                                            <Message.Header>No posee compras realizadas aún</Message.Header>
                                                        </Message>
                                                    </center>
                                                </Grid.Column>
                                            </Grid.Row>
                                        </Grid>
                                        :
                                        <Grid centered>
                                            <Grid.Row>
                                                <Grid.Column width={16}>
                                                    <center>
                                                        <h2>
                                                            Mis compras en tienda
                                                        </h2>
                                                    </center>
                                                </Grid.Column>
                                            </Grid.Row>
                                            <Divider/>

                                            <Grid.Row>
                                                <Grid.Column width={16}>
                                                    {this.renderTable()}
                                                </Grid.Column>
                                            </Grid.Row>
                                        </Grid>
                                        }
                                    </Segment>
                                </Grid.Column>
                            </Grid.Row>
                        </Grid>
                    </div>
                    <ModalManager/>
                </div>
            </div>
        )
    }

    renderTable() {
        return(
            <Grid>
                <Grid.Row centered>
                    <Grid.Column width={16}>
                        <center>
                            <div>
                                <h3>Compras realizadas en tienda</h3>
                                <br/>
                            </div>
                        </center>
                        <FiltroVentas filtro= 'ClienteCompras'/>
                    </Grid.Column>
                </Grid.Row>

                <Grid.Row centered>
                    <Grid.Column width={16}>
                        <div style={{overflow: 'auto', maxHeight: 500 }}>
                            <Table celled>
                                <Table.Header>
                                    <Table.Row>
                                    <Table.HeaderCell>ID<Icon link name='sort' onClick={() => this.ordenar('id')}/></Table.HeaderCell>
                                        <Table.HeaderCell textAlign='center'>Fecha (MM/DD/AAAA)<Icon link name='sort' onClick={() => this.ordenar('fecha')}/></Table.HeaderCell>
                                        <Table.HeaderCell textAlign='center'>Producto</Table.HeaderCell>
                                        <Table.HeaderCell textAlign='center'>Cantidad</Table.HeaderCell>
                                        <Table.HeaderCell textAlign='center'>Monto total</Table.HeaderCell>
                                        <Table.HeaderCell textAlign='center'>Vendedor</Table.HeaderCell>
                                        <Table.HeaderCell textAlign='center'>Estado</Table.HeaderCell>
                                        <Table.HeaderCell textAlign='center'>Acciones</Table.HeaderCell>
                                    </Table.Row>
                                </Table.Header>
                                <Table.Body>
                                    {this.itemsRendered}
                                </Table.Body>
                            </Table>
                        </div>
                    </Grid.Column>
                </Grid.Row>
            </Grid>
       
        )
    }

    ordenar(atributo) {

        switch(atributo) {
            case 'id':
            if(this.ordenFiltros.idAsc){
              this.props.comprasCliente.data.sort(function(a, b){
                return a.id - b.id;
                });
              } else {
                this.props.comprasCliente.data.sort(function(a, b){
                return b.id - a.id;
               })
             }
            this.ordenFiltros.idAsc = !this.ordenFiltros.idAsc;
            this.setState({ setFiltro: !this.state.setFiltro })
            break;
           
          case 'fecha':
            if(this.ordenFiltros.fechaAsc) {
              this.props.comprasCliente.data.sort(function(a, b){
                return b.fecha_hora.localeCompare(a.fecha_hora);
              });
            }
            else {
              this.props.comprasCliente.data.sort(function(a, b){
                return a.fecha_hora.localeCompare(b.fecha_hora);
              })
            }
            this.ordenFiltros.fechaAsc = !this.ordenFiltros.fechaAsc;
            this.setState({setFiltro: !this.state.setFiltro});
            break;
        }
      }
}

const mapStateToProps = state => ({
    comprasCliente: state.ventasYComprasCliente.resultComprasCliente,
    valoracionDetalle: state.tienda.valoracionDetalle
});

export default withRouter(connect(mapStateToProps, { openModal, getVentasCliente, getValoracionDetalle, valorarDetalle })(ClienteCompras));