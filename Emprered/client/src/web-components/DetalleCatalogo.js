import React, { Component } from 'react'
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { Button, Icon, Image, Grid, Input, Label, Radio, Tab, Comment, Header, Divider, Statistic, Rating, Form, TextArea, Message, Modal, Segment, Dimmer, Loader } from 'semantic-ui-react'
import MenuPrincipal from './MenuPrincipal';
import MenuEmprendedor from './MenuEmprendedor';
import SearchStandard from './SearchStandard';
import { removeItem, insertItem } from '../actions/tableFillActions';
import {getUser} from '../config/configs';
import { getCatalogo, postCatalogo, getDetalleCatalogo, postDetalleCatalogo, postComentario } from '../actions/catalogoActions';
import Logo from '../assets/EmpreRedRectangular.png';
import Avatar from '../assets/default-avatar.png';
import { urlMedia } from '../config/configs';
import { ModalManager } from './ModalManager';

class DetalleCatalogo extends Component {

    constructor(props){
        super(props);
        this.state = { cargoDetalle: false, catalogo: null, saveDisabled: true,
            detalle: null, messageOpen: false, messageInfo: null, arrayFotos: [] };
        this.userLogeado = (getUser()) ? getUser() : null;
        this.detalleSeleccionado = null; // Si es una carga de nuevo detalle se va a mantener en null
        this.productoSeleccionado = null;
        this.handleChange = this.handleChange.bind(this);
        this.idCatalogo = null;
        this.dataToSend = null;
        this.commentsRendered = null;
        this.nuevoComentario = null;
        this.fotoASubir = null;
        this.esConsulta = false;
        this.listaFotosABorrar = [];
        this.nuevasFotos = [];
    }

    componentWillMount() {

        // Creación de detalle nuevo
        if(this.props.location.state.catalogo && !this.props.location.state.idDetalle) {
            this.idCatalogo = this.props.location.state.catalogo;
            this.setData();
            this.setState({ catalogo: this.props.location.state.catalogo });
        }
        // Si es la consulta de un detalle..
        if(this.props.location.state.catalogo && this.props.location.state.idDetalle) {
            this.idCatalogo = this.props.location.state.catalogo;
            this.setState({ catalogo: this.props.location.state.catalogo });
            this.esConsulta = true;
            this.obtenerDetalle(this.props.location.state.idDetalle);
            this.setData();
            // LLamo al set data luego de la promise también para que renueve los datos
        }

    }

    handleMessageConfirm = () => {
        this.setState({messageOpen: false});

        if(this.state.messageInfo === 'Cambios guardados') {
            this.props.history.push(
                {
                pathname: '/catalogo',
                state: {catalogo: this.idCatalogo, idDetalle: null}
                }
            );
        }
        
        this.props.history.push(
            {
            pathname: '/catalogo',
            state: {catalogo: this.idCatalogo, idDetalle: null}
            }
        );
        
    }

    renderMensaje = (info) => {
        let mensaje = '';
        if(info.estado === 'OK') {
            mensaje = 'Cambios guardados';
        } else {
            mensaje = info.mensaje
        }
        this.setState({ messageInfo: mensaje, messageOpen: true });
    }

    obtenerDetalle(id) {
        this.props.getDetalleCatalogo(id)
        .then(data => {
            this.detalleSeleccionado = data.datos;
            this.setData();
            this.setState({ detalle: data.datos, cargoDetalle: true, saveDisabled: false });
        }, err => {
            console.log(err);
        })
    }

    handleChange = (e, {name, value, checked}) => {
        
        if (name === 'fotoInput'){
            
            let urlFotoASubir = URL.createObjectURL(e.target.files[0]);
            this.fotoASubir = { file: e.target.files[0], url: urlFotoASubir };
        }
        if(name === 'nuevoComentario') {
            this.nuevoComentario = value;
        }

        this.dataToSend = {
            ...this.dataToSend,
            [name]: (value) ? value : checked
        }       

        if(document.getElementById('titulo').value !== '' && document.getElementById('precio_venta').value !== '' && 
            document.getElementById('descripcion').value !== '')
            {
                this.setState({saveDisabled:false});
            } else {
                this.setState({saveDisabled:true});
            }
    };

    setData() {
        // Si es una creación de detalle..
        if(!this.detalleSeleccionado && !this.productoSeleccionado) {
            this.dataToSend = {
                catalogo: this.idCatalogo,
                titulo: null,
                producto: null,
                precio_venta: null,
                descripcion: null,
                estado: 1, // Por defecto visible
                valoracion: null,
                visitas: null,
                // VER BIEN ESTO
                nuevasFotos: [],
                fotosABorrar: [],
                comentarios: null,
            }
        } 
        // Si selecciona un producto
        else if(this.productoSeleccionado) {

            // Tuve que recurrir a la negreada porque visualmente no se updateaba
            if(document.getElementById('descripcion') && document.getElementById('precio_venta') && document.getElementById('titulo')) {
                document.getElementById('titulo').value = this.productoSeleccionado.producto
                document.getElementById('descripcion').value = this.productoSeleccionado.description
                document.getElementById('precio_venta').value = this.productoSeleccionado.price
            }
            
            this.dataToSend = {
                catalogo: this.idCatalogo,
                titulo: this.productoSeleccionado.producto,
                producto: this.productoSeleccionado.id,
                precio_venta: this.productoSeleccionado.price,
                descripcion: this.productoSeleccionado.description,
                estado: 1, // Por defecto visible
                valoracion: null,
                visitas: null,
                nuevasFotos: [],
                fotosABorrar: [],
                comentarios: null
            }

            this.setState({saveDisabled:false});
        } 
        // Si es una consulta de detalle
        else {
            // Traigo los url de las fotos que vienen en el detalle
            let fotosActual = this.state.arrayFotos;
            if(this.detalleSeleccionado.imagenes) {
                for( let foto of this.detalleSeleccionado.imagenes) {
                    const objFoto = {esVieja: true, url: foto.foto}
                    fotosActual.push(objFoto);
                }               
            }
            this.setState({arrayFotos: fotosActual});

            this.dataToSend = {
                id: this.detalleSeleccionado.id,
                catalogo: this.idCatalogo,
                titulo: this.detalleSeleccionado.titulo,
                producto: this.detalleSeleccionado.producto,
                precio_venta: this.detalleSeleccionado.precio_venta,
                descripcion: this.detalleSeleccionado.descripcion,
                estado: (this.detalleSeleccionado.estado === 'Visible') ? 1 : 2,
                valoracion: this.detalleSeleccionado.valoracion,
                visitas: this.detalleSeleccionado.visitas,
                fotosABorrar: [], // Es un array de urls
                nuevasFotos: [], // ES un array de files
                comentarios: this.detalleSeleccionado.comentarios,
            }
        }
    }

    saveChanges() {
        // Parseo el estado asignado por el radio
        this.dataToSend.estado = (this.dataToSend.estado === true || this.dataToSend.estado === 1) ? 1 : 2;

        // Parseo las fotos nuevas (envio solo el file)
        let vectorFilesFotos = [];
        for(let foto of this.nuevasFotos) {
            const fileFoto = foto.file;
            vectorFilesFotos.push(fileFoto);
        }
        // Ahora si seteo en la data a enviar el vector de nuevasFotos
        this.dataToSend.nuevasFotos = vectorFilesFotos;

        // Le defino la lista de fotos a borrar que puede ser un array vacio
        this.dataToSend.fotosABorrar = this.listaFotosABorrar;

         this.props.postDetalleCatalogo(this.dataToSend)
         .then(data => {
             this.renderMensaje(data);
         }, err => {
             console.log(err);
         })
         this.props.history.push(
            {
              pathname: '/catalogo'
            }
        );
    }

    postComment() {
        let comentarioCompleto = {id_detalle: this.detalleSeleccionado.id, titulo: `comentario en detalle ${this.detalleSeleccionado.id}`, descripcion: null};
        if(this.nuevoComentario != null) {
            comentarioCompleto.descripcion = this.nuevoComentario;
            this.props.postComentario(comentarioCompleto).
            then(data => {
                this.nuevoComentario = null;
                document.getElementById("nuevoComentario").value = "";
                document.getElementById("headerComments").scrollIntoView();
            }, err => {
                console.log(err);
            });
            
        }
    }

    agregarFoto() {
        // Solo me interesa el url en arrayFotos
        const objFoto = { esVieja: false, url: this.fotoASubir.url};
        console.log(objFoto)
        let fotosActual = this.state.arrayFotos;
        fotosActual.push(objFoto);
        this.setState({arrayFotos: fotosActual});
        // En la data a mandar si pongo todo el file
        this.nuevasFotos.push(this.fotoASubir)
    }

    eliminarFoto(foto) {
        let nuevoVectorFotos = this.state.arrayFotos;
        const index = nuevoVectorFotos.map(f => f.url).indexOf(foto);
        // Si es una foto de antes, la agrego a la lista de fotos a borrar que mando al backend, sino, la saco del dataToSend porque era nueva
        const fotoABorrar = nuevoVectorFotos[index];
        if(fotoABorrar.esVieja) {
            this.listaFotosABorrar.push(fotoABorrar.url);
            
        } else {
            // Aca busco en los files a enviar
            const indexInData = this.nuevasFotos.map(f => f.url).indexOf(fotoABorrar.url);
            this.nuevasFotos.splice(indexInData, 1)
        }
        // Sea la foto que sea la saco de lo que se muestra
        nuevoVectorFotos.splice(index, 1);
        this.setState({arrayFotos: nuevoVectorFotos});
    }

    render() {

        // Para manejar la seleccion del search hago esta koreanada:
        if(!this.esConsulta) {
            // Esto para que no se me mame con el itemsTable que uso en la parte de catalogos
            if(this.props.itemsTable[0]) {
                this.productoSeleccionado = this.props.itemsTable[0]; 
                this.setData();
                this.props.removeItem(this.props.itemsTable[0]);
            }
        }
        

        return (

            <div className="DivPrincipal" style={{overflow:'auto'}}>
                <div id="MenuTop">
                    <MenuPrincipal/>
                </div>
                <div className = "row">
                    <div id="MenuLateral">
                    {(this.userLogeado) ? <MenuEmprendedor/> : null}
                    </div>

                    <div style={{"paddingTop":"61px", marginLeft:"210px"}}>
                        
                        {
                        ((!this.esConsulta) || (this.esConsulta && this.state.cargoDetalle))
                        ?
                        <Grid>
                            <Grid.Row centered width={10}>
                                <center>
                                    <h2>
                                        Producto en Catálogo
                                    </h2>
                                </center>
                            </Grid.Row>

                            <Divider/>

                            <Grid.Row centered>
                                <Grid.Column width={5}>
                                    {
                                        (this.detalleSeleccionado === null) ?
                                        <div>
                                            <h3>Busque el producto a agregar</h3>
                                            <SearchStandard filtro='Productos'/>
                                        </div>
                                        :
                                        <h3>Editar producto publicado</h3>
                                    }
                                </Grid.Column>
                                <Grid.Column verticalAlign='middle' width={5}>
                                    <center>
                                        <Button circular color='red' icon labelPosition='left' onClick={() => this.routeCatalogo()}>
                                            <Icon name='reply'/>
                                            Volver
                                        </Button>
                                        <Button disabled={this.state.saveDisabled} circular color='green' icon labelPosition='right' onClick={() => this.saveChanges()}>
                                            <Icon name='save'/>
                                            Guardar
                                        </Button>
                                    </center>
                                </Grid.Column>
                                
                            </Grid.Row>
                            <Divider />

                            <Grid.Row centered>
                                <Grid.Column width={5}>
                                    {this.renderTitulo()}
                                    {this.renderCategoriaYSubcategoria()}
                                    {this.renderValoracionVisitasPrecio()}
                                    {this.renderDescripcion()}
                                    {this.renderVisibleRadio()}
                                    {this.renderComentarios()}
                                </Grid.Column>

                                <Divider vertical ></Divider>

                                <Grid.Column width={5}>
                                    {this.renderImagenes()}
                                    {this.renderSeleccionImagen()}
                                </Grid.Column>
                            </Grid.Row>
                        </Grid>
                        :
                        <center>
                            <Dimmer active inverted>
                                <Loader active inline='centered'/>
                            </Dimmer>
                        </center>
                        }
                        {this.renderModalMensaje()}
                    </div>
                </div>
            </div>
        
        )
    }

    routeCatalogo()
    {
        return (
                 this.props.history.push({
                    pathname: '/catalogo'
                    })
        )      
    }

    renderTitulo() {
        return(
            <Grid columns={1}>
                <Grid.Column width={16}>
                    <h4>Título de la publicación</h4>
                    <Input disabled={!this.productoSeleccionado && !this.state.cargoDetalle} fluid name='titulo' placeholder='Título de Producto' id='titulo'
                        defaultValue={(this.dataToSend.titulo) ? this.dataToSend.titulo : null} onChange={this.handleChange}>
                    </Input>
                </Grid.Column>
            </Grid>
        );
    }

    renderCategoriaYSubcategoria() {
        return(
            // Si selecciona un producto se habilitan las categorías, sino se muestra un mensaje
            (this.productoSeleccionado || this.esConsulta)
            ?
            <Grid >
                <Grid.Row verticalAlign='middle'> 
                    <Grid.Column width={16}>
                        <h4>Categoría y Subcategoría</h4>
                        <Label as='a' size='large' color='orange' tag>
                            {(this.esConsulta) ? this.dataToSend.producto.categoria : this.productoSeleccionado.categoria}
                        </Label>
                        <Label as='a' color='orange' tag>
                            {(this.esConsulta) ? this.dataToSend.producto.subcategoria : this.productoSeleccionado.subcategoria}
                        </Label>
                    </Grid.Column>
                </Grid.Row>               
            </Grid>
            :
            <Grid>               
                <Grid.Column width={16}>
                    <Message color='orange' compact>Se mostrarán las Categorias cuando seleccione un producto</Message>  
                </Grid.Column>
            </Grid>
        );
    }

    renderValoracionVisitasPrecio() {
        return(
            <Grid columns={2}>
                <Grid.Column width={8}>
                    <h4>Valoración</h4>
                    <Rating icon='star' disabled defaultRating={((this.dataToSend.valoracion) ? this.dataToSend.valoracion : 0)} maxRating={5}>
                    </Rating>
                    <br/>
                    <Statistic size='mini'>
                        <Statistic.Value>{((this.dataToSend.visitas) ? this.dataToSend.visitas : 0)}</Statistic.Value>
                        <Statistic.Label>Visitas</Statistic.Label>
                    </Statistic>
                </Grid.Column>
                <Grid.Column width={8}>
                    <h4>Precio de venta</h4>
                    <Input disabled={!this.productoSeleccionado && !this.state.cargoDetalle} placeholder='Precio' 
                        defaultValue={(this.dataToSend.precio_venta ? this.dataToSend.precio_venta : 0)} 
                        id='precio_venta' name="precio_venta" label={{ basic: true, content: '$' }} onChange={this.handleChange}>
                    </Input>
                </Grid.Column>
            </Grid>
        );
    }

    renderDescripcion() {
        return(
            <Grid columns={1}>
                <Grid.Column width={16}>
                    <h4>Descripción de la publicación</h4>
                    <Message info>
                        <Message.Header>Utilice Hashtags</Message.Header>
                        <p>Puede agregar hashtags a su publicación para identificarlo en la Tienda</p>
                        <p>Por ejemplo: #NuevaColeccion</p>
                    </Message>
                    <Form>
                        <TextArea disabled={!this.productoSeleccionado && !this.state.cargoDetalle} id='descripcion' name='descripcion' 
                            placeholder='Añada una descripción del producto a publicar' style={{ minHeight: 200 }} 
                            defaultValue={(this.dataToSend.descripcion)}
                            onChange={this.handleChange} />
                    </Form>
                </Grid.Column>
            </Grid>
        );
    }

    renderVisibleRadio() {
        return(
            <Grid verticalAlign='middle' columns={1}> 
                <h4>Visibilidad de la publicación</h4>
                <Grid.Column width={11}>
                    <Message color='orange' compact>Elija si el producto será visible o no en la tienda</Message>                   
                </Grid.Column>
                <Grid.Column width={3}>
                    <Radio name='estado' toggle defaultChecked={(this.dataToSend.estado === 1)} onChange={this.handleChange}/>                    
                </Grid.Column>
            </Grid>
        );
    }

    renderComentarioUnico(com) {
        let avatar = "";
        if(com.foto === "") {
            avatar = Avatar;
        } else {
            avatar = urlMedia + com.foto;
        }
        const date = new Date(com.fecha_hora);
        const dateString= date.toLocaleString();
        return(
            <Comment key={com.fecha_hora}>
                <Comment.Avatar src={avatar} />
                <Comment.Content>
                    <Comment.Author as='a'>{com.creador}</Comment.Author>
                    <Comment.Metadata>
                    <div>{dateString}</div>
                    </Comment.Metadata>
                    <Comment.Text>{com.descripcion}</Comment.Text>
                </Comment.Content>
            </Comment>
        )
    }

    renderComentarios() {

        if(this.props.detalle) {
            this.commentsRendered = this.props.detalle.comentarios.map(com => {
                return this.renderComentarioUnico(com)
            })
        }
        return(
            <Grid columns={1}>
                <Grid.Column width={16}>
                    <Comment.Group size='large'>
                        <Header id= 'headerComments' as='h3' dividing>
                        Comentarios
                        </Header>
                        
                        {this.commentsRendered}

                        <Form reply>
                        <Form.TextArea id='nuevoComentario' name='nuevoComentario' onChange = {this.handleChange}/>
                        <Button circular disabled={this.detalleSeleccionado === null} content='Publicar comentario' labelPosition='left' icon='edit' primary onClick={() => this.postComment()}/>
                        </Form>
                    </Comment.Group>
                </Grid.Column>
            </Grid>
        );
    }

    renderImageTab(urlFoto, srcFoto, count) {
        const tab = { 
            menuItem: `${(count !== undefined) ? `Imagen ${count}` : 'Imagen no disponible'}`, render: () => 
            <Tab.Pane>
                {
                    (count !== undefined) 
                    ?
                    <div>
                        <Button color='red' attached='top' onClick={() => this.eliminarFoto(urlFoto)}>
                            <Icon name='times'/>
                            Eliminar esta imagen
                        </Button>
                        <Segment attached size='massive'>
                            <Image src={srcFoto} fluid />
                        </Segment>
                    </div>
                    :
                    <Segment size='massive'>
                        <Message color='orange' compact>Si no selecciona imágenes a subir, se guardará por defecto la imagen del producto seleccionado</Message>  
                    </Segment>
                }
                
            </Tab.Pane> 
            }
            
        return tab;
    }

    renderImagenes() {
        let panes = [];
        let count = 0;
        // Si es la consulta de un detalle o si seleccionó un producto
        if(this.state.arrayFotos.length > 0) {
            for(let foto of this.state.arrayFotos) {
                count ++;
                let srcFoto = "";
                // Si es foto traida de consulta pongo la dire del server, sino es una foto a agregar y mando la url de una
                if(foto.esVieja) {
                    srcFoto = urlMedia + foto.url;
                } else {
                    srcFoto = foto.url;
                }
                
                const tabFoto = this.renderImageTab(foto.url, srcFoto, count);
                panes.push(tabFoto);
            }
        } else {
            // Si es una nueva carga de detalle o si no tiene foto el detalle consultado pongo por defecto el logo
            panes.push(this.renderImageTab(Logo, 1))
        }

        return(
            <Grid columns={1}>
                <Grid.Column width={16}>
                    <Tab panes={panes} />
                </Grid.Column>
            </Grid>
        );
    }

    renderSeleccionImagen() {
        return(
            
            <Grid columns={1}>
                <Grid.Column width={16}>
                    <center>
                    <Segment>
                        <Input type="file" id="foto" name='fotoInput' onChange={this.handleChange}/>
                        <br/>
                        <br/>
                        <Button circular color='orange' onClick={() => this.agregarFoto()}>
                            <Icon name='upload'/>
                            Agregar foto
                        </Button>
                    </Segment>
                    </center>                  
                </Grid.Column>
            </Grid>
        );
    }

    renderModalMensaje() {
        return (
            <Modal
                open={this.state.messageOpen}
                onClose={this.handleMessageConfirm}
                size='small'
            >
                <Header icon='browser' content='Aviso' />
                <Modal.Content>
                <h3>{this.state.messageInfo}</h3>
                </Modal.Content>
                <Modal.Actions>
            <Button color='green' onClick={this.handleMessageConfirm} inverted>
                    <Icon name='checkmark' /> Aceptar
                </Button>
                </Modal.Actions>
            </Modal>
        );
    }
}

const mapStateToProps = state => ({
    catalogo: state.catalogo.resultCatalogo,
    itemsTable: state.tableData.items,
    etiquetas: state.tipificaciones.etiquetas,
    detalle: state.catalogo.resultDetalleCatalogo
});

export default withRouter(connect(mapStateToProps, { insertItem, removeItem, getCatalogo, postCatalogo, getDetalleCatalogo, postDetalleCatalogo, postComentario })(DetalleCatalogo));