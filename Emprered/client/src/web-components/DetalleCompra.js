import React, { Component } from 'react'
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { Table, Header, Segment} from 'semantic-ui-react'
import { getCompra } from '../actions/compraActions';

class DetalleCompra extends Component {

    constructor(props){
        super(props);
        this.idCompra = props.id;
        this.state = { detalles: [], cargado: false }
    }

    componentWillMount() {
        this.detallesCompraRendered = null;
        this.obtenerDetalles();
    }

    obtenerDetalles() {
        this.props.getCompra(this.idCompra)
        .then(data => {
            this.setState({ detalles: data.datos.detalles});
            this.detallesCompraRendered = this.state.detalles.map(detalle => {  
                return this.renderItemDetalle(detalle);
            });
            this.setState({cargado:true});           
        }, err => {
            console.log(err);
        })
    }

    renderItemDetalle(detalle) {

        
        return(
            <Table.Row key={detalle.id}>
                <Table.Cell>
                <Header as='h4'>
                    <Header.Content>
                    {(detalle.materia_prima) ? detalle.materia_prima : detalle.producto}
                    </Header.Content>
                </Header>
                </Table.Cell>
                <Table.Cell textAlign='center'>{(detalle.materia_prima) ? 'Materia Prima' : 'Producto'}</Table.Cell>
                <Table.Cell textAlign='center'>{detalle.monto}</Table.Cell>
                <Table.Cell textAlign='center'>{detalle.cantidad}</Table.Cell>
                <Table.Cell textAlign='center'>{detalle.unidad_medida}</Table.Cell>
            </Table.Row>
        )  
    }

    render() {
            return (
                <div style={{overflow: 'auto', maxHeight: 500 }}>
                    <Segment loading={!this.state.cargado}>
                        <Table celled color="orange">
                            <Table.Header>
                            <Table.Row>
                                <Table.HeaderCell>Nombre</Table.HeaderCell>
                                <Table.HeaderCell textAlign='center'>Tipo de item</Table.HeaderCell>
                                <Table.HeaderCell textAlign='center'>Costo unitario</Table.HeaderCell>               
                                <Table.HeaderCell textAlign='center'>Cantidad</Table.HeaderCell>
                                <Table.HeaderCell textAlign='center'>Unidad de medida</Table.HeaderCell>
                            </Table.Row>
                            </Table.Header>
                            <Table.Body>
                                {this.detallesCompraRendered}
                            </Table.Body>
                        </Table>
                        <br/>
                    </Segment>
                </div>          
            )
    }
}

const mapStateToProps = state => ({
    compra: state.compras.resultCompra
  });

export default withRouter(connect(mapStateToProps, { getCompra })(DetalleCompra));

