import React,{Component} from 'react'
import { Button, Grid, Segment, Divider, Message, Icon } from 'semantic-ui-react'
import 'semantic-ui-css/semantic.min.css';
import MenuPrincipal from './MenuPrincipal';
import { connect } from 'react-redux';
import { openModal } from '../actions/modalActions';
import { withRouter } from 'react-router-dom';
import RegisterC from './RegisterCliente.js';
import DatosPersonales from './RegistroDatosPersonales.js';
import ModalManager from './ModalManager';

class Entry extends Component{


render()
{
  
  return(
    <div className="DivPrincipal" style={{overflow:'auto'}}>
        <div id="MenuTop">
          <MenuPrincipal/>
        </div>
        <div className = "row">
          <div style={{"paddingTop":"61px"}}>
            <Grid centered>
              <Grid.Row>
                <Grid.Column width={6}>
                  <Segment color='orange'>
                    <Grid>
                      <Grid.Row centered>
                        <Grid.Column width={16}>
                          <center><h1>Selección de rol</h1></center>
                        </Grid.Column>
                      </Grid.Row>
                      <Divider/>
                      <Grid.Row centered>
                        <Grid.Column width={16}>
                          <Message info icon>
                            <Icon name='info circle'/>
                            <Message.Content>
                              <Message.Header>Información</Message.Header>
                              <p>Seleccione con que rol quiere registrarse en la plataforma</p>
                            </Message.Content>
                          </Message>
                        </Grid.Column>
                      </Grid.Row>
                      <Grid.Row centered>
                        <Grid.Column width={16}>
                          <Segment color='orange'>
                            <center>
                              <Divider horizontal/>
                              {/* <Button primary fluid href='/cliente'> */}
                              <Button primary fluid onClick={()=>this.props.openModal({
                                header: "Registro de Cliente",
                                content: <RegisterC/>
                              })}>
                                Cliente
                              </Button>
                              <Divider horizontal>O</Divider>
                              {/* <Button secondary fluid href='/microemprendedor'>
                                Microemprendedor
                              </Button> */}
                              <Button secondary fluid onClick={()=>this.props.openModal({
                                header: "Registro de Microemprendedor",
                                content: <DatosPersonales/>
                              })}>
                              Microemprendedor
                              </Button>
                              <Divider horizontal/>
                            </center>
                          </Segment>
                        </Grid.Column>
                      </Grid.Row>
                    </Grid>
                    
                  </Segment> 
                </Grid.Column>
              </Grid.Row>
            </Grid>

            <ModalManager />
          </div>
        </div>

      
      
     </div>
  )
}
}


const mapStateToProps = state => ({
});

export default withRouter(connect(mapStateToProps, { openModal })(Entry));