import React, { Component } from 'react'
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { Grid, Input, Container, Accordion, Icon, Button, Segment, Divider } from 'semantic-ui-react';
import { getCatalogo } from '../actions/catalogoActions';

class FiltroCatalogo extends Component{

    constructor(props)
    {
        super(props)
        this.state = { showfilter: false, limpiarFiltro: false}
        this.query = { titulo:'', precio_desde:'', precio_hasta:'' }
    }

    handleChange = (e, {name, value, checked}) => {

        this.query = {
           ...this.query,
           [name]: (value) ? value : checked
        }
    };


    render()
    {
        return(
            <Grid>
                <Grid.Row centered>
                    <Grid.Column width={16}>
                        <Accordion>
                            <Accordion.Title active={this.state.showfilter} index={0} onClick={()=>
                                this.setState({
                                showfilter: !this.state.showfilter
                                })}>
                                <Icon name='dropdown' />
                                Filtros
                            </Accordion.Title>
                        </Accordion>
                        {this.renderFiltros()}
                    </Grid.Column>
                </Grid.Row>    
           </Grid>
        )
    }

    
    enviarQuery(){
        // Chequeamos que nada quede en undefined
        this.query = {
            titulo: (this.query.titulo) ? this.query.titulo : '',
            precio_desde: (this.query.precio_desde) ? this.query.precio_desde : '',
            precio_hasta: (this.query.precio_hasta) ? this.query.precio_hasta : '',
        }
        
        this.props.getCatalogo(this.query);

        this.setState({
            limpiarFiltro: true
        })
    }
    
    renderFiltros()
    {
       

        if(this.state.showfilter)
        {
        return(
        <Grid>
           <Grid.Row>
                <Grid.Column width={4} verticalAlign='top'>
                    <Grid>
                        <Grid.Row>
                        <Grid.Column width={16}>
                            <h4>Por nombre</h4>
                            <Input fluid circular size='mini' name='titulo' onChange={this.handleChange} id={0}/>
                        </Grid.Column>   
                        </Grid.Row>
                    </Grid>
                </Grid.Column>   
                <Grid.Column width={4} verticalAlign='top'>
                   <h4>Precio</h4>
                        <Grid>
                                <Grid.Row>
                                    <Grid.Column width={8}>
                                        <Grid>
                                            <Grid.Row>
                                                <Grid.Column width={6}>
                                                       Desde:           
                                                </Grid.Column>
                                                <Grid.Column width={10}>
                                                      <Container content>
                                                          <Input fluid circular type='number' size='mini' name='precio_desde' onChange={this.handleChange} id={3}/>
                                                      </Container>                                            
                                                </Grid.Column>
                                            </Grid.Row>
                                        </Grid>
                                    </Grid.Column>
                                    <Grid.Column width={8}>
                                    <Grid>
                                          <Grid.Row>
                                              <Grid.Column width={6}>
                                                     Hasta:           
                                              </Grid.Column>
                                              <Grid.Column width={10}>
                                                    <Container content>
                                                        <Input fluid circular type='number' size='mini' name='precio_hasta' onChange={this.handleChange} id={4}/>
                                                    </Container>                                            
                                              </Grid.Column>
                                          </Grid.Row>
                                            </Grid>
                                    </Grid.Column>
                                </Grid.Row>
                        </Grid>
                </Grid.Column>    
            </Grid.Row>
             <Grid.Row>
                <Grid.Column width={4}>
                    <Button color='red' style={{ marginBottom:10}} onClick={()=> this.limpiarFiltros()} disabled = {!this.state.limpiarFiltro}>
                        Mostrar todo
                    </Button>
                </Grid.Column> 
                <Grid.Column width={4}>
                    <Button name='buscar' color='orange' style={{ marginBottom:10}} onClick={()=> this.enviarQuery()}>
                        Buscar
                    </Button>    
                </Grid.Column>
            </Grid.Row>
         </Grid>        
        )
        }
    }

    limpiarFiltros()
    {
        this.props.getCatalogo();
    }
}
   const mapStateToProps = state => ({
    itemsTable: state.tableData.items
 });
  export default withRouter(connect(mapStateToProps, { getCatalogo })(FiltroCatalogo));