import React, { Component } from 'react'
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { getTodasMPEmp } from '../actions/matPrimaEmpActions';
import { getProductos } from '../actions/productosActions';
import { getSubcategoriasDeCategoria, getCategoriasProducto } from '../actions/tipificacionesActions';
import { Grid, Input, Radio, Container, Dropdown, Accordion, Icon, Button, Form } from 'semantic-ui-react'
import { getUnidadesMedida } from '../actions/tipificacionesActions';

class FiltrosMPyProd extends Component{

    constructor(props)
    {
        super(props)
        super(props);
        // Recibe por param el tipo de filtro a usar
        this.filtro = props.filtro;
        this.state = { showfilter: false, limpiarFiltro: false, cargoCategorias: false}
        this.query = { nombre:'', costo_desde:'', costo_hasta:'', stock_desde:'', stock_hasta:'', unidad_medida:'', stock_bajo:'', categoria: '', subcategoria: '' }
    }

    componentWillMount(){
        this.props.getUnidadesMedida();
        this.categoriasProducto = [];
        this.optionsUnidades = [];
        this.optionsCategorias = [];
        this.optionsSubcategorias = [];
        if(this.filtro === 'Productos') {
            this.props.getCategoriasProducto()
            .then(data => {
                this.categoriasProducto = data
                this.setState({cargoCategorias:true});
            }, err => {
                console.log(err);
            });
        }
    }

    handleChange = (e, {name, value, checked}) => {

        if (name == 'categoria'){
            // Si elige una categoria filtro sus subcategorias, limpiando previamente
            this.optionsSubcategorias= [];
            // SI es -1 es porque lo dejó en Elegir...
            if(value !== -1) {
                this.props.getSubcategoriasDeCategoria(value)
                .then(data => {
                  for (const subcat of data) {
                    const opt = {key: subcat.id, text: subcat.descripcion, value: subcat.id}
                    this.optionsSubcategorias.push(opt);
                  }
                  this.setState({ cambioCategoria: !this.state.cambioCategoria});
                }, err => {
                  console.log(err);
                })
            } else {
                // Limpio la seleccion de subcategoria
                this.optionsSubcategorias = [];
                this.query.subcategoria = '';
                this.setState({ cambioCategoria: !this.state.cambioCategoria});
            }
        }

        this.query = {
           ...this.query,
           // Este handleChange en especial necesita asignar un value si o si a los atributos de la query, por eso se evalua si el value es undefined
           [name]: (value) ? value : checked
        }
    };

    obtenerSubcategoriasDefault() {
        // Obtengo las subcategorias para llenar el combo y dejar seleccionada la que tenía el producto
  
        this.props.getSubcategoriasDeCategoria(this.query.categoria)
            .then(data => {
              this.optionsSubcategorias= [];
              for (const subcat of data) {
                const opt = {key: subcat.id, text: subcat.descripcion, value: subcat.id}
                this.optionsSubcategorias.push(opt);
              }
              this.setState({ cambioCategoria: !this.state.cambioCategoria});
            }, err => {
              console.log(err);
            })
    }
  

    render()
    {
        return(
            <div>
            <Accordion>
            <Accordion.Title active={this.state.showfilter} index={0} onClick={()=>
             this.setState({
               showfilter: !this.state.showfilter
             })}>
             <Icon name='dropdown' />
               Filtros
           </Accordion.Title>
           </Accordion>
           {this.renderFiltros()}
           </div>
        )
    }

    getOptions() {
        this.optionsCategorias = [];
        const def = { key: -1, text:'Elegir...', value: -1 };
        this.optionsCategorias.push(def);
        for (const cat of this.categoriasProducto) {
            const opt = {key: cat.id, text: cat.descripcion, value: cat.id}
            this.optionsCategorias.push(opt);
        }
        if(this.props.unidadesMedida)
        {        
          this.optionsUnidades = [];
          //seteamos valor por default
          this.optionsUnidades.push(def);
          for (const unid of this.props.unidadesMedida) {
              const opt = {key: unid.id, text: unid.descripcion, value: unid.id}
              this.optionsUnidades.push(opt);
          }
        }
    }   
    
    enviarQuery(){
        // Chequeamos que nada quede en undefined
        this.query = {
            nombre: (this.query.nombre) ? this.query.nombre : '',
            unidad_medida: (this.query.unidad_medida) ? this.query.unidad_medida : '',
            costo_desde: (this.query.costo_desde) ? this.query.costo_desde : '',
            costo_hasta: (this.query.costo_hasta) ? this.query.costo_hasta : '',
            stock_desde: (this.query.stock_desde) ? this.query.stock_desde : '',
            stock_hasta: (this.query.stock_hasta) ? this.query.stock_hasta : '',
            stock_bajo: (this.query.stock_bajo) ? this.query.stock_bajo : '',
            categoria: (this.query.categoria) ? this.query.categoria : '',
            subcategoria: (this.query.subcategoria) ? this.query.subcategoria : ''
        }
        // Si me dejo seleccionada la opción Elegir, se toma como -1, por ende lo parseamos
        if(this.query.unidad_medida === -1) {
            this.query.unidad_medida = '';
        }
        if(this.filtro === 'Productos' && this.query.categoria === -1) {
            this.query.categoria = '';
        }
        if(this.filtro === 'Productos' && this.query.subcategoria === -1) {
            this.query.subcategoria = '';
        }
        // Si viene stock_bajo como true lo parseamos a 1, sino a string vacio
        if(this.query.stock_bajo) {
            this.query.stock_bajo = 1;
        } else {
            this.query.stock_bajo = '';
        }
        // Si estamos en Materias Primas hacemos la búsqueda ahí, sino en el otro
        if(this.filtro === 'Materia Prima') {
            this.props.getTodasMPEmp(this.query);
        } else {
            this.props.getProductos(this.query);
        }
        this.setState({
            limpiarFiltro: true
        })
    }
    
    renderFiltros()
    {
        this.getOptions();

        if(this.state.showfilter)
        {
        return(
        <div>
        <Grid>
        <Grid.Row>
        <Grid.Column width={16}>
        <Grid>
           <Grid.Row>
                <Grid.Column width={4}>
                    <Grid>
                        <Grid.Row centered verticalAlign='middle'>
                        <Grid.Column width={16}>
                            <h4>Por nombre</h4>
                            <Input fluid circular size='mini' name='nombre' onChange={this.handleChange} id={0}/>
                        </Grid.Column>   
                        </Grid.Row>
                    </Grid>
                </Grid.Column>   
                <Grid.Column width={4}>
                        <h4>Stock</h4>
                        <Grid>
                                <Grid.Row>
                                    <Grid.Column width={8}>
                                        <Grid>
                                            <Grid.Row>
                                                <Grid.Column width={6}>
                                                       Desde:           
                                                </Grid.Column>
                                                <Grid.Column width={10}>
                                                      <Container content>
                                                          <Input fluid circular type='number' size='mini' name='stock_desde' onChange={this.handleChange} id={1}/>
                                                      </Container>                                            
                                                </Grid.Column>
                                            </Grid.Row>
                                        </Grid>
                                    </Grid.Column>
                                    <Grid.Column width={8}>
                                    <Grid>
                                          <Grid.Row>
                                              <Grid.Column width={6}>
                                                     Hasta:           
                                              </Grid.Column>
                                              <Grid.Column width={10}>
                                                    <Container content>
                                                        <Input fluid circular type='number' size='mini' name='stock_hasta' onChange={this.handleChange} id={2}/>
                                                    </Container>                                            
                                              </Grid.Column>
                                          </Grid.Row>
                                            </Grid>
                                    </Grid.Column>
                                </Grid.Row>
                                </Grid>
                </Grid.Column>
                <Grid.Column width={4}>
                   <h4>Costo</h4>
                        <Grid>
                                <Grid.Row>
                                    <Grid.Column width={8}>
                                        <Grid>
                                            <Grid.Row>
                                                <Grid.Column width={6}>
                                                       Desde:           
                                                </Grid.Column>
                                                <Grid.Column width={10}>
                                                      <Container content>
                                                          <Input fluid circular type='number' size='mini' name='costo_desde' onChange={this.handleChange} id={3}/>
                                                      </Container>                                            
                                                </Grid.Column>
                                            </Grid.Row>
                                        </Grid>
                                    </Grid.Column>
                                    <Grid.Column width={8}>
                                    <Grid>
                                          <Grid.Row>
                                              <Grid.Column width={6}>
                                                     Hasta:           
                                              </Grid.Column>
                                              <Grid.Column width={10}>
                                                    <Container content>
                                                        <Input fluid circular type='number' size='mini' name='costo_hasta' onChange={this.handleChange} id={4}/>
                                                    </Container>                                            
                                              </Grid.Column>
                                          </Grid.Row>
                                            </Grid>
                                    </Grid.Column>
                                </Grid.Row>
                        </Grid>
                </Grid.Column>    
                <Grid.Column width={2}>
                      <h4>Unidad de medida</h4>
                        <Dropdown defaultValue={-1} selection compact options={this.optionsUnidades} name='unidad_medida' onChange={this.handleChange} id={5}/>
                </Grid.Column>
                <Grid.Column width={2}>
                       <h4>Stock bajo</h4> 
                       <Radio toggle name='stock_bajo' onChange={this.handleChange} id={6}/>
                </Grid.Column>
            </Grid.Row> 

            {(this.filtro === 'Productos') ?
                <Grid.Row>
                    <Grid.Column width={8}>
                        <Grid>
                            
                            <Grid.Row>
                                <Grid.Column width={6}>
                                    <h4>Por categoría / subcategoría:</h4> 
                                </Grid.Column>
                                <Grid.Column width={5}>
                                    <Form.Select fluid label='Categoría' placeholder='Categoría' name='categoria' options={this.optionsCategorias} onChange={this.handleChange} />
                                </Grid.Column>
                                <Grid.Column width={5}>
                                    <Form.Select fluid label='Subcategoría' placeholder='Subcategoría' name='subcategoria' options={this.optionsSubcategorias} onChange={this.handleChange} />
                                </Grid.Column>
                            </Grid.Row>
                        </Grid>
                    </Grid.Column>
                </Grid.Row> 
                :
                null   
            }
         </Grid>  
        </Grid.Column>
        </Grid.Row>
        <Grid.Row centered>
            <Grid.Column width={8}>
                <Button color='red' style={{ marginBottom:10}} onClick={()=> this.limpiarFiltros()} disabled = {!this.state.limpiarFiltro}>
                    Mostrar todo
                </Button>
            </Grid.Column> 
            <Grid.Column width={8}>
                <Button name='buscar' color='orange' style={{ marginBottom:10}} onClick={()=> this.enviarQuery()}>
                    Buscar
                </Button>    
            </Grid.Column>
        </Grid.Row>
        </Grid>
       </div>
        )
        }
    }

    limpiarFiltros()
    {
        // var i;
        //     for (i = 0; i < 5; i++) {
        //         var x = document.getElementById(i);
        //         x.value='';
        //     } 
        //     var x = document.getElementById(5);
        //     x.selectedIndex = "-1"
        //     x = document.getElementById(6);
        //     x.checked = false;
            
        (this.filtro === 'Materia Prima') ? this.props.getTodasMPEmp() : this.props.getProductos();   
        this.setState({
            limpiarFiltro: false
        })
    }
}

const mapStateToProps = state => ({
    unidadesMedida: state.tipificaciones.unidadesMedida
  });

  export default withRouter(connect(mapStateToProps, { getUnidadesMedida, getTodasMPEmp, getProductos, getSubcategoriasDeCategoria, getCategoriasProducto })(FiltrosMPyProd));