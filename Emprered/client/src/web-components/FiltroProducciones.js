import React, { Component } from 'react'
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { getTodasMPEmp } from '../actions/matPrimaEmpActions';
import { getProductos } from '../actions/productosActions';
import { Grid, Input, Container, Dropdown, Accordion, Icon, Button } from 'semantic-ui-react';
import { getProducciones } from '../actions/produccionActions';
import { getCompras } from '../actions/compraActions';
import { getMovimientos } from '../actions/stockActions';
import SearchMPProd from './SearchMpProd.js';
import { removeMP, removeProd } from '../actions/mpOrProdSelectActions';

class FiltrosProducciones extends Component{

    constructor(props)
    {
        super(props)
        super(props);
        // Recibe por param el tipo de filtro a usar
        this.filtro = props.filtro;
        this.optionItem= [
            { key: -1, text:'Elegir...', value: -1 }, 
            { key: 0, text:'Materia Prima', value: 0 },
            { key: 1, text:'Producto', value: 1 }
        ]
        this.optionState= [
           { key: -1, text:'Elegir...', value: -1 }, 
           { key: 0, text:'Pendiente', value: 1 },
           { key: 1, text:'En proceso', value: 2 },
           { key: 2, text:'Terminada', value: 3 },
           { key: 3, text:'Anulada', value: 4 }
        ]
        this.optionTipoMov= [
            { key: -1, text:'Elegir...', value: -1 }, 
            { key: 0, text:'Ingreso', value: 1 },
            { key: 1, text:'Egreso', value: 2 }
        ]
        this.optionProveedor = this.cargarOptionsProveedores();
        this.state = { showfilter: false, limpiarFiltro: false, producto: false, itemElegido: false }
        this.query = { producto:'', materia_prima:'', estado:'', fecha_desde:'', fecha_hasta:'', descripcion:''}
    }

    componentWillMount(){
        
    }

    cargarOptionsProveedores() {
        if(this.filtro === 'Compras') {
            let vectorOptionsProveedores = [{ key: -1, text:'Elegir...', value: -1 }];
            for(let proveedor of this.props.proveedores) {
                const option = { key: proveedor.id, text:proveedor.nombre, value: proveedor.id };
                vectorOptionsProveedores.push(option);
            }
            return vectorOptionsProveedores;
        } else {
            return [];
        }
        
    }

    handleChange = (e, {name, value, checked}) => {
        if(name === 'item')
        {
           this.setState({
                itemElegido: true
            })

            if(document.getElementById('search')) {
                console.log('deberia cambiar')
                console.log(document.getElementById('search').value)
                document.getElementById('search').value = ''
            }

            switch (value){
              
              case 0:  
              this.setState({
                producto: false
                })
                break;
               case 1:
               this.setState({
                    producto: true
                })
                break;
               case -1:
               this.setState({
                itemElegido: false
                })
                break;
            }
        }
        else
        {
        this.query = {
           ...this.query,
           // Este handleChange en especial necesita asignar un value si o si a los atributos de la query, por eso se evalua si el value es undefined
           [name]: (value) ? value : checked
        }
       }
    };

    render()
    {
        return(
            <div>
            <Accordion>
            <Accordion.Title active={this.state.showfilter} index={0} onClick={()=>
             this.setState({
               showfilter: !this.state.showfilter
             })}>
             <Icon name='dropdown' />
               Filtros
           </Accordion.Title>
           </Accordion>
           {this.renderFiltros()}
           </div>
        )
    }

        
    enviarQuery(){
        // Tomo los valores de lo que esté seleccionado
        if(this.state.itemElegido) {
            this.query.producto = this.props.productoSeleccionado;
            this.query.materia_prima = this.props.mpSeleccionada;
        } else {
            this.query.producto = '';
            this.query.materia_prima = '';
        }
        // Chequeamos que nada quede en undefined
        this.query = {
            producto: (this.query.producto) ? this.query.producto : '',
            materia_prima: (this.query.materia_prima) ? this.query.materia_prima : '',
            estado: (this.query.estado) ? this.query.estado : '',
            fecha_desde: (this.query.fecha_desde) ? this.query.fecha_desde : '',
            fecha_hasta: (this.query.fecha_hasta) ? this.query.fecha_hasta : '',
            descripcion: (this.query.descripcion) ? this.query.descripcion : '',
            motivo: (this.query.motivo) ? this.query.motivo : '',
            tipo_movimiento : (this.query.tipo_movimiento) ? this.query.tipo_movimiento : '',
            proveedor: (this.query.proveedor) ? this.query.proveedor : '',
            comprobante: (this.query.comprobante) ? this.query.comprobante : '',
        }
        // Si me dejo seleccionada la opción Elegir, se toma como -1, por ende lo parseamos
        if(this.query.estado === -1) {
            this.query.estado = '';
        }

        if(this.query.tipo_movimiento === -1) {
            this.query.tipo_movimiento = '';
        }

        if(this.query.proveedor === -1) {
            this.query.proveedor = '';
        }
        
        this.setState({
                limpiarFiltro: true
        })

        if(this.filtro === 'Producciones'){
            this.props.getProducciones(this.query);
        }
        else if (this.filtro === 'Compras') {
            this.props.getCompras(this.query);
        } else {
            this.props.getMovimientos(this.query);
        }
    }
    
    renderSearchItem()
    {
        if(this.state.itemElegido)
        {
            return(
                //<h4>{this.state.itemElegido ? ( this.state.producto ? 'Producto': 'Materia Prima') : 'Elegir item'}</h4>
                <div>
                    <h4>{this.state.producto ? 'Producto': 'Materia Prima'}</h4>
                    <SearchMPProd item = { this.state.producto ? 'Producto': 'Materia Prima' }/>
                </div>
            )
        }
    }

    renderFiltros()
    {
        
        if(this.state.showfilter)
        {
        return(
        <div>
        <Grid>
        <Grid.Row>
        <Grid.Column width={16}>
          <Grid>
           <Grid.Row>
           <Grid.Column width={6}>
               <Grid>
                  <Grid.Row>
                    <Grid.Column width={16}>
                     <Grid>                      
                      <Grid.Row>
                         <Grid.Column width={5}>
                             <h4>Buscar por:</h4>
                              <Dropdown defaultValue={-1} selection compact defaultValue={-1} options={this.optionItem} name='item' onChange={this.handleChange} id={5}/>
                         </Grid.Column>
                         <Grid.Column width={11}>
                            {/* <Input fluid circular size='mini' name='nombre' onChange={this.handleChange} id={0}/> */}
                            { this.renderSearchItem()}
                         </Grid.Column>
                         </Grid.Row>     
                     </Grid>
                    </Grid.Column>
                   </Grid.Row>
                 </Grid>
            </Grid.Column>

            <Grid.Column width={6}>
                    <h4>Fecha</h4>
                    <Grid>
                        <Grid.Row>
                            <Grid.Column width={16}>
                                <Grid>
                                    <Grid.Row>
                                        <Grid.Column width={2}>
                                                Desde:           
                                        </Grid.Column>
                                        <Grid.Column width={6}>
                                                <Container content>
                                                    <Input fluid circular type='date' size='mini' name='fecha_desde' onChange={this.handleChange} id={1}/>
                                                </Container>                                            
                                        </Grid.Column>
                                        <Grid.Column width={2}>
                                                Hasta:            
                                        </Grid.Column>
                                        <Grid.Column width={6}>
                                        <Container content>
                                                    <Input fluid circular type='date' size='mini' name='fecha_hasta' onChange={this.handleChange} id={2}/>
                                                </Container>    
                                        </Grid.Column>
                                        
                                    </Grid.Row>
                                </Grid>       
                            </Grid.Column>
                        </Grid.Row>
                    </Grid>
            </Grid.Column>

            <Grid.Column width={2}>
                   {(this.filtro === 'Compras') ? this.renderSearchProveedor() : this.renderSearchHastag()}    
            </Grid.Column>
            <Grid.Column width={2}>
                    {(this.filtro === 'Compras') ? this.renderSearchComprobante() : this.renderSearchStateOTipoMov()}    
            </Grid.Column> 
            
                
         </Grid.Row>
         </Grid>  
        </Grid.Column>
        </Grid.Row>
        <Grid.Row centered>
            <Grid.Column width={8}>
                <Button color='red' style={{ marginBottom:10}} onClick={()=> this.limpiarFiltros()} disabled = {!this.state.limpiarFiltro}>
                    Mostrar todo
                </Button>
            </Grid.Column> 
            <Grid.Column width={8}>
                <Button name='buscar' color='orange' style={{ marginBottom:10}} onClick={()=> this.enviarQuery()}>
                    Buscar
                </Button>    
            </Grid.Column>
        </Grid.Row>
        </Grid>
       </div>
        )
        }
    }

    renderSearchStateOTipoMov()
    {
        if(this.filtro === 'Producciones')
        {
            return(
                <Grid>
                    <Grid.Row>
                        <Grid.Column>
                            <h4>Por estado</h4>
                            <Dropdown defaultValue={-1} selection compact defaultValue={-1} options={this.optionState} name='estado' onChange={this.handleChange}/>
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            )
        }
        if(this.filtro === 'Movimientos')
        {
            return(
                <Grid>
                    <Grid.Row>
                        <Grid.Column>
                            <h4>Por Tipo</h4>
                            <Dropdown defaultValue={-1} selection compact defaultValue={-1} options={this.optionTipoMov} name='tipo_movimiento' onChange={this.handleChange}/>
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            )
        }
    }

    renderSearchHastag()
    {
        if(this.filtro === 'Producciones' || this.filtro === 'Movimientos')
        {
            return(
                    (this.filtro === 'Producciones') ?
                      <Grid>
                        <Grid.Row>
                           <Grid.Column width={16}>
                               <h4>Por descripción</h4>
                               <Input fluid circular size='mini' name='descripcion' onChange={this.handleChange} id={0}/>
                             </Grid.Column>   
                         </Grid.Row>
                      </Grid>
                      :
                      <Grid>
                        <Grid.Row>
                           <Grid.Column width={16}>
                               <h4>Por Motivo</h4>
                               <Input fluid circular size='mini' name='motivo' onChange={this.handleChange} id={0}/>
                             </Grid.Column>   
                         </Grid.Row>
                      </Grid>
            )
        }
         
    }

    renderSearchProveedor(){
        return(
            <Grid>
                <Grid.Row>
                    <Grid.Column>
                        <h4>Proveedor</h4>
                        <Dropdown defaultValue={-1} selection compact defaultValue={-1} options={this.optionProveedor} name='proveedor' onChange={this.handleChange}/>
                    </Grid.Column>
                </Grid.Row>
            </Grid>
        ) 
    }

    renderSearchComprobante() {
        return (
            <Grid>
                <Grid.Row>
                    <Grid.Column width={16}>
                        <h4>Comprobante</h4>
                        <Input fluid circular type='number' size='mini' name='comprobante' onChange={this.handleChange} id={0}/>
                    </Grid.Column>   
                </Grid.Row>
             </Grid>
        )
    }

    limpiarFiltros()
    {
        (this.filtro === 'Producciones') ? this.props.getProducciones() : ((this.filtro === 'Compras') ? this.props.getCompras() : this.props.getMovimientos());   
        this.setState({
            limpiarFiltro: false
        })
    }
}

const mapStateToProps = state => ({
    unidadesMedida: state.tipificaciones.unidadesMedida,
    mpSeleccionada: state.mpOrProd.itemMP,
    productoSeleccionado: state.mpOrProd.itemProd,
    proveedores: state.proveedores.proveedores
  });

  export default withRouter(connect(mapStateToProps, { getTodasMPEmp, getProductos, getProducciones, getCompras, getMovimientos, removeMP, removeProd })(FiltrosProducciones));