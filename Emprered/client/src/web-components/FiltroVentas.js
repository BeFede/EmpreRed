import React, { Component } from 'react'
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { Grid, Input, Container, Dropdown, Accordion, Icon, Button } from 'semantic-ui-react';
import { getVentas, getVentasCliente } from '../actions/ventasActions';

class FiltroVentas extends Component{

    constructor(props)
    {
        super(props)
        super(props);
        // Recibe por param el tipo de filtro a usar
        this.filtro = props.filtro;
        
        this.optionState= [
           { key: -1, text:'Elegir...', value: -1 }, 
           { key: 0, text:'Pendiente de pago', value: 1 },
           { key: 1, text:'Confirmada', value: 2 }
           
        ]
        
        this.state = { paginaActual: 1, showfilter: false, limpiarFiltro: false }
        this.query = { emprendedor:'', comprador:'', estado:'', fecha_desde:'', fecha_hasta:'', monto_desde:'', monto_hasta:''}
    }

    componentWillMount(){
        
    }

    handleChange = (e, {name, value}) => {   
        
        this.query = {
           ...this.query,
           // Este handleChange en especial necesita asignar un value si o si a los atributos de la query, por eso se evalua si el value es undefined
           [name]: (value) ? value : ''
        }
    };

    render()
    {
        return(
            <div>
            <Accordion>
            <Accordion.Title active={this.state.showfilter} index={0} onClick={()=>
             this.setState({
               showfilter: !this.state.showfilter
             })}>
             <Icon name='dropdown' />
               Filtros
           </Accordion.Title>
           </Accordion>
           {this.renderFiltros()}
           </div>
        )
    }

    cleanTableData() {
        for(let item of this.props.itemsTable) {
            this.props.removeItem(item);
        }
    }
        
    enviarQuery(){
        
        // Chequeamos que nada quede en undefined
        this.query = {
            emprendedor_apellido: (this.query.emprendedor_apellido) ? this.query.emprendedor_apellido : '',
            comprador_nombre: (this.query.comprador_nombre) ? this.query.comprador_nombre : '',
            estado: (this.query.estado) ? this.query.estado : '',
            fecha_desde: (this.query.fecha_desde) ? this.query.fecha_desde : '',
            fecha_hasta: (this.query.fecha_hasta) ? this.query.fecha_hasta : '',
            monto_desde: (this.query.monto_desde) ? this.query.monto_desde : '',
            monto_hasta: (this.query.monto_hasta) ? this.query.monto_hasta : '',
        }
        // Si me dejo seleccionada la opción Elegir, se toma como -1, por ende lo parseamos
        if(this.query.estado === -1) {
            this.query.estado = '';
        }
        
        this.setState({
                limpiarFiltro: true
        })

        if(this.filtro === 'Ventas'){
            this.props.getVentas(this.state.paginaActual, this.query)
        }
        else if (this.filtro === 'ClienteCompras') {
            this.props.getVentasCliente(this.state.paginaActual, this.query);
        }
    }

    renderFiltros()
    {
        
        if(this.state.showfilter)
        {
        return(
        <div>
        <Grid>
        <Grid.Row>
        <Grid.Column width={16}>
          <Grid>
           <Grid.Row>
           <Grid.Column width={3}>
               <Grid>
                  <Grid.Row>
                    <Grid.Column width={16}>
                     <Grid>                      
                      <Grid.Row>
                         <Grid.Column width={16}>
                            {this.renderSearchEmprendedorOComprador()}
                         </Grid.Column>
                         </Grid.Row>     
                     </Grid>
                    </Grid.Column>
                   </Grid.Row>
                 </Grid>
            </Grid.Column>

            <Grid.Column width={6}>
                    <h4>Fecha</h4>
                    <Grid>
                        <Grid.Row>
                            <Grid.Column width={16}>
                                <Grid>
                                    <Grid.Row>
                                        <Grid.Column width={2}>
                                                Desde:           
                                        </Grid.Column>
                                        <Grid.Column width={6}>
                                                <Container content>
                                                    <Input fluid circular type='date' size='mini' name='fecha_desde' onChange={this.handleChange} id={1}/>
                                                </Container>                                            
                                        </Grid.Column>
                                        <Grid.Column width={2}>
                                                Hasta:            
                                        </Grid.Column>
                                        <Grid.Column width={6}>
                                        <Container content>
                                                    <Input fluid circular type='date' size='mini' name='fecha_hasta' onChange={this.handleChange} id={2}/>
                                                </Container>    
                                        </Grid.Column>
                                        
                                    </Grid.Row>
                                </Grid>       
                            </Grid.Column>
                        </Grid.Row>
                    </Grid>
            </Grid.Column>

            <Grid.Column width={5}>
                    <h4>Monto</h4>
                    <Grid>
                        <Grid.Row>
                            <Grid.Column width={16}>
                                <Grid>
                                    <Grid.Row>
                                        <Grid.Column width={2}>
                                                Desde:           
                                        </Grid.Column>
                                        <Grid.Column width={6}>
                                                <Container content>
                                                    <Input fluid circular type='number' size='mini' name='monto_desde' onChange={this.handleChange} id={1}/>
                                                </Container>                                            
                                        </Grid.Column>
                                        <Grid.Column width={2}>
                                                Hasta:            
                                        </Grid.Column>
                                        <Grid.Column width={6}>
                                        <Container content>
                                                    <Input fluid circular type='number' size='mini' name='monto_hasta' onChange={this.handleChange} id={2}/>
                                                </Container>    
                                        </Grid.Column>
                                        
                                    </Grid.Row>
                                </Grid>       
                            </Grid.Column>
                        </Grid.Row>
                    </Grid>
            </Grid.Column>

            <Grid.Column width={2}>
                    {this.renderSearchState()}    
            </Grid.Column> 
            
                
         </Grid.Row>
         </Grid>  
        </Grid.Column>
        </Grid.Row>
        <Grid.Row centered>
            <Grid.Column width={8}>
                <Button color='red' style={{ marginBottom:10}} onClick={()=> this.limpiarFiltros()} disabled = {!this.state.limpiarFiltro}>
                    Mostrar todo
                </Button>
            </Grid.Column> 
            <Grid.Column width={8}>
                <Button name='buscar' color='orange' style={{ marginBottom:10}} onClick={()=> this.enviarQuery()}>
                    Buscar
                </Button>    
            </Grid.Column>
        </Grid.Row>
        </Grid>
       </div>
        )
        }
    }

    renderSearchState()
    {
        return(
            <Grid>
                <Grid.Row>
                    <Grid.Column>
                        <h4>Estado</h4>
                        <Dropdown defaultValue={-1} selection compact defaultValue={-1} options={this.optionState} name='estado' onChange={this.handleChange}/>
                    </Grid.Column>
                </Grid.Row>
            </Grid>
        )
    }

    renderSearchEmprendedorOComprador()
    {
        if(this.filtro === 'Ventas')
        {
            return(
                <Grid>
                <Grid.Row>
                    <Grid.Column width={16}>
                        <h4>Usuario del comprador</h4>
                        <Input fluid circular size='mini' name='comprador_nombre' onChange={this.handleChange} id={0}/>
                        </Grid.Column>   
                    </Grid.Row>
                </Grid>
            )
        } else {
            return(
                <Grid>
                <Grid.Row>
                    <Grid.Column width={16}>
                        <h4>Apellido del vendedor</h4>
                        <Input fluid circular size='mini' name='emprendedor_apellido' onChange={this.handleChange} id={0}/>
                        </Grid.Column>   
                    </Grid.Row>
                </Grid>
            )
        }
         
    }

    limpiarFiltros()
    {
        (this.filtro === 'Ventas') ? this.props.getVentas(this.state.paginaActual) : this.props.getVentasCliente(this.state.paginaActual);   
        this.setState({
            limpiarFiltro: false
        })
    }
}

const mapStateToProps = state => ({
    
  });

export default withRouter(connect(mapStateToProps, { getVentas, getVentasCliente })(FiltroVentas));