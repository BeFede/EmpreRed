import React, { Component } from 'react';
import '../App.css';

import { Button, Header, Icon, Image, Menu, Segment, Sidebar, Input } from 'semantic-ui-react'

import Logo from '../assets/EmpreRed2.png';
import LogoEmpreRed from '../assets/EmpreRedLogoE.png'

class Footer extends Component{


render(){

return (
  <footer className='footer'>
  <div class="container-footer-all">

            <div className="container-body">
            <div className="colum2">
            <p style={{color:'white'}}>EmpreRed © 2019</p>
            </div>
            <div className="colum3">
                    <center>
                      <Image src= {LogoEmpreRed} width='50' height='50'/>
                      <p style={{color:'white'}}>EmpreRed © 2019</p>
                </center>
            </div>
            <div className="colum1">
                     <div className="divColumn1">
                          <Button circular color='contact' icon='mail' href="mailto:emprenderproject@gmail.com"/>
                          <Button circular color='twitter' icon='telegram' href="https://t.me/EmpreRedProect"/>
                          <Button circular color='twitter' icon='twitter' href="https://www.twitter.com/EmpreRedProject"/>
                      </div>
            </div>
            </div>
        </div>
  </footer>

);
}
}
export default Footer;
