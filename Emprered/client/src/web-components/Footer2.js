import React, { Component } from 'react';
import  '../styles/footer2.css'
import { Button, Header, Icon, Image, Menu, Segment, Sidebar, Input } from 'semantic-ui-react'
import Logo from '../assets/EmpreRed2.png';
class Footer extends Component{


render(){

return (
  <footer className='footer2'>
  <div class="container-footer-all">

            <div className="container-body">
            <div className="colum2">
                <br/>
                <br/>
            </div>
            <div className="colum3">
                    <center>
                      <Image src= {Logo} width='100' height='100'/>
                </center>
            </div>
            <div className="colum1">
                    <div>
                     <div className="divColumn1">
                     <br/>
                          <h1>
                          <a className="email" href="mailto:emprenderproject@gmail.com">  <Button circular color='contact' icon='mail' />   -    </a>
                          <a className="telegram" href="https://t.me/EmpreRedProect"> <Button circular color='twitter' icon='telegram' />   -    </a>
                          <Button circular color='twitter' icon='twitter' /> <a className="twitter" href="https://www.twitter.com/EmpreRedProject">       </a>
                          </h1>
                      </div>
                    </div>
            </div>
            </div>
        </div>
  </footer>

);
}
}
export default Footer;
