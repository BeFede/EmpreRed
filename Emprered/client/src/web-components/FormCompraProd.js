import React, { Component } from 'react';
import { Form, Button, Message, Container } from 'semantic-ui-react';
import { connect } from 'react-redux';
import { closeModal } from '../actions/modalActions';
import { insertItemBisexual } from '../actions/tableFillActions';

class FormCompraMP extends Component {

    constructor(props) {
        super(props);

        this.data = {tipo: "producto", id: null, producto: null, cantidad: null, costo: null, unidadMedida: null};
        this.data.producto = (props.data.nombre) ? props.data.nombre : null;
        this.data.id = (props.data.id) ? props.data.id : null;
        this.data.unidadMedida = (props.data.unidad_medida) ? props.data.unidad_medida : null;

    }

    handleChange = (e, {name, value}) => {
         this.data = {
            ...this.data,
            [name]: (value) ? value : null
         }
    };

    handleSubmit = () => {
        this.props.insertItemBisexual(this.data);
        this.props.closeModal();
    }

    render() {
        return (
        <Container style={{padding: '20px',  marginBottom:'30px'}}>
        <Form onSubmit={this.handleSubmit}>
            <Form.Group widths='equal'>
            <Form.Input type="number" autoComplete='off' label={`Cantidad comprada (medida en ${this.data.unidadMedida})`} name='cantidad' onChange={this.handleChange} required />
            <Form.Input icon='dollar sign' iconPosition='left' type="number" min="0.00" step="0.001"  autoComplete='off' label='Costo individual' name='costo' onChange={this.handleChange} required/>
            
            </Form.Group>
            <Message success header='Compra registrada' content="El stock se actualizó correctamente" />
            <Button positive  icon='check' content="Confirmar" floated='right'></Button>
            {/* <Button negative  icon='cancel' content="Cancelar" floated='right' onClick={this.props.closeModal}></Button> */}

        </Form>

        </Container>
        )
     }
}

const mapStateToProps = state => ({
    itemsTable: state.tableData.items
});

export default connect(mapStateToProps, { insertItemBisexual, closeModal })(FormCompraMP);
