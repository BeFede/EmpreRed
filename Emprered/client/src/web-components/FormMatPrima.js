import React, { Component } from 'react';
import { Form, TextArea, Button, Message, Container, Input } from 'semantic-ui-react';
import { connect } from 'react-redux';
import { putMateriaPrimaEmp, getTodasMPEmp } from '../actions/matPrimaEmpActions';
import { closeModal } from '../actions/modalActions';

class FormMatPrima extends Component {

    constructor(props) {
        super(props);

        this.data = (props.data) ? props.data : null;

    }

    componentWillMount(){
        // Ya estan las props cargadas en el store por el ProductosConsulta
        
        this.unidades = this.props.unidadesMedida;
        this.disabledStock(this.props.data)
        
        this.optionsUnidades = [];
    }

    state ={ stockDisabled: true }

    disabledStock(obj)
    {
        if(obj==null)
        {
        this.setState(
            {
                stockDisabled: !this.state.stockDisabled
            }
        )
    }
    }
    handleChange = (e, {name, value, checked}) => {
         this.data = {
            ...this.data,
            [name]: (value) ? value : checked
         }
    };

    handleSubmit = () => {
      this.props.putMateriaPrimaEmp(this.data);
      this.props.closeModal();
    }

    getOptions() {
        
        for (const unid of this.unidades) {
            const opt = {key: unid.id, text: unid.descripcion, value: unid.id}
            this.optionsUnidades.push(opt);
        }
    }

    render() {

        if (!this.data){
          this.data = {
            
          }
        }

        this.getOptions();

        return (
        <Container style={{padding: '20px',  marginBottom:'30px'}}>
        <Form onSubmit={this.handleSubmit}>
            <Form.Input autoComplete='off' label='Nombre' name='nombre' defaultValue={this.data.nombre} onChange={this.handleChange}/>
            <Form.Group widths='equal'>
                <Form.Select fluid label='Unidad de Medida' name='unidad_medida_id' defaultValue={this.data.unidad_medida_id} options={this.optionsUnidades} onChange={this.handleChange} />
                
            </Form.Group>
            <Form.Group widths='equal'>
              <Form.Field >
                <label>Stock Mínimo</label>
                <Input  type="number"  step="any" min={0}  name='stock_minimo' defaultValue={this.data.stock_minimo} onChange={this.handleChange} />
              </Form.Field>
            </Form.Group>
            
            <Form.Field
                id='form-textarea-observaciones'
                name='observaciones'
                control={TextArea}
                defaultValue={this.data.observaciones}
                label='Observaciones'
                onChange={this.handleChange}
            />

            <Message success header='Edición Completada' content="Se editó la materia prima seleccionada" />
            <Button positive  icon='check' content="Confirmar" floated='right'></Button>
        </Form>
        <Button negative  icon='cancel' content="Cancelar" floated='right' onClick={this.props.closeModal}></Button>
        </Container>
        )
     }
}

const mapStateToProps = state => ({
    mensajeRetorno: state.mpsEmp.messagePutMateriaPrima,    
    unidadesMedida: state.tipificaciones.unidadesMedida
  });

export default connect(mapStateToProps, { putMateriaPrimaEmp, getTodasMPEmp, closeModal })(FormMatPrima);
