import React, { Component } from 'react';
import { Form, TextArea, Button, Container, FormGroup, Modal, Header, Icon } from 'semantic-ui-react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { postMovimientos} from '../actions/stockActions';
import { closeModal } from '../actions/modalActions';
import { ModalManager } from './ModalManager';

class FormMov extends Component {

    constructor(props) {
        super(props);
        this.data = props.data ? props.data : null; 
    }

    componentWillMount() {
        this.state = {messageOpen: false, messageInfo: null};
    }

    handleChange = (e, {name, value}) => {
        this.data = {
           ...this.data,
           [name]: (value) ? value : null
        }
   };

   handleConfirm = () => {
    this.setState({messageOpen: false});
    this.props.closeModal();
    this.props.history.push('/mismovimientos'); 
   }

   handleSubmit = () => {
       this.props.postMovimientos(this.data)
       .then(data => {
           this.renderMensaje(data);
        }, err => {
            console.log(err);
        });    
   }

   renderMensaje = (data) => {
        let mensaje = '';
        if(data.estado === 'OK') {
            mensaje = 'Se cargó el movimiento de stock correctamente';
        } else {
            console.log(data);
            mensaje = data.mensaje;
        }
        this.setState({messageInfo: mensaje, messageOpen: true});
    }

   render() {
       return (
       <Container style={{padding: '20px',  marginBottom:'30px'}}>
            <Form onSubmit={this.handleSubmit}>
                <Form.Group widths='equal'>
                <Form.Input type="number" autoComplete='off' label={`Cantidad (medida en ${this.data.unidad_medida})`} name='cantidad' onChange={this.handleChange} required />
                </Form.Group>
                <b>Motivo:</b>
                <br/>
                <br/>
                <FormGroup header='Motivo'>
                <TextArea name='motivo' label='Motivo' onChange={this.handleChange} required/>
                </FormGroup>
                <Button positive  icon='check' content="Confirmar" floated='right'></Button>
            </Form>
            <ModalManager/>
            <Modal
                open={this.state.messageOpen}
                onClose={this.handleConfirm}
                size='small'
            >
                <Header icon='browser' content='Aviso' />
                <Modal.Content>
                <h3>{this.state.messageInfo}</h3>
                </Modal.Content>
                <Modal.Actions>
                <Button color='green' onClick={this.handleConfirm} inverted> 
                    <Icon name='checkmark' /> Aceptar
                </Button>
                </Modal.Actions>
            </Modal>
       </Container>
       )
    }
}

const mapStateToProps = state => ({
   itemsTable: state.tableData.items
});

export default withRouter(connect(mapStateToProps, { closeModal, postMovimientos })(FormMov));