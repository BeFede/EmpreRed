import React, { Component } from 'react';
import { Form, TextArea, Button, Message, Container, Input } from 'semantic-ui-react';
import { connect } from 'react-redux';
import { putProducto, getProductos } from '../actions/productosActions';
import { getSubcategoriasDeCategoria } from '../actions/tipificacionesActions';
import { closeModal } from '../actions/modalActions';
import ReactTooltip from 'react-tooltip'
import { urlMedia } from '../config/configs';

class FormProducto extends Component {

    constructor(props) {
        super(props);
        this.server = urlMedia;
        this.handleChange = this.handleChange.bind(this);
        this.optionsCategorias = [];
        this.optionsSubcategorias = [];
        this.optionsUnidades = [];
        this.fotuli = null; // acá se guarda el file de la foto
        this.data = (props.data) ? props.data : null;
        this.data = (this.data) ?
        {...this.data,
          precio:0
        } : null;
        if(props.data) {this.obtenerSubcategoriasDefault();}
        this.state = { cambioCategoria: false};
    }

    componentWillMount(){
        // Ya estan las props cargadas en el store por el ProductosConsulta
        this.categoriasProducto = this.props.categoriasProducto;
        this.unidades = this.props.unidadesMedida;  
    }

    handleChange = (e, {name, value, checked}) => {
        this.data.precio = 0;
        this.data.utilidad = 0;
        this.data.costo = 0;

        if (name == 'foto'){
            // La única forma que encontré para hacerlo andar *shrug*
            this.fotuli = e.target.files[0];
        }

        if (name == 'id_categoria'){
          // Si elige una categoria filtro sus subcategorias, limpiando previamente
          this.props.getSubcategoriasDeCategoria(value)
          .then(data => {
            this.optionsSubcategorias= [];
            for (const subcat of data) {
              const opt = {key: subcat.id, text: subcat.descripcion, value: subcat.id}
              this.optionsSubcategorias.push(opt);
            }
            this.setState({ cambioCategoria: !this.state.cambioCategoria});
          }, err => {
            console.log(err);
          })
        }

        this.data = {
            ...this.data,
            [name]: (value) ? value : checked
        }

    };


    //Copiadisimo
     doEvent( obj, event ) {
      var event = new Event( event, {target: obj, bubbles: true} );
      return obj ? obj.dispatchEvent(event) : false;
    }

    cancelar (){
      this.props.closeModal()
    }

    obtenerSubcategoriasDefault() {
      // Obtengo las subcategorias para llenar el combo y dejar seleccionada la que tenía el producto

      this.props.getSubcategoriasDeCategoria(this.data.id_categoria)
          .then(data => {
            this.optionsSubcategorias= [];
            for (const subcat of data) {
              const opt = {key: subcat.id, text: subcat.descripcion, value: subcat.id}
              this.optionsSubcategorias.push(opt);
            }
            this.setState({ cambioCategoria: !this.state.cambioCategoria});
          }, err => {
            console.log(err);
          })
    }

    handleSubmit = () => {
        // Guardamos el file en this.data.foto (de otro modo, react sólo guarda
        // el fakepath, y no el file propiamente dicho)
        this.data.foto = this.fotuli;
        this.props.putProducto(this.data);
        this.props.closeModal()
    }

    getOptions() {
        this.optionsCategorias = [];
        this.optionsUnidades = [];
        for (const cat of this.categoriasProducto) {
            const opt = {key: cat.id, text: cat.descripcion, value: cat.id}
            this.optionsCategorias.push(opt);
        }

        for (const unid of this.unidades) {
            const opt = {key: unid.id, text: unid.descripcion, value: unid.id}
            this.optionsUnidades.push(opt);
        }
    }

    render() {


        if (!this.data){
          this.data = {
            es_fabricado: false,
            foto: ""
          }
        }


        this.getOptions();

        return (
        <Container style={{padding: '20px', marginBottom:'30px'}}>
        <Form onSubmit={this.handleSubmit}>
              <Form.Input autoComplete='off' label='Nombre' name='nombre' defaultValue={this.data.nombre} onChange={this.handleChange}/>
              <Form.Group width='equal'>
                <Form.Field>
                  <label>Stock Mínimo</label>
                  <Input type="number"  step="any" min={0} name='stock_minimo' defaultValue={this.data.stock_minimo} onChange={this.handleChange}/>
                </Form.Field>
                <Form.Select fluid label='Unidad de Medida' placeholder='Unidad de Medida' name='unidad_medida_id' defaultValue={this.data.unidad_medida_id} options={this.optionsUnidades} onChange={this.handleChange} />
              </Form.Group>
              <Form.Group widths='equal'>
                <Form.Select fluid label='Categoría' placeholder='Categoría' name='id_categoria' defaultValue={this.data.id_categoria} options={this.optionsCategorias} onChange={this.handleChange} />
                <Form.Select fluid label='Subcategoría' placeholder='Subcategoría' name='id_subcategoria' defaultValue={this.data.id_subcategoria} options={this.optionsSubcategorias} onChange={this.handleChange} />
              </Form.Group>
              <Form.Group widths='equal'>
                <Form.Field width={8}>
                  <label>Cantidad Mayorista</label>
                  <Input type="number" min={0} name='cantidad_mayorista' defaultValue={this.data.cantidad_mayorista} onChange={this.handleChange}/>
                </Form.Field>
                <Form.Field with={8}>
                <a data-tip data-for='foto_tip'>Foto (actual: {this.data.foto.split('\/')[1] || "ninguna"})</a>
                <ReactTooltip id='foto_tip' place="top" type="dark" effect="solid">
                <img src={ this.server + this.data.foto } height="200"></img>
                </ReactTooltip>
                  <Input type="file" id="foto" name='foto' onChange={this.handleChange}/>
                </Form.Field>
              </Form.Group>

                <Form.Checkbox label='Es fabricado' name='es_fabricado' defaultChecked={this.data.es_fabricado} onChange={this.handleChange}/>
                <Form.Field
                    id='form-textarea-observaciones'
                    name='observaciones'
                    control={TextArea}
                    defaultValue={this.data.observaciones}
                    label='Observaciones'
                    onChange={this.handleChange}
                />


            <Message success header='Edición Completada' content="Se editó el producto seleccionado" />
            <Button positive  icon='check' content="Confirmar" floated='right'></Button>
          </Form>
          <Button negative  icon='cancel' content="Cancelar" floated='right' onClick={  this.props.closeModal}></Button>

        </Container>
        )
     }
}

const mapStateToProps = state => ({
    mensajeRetorno: state.productos.messagePutMateriaPrima,
    categoriasProducto: state.tipificaciones.categoriasProducto,
    subcategorias: state.tipificaciones.subcategorias,
    unidadesMedida: state.tipificaciones.unidadesMedida
  });

export default connect(mapStateToProps, { putProducto, getProductos, closeModal, getSubcategoriasDeCategoria })(FormProducto);
