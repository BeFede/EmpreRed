import React, { Component } from 'react';
import { Form, Button, Container, Modal, Header, Icon} from 'semantic-ui-react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { closeModal } from '../actions/modalActions';
import { registrarUsuario } from '../actions/usuarioActions';

class FormUser extends Component {

  constructor(props) {
      super(props);
      this.data = (props.data) ? props.data : {
        password:'',
        password_repetida: ''
      };
      this.state = { messageOpen: false, messageInfo: null, estadoResponse: null, equalPass: null }
      
  }

  handleConfirm = () => {
    this.setState({messageOpen: false});

    // Si el usuario fue registrado redirecciono
    
    if(this.state.estadoResponse === 'OK') {
      this.props.closeModal();
      this.props.history.push('/Ingreso');
    } 
  }


  handleChange = (e, {name, value, checked}) => {
       this.data = {
          ...this.data,
          [name]: (value) ? value : checked
       }
       this.comparePass();
  };

  comparePass(){

    if(this.data.password != '')
    {
        if( this.data.password === this.data.password_repetida)
        {
            this.setState({
                equalPass: true
            })
            return;
        }   
        this.setState({
            equalPass: false
        })
    }
    else
    {
        this.setState({
            equalPass: null
        })
    }        
}

  handleSubmit = () => {

    // Aca invoco a la promise y agarro lo que resuelva, sea que haya entrado por then o catch en la action
    this.props.registrarUsuario(this.data)
    .then((data) => {
        if(data.estado === 'OK') {
          const stringMensaje = '. Accede a tu email para verificar tu cuenta';
          this.renderMensaje(data, stringMensaje);
        } else {
          this.renderMensaje(data);
        }
    }, err => {
      console.log(err);
    });
       
  }

  renderMensaje = (info, mensajeAdicional) => {
    this.setState({messageInfo: info.mensaje + mensajeAdicional, messageOpen: true, estadoResponse:info.estado});
  }

    render() {

        return (
        <Container style={{padding: '30px', marginBottom:'30px'}}>
        <Form onSubmit={this.handleSubmit}>
            <Form.Input  name='email' label='Email' placeholder='Email' onChange={this.handleChange} required/>
            <Form.Input  name='username' label='Nombre de usuario' placeholder='Nombre de usuario' onChange={this.handleChange} required/>
            <Form.Input  icon={<Icon name={this.data.password != '' ? (this.data.password.length >= 8 ? 'check circle' : 'times circle') : null } color={this.data.password!='' ? (this.data.password.length >= 8 ? 'green' : 'red') : null }/>} name='password' type='password' label='Contraseña' placeholder='Contraseña' onChange={this.handleChange} required/>
            <Form.Input  icon={<Icon name={this.state.equalPass ? 'check circle' : (this.state.equalPass!=null ? 'times circle' : null) } color={this.state.equalPass ? 'green' : (this.state.equalPass!=null ? 'red' : null) } />} name='password_repetida' type='password' label='Repetir contraseña' placeholder='Contraseña' onChange={this.handleChange} required/>
            <center>
            <Button positive disabled={!this.state.equalPass || this.data.password < 8} icon='check' content="Registrar" floated='right'></Button>
            </center>
          </Form>
          <Button negative  icon='cancel' content="Cancelar" floated='right' onClick={  this.props.closeModal}></Button>


          {/* Este es el modal para el mensaje */}
          <Modal
            open={this.state.messageOpen}
            onClose={this.handleConfirm}
            size='small'
          >
            <Header icon='browser' content='Aviso' />
            <Modal.Content>
              <h3>{this.state.messageInfo}</h3>
            </Modal.Content>
            <Modal.Actions>
              <Button color='green' disabled onClick={this.handleConfirm} inverted>
                <Icon name='checkmark' /> Aceptar
              </Button>
            </Modal.Actions>
          </Modal>
        </Container>
        )
     }

}
const mapStateToProps = state => ({
  response: state.usuarios.mensaje
});

export default withRouter(connect(mapStateToProps,{closeModal, registrarUsuario})(FormUser));
