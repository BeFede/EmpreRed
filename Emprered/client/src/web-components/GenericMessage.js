import React, { Component } from 'react';
import { Button, Modal, Header, Icon} from 'semantic-ui-react';

class GenericMessage extends Component {

    constructor(props) {
        super(props);
        this.state = { messageOpen: false, messageInfo: null, estadoResponse: null, redirect: null }
        console.log('creo el modal');
    }

    componentDidMount() {
        this.setState(
            {
                messageOpen: this.props.open, 
                messageInfo: this.props.info,
                estadoResponse: this.props.estado,
                redirect: this.props.redirect    
            }
        )
        console.log(this.state);
    }

    handleConfirm = () => {
        this.setState({messageOpen: false});
    
        // Si el usuario fue registrado redirecciono
        
        if(this.state.estadoResponse === 'OK') {
          this.props.history.push(this.state.redirect);
        } 
    }

    render() {

        return (
        <Modal
                open={this.state.messageOpen}
                onClose={this.handleConfirm}
                size='small'
            >
                <Header icon='browser' content='Aviso' />
                <Modal.Content>
                <h3>{this.state.messageInfo}</h3>
                </Modal.Content>
                <Modal.Actions>
                <Button color='green' onClick={this.handleConfirm} inverted>
                    <Icon name='checkmark' /> Aceptar
                </Button>
                </Modal.Actions>
            </Modal>
        )
     }
}

export default GenericMessage;