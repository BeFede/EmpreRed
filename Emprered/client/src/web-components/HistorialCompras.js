import React, { Component } from 'react'
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import ModalManager from './ModalManager';
import { openModal } from '../actions/modalActions';
import { Button, Header, Icon, Segment, Grid, Divider } from 'semantic-ui-react'
import { Table } from 'semantic-ui-react';
import MenuPrincipal from './MenuPrincipal';
import MenuEmprendedor from './MenuEmprendedor';
import DetalleCompra from './DetalleCompra';
import { getUser } from '../config/configs';
import { getCompras, getCompra } from '../actions/compraActions';
import { getProveedores } from '../actions/proveedoresActions';
import FiltrosProducciones from './FiltroProducciones.js';

class MisCompras extends Component {

  componentWillMount()
  {
    this.userLogeado = (getUser()) ? getUser() : null;
    this.itemsCompraRendered = null;
    this.itemsHistorialRendered = null;

    this.state ={
      cargoCompras: false,
      cargoProveedores: false,
      setFiltro: false
    }
    
    // Llamo a esto para llenar el combo del filtro
    this.props.getProveedores()
    .then(data => {
      this.setState({cargoProveedores:true});
    },err => {
      console.log(err);
    });

    this.props.getCompras()
    .then(data => {
      this.setState({cargoCompras:true});
    },err => {
      console.log(err);
    });
    
    this.ordenFiltros = { fechaAsc: false, idAsc: true };

  }

  obtenerHistorial() {

    let vectorFinalCompras = [];
      for (let compra of this.props.compras.compras) {
        const date = new Date(compra.fecha_hora);
        const dateString= date.toLocaleString();
        const compraConFechaFormateada = {...compra, fecha_hora: dateString};
        vectorFinalCompras.push(compraConFechaFormateada);
      }
      this.itemsHistorialRendered = vectorFinalCompras.map(compra => {
        return this.renderItemHistorial(compra);
      })
     }

  render() {

    if(this.state.cargoCompras && this.state.cargoProveedores)
    {
      this.obtenerHistorial();
    }
    return(
    <div className="DivPrincipal" style={{overflow:'auto'}}>
        <div id="MenuTop">
          <MenuPrincipal/>
        </div>
        <div className = "row">
          <div id="MenuLateral">
            {(this.userLogeado) ? <MenuEmprendedor/> : null}
           </div>
          <div style={{"paddingTop":"61px", marginLeft:"210px"}}>
                    <Grid centered>
                      <Grid.Row>
                        <Grid.Column width={14}>
                          <Grid>
                            <Grid.Row centered>
                              <Grid.Column width={16}>
                                <center>
                                  <h2>
                                      Historial de Compras
                                  </h2>
                                </center>
                              </Grid.Column>
                            </Grid.Row>

                            <Divider/>

                            <Grid.Row>
                              <Grid.Column width={16}>
                                <Segment loading={!this.state.cargoCompras && !this.state.cargoProveedores}>
                                  <Grid centered>
                                    <Grid.Row>
                                      <Grid.Column width={16} center>
                                        {
                                          (this.state.cargoProveedores)
                                          ?
                                          <div>
                                            <FiltrosProducciones filtro='Compras'/>
                                            <Divider/>
                                          </div>
                                          :
                                          null
                                        }
                                      </Grid.Column>
                                    </Grid.Row>
                                  </Grid>
                                  <Grid centered>
                                    <Grid.Row>
                                      <Grid.Column width={16}>
                                        {this.renderHistorial()}
                                      </Grid.Column>
                                    </Grid.Row>
                                  </Grid>
                                </Segment>
                              </Grid.Column>
                            </Grid.Row>
                          </Grid>
                        </Grid.Column>
                      </Grid.Row>
                    </Grid>

                    
                    

                    

          </div>
        </div>
        <ModalManager />
    </div>
    )
  }

  renderItemHistorial(compra)
  {
      return(
        <Table.Row  key={compra.id}>
        <Table.Cell>
          <Header as='h4'>
            <Header.Content>
              {compra.id}
            </Header.Content>
          </Header>
        </Table.Cell>
        <Table.Cell textAlign='center'>{compra.fecha_hora}</Table.Cell>
        <Table.Cell textAlign='center'>${compra.monto}</Table.Cell>
        <Table.Cell textAlign='center'>{compra.proveedor}</Table.Cell>
        <Table.Cell textAlign='center'>{compra.comprobante}</Table.Cell>

        <Table.Cell textAlign='center' content>
          <Button color="orange" circular icon="eye"
            onClick={() => this.props.openModal({
                          header: "Detalle de Compra",
                          content: <DetalleCompra id={compra.id}/>
            })}
                  />
        </Table.Cell>
      </Table.Row>
      )

  }

  ordenar(atributo){
    switch(atributo){
      case 'id':
        if(this.ordenFiltros.idAsc){
          this.props.compras.sort(function(a, b){
            return a.id - b.id;
            });
          } else {
            this.props.compras.sort(function(a, b){
            return b.id - a.id;
           })
         }
        this.ordenFiltros.idAsc = !this.ordenFiltros.idAsc;
        this.setState({ setFiltro: !this.state.setFiltro })
        break;
      case 'fecha':
        if(this.ordenFiltros.fechaAsc) {
          this.props.compras.compras.sort(function(a, b){
            return b.fecha_hora.localeCompare(a.fecha_hora);
          });
        }
        else {
          this.props.compras.compras.sort(function(a, b){
            return a.fecha_hora.localeCompare(b.fecha_hora);
          })
        }
        this.ordenFiltros.fechaAsc = !this.ordenFiltros.fechaAsc;
        this.setState({setFiltro: !this.state.setFiltro});
        break;
    }
  }

  renderHistorial()
  {
    return(
      <div>
        <div style={{overflow: 'auto', maxHeight: 500 }}>
          <Table singleLine striped color="orange">
            <Table.Header>
              <Table.Row>
                  <Table.HeaderCell>ID<Icon link name='sort' onClick={() => this.ordenar('fecha')}></Icon></Table.HeaderCell>
                  <Table.HeaderCell textAlign='center'>Fecha<Icon link name='sort' onClick={() => this.ordenar('fecha')}></Icon></Table.HeaderCell>
                  <Table.HeaderCell textAlign='center'>Monto total</Table.HeaderCell>
                  <Table.HeaderCell textAlign='center'>Proveedor</Table.HeaderCell>
                  <Table.HeaderCell textAlign='center'>Número de Comprobante</Table.HeaderCell>
                  <Table.HeaderCell textAlign='center'>Acciones</Table.HeaderCell>
              </Table.Row>
            </Table.Header>
            <Table.Body>
                {this.itemsHistorialRendered}
            </Table.Body>
          </Table>
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  itemsTable: state.tableData.items,
  compras: state.compras.resultCompras
});

export default withRouter(connect(mapStateToProps, { openModal , getCompra, getCompras, getProveedores })(MisCompras));
