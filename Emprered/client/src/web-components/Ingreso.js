import React, { Component } from 'react';
import '../App.css';
import { Button, Header, Icon, Segment, Input, Modal, Grid, Image, Container, Divider, Form} from 'semantic-ui-react'
import { connect } from 'react-redux';
import MenuPrincipal from './MenuPrincipal';
import {login, loginGoogle} from '../actions/sesionActions';
import { GoogleLogin } from 'react-google-login';
import LogoE from '../assets/EmpreRedLogoE.png'
import {getUser} from '../config/configs';
import '../styles/Ingreso.css';
import Emp2 from '../assets/EmpreRedRectangular.png'
import Footer from './Footer'

class Ingreso extends Component {

  constructor(props) {
    super(props);
    this.data = (props.data) ? props.data : null;
    this.login = this.login.bind(this);
    this.responseSuccessLoginGoogle = this.responseSuccessLoginGoogle.bind(this);
    this.state = {messageInfo: null, messageOpen: null}

    // Puede venir redireccionado de Publicacion
    this.publicacionARedireccionar = (this.props.location.state) ? this.props.location.state.redireccionaDePublicacion : null;
  }

  componentWillMount() {
    //this.props.obtenerSesion()
    window.addEventListener("keypress",this.onKeyPress,false);
  }

  handleChange = (e, {name, value}) => {
    this.data = {
       ...this.data,
       [name]: (value) ? value : null
    }
  };

  handleMessageConfirm = () => {
    this.setState({messageOpen: false});
  }

  renderMensaje = (mensaje) => {
    this.setState({messageInfo: mensaje, messageOpen: true});
  }

  login() {
    if(this.data!=null)
    {
    this.props.login(this.data.username, this.data.password, 'emprered')
    .then((data) => {
      const user = getUser();
      
      if(!user) {
        this.renderMensaje(data.mensaje)
      }

      else if (user.es_activo){
        if(this.publicacionARedireccionar) {
          this.props.history.push(
            {
              pathname: '/publicacion',
              state: {idDetalle: this.publicacionARedireccionar}
            }
          )
        } else {
            this.props.history.push('/');
        }       
      }
      else {
        this.props.history.push('/entry');
      }
    }, err => {
      console.log(err);
    });
  }
  }

  responseSuccessLoginGoogle = (response) => {

    this.props.loginGoogle(response.tokenId, 'google')
    .then((data) => {
      const user = getUser();

      if (user.es_activo){
        this.props.history.push('/');
      }
      else {
        this.props.history.push('/entry');
      }
    }, err => {
      console.log(err);
    });

  }

  responseFailureLoginGoogle = (response) => {
    console.log(response);
  }

  onKeyPress = (e) => {
    if(e.key === 'Enter'){
        this.login();
    }
  }

  mostrarPassword(){
    (document.getElementById("password").type === "password") ? document.getElementById("password").type = "text" : document.getElementById("password").type = "password";
  }

  render() {

    return (
      <body>
    
      <div className="fondo">
      <div className="Ingreso">
        <MenuPrincipal/>
      
       
      <center>
        
          <Container>
            <Grid>
          
              <Grid.Row columns={2}>
                <Grid.Column>
  
                <Image src={LogoE} className="logoE"></Image>
  
            </Grid.Column>
  
                <Grid.Column>
              
                  <Segment basic>
                  
                    <Segment raised large>
                        <Header as='h2' >Ingresá</Header>
                          <Form>
                            <Form.Field>
                              <Input icon="user" name ="username" iconPosition='left' placeholder='Usuario' onChange={this.handleChange}/>  
                            </Form.Field>
                            <Form.Field>       
                              <Input icon= {{name:"eye slash outline", link: true, circular:true, onClick:()=>this.mostrarPassword()}} id="password" name="password" type='password' placeholder='Contraseña' onChange={this.handleChange}/>
                            </Form.Field>
                            <center>
                            <Form.Field>
                              <Button onClick={this.login} style={{"width": "100%",
                                  "border-radius": "2px","border": "1px solid transparent","font-size": "16px", "font-weight": "bold","font-family": "Roboto"}}
                                  color='green'>Ingresá</Button>
                            </Form.Field>    
                            <Form.Field>
                              <Divider></Divider>
                              
                              <GoogleLogin 
                                  type="light"
                                  buttonText="Ingresar con Google"
                                  style={ {'width':'100%' , 'background-color':'#DD4B39 ', 'height': '45px' ,'color':'white', "font-weight": "bold"}}                                
                                  clientId="216986984118-creegjhvfh6p3rpsm38t1qao8306jera.apps.googleusercontent.com"                                  
                                  onSuccess={this.responseSuccessLoginGoogle}  
                                  onFailure={this.responseFailureLoginGoogle} 

                              />

                            </Form.Field>   
                          </center>
                          </Form>
                    </Segment>
                  </Segment>
            
                </Grid.Column>
                </Grid.Row>
            </Grid>
          </Container>
        
      </center>
        
  
  
  
        
        {/* Este es el modal para el mensaje */}
        <Modal
            open={this.state.messageOpen}
            onClose={this.handleMessageConfirm}
            size='small'
        >
            <Header icon='browser' content='Aviso' />
            <Modal.Content>
            <h3>{this.state.messageInfo}</h3>
            </Modal.Content>
            <Modal.Actions>
            <Button color='green' onClick={this.handleMessageConfirm} inverted>
                <Icon name='checkmark' /> Aceptar
            </Button>
            </Modal.Actions>
        </Modal>
  
        
      </div>
      
      </div>
      <div className="Ingreso2">
      <Footer/>
      </div>
      </body>
    );
  }
}



const mapStateToProps = state => ({
  sesion: state.sesion.sesion
});

export default connect(mapStateToProps, { login, loginGoogle })(Ingreso);
