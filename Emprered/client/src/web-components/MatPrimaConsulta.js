import React, { Component } from 'react'
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { getTodasMPEmp, delMateriaPrimaEmp } from '../actions/matPrimaEmpActions';
import ModalManager from './ModalManager';
import { openModal } from '../actions/modalActions';
import FormMatPrima from './FormMatPrima';
import { Button, Header, Icon, Segment, Confirm, Grid, Divider, Input, Dropdown, Radio, Container, Accordion} from 'semantic-ui-react'
import {Table} from 'semantic-ui-react';
import { getAlicuotas, getUnidadesMedida } from '../actions/tipificacionesActions';
import MenuPrincipal from './MenuPrincipal';
import MenuEmprendedor from './MenuEmprendedor';
import {getUser} from '../config/configs';
import FiltrosMPyProd from './FiltroMPyProd.js';
import '../styles/MatPrimaConsulta.css'
import '../styles/modal.css'


class MatPrimaConsulta extends Component {

  componentWillMount() {
    this.props.getUnidadesMedida();
    this.props.getTodasMPEmp();
    this.props.getAlicuotas();
    this.userLogeado = (getUser()) ? getUser() : null;
    this.optionsUnidades = [];
    this.ordenFiltros = { stockAsc: true, costoAsc: true, nombreAsc: true }
  }

  state = { open: false, idABorrar: null, showfilter: false, setFiltro: true};

  showConfirmDialog = (id) => this.setState({open: true, idABorrar: id})
  handleCancel = () => this.setState({open: false})

  handleConfirm = () => {
    this.setState({open: false});
    this.deleteMateriaPrima(this.state.idABorrar);
  }

  deleteMateriaPrima(id) {
    this.props.delMateriaPrimaEmp(this.props.mpsEmp, id);
  }

  renderItem(mp)
    {
      return(
        <Table.Row  key={mp.nombre}>
        <Table.Cell>
          <Header as='h4'>

            <Header.Content>
              {mp.nombre}
            </Header.Content>
          </Header>
        </Table.Cell>
        <Table.Cell textAlign='center'>{(mp.stock_actual <= 0) ? 'No cargado' : mp.stock_actual}</Table.Cell>
        <Table.Cell textAlign='center'>{(mp.costo <= 0) ? 'No cargado' : mp.costo}</Table.Cell>
        <Table.Cell textAlign='center'>{mp.unidad_medida}</Table.Cell>
        <Table.Cell textAlign='right' content>
        <center>
                <Button primary circular icon="edit"
                  onClick={() => this.props.openModal({
                    header: "Editar Materia prima",
                    content: <FormMatPrima data={mp}/>
                  })}
                />
                <Button negative circular icon="trash"
                  onClick = {() => this.showConfirmDialog(mp.id)}
                />
        </center>
        </Table.Cell>
      </Table.Row>
      )
    }

  handleButtonClick = () => this.setState({ visible: !this.state.visible });;

  getOptions() {
  if(this.props.unidadesMedida)
  {        
    this.optionsUnidades = [];
    for (const unid of this.props.unidadesMedida) {
        const opt = {key: unid.id, text: unid.descripcion, value: unid.id}
        this.optionsUnidades.push(opt);
    }
  }
}    

  render() {

    const itemsRendered = this.props.mpsEmp.map(mp => (
      this.renderItem(mp)
    ));

    this.getOptions();    

    return(

    <div className="DivPrincipal" style={{overflow:'auto'}}>
        <div id="MenuTop">
          <MenuPrincipal/>
        </div>
        <div className = "row">
          <div id="MenuLateral">
            {(this.userLogeado) ? <MenuEmprendedor/> : null}
          </div>
          <div style={{"paddingTop":"61px", marginLeft:"210px"}}>

            <Segment basic>

            <Grid>
            <Grid.Column width='4' floated='left'>
            <Header as='h2'><Icon name='list' size='tiny'/>Materias Primas</Header>
            </Grid.Column>

                  <br/>
                  <ModalManager />
                  <Grid.Column width='4' floated='right'>
                  <div align='right'>
                    <Button style={{marginBottom:'1px'}} color='green'
                      icon labelPosition='left'
                      onClick={() => this.props.openModal({
                      header: "Nueva Materia Prima",
                      content: <FormMatPrima/>
                      })}>
                      <Icon name='add circle' />
                      Registrar nueva materia prima
                    </Button>
                  </div>
            </Grid.Column>
              </Grid>
                <FiltrosMPyProd filtro='Materia Prima'/>
                <Divider/>
                <Grid>
                <Grid.Column width={16}>       
                    <div id="itemGroup" >
                      <Table singleLine striped>
                        <Table.Header>
                        <Table.Row>
                            <Table.HeaderCell>Nombre<Icon link name='sort' onClick={()=>this.ordenar('nombre')}/></Table.HeaderCell>
                            <Table.HeaderCell textAlign='center'>Stock<Icon link name='sort' onClick={()=>this.ordenar('stock')}/></Table.HeaderCell>
                            <Table.HeaderCell textAlign='center'>Costo<Icon link name='sort' onClick={()=>this.ordenar('costo')}/></Table.HeaderCell>
                            <Table.HeaderCell textAlign='center'>Unidad de medida</Table.HeaderCell>
                            <Table.HeaderCell textAlign='center'>Acciones</Table.HeaderCell>
                        </Table.Row>
                      </Table.Header>
                      <Table.Body>
                        {itemsRendered}
                        </Table.Body>
                      </Table>
                      <Confirm
                        open={this.state.open}
                        header='Borrar materia prima'
                        content='¿Está seguro que desea borrar esta materia prima?'
                        cancelButton='No'
                        confirmButton='Si'
                        onCancel={this.handleCancel}
                        onConfirm={this.handleConfirm}
                      />
                    </div>
                </Grid.Column>
                </Grid>
            </Segment>
          </div>

        </div>
    </div>
    )

  }

ordenar(atributo) {

  switch(atributo) {
    case 'stock': 
      if(this.ordenFiltros.stockAsc) {
        this.props.mpsEmp.sort(function(a, b){
          return a.stock_actual - b.stock_actual;
        });
      } else {
        this.props.mpsEmp.sort(function(a, b){
          return b.stock_actual - a.stock_actual;
        });
      }
      this.ordenFiltros.stockAsc = !this.ordenFiltros.stockAsc;
      this.setState({setFiltro: !this.state.setFiltro});
      break;
    case 'nombre': 
      if(this.ordenFiltros.nombreAsc) {
        this.props.mpsEmp.sort(function(a, b){
          return a.nombre.localeCompare(b.nombre);
        });
      }
      else {
        this.props.mpsEmp.sort(function(a, b){
          return b.nombre.localeCompare(a.nombre);
        })
      }
      this.ordenFiltros.nombreAsc = !this.ordenFiltros.nombreAsc;
      this.setState({setFiltro: !this.state.setFiltro});
      break;

    case 'costo': 
      if(this.ordenFiltros.costoAsc) {
        this.props.mpsEmp.sort(function(a, b){
          return a.costo - b.costo;
        });
      } else {
        this.props.mpsEmp.sort(function(a, b){
          return b.costo - a.costo;
        });
      }
      this.ordenFiltros.costoAsc = !this.ordenFiltros.costoAsc;
      this.setState({setFiltro: !this.state.setFiltro});
      break;
  }
}

}



const mapStateToProps = state => ({
  mpsEmp: state.mpsEmp.items,
  alicuotas: state.tipificaciones.alicuotas,
  unidadesMedida: state.tipificaciones.unidadesMedida
});

export default withRouter(connect(mapStateToProps, { getTodasMPEmp, openModal, delMateriaPrimaEmp, getAlicuotas, getUnidadesMedida })(MatPrimaConsulta));
