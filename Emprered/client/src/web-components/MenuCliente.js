import React, { Component } from 'react';
import { Menu, Item, Icon } from 'semantic-ui-react';
import { getUser } from '../config/configs';
import '../styles/MenuEmprendedor.css';
import Avatar from '../assets/default-avatar.png';

export default class MenuCliente extends Component {

    constructor(props) {
        super(props);
        this.server = "http://localhost:8000/server/media/";
        this.foto = getUser().foto;

        if (this.foto !== ""){
          this.foto = this.server + this.foto;
        }
        else {
          this.foto = Avatar;
        }
      }

  render() {
  return (
            <Menu vertical inverted className='MenuEmprendedor'

            >
                <Menu.Item>
                <Item>
                    <Item.Image circular size="medium"  src={this.foto} />
                    <br/>
                    <br/>
                    <center>
                        <Item.Content>
                            <Item.Header style={{"overflow": "hidden",
                                            "word-wrap": "break-word"}}>
                            {getUser().username}
                            </Item.Header>
                        </Item.Content>
                    </center>
                </Item>
                </Menu.Item>

                <Menu.Item
                    name='Tienda'
                    active={false}
                    href='/tienda'
                >
                    <right>
                    <Icon name='shopping cart'/>
                    </right> Tienda
                </Menu.Item>

                <Menu.Item
                    name='Mis Compras'
                    active={false}
                    href='/compras_cliente'
                >
                    <right>
                    <Icon name='dollar sign'/>
                    </right> Mis Compras
                </Menu.Item>
            </Menu>
    )
  }
}
