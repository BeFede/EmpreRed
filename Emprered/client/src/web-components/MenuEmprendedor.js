import React, { Component } from 'react';
import { Menu, Item, Dropdown, Icon } from 'semantic-ui-react';
import { getUser } from '../config/configs';
import '../styles/MenuEmprendedor.css';
import Avatar from '../assets/default-avatar.png';

class MenuEmprendedor extends Component {

    constructor(props) {
        super(props);
        this.server = "http://localhost:8000/server/media/";
        this.foto = getUser().foto;

        if (this.foto !== ""){
          this.foto = this.server + this.foto;
        }
        else {
          this.foto = Avatar;
        }
      }

  render() {

    return (
            <Menu vertical inverted className='MenuEmprendedor'>
                <Menu.Item>
                <Item>
                    <Item.Image circular size="medium"  src={this.foto} />
                    <br/>
                    <br/>
                    <center>
                        <Item.Content>
                            <Item.Header style={{"overflow": "hidden",
                                            "word-wrap": "break-word",}}>
                            <b>{getUser().username}</b><br/>

                            </Item.Header>
                        </Item.Content>
                    </center>
                </Item>
                </Menu.Item>


                <Menu.Item
                    name='Mi Catálogo'
                    active={false}
                    href='/catalogo'
                >
                  <right>
                  <Icon name='list alternate outline'/>
                  </right> Mi catalogo
                </Menu.Item>

                <Menu.Item
                    name='materias primas'
                    active={false}
                    href='/matprimas'
                >
                  <right>
                  <Icon name='cut'/>
                  </right> Materias Primas
                </Menu.Item>

                <Menu.Item
                    name='Mis Productos'
                    active={false}
                    href='/productos'
                >
                  <right>
                  <Icon name='boxes'/>
                  </right> Productos
                </Menu.Item>

                <Menu.Item
                    name='Mi Producción'
                    active={false}
                    href='/produccion'
                >
                  <right>
                  <Icon name='wrench'/>
                  </right> Producciones
                </Menu.Item>

                <Dropdown item text='Mis movimientos' >
                  <Dropdown.Menu text='Mis movimientos' active={false}>
                  <Dropdown.Item active={true}  href='/mismovimientos'> <Icon name='exchange'/>Historial de Movimientos</Dropdown.Item>
                  <Dropdown.Item active={true} href='/movimientos'><Icon name='plus square'/>Nuevo Movimiento</Dropdown.Item>
                  </Dropdown.Menu>
                </Dropdown>


                <Dropdown item center text='Mis compras' style={{align:'centered'}}>
                  <Dropdown.Menu active={false} >
                    <Dropdown.Item active={true} href='/historialCompras'><Icon name='list alternate outline'/>Historial de Compras</Dropdown.Item>
                    <Dropdown.Item active={true} href='/compras'><Icon name='shopping cart'/>Nueva Compra</Dropdown.Item>
                  </Dropdown.Menu>

                </Dropdown>

                <Menu.Item
                    name='Mis Ventas'
                    active={false}
                    href='/ventas'
                >
                  <right>
                  <Icon name='handshake outline'/>
                  </right> Mis Ventas
                </Menu.Item>

                <Menu.Item
                    name='Compras en tienda'
                    active={false}
                    href='/compras_cliente'
                >
                  <right>
                  <Icon name='dollar sign'/>
                  </right> Compras en tienda
                </Menu.Item>
                
                <Menu.Item
                    name='Mis tareas'
                    active={false}
                    href='/tareas'
                >
                  <right>
                  <Icon name='tasks'/>
                  </right> Mis tareas
                </Menu.Item>


            </Menu>
    )
  }
}

export default MenuEmprendedor;
