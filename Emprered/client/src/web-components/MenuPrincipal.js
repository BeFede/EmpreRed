import React, { Component } from 'react';
import { Menu, Input, Dropdown, Icon, Label } from 'semantic-ui-react';
import { getUser } from '../config/configs';
import logoEmpreRed from '../assets/EmpreRedLogoE.png'

class MenuPrincipal extends Component {

    constructor(props) {
        super(props);
        this.state = {notificaciones: []}
    }

  logout() {
       localStorage.removeItem('token');
       localStorage.removeItem('usuario');
  }
  componentWillMount() {
    this.userLogeado = (getUser()) ? getUser() : null;

    this.setState({
        notificaciones : 
        [
        { descripcion: 'Se venció la tarea "Comprar enanos" el día 3/4 a las 17hs', id:1 }
        ]
    });

    setInterval(() => 
    {   
        let notificaciones = this.state.notificaciones;
        const newNotificacion = {descripcion: 'Otra noti man', id: 2} 
        notificaciones.push(newNotificacion);
        this.setState({notificaciones:notificaciones});
    }
    , 10000);
  }

  render() {

    if(!localStorage.getItem('token') && localStorage.getItem('usuario')) {
        this.logout();
        window.location = '/';
    }
    return (

      <div>
            <Menu fixed="top" inverted
                style= {{ "marginBottom": "0px",
                "display": "flex",
                "flexDirection": "row",
                "flex":1
                }}
            >

                <Menu.Menu position= 'left'>
                <Menu.Item>
         <img src={logoEmpreRed} />
         </Menu.Item>

                    <Menu.Item
                        name='Home'
                        active={false}
                        href='/'
                    >
                        <Icon name='home'/>
                    </Menu.Item>

                    <Menu.Item
                        name='Store'
                        active={false}
                        href='/tienda'
                    >
                        <Icon name='shop'/>
                    </Menu.Item>
                </Menu.Menu>
                {

                    (!this.userLogeado) ?

                    <Menu.Menu position='right'>
                    <Menu.Item>
                            <Input icon='search' placeholder='Search...' />
                    </Menu.Item>

                    <Menu.Item
                        icon name='user'
                        name='Registrarme'
                        active={false}
                        href='/regUser'
                    />
                    <Menu.Item
                        name='Ingresar'
                        active={false}
                        href='/ingreso'
                    />
                    </Menu.Menu>
                    :
                    <Menu.Menu position='right'attached>
                    <Menu.Item
                        name='Notificaciones'
                        active={false}
                        href='/notificaciones'
                    >
                            <Icon name='bell' />
                            {
                                (this.state.notificaciones.length > 0)
                                ?
                                <Label color='red'>
                                    {this.state.notificaciones.length}
                                </Label>
                                :
                                null
                            }
                            
                    </Menu.Item>
                    <Menu.Item>
                        {/* <Label color='teal'>
                        <Icon name='user circle'
                        /> */}
                            {
                                (getUser()) ? getUser().username : ''
                            }
                        {/* </Label> */}
                    </Menu.Item>
                    <Dropdown item icon='cogs' simple>
                        <Dropdown.Menu>
                        <Dropdown.Item href={(getUser().rol == 'Emprendedor')? '/micuentae' : 
                        '/micuentac'
                    }>Mi cuenta</Dropdown.Item>
                        <Dropdown.Item>Configuración</Dropdown.Item>
                        <Dropdown.Item  href='/' onClick={this.logout}>Cerrar sesión</Dropdown.Item>
                        </Dropdown.Menu>
                    </Dropdown>


                    </Menu.Menu>
                }
            </Menu>
            </div>
    )
  }
}

export default MenuPrincipal;
