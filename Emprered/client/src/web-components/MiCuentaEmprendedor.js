import React,{Component} from 'react';
import { Item, Menu, Segment, Icon, Form, Button, Message, Header, Input, Image, Modal, Grid, Divider } from 'semantic-ui-react'
import MenuPrincipal from './MenuPrincipal';
import ReactTooltip from 'react-tooltip';
import { connect } from 'react-redux';
import { editarUsuario } from '../actions/editarPerfilActions'
import { getUser, envProduccion } from '../config/configs';
import { withRouter } from 'react-router-dom';
import { urlMedia } from '../config/configs';
import Avatar from '../assets/default-avatar.png';
import { getCondicionesFrenteAIVA } from '../actions/tipificacionesActions';
import { getDatosP, postDatosP } from '../actions/editarDatosPActions';
import { getMarcas, postMarca } from '../actions/marcasActions';
import imageNotFound from '../assets/imageNotFound.jpeg'

class DMicroemprendimiento extends Component
{

    constructor(props) {
        super(props);
        this.server = urlMedia;
        this.marca = (this.props.marca) ? this.props.marca : {
            nombre:'',
            descripcion:'',
            logo: '',
            frase:''
        };;
        this.foto = JSON.parse(localStorage.getItem('usuario')).foto;
        this.fotuli = null;
        this.state = {
            messageInfo: '',
            messageOpen: false,
            estadoResponse: null,
            cargado: true,
            fotoSet: false
        }
        if (this.foto != ""){
          this.foto = this.server + this.foto;
        }
        else {
          this.foto = Avatar;
        }
        this.submit = this.submit.bind(this);
      }

       renderMensaje = (info) => {
          let mensaje = '';
          if (info.estado === 'OK') {
              mensaje = 'Se actualizaron los datos correctamente';
          } else {
              mensaje = info.mensaje;
          }
          this.setState({ messageInfo: mensaje, messageOpen: true, estadoResponse: info.estado });
      }


    handleChange = (e, {name, value}) => {
        if (name == 'logo') {
            this.fotuli = e.target.files[0];
            this.setState({
                fotoSet: true
            })
        }
        else{
            this.marca = {
               ...this.marca,
               [name]: (value) ? value : null
            }
        }
       }

       handleMessageConfirm = () => {
        this.setState({ messageOpen: false });
        }

       submit()
       {

        if(this.fotuli!=null)
        {
        this.marca.logo = this.fotuli;
        this.setState({
            fotoSet: false
        })
        }

        this.props.postMarca(this.marca)
        .then((data) => {
            if(data.estado === 'OK') {
              this.renderMensaje(data)
            } else {
              this.renderMensaje(data);
            }
        }, err => {
          console.log(err);
        });
       }

     renderLogoMarca()
     {
         if(this.server + this.marca.logo)
         {
         return(
            <div>
                {
                    (this.marca.logo !== '')
                    ?
                    <Image src={ this.state.fotoSet ? URL.createObjectURL(this.fotuli) :  this.server + this.marca.logo } size='small'/>
                    :
                    <Image src={imageNotFound} size='small'/>
                }
            </div>
         )
         }
     }
    render()
    {
        return(
            <Segment>                
                <Grid>
                    <Grid.Row centered>
                         <Grid.Column width={12}>
                         <Grid>
                            <Grid.Row centered>
                                    <Grid.Column width={6}>
                                    <center>
                                            <Header as='h2'>Microemprendimiento</Header>
                                    </center>
                                    </Grid.Column>
                            </Grid.Row>
                        </Grid>    
                        <Grid>
                             <Grid.Row centered>
                                <Grid.Column width={8}>
                                    <Grid>
                                        <Grid.Row centered>
                                            <Grid.Column width={6}>
                                                <center>
                                                    <header as='h3' style={{"color": "orange", "font-weight": "bold", "font-size": "20px"}}><b>Marca</b></header>
                                                </center>
                                            </Grid.Column>
                                        </Grid.Row>
                                    </Grid>
                                    <Divider/>
                                    <Grid>
                                        <Grid.Row centered>
                                            <Grid.Column width={12}>
                                                <center>
                                                    <Form.Input content name="nombre" label='Nombre (Marca):   'defaultValue={(this.marca.nombre != 'null') ? this.marca.nombre : '' } onChange={this.handleChange}/>
                                                </center>
                                            </Grid.Column>
                                        </Grid.Row>

                                    </Grid>
                                    <Grid>
                                        <Grid.Row centered>  
                                            <Grid.Column width={12}>
                                                <center>
                                                    <Form.Input name="descripcion" label='Descripción:   ' type='text' defaultValue={(this.marca.descripcion != 'null') ? this.marca.descripcion : '' } onChange={this.handleChange}/>
                                                </center>
                                            </Grid.Column>      
                                        </Grid.Row>
                                    </Grid>
                                    <Grid>
                                        <Grid.Row centered>
                                            <Grid.Column width={12}>                                                
                                                    <center>
                                                        <Form.Input name="frase" label='Slogan:   ' defaultValue={(this.marca.frase != 'null') ? this.marca.frase : '' } onChange={this.handleChange}/>                                            
                                                    </center>                                                
                                            </Grid.Column>
                                        </Grid.Row>
                                    </Grid>
                                    <Grid>
                                        <Grid.Row centered>
                                            <Grid.Column width={6}>
                                                <center>
                                                    <header as='h3' style={{"color": "orange", "font-weight": "bold", "font-size": "20px"}}><b>Logo</b></header>
                                                </center>
                                            </Grid.Column>   
                                        </Grid.Row>
                                    </Grid>
                                    <Divider/>
                                    <Grid>
                                    <Grid.Row centered>
                                        <Grid.Column width={12}>
                                            <center>
                                                <Grid.Column width={12}>
                                                    <Grid>
                                                        <Grid.Row centered>
                                                            {this.renderLogoMarca()}
                                                        </Grid.Row>
                                                    </Grid>
                                                    <Grid>
                                                        <Grid.Row centered>
                                                                <Input type="file" id="logo" name="logo" onChange={this.handleChange}/>
                                                        </Grid.Row>
                                                    </Grid>    
                                                </Grid.Column>   
                                            </center>
                                        </Grid.Column>
                                    </Grid.Row>                                        
                                    </Grid>
                                </Grid.Column>
                            </Grid.Row>
                        </Grid>
                                    
                        <Grid>
                            <Grid.Row centered>
                                <Grid.Column width={6}>
                                    <center>
                                    <Button positive  icon='check'  content="Guardar" onClick={this.submit}></Button>
                                    </center>
                                </Grid.Column>
                            </Grid.Row>
                        </Grid>
                                    <Modal
                                        open={this.state.messageOpen}
                                        onClose={this.handleMessageConfirm}
                                        size='small'
                                    >
                                        <Header icon='browser' content='Aviso' />
                                        <Modal.Content>
                                            <h3>{this.state.messageInfo}</h3>
                                        </Modal.Content>
                                        <Modal.Actions>
                                            <Button color='green' onClick={this.handleMessageConfirm} inverted href= {this.state.estadoResponse=='OK' ? '/micuentae' : null}>
                                                <Icon name='checkmark' /> Aceptar
                                                </Button>
                                        </Modal.Actions>
                                    </Modal>

                         </Grid.Column>   
                    </Grid.Row>
                </Grid>
            </Segment>
        )
    }
}




class DUsuario extends Component
{
    constructor(props) {
        super(props);
        this.data = (this.props.data) ? props.data : {};
        this.state = {
            showEditU: false,
            messageInfo: null,
            messageOpen: null,
            messageOpenUsername: null,
            messageOpenFoto: null,
            messageOpenContraseña: null,
            messageOpenNombre: null,
            messageOpenApellido: null,
            messageOpenFecha: null,
            messageOpenTel: null,
            messageOpenDocumento: null,
            estadoResponse: null,
            setUsername:false,
            usernameAnterior: getUser().username,
            setFoto: false,
            esAutorizadoMercadoPago: false,
            equalPass: null
        }
        this.server = urlMedia;
        this.handleChangeP = this.handleChangeP.bind(this);
        this.handleChangeUs = this.handleChangeUs.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.fotuli = null; // acá se guarda el file de la foto
        
        this.dataP = (this.props.dataP) ? props.dataP : {};
        this.inputToSave = {
            input: null,
            payload: ''
        }
        this.fotoEvento = null;
        this.changePassword = false;
        this.changePass = {
            repetir_clave:'',
            nueva_clave: '',
            clave_actual: ''
        }
        this.optionsCondicionesIVA = [];
    }
      
    componentWillMount() {
        // Chequeo si estamos en produccion y si esta autorizado por mercado pago
        if(envProduccion && getUser().es_autorizado_mercado_pago) {
            this.setState({esAutorizadoMercadoPago: true});
        }

        // SI no estamos en prod lo pongo en true por defecto alv
        if(!envProduccion) {
            this.setState({esAutorizadoMercadoPago: true});
        }
    }

    handleChangeP = (e, {name, value, checked}) => {
        
        this.inputToSave.input = name;
        this.inputToSave.payload = value;
    }

    saveChangeP ()
    {
        this.dataP = {
            ...this.dataP,
            [this.inputToSave.input]: this.inputToSave.payload ? this.inputToSave.payload : ''
        }
    }

    comparePass(){

        
        if(this.changePass.nueva_clave != '')
        {
            if( this.changePass.nueva_clave === this.changePass.repetir_clave)
            {
                this.setState({
                    equalPass: true
                })
                return;
            }   
            this.setState({
                equalPass: false
            })
        }
        else
        {
            this.setState({
                equalPass: null
            })
        }        
    }
    saveChangeUs(){
        
        if(this.changePassword)
        {
            this.data = {
                ...this.data,
                repetir_clave:'',
                nueva_clave:'', 
                clave_actual:''
            }
            this.data = {
                ...this.data,
                ["repetir_clave"]: this.changePass.repetir_clave ? this.changePass.repetir_clave : '',
                ["nueva_clave"]: this.changePass.nueva_clave ? this.changePass.nueva_clave : '',
                ["clave_actual"]: this.changePass.clave_actual ? this.changePass.clave_actual : '',
            }
        }
        else{
            this.data = {
               ...this.data,
               [this.inputToSave.input]: (this.inputToSave.payload) ? this.inputToSave.payload : ''
            }
        }
    }

    handleChangeUs = (e, {name, value, type}) => {
        
        this.inputToSave.input = name;
        if(name == 'username'){
            this.setState({
                setUsername: true
            })
        }

        if (name == 'foto') {
            // La única forma que encontré para hacerlo andar *shrug*
            this.inputToSave.payload = e.target.files[0];
            this.setState({
                setFoto: true
            })
            this.fotoEvento = e.target.files[0]
        }
        else{
            if(type==="password")
            {
                this.changePassword = true;
                this.changePass[this.inputToSave.input] = value
                this.comparePass();
                return;
            }
            this.inputToSave.payload = value

        }
        
    }

    renderMensaje = (info) => {
        let mensaje = '';
        if (info.estado === 'OK') {
            mensaje = "Los cambios se registraron correctamente";
        } else {
            mensaje = info.mensaje
        }
        this.setState({ messageInfo: mensaje, messageOpen: true, estadoResponse: info.estado });
    }

    handleMessageConfirm = () => {
        this.setState({ messageOpen: false });
        ;
        // this.props.history.push('/micuentau');
    }

     handleSubmit = () => {
        // Aca invoco a la promise y agarro lo que resuelva, sea que haya entrado por then o catch en la action
       
        this.setState({
            setFoto: false
        })  
            this.props.postDatosP(this.dataP)
            .then((data) => {
                if(data.estado === 'OK') {

                    this.props.editarUsuario(this.data)
                    .then((data) => {
                        if(data.estado === 'OK') {
                          this.renderMensaje(data)
                        } else {
                          this.renderMensaje(data);
                        }
                    }, err => {
                      console.log(err);
                    });


                } else {
                  this.renderMensaje(data);
                }
            }, err => {
              console.log(err);
            });
        
      }
   
      getOptions() {
          for (const cond of this.props.condiciones_iva) {
              const opt = {key: cond.id, text: cond.descripcion, value: cond.id}
              this.optionsCondicionesIVA.push(opt);
       }
    }      


    renderImg()
    {
        return (this.server + this.data.foto) ? this.server + this.data.foto : Avatar;
    }

    autorizarMercadoPago(){
        let urlRetorno = '';
        const idEmprendedor = getUser().id_emprendedor;
        if(envProduccion) {
            urlRetorno = 'https://www.empreredapp.com/micuentae'
        } else {
            urlRetorno = 'http://localhost:3000/micuentae'
        }
        // Lo hardcodeo a true pero despues al renderizar de nuevo el componente se pregunta si se seteo efectivamente con un get contra una api
        window.localStorage.setItem('usuario.es_autorizado_mercado_pago', true);
        window.location = `https://www.empreredapp.com/server/mercado_pago/autorizar_emprendedor/vincular_cuenta?id_emprendedor=${idEmprendedor}&redirect_uri=${urlRetorno}`
        
    }

    render()
    {
       this.optionsCondicionesIVA = [];
       this.getOptions();
    return(

            <Grid>
                <Grid.Row centered>
                    <Grid.Column width={16}>
                        <Segment loading={!this.dataP}>
                        
                        <Grid>
                            <Grid.Row>
                                <Grid.Column width={16}>     
                                    <Grid>
                                        <Grid.Row centered>
                                            <Grid.Column width={8}>
                                                <center>
                                                    <header as='h3' style={{"color": "orange", "font-weight": "bold", "font-size": "20px"}}><b>Datos de cuenta</b></header>             
                                                </center>
                                            <Divider/>  
                                            </Grid.Column>
                                        </Grid.Row>
                                        <Grid.Row centered>
                                            <Grid.Column width={8}>
                                                <Grid>
                                                    <Grid.Row centered>
                                                        <Grid.Column width={4}>
                                                            <center>

                                                                <Form.Input name="usernameActual"  label='Usuario: '>               
                                                                {(this.data) ? this.data.username : ''} 
                                                                </Form.Input>
                                                            </center>
                                                        </Grid.Column>
                                                        <Grid.Column width={2}>
                                                            <a onClick={() => this.setState({ messageOpenUsername: true }) } style={{ cursor: "pointer" }} link>editar</a>
                                                        </Grid.Column>
                                                    </Grid.Row>
                                                </Grid>
                                      
                                            </Grid.Column>
                                        </Grid.Row>
                                      
                                        <Grid.Row centered>
                                            <Grid.Column width={8}>
                                                <Grid>
                                                    <Grid.Row centered>
                                                        <Grid.Column width={5}>
                                                            <center>
                                                                <Form.Field>
                                                                    <a data-tip data-for='foto_tip' style={{"color":"black"}}>Foto (actual: { this.state.setFoto ? this.fotoEvento.name : getUser().foto.split('\/')[1] || "ninguna" })</a>
                                                                </Form.Field>
                                                            </center>
                                                        </Grid.Column>
                                                        <Grid.Column width={2}>
                                                        <a onClick={() => this.setState({ messageOpenFoto: true }) } style={{ cursor: "pointer" }} link>editar</a>
                                                        </Grid.Column>
                                                    </Grid.Row>
                                                </Grid>
                                            </Grid.Column>
                                        </Grid.Row>
                                        <Grid.Row centered>
                                            <Grid.Column>
                                                <center>
                                                    <Form.Input name="email" label='Email: '>
                                                        {(this.data) ? this.data.email : ''}
                                                    </Form.Input>
                                                </center>
                                            </Grid.Column>
                                        </Grid.Row>
                                        <Grid.Row centered>
                                            <Grid.Column width={6}>
                                                <Grid>
                                                    <Grid.Row centered>
                                                        <Grid.Column width={4}>
                                                            <Form.Input name="contraseña" label='Contraseña: '>
                                                                ******
                                                            </Form.Input>
                                                        </Grid.Column>
                                                        {
                                                            this.data.tipo_registro === 'emprered' ? 
                                                            <Grid.Column width={2}>
                                                                <a onClick={() => this.setState({ messageOpenContraseña: true }) } style={{ cursor: "pointer" }} link>editar</a>
                                                            </Grid.Column>
                                                            :
                                                             null
                                                        }
                                                    </Grid.Row>
                                                </Grid>

                                            </Grid.Column>                
                                        </Grid.Row>
                                        <Grid.Row centered>
                                            <Grid.Column width={8}>
                                                <center>
                                                    <header as='h3' style={{"color": "orange", "font-weight": "bold", "font-size": "20px"}}><b>Datos personales</b></header>             
                                                </center>
                                            <Divider/>  
                                            </Grid.Column>
                                        </Grid.Row>
                                        <Grid.Row centered>
                                            <Grid.Column width={8}>
                                                <Grid>
                                                    <Grid.Row centered>
                                                            <Grid.Column width={4}>
                                                                <Form.Input name="nombre" label='Nombre: '>
                                                                        {this.dataP.nombre ? this.dataP.nombre : ''}
                                                                </Form.Input>
                                                            </Grid.Column>
                                                            <Grid.Column width={2}>
                                                                <a onClick={() => this.setState({ messageOpenNombre: true }) } style={{ cursor: "pointer" }} link>editar</a>
                                                            </Grid.Column>
                                                    </Grid.Row>
                                                    <Grid.Row centered>
                                                            <Grid.Column width={4}>
                                                                <Form.Input name="apellido" label='Apellido: '>
                                                                        {this.dataP.apellido ? this.dataP.apellido : ''}
                                                                </Form.Input>
                                                            </Grid.Column>
                                                            <Grid.Column width={2}>
                                                                <a onClick={() => this.setState({ messageOpenApellido: true }) } style={{ cursor: "pointer" }} link>editar</a>
                                                            </Grid.Column>
                                                    </Grid.Row>
                                                    <Grid.Row centered>
                                                            <Grid.Column width={4}>
                                                                <Form.Input name="numero_documento" label='Documento: '>
                                                                        {this.dataP.numero_documento ? this.dataP.numero_documento : ''}
                                                                </Form.Input>
                                                            </Grid.Column>
                                                            {
                                                                this.dataP.documento ? 
                                                                <Grid.Column width={2}>
                                                                    <a onClick={() => this.setState({ messageOpenDocumento: true }) } style={{ cursor: "pointer" }} link>cargar</a>
                                                                </Grid.Column>
                                                                :
                                                                null
                                                            }
                                                    </Grid.Row>
                                                    <Grid.Row centered>
                                                            <Grid.Column width={5}>
                                                                <Form.Input name="fecha_nacimiento" label='Fecha de nacimiento: '>
                                                                        {this.dataP.fecha_nacimiento ? this.dataP.fecha_nacimiento : ''}
                                                                </Form.Input>
                                                            </Grid.Column>
                                                            <Grid.Column width={2}>
                                                                <a onClick={() => this.setState({ messageOpenFecha: true }) } style={{ cursor: "pointer" }} link>editar</a>
                                                            </Grid.Column>
                                                    </Grid.Row>
                                                    <Grid.Row centered>
                                                            <Grid.Column width={5}>
                                                                <center>    
                                                                    <Form.Input name="telefono" label='Teléfono: '>
                                                                            {this.dataP.telefono ? this.dataP.telefono : ''}
                                                                    </Form.Input>
                                                                </center>
                                                            </Grid.Column>
                                                            <Grid.Column width={2}>
                                                                <a onClick={() => this.setState({ messageOpenTel: true })  } style={{ cursor: "pointer" }} link>editar</a>
                                                            </Grid.Column>
                                                    </Grid.Row>
                                                    <Grid.Row centered>
                                                          <Grid.Column width={8}>
                                                              <center>
                                                                <Form.Select name="condicion_frente_IVA" label='Condición frente a IVA:   ' onChange={this.handleChange} options={this.optionsCondicionesIVA} defaultValue={this.dataP.condicion_frente_IVA}/>
                                                              </center>
                                                          </Grid.Column>      
                                                    </Grid.Row>
                                                </Grid>
                                            </Grid.Column>
                                        </Grid.Row>

                                        <Grid.Row centered>
                                            <Grid.Column width={6}>
                                                {
                                                    (!this.state.esAutorizadoMercadoPago)
                                                    ?
                                                    <Grid>
                                                        <Grid.Row>
                                                            <Grid.Column width={12}>
                                                                <center>
                                                                    <Message info icon>
                                                                        <Icon name='info circle'/>
                                                                        <Message.Content>
                                                                            <p>Para poder operar a través de Mercado Pago en nuestra aplicación debe autorizarse</p>
                                                                        </Message.Content>
                                                                    </Message>
                                                                </center>
                                                            </Grid.Column>
                                                            <Grid.Column width={4}>
                                                                    <Button primary content="Verificar en Mercado Pago" onClick={()=>this.autorizarMercadoPago()}></Button>
                                                            </Grid.Column>
                                                        </Grid.Row>
                                                    </Grid>
                                                    :
                                                    <center>
                                                        <Message positive icon>
                                                            <Icon name='check circle'/>
                                                            <Message.Content>
                                                                <Message.Header>Autorizado en Mercado Pago</Message.Header>
                                                                <p>Está autorizado para operar a través de Mercado Pago</p>
                                                            </Message.Content>
                                                        </Message>
                                                    </center>
                                                }
                                            
                                            </Grid.Column>                    
                                        </Grid.Row>
                                       

                                        <Grid.Row centered>
                                            <Grid.Column width={8}>
                                            <center>
                                                <Button positive  icon='check'  content="Guardar" onClick={this.handleSubmit}></Button>
                                            </center>
                                            </Grid.Column>                    
                                        </Grid.Row>
                                    </Grid>
                                        
                                        
                                </Grid.Column>
                            </Grid.Row>
                        </Grid>
                                        <Modal
                                        open={this.state.messageOpenTel}
                                        onClose={this.handleChange}
                                        size='small'
                                        >
                                        <Header icon='edit outline' content='Modificación'/>
                                        <Modal.Content>
                                            <center>
                                                <Form.Group >
                                                    <Form.Input name="telefono" label='Teléfono:   ' placeholder='Teléfono' onChange={this.handleChangeP}/>
                                                </Form.Group>
                                            </center>
                                        </Modal.Content>
                                        <Modal.Actions>
                                            <center>
                                                <Button color='green' onClick={()=> { this.setState( { messageOpenTel: false } ); this.saveChangeP() } }>
                                                    Guardar
                                                </Button>
                                                <Button color='red' onClick={()=> this.setState( { messageOpenTel: false } ) }>
                                                    Cancelar
                                                </Button>
                                            </center>
                                        </Modal.Actions>
                                        </Modal>

                                        <Modal
                                        open={this.state.messageOpenDocumento}
                                        onClose={this.handleChange}
                                        size='small'
                                        >
                                        <Header icon='edit outline' content='Modificación'/>
                                        <Modal.Content>
                                            <center>
                                                <Form.Group >
                                                    <Form.Input name="documento" label='Documento:   ' placeholder='Documento' onChange={this.handleChangeP}/>
                                                </Form.Group>
                                            </center>
                                        </Modal.Content>
                                        <Modal.Actions>
                                            <center>
                                                <Button color='green' onClick={()=> { this.setState( { messageOpenDocumento: false } ); this.saveChangeP() } }>
                                                    Guardar
                                                </Button>
                                                <Button color='red' onClick={()=> this.setState( { messageOpenDocumento: false } ) }>
                                                    Cancelar
                                                </Button>
                                            </center>
                                        </Modal.Actions>
                                        </Modal>                        

                                        <Modal
                                        open={this.state.messageOpenUsername}
                                        onClose={this.handleChange}
                                        size='small'
                                        >
                                        <Header icon='edit outline' content='Modificación'/>
                                        <Modal.Content>
                                            <center>
                                                <Form.Group >
                                                    <Form.Input name="username"  label='Nombre de usuario:  ' size='mini' onChange={this.handleChangeUs}>               
                                                    </Form.Input>       
                                                </Form.Group>
                                            </center>
                                        </Modal.Content>
                                        <Modal.Actions>
                                            <center>
                                                <Button color='green' onClick={()=> { this.setState( { messageOpenUsername: false } ); this.saveChangeUs()  } }>
                                                    Guardar
                                                </Button>
                                                <Button color='red' onClick={()=> this.setState( { messageOpenUsername: false } ) }>
                                                    Cancelar
                                                </Button>
                                            </center>
                                        </Modal.Actions>
                                        </Modal>

                                        <Modal
                                        open={this.state.messageOpenFoto}
                                        onClose={this.handleChange}
                                        size='small'
                                        >
                                        <Header icon='edit outline' content='Modificación'/>
                                        <Modal.Content>
                                            <center>
                                                 <Form.Field>
                                                     <Grid>
                                                         <Grid.Row centered>
                                                            <Grid.Column width={16}>
                                                                <Grid>
                                                                    <Grid.Row centered>
                                                                        <Image src={this.state.setFoto ? URL.createObjectURL(this.fotoEvento) : this.renderImg()} height="200"></Image>
                                                                    </Grid.Row>
                                                                </Grid>
                                                                <Grid>
                                                                    <Grid.Row centered>
                                                                        <Input type="file" id="foto" name="foto" label='Foto de Perfil  ' onChange={this.handleChangeUs}/>
                                                                    </Grid.Row>
                                                                </Grid>
                                                            </Grid.Column>
                                                         </Grid.Row>
                                                     </Grid>
                                                 </Form.Field>
                                            </center>
                                        </Modal.Content>
                                        <Modal.Actions>
                                            <center>
                                                <Button color='green' onClick={()=> { this.setState( { messageOpenFoto: false } ); this.saveChangeUs() } }>
                                                    Guardar
                                                </Button>
                                                <Button color='red' onClick={()=> this.setState( { messageOpenFoto: false, setFoto: false } ) }>
                                                    Cancelar
                                                </Button>
                                            </center>
                                        </Modal.Actions>
                                        </Modal>

                                        <Modal
                                        open={this.state.messageOpenContraseña}
                                        onClose={this.handleChange}
                                        size='small'
                                        >
                                        <Header icon='edit outline' content='Modificación'/>
                                        <Modal.Content>
                                            <center>
                                                <Form.Group widths='equal'>
                                                    <Form.Input name="clave_actual" type="password" label='Contraseña actual:   ' placeholder='Contraseña' onChange={this.handleChangeUs} /> <br/>
                                                    <Form.Input  icon={<Icon name={this.changePass.nueva_clave!='' ? (this.changePass.nueva_clave.length >= 8 ? 'check circle' : 'times circle') : null } color={this.changePass.nueva_clave!='' ? (this.changePass.nueva_clave.length >= 8 ? 'green' : 'red') : null }/>} name="nueva_clave" type="password" label='Nueva contraseña:   ' placeholder='Nueva contraseña' onChange={this.handleChangeUs} /> <br/>
                                                    <Form.Input  icon={<Icon name={this.state.equalPass ? 'check circle' : (this.state.equalPass!=null ? 'times circle' : null) } color={this.state.equalPass ? 'green' : (this.state.equalPass!=null ? 'red' : null) } />}name="repetir_clave"type="password" label='Repita contraseña:   ' placeholder='Repita contraseña' onChange={this.handleChangeUs}/> <br/>
                                                </Form.Group>
                                            </center>
                                        </Modal.Content>
                                        <Modal.Actions>
                                            <center>
                                            <Button color='green' disabled={!this.state.equalPass || this.changePass.nueva_clave.length < 8 || this.changePass.clave_actual === ''}  onClick={()=> { this.setState( { messageOpenContraseña: false } ); this.saveChangeUs()  } }>
                                                    Guardar
                                                </Button>
                                                <Button color='red' onClick={()=> { this.setState( { messageOpenContraseña: false } ); this.changePassword = false } }>
                                                    Cancelar
                                                </Button>
                                            </center>
                                        </Modal.Actions>
                                        </Modal>

                                        <Modal
                                        open={this.state.messageOpenNombre}
                                        onClose={this.handleChange}
                                        size='small'
                                        >
                                        <Header icon='edit outline' content='Modificación'/>
                                        <Modal.Content>
                                            <center>
                                                <Form.Group >
                                                <Form.Input name="nombre"  label='Nombre:  ' onChange={this.handleChangeP}/>
                                                </Form.Group>
                                            </center>
                                        </Modal.Content>
                                        <Modal.Actions>
                                            <center>
                                                <Button color='green' onClick={()=> { this.setState( { messageOpenNombre: false } ); this.saveChangeP()  } }>
                                                    Guardar
                                                </Button>
                                                <Button color='red' onClick={()=> this.setState( { messageOpenNombre: false } ) }>
                                                    Cancelar
                                                </Button>
                                            </center>
                                        </Modal.Actions>
                                        </Modal>

                                        <Modal
                                        open={this.state.messageOpenApellido}
                                        onClose={this.handleChange}
                                        size='small'
                                        centered
                                        >
                                        <Header icon='edit outline' content='Modificación'/>
                                        <Modal.Content content>
                                            <center>
                                                <Form.Group>
                                                    <Form.Input name="apellido"  label='Apellido:   ' onChange={this.handleChangeP}/>     
                                                </Form.Group>
                                            </center>
                                        </Modal.Content>
                                        <Modal.Actions>
                                            <center>
                                                <Button color='green' onClick={()=> { this.setState( { messageOpenApellido: false } ); this.saveChangeP()  } }>
                                                    Guardar
                                                </Button>
                                                <Button color='red' onClick={()=> this.setState( { messageOpenApellido: false } ) }>
                                                    Cancelar
                                                </Button>
                                            </center>

                                        </Modal.Actions>
                                        </Modal>

                                        <Modal
                                        open={this.state.messageOpenFecha}
                                        onClose={this.handleChange}
                                        size='small'
                                        >
                                        <Header icon='edit outline' content='Modificación'/>
                                        <Modal.Content>
                                            <center>
                                                <Form.Group >
                                                <Form.Input name="fecha_nacimiento" type='date' label='Fecha de nacimiento:   ' onChange={this.handleChangeP}/>      
                                                </Form.Group>
                                            </center>
                                        </Modal.Content>
                                        <Modal.Actions>
                                            <center>
                                                <Button color='green' onClick={()=> { this.setState( { messageOpenFecha: false } ); this.saveChangeP()  } }>
                                                    Guardar
                                                </Button>
                                                <Button color='red' onClick={()=> this.setState( { messageOpenFecha: false } ) }>
                                                    Cancelar
                                                </Button>
                                            </center>
                                        </Modal.Actions>
                                        </Modal>

                                        {/* Este es el modal para el mensaje */}
                                        <Modal
                                            open={this.state.messageOpen}
                                            onClose={this.handleMessageConfirm}
                                            size='small'
                                        >
                                            <Header icon='browser' content='Aviso' />
                                            <Modal.Content>
                                                <h3>{this.state.messageInfo}</h3>
                                            </Modal.Content>
                                            <Modal.Actions>
                                                <Button color='green' onClick={this.handleMessageConfirm} inverted href= {this.state.estadoResponse=='OK' ? '/micuentae' : null}>
                                                    <Icon name='checkmark' /> Aceptar
                                                    </Button>
                                            </Modal.Actions>
                                        </Modal>
                    </Segment>
                    </Grid.Column>
                </Grid.Row>
            </Grid>

    )
    
    }
}



class MiCuentaEmprendedor extends Component
{
    
    constructor(props) {
        super(props);
        this.server = "http://localhost:8000/server/media/";
        this.foto = getUser().foto;

        if (this.foto != ""){
          this.foto = this.server + this.foto;
        }
        else {
          this.foto = Avatar;
        }
        this.state = {
            datosUser: false,
            datosPersonales: false,
            datosMicroemprend: false,
            condiciones_iva: [],
        }
        this.data = getUser();
        this.marca = null;
        this.dataP = null;
      }

      componentWillMount() {
        
        this.props.getMarcas()
           .then(data => {
                if(data.estado === 'OK') {
                    this.marca = data.datos.marca;
                } else {
                    this.marca = null;
                }
               
           }, err => {
               console.log(err);
           });
        
        
        this.props.getDatosP()
       .then( data => {
         this.dataP = data.datos.emprendedores;
         this.setState({
             datosUser: true
         })
       });
       this.props.getCondicionesFrenteAIVA()
       .then(data=>{
         this.setState({
           condiciones_iva: data
         })
       });
        this.optionsCondicionesIVA = [];
     }

    render()
    {         
        return(
            <div id="DivPrincipal" style={{overflow:'auto'}}>
                <div id="MenuTop">        
                    <MenuPrincipal/>
                </div>
                <div className = "row">
                    <div id="MenuLateral">
                            <Menu vertical inverted
                                style={{"height": "100vh",
                                "paddingTop": "61px",
                                "position":"fixed",
                                "display": "flex",
                                "flexDirection": "column",
                                "flex": 1
                                }}
                            >
                        <Menu.Item>
                        <Item >
                            <Item.Image circular size="medium"  src={this.foto} />
                            <br/>
                            <br/>
                            <center>
                                <Item.Content>
                                    <Item.Header style={{"overflow": "hidden",
                                                    "word-wrap": "break-word"}}>
                                        {getUser().username}
                                    </Item.Header>
                                </Item.Content>
                            </center>
                        </Item>
                        </Menu.Item>
                        <Menu.Item
                            name='Datos de usuario'
                            active={false}
                            onClick={()=>(this.setState({
                                datosUser: true,
                                datosPersonales: false,
                                datosMicroemprend: false
                            }))}
                        />
                        <Menu.Item
                            name='Microemprendimiento'
                            active={false}
                            onClick={()=>(this.setState({
                                datosUser: false,
                                datosPersonales: false,
                                datosMicroemprend: true
                            }))}
                        />
                            </Menu>                   
                    </div>
                </div>
            
                <div style={{"paddingTop":"61px", marginLeft:"220px"}}>
                    <Grid>
                        <Grid.Row>
                            <Grid.Column width={16}>
                                {(this.state.datosUser) ? <DUsuario editarUsuario={this.props.editarUsuario} data={this.data} condiciones_iva={this.state.condiciones_iva} dataP = {this.dataP} postDatosP={this.props.postDatosP}/>: null}
                                {(this.state.datosMicroemprend) ? <DMicroemprendimiento postMarca={this.props.postMarca} marca= {this.marca} />: null}
                            </Grid.Column>
                        </Grid.Row>
                    </Grid>
                </div>    
            </div>
        )
    }
}

const mapStateToProps = state => ({
    condiciones_iva: state.tipificaciones.condiciones_iva,
    data: state.datosPersonales.datosP,
    marcas: state.marcas.resultMarcas
  });

export default withRouter(connect(mapStateToProps, { editarUsuario, getCondicionesFrenteAIVA, postDatosP, getDatosP, getUser, getMarcas, postMarca })(MiCuentaEmprendedor));