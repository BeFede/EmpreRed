import React, { Component } from 'react'
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { getTodasMPEmp, delMateriaPrimaEmp } from '../actions/matPrimaEmpActions';
import { getProductos } from '../actions/productosActions';
import ModalManager from './ModalManager';
import { openModal } from '../actions/modalActions';
import FormCompraMP from './FormCompraMP';
import FormCompraProd from './FormCompraProd';
import { removeItemBisexual } from '../actions/tableFillActions';
import { Modal, Button, Header, Icon, Segment, Grid, Divider, Label, Message, Input } from 'semantic-ui-react'
import {Table} from 'semantic-ui-react';
import MenuPrincipal from './MenuPrincipal';
import MenuEmprendedor from './MenuEmprendedor';
import {getUser} from '../config/configs';
import { postCompra} from '../actions/compraActions';
import ProveedoresModal from './ProveedoresModal';

class MisCompras extends Component {

  componentWillMount() {
    this.props.getTodasMPEmp();
    this.props.getProductos();
    this.userLogeado = (getUser()) ? getUser() : null;
    this.dataToSend = { detalles: [] };
    this.registrarCompra = this.registrarCompra.bind(this);

    // Para habilitar el boton de registrar uso 2 booleans (itemsAgregados y comprobanteAgregado dentro de state)
    this.itemsAgregados = false;
    this.state = {
        open: false, idABorrar: null, setFiltro: true, comprobanteAgregado: false,
        compraMP: false, compraProd: false, tableCompra:false, messageInfo: "", messageOpen: null
    };

    this.comprobante = '';
    this.handleChange = this.handleChange.bind(this);
    this.ordenFiltros = { nombreAsc: true }
  }

  showConfirmDialog = (id) => this.setState({open: true, idABorrar: id})
  handleCancel = () => this.setState({open: false})

  handleMessageConfirm = () => {
    this.setState({messageOpen: false});
    this.props.history.push('/historialCompras');  
  }

  handleConfirm = () => {
    this.setState({open: false});
  }

  handleChange = (e, {name, value}) => {
    if(name === 'comprobante') {
      this.comprobante = value;
      if(this.comprobante !== '') {
        this.setState({comprobanteAgregado:true});
      } else {
        this.setState({comprobanteAgregado:false});
      }
    }
  }

  handleButtonClick = () => this.setState({ visible: !this.state.visible });;

  renderMensaje = (info) => {
    let mensaje = '';
    if(info.estado === 'OK') {
        mensaje = 'Compra realizada exitosamente';
    } else {
        mensaje = info.mensaje
    }
    this.cleanAll();
    this.setState({messageInfo: mensaje, messageOpen: true, estadoResponse:info.estado});
  }

  cleanAll() {
    this.dataToSend = { detalles: [] };
    this.itemsCompraRendered = null;
    this.itemsAgregados = false;
    this.setState({compraMP: false, compraProd: false, tableCompra: false });
  }

  showAllMP()
  {
    if(this.state.compraMP)
    {
      return(
        this.renderMP()
      )
    }
  }

  renderItem(a)
  {
      return(
        <Table.Row  key={a.nombre}>
        <Table.Cell>
          <Header as='h4'>
            <Header.Content>
              {a.nombre}
            </Header.Content>
          </Header>
        </Table.Cell>
        <Table.Cell textAlign='center'>{a.stock_minimo}</Table.Cell>
        <Table.Cell textAlign='center'>{a.stock_actual}</Table.Cell>
        <Table.Cell textAlign='center'>{a.unidad_medida}</Table.Cell>
        <Table.Cell textAlign='center' content>
                <Button circular icon="add" color="orange"
                  onClick={() => this.props.openModal({
                    header: `Agregar materia prima: "${a.nombre}"`,
                    content: <FormCompraMP data={a}/>
                  })}
                >
                Agregar
                </Button>
        </Table.Cell>
      </Table.Row>
      )

  }

  renderItemProd(a)
  {
    if(!a.es_fabricado)
    {
      return(
        <Table.Row  key={a.nombre}>
        <Table.Cell>
          <Header as='h4'>
            <Header.Content>
              {a.nombre}
            </Header.Content>
          </Header>
        </Table.Cell>
        <Table.Cell textAlign='center'>{a.stock_minimo}</Table.Cell>
        <Table.Cell textAlign='center'>{a.stock_actual}</Table.Cell>
        <Table.Cell textAlign='center'>{a.unidad_medida}</Table.Cell>
        <Table.Cell textAlign='center' content>
                <Button primary circular icon="add" color="green"
                  onClick={() => this.props.openModal({
                    header: `Agregar Producto: "${a.nombre}"`,
                    content: <FormCompraProd data={a}/>
                  })}
                >
                Agregar
                </Button>
        </Table.Cell>
      </Table.Row>
      )
    }
  }

  renderItemCompra(det)
  {
    this.itemsAgregados = true;
      return(
        <Table.Row  key={(det.materia_prima) ? det.materia_prima : det.producto}>
        <Table.Cell>
          <Header as='h4'>
            <Header.Content>
              {(det.materia_prima) ? det.materia_prima : det.producto}
            </Header.Content>
          </Header>
        </Table.Cell>
        <Table.Cell textAlign='center'>{det.cantidad}</Table.Cell>
        <Table.Cell textAlign='center'>{det.costo}</Table.Cell>
        <Table.Cell textAlign='center'>{det.unidadMedida}</Table.Cell>

        <Table.Cell textAlign='center' content>
        <Button negative circular icon="times"
                  onClick = {() => {
                      // Si el usuario elimina un item de la seleccion lo saco de los detalles a enviar y de los items de la tabla
                      this.dataToSend.detalles = this.dataToSend.detalles.filter((v,i) => {
                        return (v["id"] !== det.id || v["tipo"] != det.tipo);
                      });
                      this.props.removeItemBisexual(det);
                  }}
                />
        </Table.Cell>
      </Table.Row>
      )

  }

  showButtonCompra()
  {

    return(
      <Grid centered>
        <Grid.Row>
          <Grid.Column width={16}>
            <Message info>
                <Message.Header>Puede seleccionar el tipo de compra a realizar</Message.Header>
            </Message>
          </Grid.Column>
        </Grid.Row>

        <Grid.Row centered>
          <Grid.Column width={16}>
            <center>
              <Button.Group>
                <Button color='blue' onClick={()=> this.setState({
                            compraMP: false,
                            compraProd: true
                  })}>
                <Icon name='add circle' />
                Producto
                </Button >
                <Button.Or text='' />
                <Button color='orange' onClick={()=>this.setState({
                  compraMP: true,
                  compraProd: false
                })}>
                <Icon name='add circle' />
                Materia Prima
                </Button>
              </Button.Group>
            </center>
          </Grid.Column>
        </Grid.Row>

      </Grid>
    )
  }

  renderTableCompra()
  {

      if(this.dataToSend.detalles.length === 0) {
        this.itemsAgregados = false;
      }
    return(
      <div>
        <center>
        </center>
        <div style={{overflow: 'auto', maxHeight: 500 }}>
          <Table singleLine striped>
            <Table.Header>
              <Table.Row>
                  <Table.HeaderCell>Producto / Materia Prima</Table.HeaderCell>
                  <Table.HeaderCell textAlign='center'>Cantidad</Table.HeaderCell>
                  <Table.HeaderCell textAlign='center'>Costo por unidad</Table.HeaderCell>
                  <Table.HeaderCell textAlign='center'>Unidad de medida</Table.HeaderCell>
                  <Table.HeaderCell textAlign='center'>Acciones</Table.HeaderCell>
              </Table.Row>
            </Table.Header>
            <Table.Body>
                {this.itemsCompraRendered}
            </Table.Body>
          </Table>
          <center>
          <Button circular color='green' disabled = {!this.itemsAgregados || !this.state.comprobanteAgregado} onClick={this.registrarCompra}>
            <Icon name='save'></Icon>
            Registrar compra
          </Button>
          </center>
        </div>


      </div>
    )
  }


  registrarCompra() {
    let detallesFinal = [];
    for(let det of this.dataToSend.detalles) {
      if(det.tipo === 'materia_prima') {
        const cantidadInt = parseInt(det.cantidad);
        const costoInt = parseFloat(det.costo);
        //const descuentoInt = parseInt(det.descuento);
        const detNuevo = {id_materia_prima: det.id, cantidad: cantidadInt, monto: costoInt};
        detallesFinal.push(detNuevo);
      } else {
        const cantidadInt = parseInt(det.cantidad);
        const costoInt = parseFloat(det.costo);
        //const descuentoInt = parseInt(det.descuento);
        const detNuevo = {id_producto: det.id, cantidad: cantidadInt, monto: costoInt};
        detallesFinal.push(detNuevo);
      }
      this.props.removeItemBisexual(det);
    }

    this.dataToSend = { proveedor: this.props.proveedorSeleccionado.id, detalles: detallesFinal, comprobante: this.comprobante };
    console.log(this.dataToSend)
    this.props.postCompra(this.dataToSend)
    .then(data => {
        this.renderMensaje(data);
    }, err => {
      console.log(err);
    })

  }

  renderProd()
  {
      const itemsRendered = this.props.productos.map(p => (
        this.renderItemProd(p))
      );

      return(
        <div id="itemGroup" style={{overflow: 'auto', maxHeight: 500 }}>
        <Table celled>
          <Table.Header>
          <Table.Row>
              <Table.HeaderCell>Nombre<Icon link name='sort' onClick={()=>this.ordenarProd('nombre')}/></Table.HeaderCell>
              <Table.HeaderCell textAlign='center'>Stock mínimo</Table.HeaderCell>
              <Table.HeaderCell textAlign='center'>Stock actual</Table.HeaderCell>
              <Table.HeaderCell textAlign='center'>Unidad de medida</Table.HeaderCell>
              <Table.HeaderCell textAlign='center'>Acciones</Table.HeaderCell>
          </Table.Row>
        </Table.Header>
        <Table.Body>
          {itemsRendered}
          </Table.Body>
        </Table>
      </div>
      )
  }

  showSeleccionProveedor(){
    return(
      <Grid>
        <Grid.Row verticalAlign='middle'>
          <Grid.Column width={8}>
            <Grid>
              <Grid.Row centered verticalAlign='middle'>
                {
                (this.props.proveedorSeleccionado)
                ?
                <Segment>
                  <h3>Proveedor seleccionado: </h3>
                  <Label size='huge' color='grey' content={this.props.proveedorSeleccionado.nombre} />
                  <h4>Ingrese número de comprobante: </h4>
                  <Input name='comprobante' id='comprobante' onChange={this.handleChange}/>
                </Segment>
                :
                <Message info>
                    <Message.Header>Seleccione un proveedor</Message.Header>
                </Message>
                }
              </Grid.Row>
              <Grid.Row centered>
                <center>
                  <Button circular color='green' onClick={() => this.props.openModal({
                          header: "Seleccionar Proveedor",
                          content: <ProveedoresModal />
                    })}
                  >
                    <Icon name='user circle' />
                    Seleccionar proveedor
                  </Button>
                </center>
              </Grid.Row>
            </Grid>
          </Grid.Column>
          <Grid.Column width={8}>
            {this.showButtonCompra()}
          </Grid.Column>
        </Grid.Row>
      </Grid>
    )
  }

  showAllProd()
  {
    if(this.state.compraProd)
    {
      return(
        this.renderProd()
      )
    }
  }

  render() {

    this.itemsCompraRendered = this.props.itemsTable.map(detalle => {

            let encontroDetalle = false;
            this.dataToSend.detalles.map((v,i) => {
              if(v.id === detalle.id && v.tipo === detalle.tipo) {
                encontroDetalle = true;
            }});
            if(encontroDetalle == false) {
              this.dataToSend.detalles.push(detalle);
            }
            return this.renderItemCompra(detalle);
    });



    return(

      <body style={{ overflow: 'auto' }}>
      <div id="DivPrincipal">

        <div id="MenuTop">
          <MenuPrincipal/>
        </div>
        <div className = "row">
          <div id="MenuLateral">
            {(this.userLogeado) ? <MenuEmprendedor/> : null}
           </div>
          <div style={{"paddingTop":"61px", marginLeft:"210px"}}>

            <Grid container centered>
              <Grid.Row>
                <Grid.Column width={16}>
                  <center>
                    <h2>
                        Nueva Compra
                    </h2>
                  </center>
                </Grid.Column>
              </Grid.Row>

              <Divider/>
              <Grid.Row centered>

                  <Grid.Column centered width={10}>
                      {this.showSeleccionProveedor()}
                  </Grid.Column>

              </Grid.Row>

              <Divider/>
              <Grid.Row>
                <Grid.Column width='16'>
                  { this.renderTableCompra()}
                </Grid.Column>
              </Grid.Row>
              <Grid.Row>
                <Grid.Column width='16'>
                  <Segment basic>
                        {this.showAllMP()}
                        {this.showAllProd()}
                  </Segment>
                </Grid.Column>
              </Grid.Row>

            </Grid>

            <ModalManager />

            {/* Este es el modal para el mensaje */}
            <Modal
              open={this.state.messageOpen}
              onClose={this.handleMessageConfirm}
              size='small'
          >
              <Header icon='browser' content='Aviso' />
              <Modal.Content>
              <h3>{this.state.messageInfo}</h3>
              </Modal.Content>
              <Modal.Actions>
              <Button color='green' onClick={this.handleMessageConfirm} inverted>
                  <Icon name='checkmark' /> Aceptar
              </Button>
              </Modal.Actions>
          </Modal>
        </div>
      </div>
    </div>

      </body>

    )
  }

  renderMP()
  {
    const itemsRendered = this.props.mpsEmp.map(mp => (
      this.renderItem(mp))
    );

    return(
      <div id="itemGroup" style={{overflow: 'auto', maxHeight: 500 }}>
        <Table celled>
          <Table.Header>
          <Table.Row>
              <Table.HeaderCell>Nombre<Icon link name='sort' onClick={()=>this.ordenarMP('nombre')}/></Table.HeaderCell>
              <Table.HeaderCell textAlign='center'>Stock Mínimo</Table.HeaderCell>
              <Table.HeaderCell textAlign='center'>Stock actual</Table.HeaderCell>
              <Table.HeaderCell textAlign='center'>Unidad de medida</Table.HeaderCell>
              <Table.HeaderCell textAlign='center'>Acciones</Table.HeaderCell>
          </Table.Row>
        </Table.Header>
        <Table.Body>
          {itemsRendered}
          </Table.Body>
        </Table>
      </div>
    )
  }

  ordenarMP(atributo) {
    switch(atributo) {
      case 'nombre': 
        if(this.ordenFiltros.nombreAsc) {
          this.props.mpsEmp.sort(function(a, b){
            return a.nombre.localeCompare(b.nombre);
          });
        }
        else {
          this.props.mpsEmp.sort(function(a, b){
            return b.nombre.localeCompare(a.nombre);
          })
        }
        this.ordenFiltros.nombreAsc = !this.ordenFiltros.nombreAsc;
        this.setState({setFiltro: !this.state.setFiltro});
        break;
    }
  }

  ordenarProd(atributo) {
    switch(atributo) {
      case 'nombre': 
        if(this.ordenFiltros.nombreAsc) {
          this.props.productos.sort(function(a, b){
            return a.nombre.localeCompare(b.nombre);
          });
        }
        else {
          this.props.productos.sort(function(a, b){
            return b.nombre.localeCompare(a.nombre);
          })
        }
        this.ordenFiltros.nombreAsc = !this.ordenFiltros.nombreAsc;
        this.setState({setFiltro: !this.state.setFiltro});
        break;
    }
  }

}

const mapStateToProps = state => ({
  mpsEmp: state.mpsEmp.items,
  productos: state.productos.items,
  itemsTable: state.tableData.items,
  proveedorSeleccionado: state.proveedorSeleccionado.item
});

export default withRouter(connect(mapStateToProps, { getTodasMPEmp, getProductos, openModal, delMateriaPrimaEmp, removeItemBisexual, postCompra })(MisCompras));
