import React, { Component } from 'react'
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { Button, Header, Grid, Table, Message, Divider, Segment, Icon } from 'semantic-ui-react'
import MenuPrincipal from './MenuPrincipal';
import MenuEmprendedor from './MenuEmprendedor';
import {getUser} from '../config/configs';
import ModalManager from './ModalManager';
import { openModal } from '../actions/modalActions';
import { getVentas } from '../actions/ventasActions';
import { ModalMisVentas } from './ModalMisVentas';
import FiltroVentas from './FiltroVentas';

class MisVentas extends Component
{

    constructor(props){
        super(props);
        this.state = { cargado: false, ventas: null, messageOpen: false, messageInfo: null, tieneVentas: false, paginaActual: 1, setFiltro: true };
        this.userLogeado = (getUser()) ? getUser() : null;
        this.ordenFiltros = { idAsc: false, fechaAsc: true }
    }

    componentWillMount() {
        this.itemsRendered = [];
        this.ventas = [];
        this.obtenerVentas();
    }

    asignarColor(estado_id) {
        if(estado_id === 2) {
            return 'green';
        } else if(estado_id === 3) {
            return 'red';
        } else {
            return 'yellow'
        }
    }

    asignarIcon(estado_id) {
        if(estado_id === 2) {
            return 'check circle';
        } else if(estado_id === 3) {
            return 'times circle';
        } else {
            return 'exclamation circle'
        }
    }

    obtenerVentas() {

        this.props.getVentas(this.state.paginaActual)
        .then(data => {
            
            this.setState({cargado: true});

            if(data.datos.data && data.datos.data.length > 0) {

                for(let item of data.datos.data) {
                        this.ventas.push(item);
                }

                this.setState( { ventas: this.ventas, tieneVentas : true});
            }  
            
        }, err => {
        console.log(err);
        }) ;
    }

    renderItem(venta)
    {
        const date = new Date(venta.fecha_hora);
        const fechaFormateada = date.toLocaleString();

        return(
            <Table.Row  key={venta.id}>
            <Table.Cell>
            <Header as='h4'>
                <Header.Content>
                {venta.id}
                </Header.Content>
            </Header>
            </Table.Cell>
            <Table.Cell textAlign='center'>{fechaFormateada}</Table.Cell>
            <Table.Cell textAlign='center'>{venta.detalles[0].producto_nombre}</Table.Cell>
            <Table.Cell textAlign='center'>{venta.detalles[0].cantidad}</Table.Cell>
            <Table.Cell textAlign='center'>${venta.monto}</Table.Cell>
            <Table.Cell textAlign='center'>{venta.comprador.username}</Table.Cell>
            <Table.Cell textAlign='center'>                
                    <Icon name={this.asignarIcon(venta.estado_id)} color={this.asignarColor(venta.estado_id)} size='big' />
            </Table.Cell>

            <Table.Cell textAlign='center' content>
                <Button color="orange" circular icon="eye"
                    onClick={() => 
                    this.props.openModal({
                        header: `Venta número ${venta.id}`,
                        content: <ModalMisVentas data={venta}/>
                    })}
                />
            </Table.Cell>
        </Table.Row>
        )
    }

    render()
    {   
        // Si tiene ventas
        if(this.state.cargado && this.props.ventas) {
            // Si no tiene ventas viene un vector vacio en this.props.ventas, si tiene ventas viene un "data" que es un vector con las mismas
            if(this.props.ventas.data) {
                this.itemsRendered = this.props.ventas.data.map(venta => {
                
                    return this.renderItem(venta);
                })
            } else {
                this.itemsRendered = this.props.ventas.map(venta => {
                
                    return this.renderItem(venta);
                })
            }
            
        }

        return (
            <div className="DivPrincipal" style={{overflow:'auto'}}>
                <div id="MenuTop">
                    <MenuPrincipal/>
                </div>
                <div className = "row">
                    <div id="MenuLateral">
                    {(this.userLogeado) ? <MenuEmprendedor/> : null}
                    </div>

                    <div style={{"paddingTop":"61px", marginLeft:"210px"}}>
                        <Grid centered>
                            <Grid.Row>
                                <Grid.Column width={12}>
                                    <Segment loading={!this.state.cargado}>
                                        {
                                        (!this.state.tieneVentas) ?
                                        <Grid centered>
                                            <Grid.Row>
                                                <Grid.Column width={10}>
                                                    <center>
                                                        <Message warning size='massive'>
                                                            <Message.Header>No posee ventas realizadas aún</Message.Header>
                                                        </Message>
                                                    </center>
                                                </Grid.Column>
                                            </Grid.Row>
                                        </Grid>
                                        :
                                        <Grid>
                                            <Grid.Row>
                                                <Grid.Column width={16}>
                                                    <center>
                                                        <h2>
                                                            Mis ventas
                                                        </h2>
                                                    </center>
                                                </Grid.Column>
                                            </Grid.Row>
                                            <Divider/>

                                            <Grid.Row>
                                                <Grid.Column width={16}>
                                                    {this.renderTable()}
                                                </Grid.Column>
                                            </Grid.Row>
                                        </Grid>
                                        }
                                    </Segment>
                                </Grid.Column>
                            </Grid.Row>
                        </Grid>
                    </div>
                    <ModalManager/>
                </div>
            </div>
        )
    }

    renderTable() {
        return(
            <Grid>
                <Grid.Row centered>
                    <Grid.Column width={16}>
                        <center>
                            <div>
                                <h3>Ventas realizadas</h3>
                                <br/>
                            </div>
                        </center>
                        <FiltroVentas filtro= 'Ventas'/>
                        <Divider/>
                    </Grid.Column>
                </Grid.Row>
                <Grid.Row centered>
                    <Grid.Column width={16}>
                        <div>
                            <div style={{overflow: 'auto', maxHeight: 500 }}>
                                <Table celled>
                                    <Table.Header>
                                        <Table.Row>
                                            <Table.HeaderCell>ID<Icon link name='sort' onClick={() => this.ordenar('id')}/></Table.HeaderCell>
                                            <Table.HeaderCell textAlign='center'>Fecha (MM/DD/AAAA)<Icon link name='sort' onClick={() => this.ordenar('fecha')}/></Table.HeaderCell>
                                            <Table.HeaderCell textAlign='center'>Producto</Table.HeaderCell>
                                            <Table.HeaderCell textAlign='center'>Cantidad</Table.HeaderCell>
                                            <Table.HeaderCell textAlign='center'>Monto total</Table.HeaderCell>
                                            <Table.HeaderCell textAlign='center'>Comprador</Table.HeaderCell>
                                            <Table.HeaderCell textAlign='center'>Estado</Table.HeaderCell>
                                            <Table.HeaderCell textAlign='center'>Acciones</Table.HeaderCell>
                                        </Table.Row>
                                    </Table.Header>
                                    <Table.Body>
                                        {this.itemsRendered}
                                    </Table.Body>
                                </Table>
                            </div>
                        </div>
                    </Grid.Column>
                </Grid.Row>
            </Grid>
        
        )
    }

    ordenar(atributo) {

        switch(atributo) {
            case 'id':
            if(this.ordenFiltros.idAsc){
              this.props.ventas.data.sort(function(a, b){
                return a.id - b.id;
                });
              } else {
                this.props.ventas.data.sort(function(a, b){
                return b.id - a.id;
               })
             }
            this.ordenFiltros.idAsc = !this.ordenFiltros.idAsc;
            this.setState({ setFiltro: !this.state.setFiltro })
            break;
           
          case 'fecha':
            if(this.ordenFiltros.fechaAsc) {
              this.props.ventas.data.sort(function(a, b){
                return b.fecha_hora.localeCompare(a.fecha_hora);
              });
            }
            else {
              this.props.ventas.data.sort(function(a, b){
                return a.fecha_hora.localeCompare(b.fecha_hora);
              })
            }
            this.ordenFiltros.fechaAsc = !this.ordenFiltros.fechaAsc;
            this.setState({setFiltro: !this.state.setFiltro});
            break;
        }
      }
}

const mapStateToProps = state => ({
    ventas: state.ventasYComprasCliente.resultVentas
});

export default withRouter(connect(mapStateToProps, { getVentas, openModal })(MisVentas));