import React, { Component } from 'react';
import { Grid, Divider, Segment, Card, Input, Button, Item, Label, Icon } from 'semantic-ui-react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { closeModal } from '../actions/modalActions';
import logoMercadoPago from '../assets/MercadoPago.png';

export class ModalMisVentas extends Component {

    constructor(props) {
        super(props);
        this.data = (props.data) ? props.data : null;
    }

    componentWillMount(){
        this.venta = this.data;
    }

    renderCards(){
        if(this.venta.pagos.metodo_pago !== '') {
            if(this.venta.pagos.metodo_pago === 'credit_card') {
                this.venta.pagos.metodo_pago = 'Tarjeta de crédito'
            } else if (this.venta.pagos.metodo_pago === 'debit_card') {
                this.venta.pagos.metodo_pago = 'Tarjeta de débito'
            } else {
                this.venta.pagos.metodo_pago = 'Efectivo'
            }
        }


        return(
            <Card.Group centered itemsPerRow={3}>
                <Card color='orange'>
                    <Card.Content>
                        <Card.Header>Información del comprador</Card.Header>
                    </Card.Content>
                    <Card.Content>
                        <Card.Description>
                            <strong>Nombre de usuario: </strong> <br/> {this.venta.comprador.username} <br/><br/>
                            <strong>Mail: </strong> <br/> {this.venta.comprador.email}
                        </Card.Description>
                    </Card.Content>
                </Card>
                <Card color='orange'>
                    <Card.Content>
                        <Card.Header>Información de venta y pago</Card.Header>
                    </Card.Content>
                    <Card.Content>
                        <Card.Description>
                            <strong>Estado de venta: </strong> <br/> {this.venta.estado} <br/><br/>
                            <strong>Estado de pago: </strong> <br/> {this.venta.pagos.estado} <br/><br/>
                            <strong>Método de pago: </strong> <br/> {(this.venta.pagos.metodo_pago !== '') ? this.venta.pagos.metodo_pago : 'No aplica'} <br/><br/>
                            <strong>Tarjeta: </strong> <br/> {(this.venta.pagos.metodo_pago !== '') ? (this.venta.pagos.tarjeta.tipo + ' terminada en ' + this.venta.pagos.tarjeta.ultimos_cuatro_digitos + ' de titular: ' + this.venta.pagos.tarjeta.nombre_titular)  : 'No aplica'} <br/><br/>
                            <strong>Monto total: </strong> <br/> {this.venta.monto}
                        </Card.Description>
                    </Card.Content>
                </Card>
                <Card color='orange'>
                    <Card.Content>
                        <Card.Header>Detalle de la venta</Card.Header>
                    </Card.Content>
                    <Card.Content>
                        <Card.Description>
                            <strong>Producto: </strong> <br/> {this.venta.detalles[0].producto_nombre} <br/><br/>
                            <strong>Precio individual: </strong> <br/> ${this.venta.detalles[0].precio_individual} <br/><br/>
                            <strong>Cantidad : </strong> <br/> {this.venta.detalles[0].cantidad} <br/><br/>
                        </Card.Description>
                    </Card.Content>
                </Card>
            </Card.Group>
        )
    }

    render() {
        return (
            <div>
                <Grid>
                    <Grid.Row>
                        <Grid.Column width={16}>
                            <Segment size ='big'>
                                <Grid verticalAlign='middle'>
                                    <Grid.Row centered>
                                        <Grid.Column width={16}>
                                            {this.renderCards()}
                                        </Grid.Column>
                                    </Grid.Row>
                                </Grid>
                            </Segment>
                        </Grid.Column>
                    </Grid.Row>

                </Grid>
            </div>
        )
    }

}

const mapStateToProps = state => ({
    
});

export default withRouter(connect(mapStateToProps, { closeModal })(ModalMisVentas));
