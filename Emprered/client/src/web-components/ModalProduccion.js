import React, { Component } from 'react'
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { getProductos } from '../actions/productosActions';
import { openModal, closeModal } from '../actions/modalActions';
import { Input, Segment, Button, Header, Grid, Form, TextArea, Modal, Icon, Confirm, Divider } from 'semantic-ui-react'
import {Table} from 'semantic-ui-react';
import SearchStandard from './SearchStandard';
import { removeItem, insertItem } from '../actions/tableFillActions';
import { postProduccion, getProducciones, getProduccion } from '../actions/produccionActions';

class ModalProduccion extends Component {

    constructor(props) {
        super(props);
        this.idProduccion = props.idProduccion;
        this.produccionSeleccionada = props.produccionSeleccionada;
    }

    componentWillMount() {
        this.clearItemsTable();
        this.itemsProduccionRendered = null;
        this.dataToSend = { id_produccion: this.produccionSeleccionada.id, descripcion: null, estado: null, detalles: [] };
        this.obtenerDetalles();
        this.descripcionProduccion = this.produccionSeleccionada.descripcion;
        this.registrarProduccionEnProceso = this.registrarProduccionEnProceso.bind(this);
        this.registrarProduccionTerminada = this.registrarProduccionTerminada.bind(this);
        this.anularProduccion = this.anularProduccion.bind(this);
        this.actualizarProduccion = this.actualizarProduccion.bind(this);
        this.state = {
            cargado: false, open: false, produccionEstaEnProceso: false, produccionEstaPendiente: false, produccionEstaTerminada: false,
            produccionSePuedeAnular: false, messageInfo: null, messageOpen: null
        };
        this.detalles = null;
    }

    showConfirmDialog = () => {
        this.setState({open: true});
    }
    handleConfirm = () => {
        this.setState({open: false});
        this.anularProduccion();
    }
    handleCancel = () => this.setState({open: false})

    handleChange = (e, {name, prodseleccionado, value}) => {
        // Si lo que cambia es la descripcion lo trato de una forma
        if(name === 'descripcion') {
            this.dataToSend.descripcion = value
        } else {

            // Si lo que cambia son los detalles:
            // Lo trackeo en los detalles a enviar y le agrego lo que haya escrito el usuario
            const index = this.detalles.map(e => e.id).indexOf(prodseleccionado);
            if(index !== -1) {
              this.detalles[index] = {
                  ...this.detalles[index],
                  [name]: value
              }
            }

        }
    }

    handleMessageConfirm = () => {
        this.setState({messageOpen: false});
        this.props.closeModal();
    }

    renderMensaje = (estado, data) => {
        let mensaje = '';
        if(data.estado === 'OK') {
            if(estado === 2) {
                this.setState({produccionEstaEnProceso: true, produccionEstaPendiente: false, produccionSePuedeAnular: true});
                mensaje = 'Producción puesta en proceso';
            } else if (estado === 3) {
                this.setState({produccionEstaEnProceso: false, produccionEstaTerminada: true, produccionSePuedeAnular: false});
                mensaje = 'Producción terminada';
            } else if (estado === 4) {
                this.setState({produccionEstaTerminada: true});
                mensaje = 'Produccion anulada';
            } else {
                this.setState({produccionEstaPendiente: true});
                mensaje = 'Produccion actualizada';
            }
        }
        else {
            mensaje = data.mensaje;
        }

        this.setState({messageInfo: mensaje, messageOpen: true});
    }

    clearItemsTable() {
        for(let item of this.props.itemsTable) {
            this.props.removeItem(item);
        }
    }

    obtenerDetalles() {
        const id = this.idProduccion;
        this.props.getProduccion(id)
        .then(data => {
            this.asignarBooleans(this.produccionSeleccionada.estado_id);
            this.dataToSend = {id_produccion: this.idProduccion, descripcion: data.datos.descripcion, estado: data.datos.estado_id};
            this.detalles = data.datos.detalles;
            for(let item of this.detalles) {
                const newItem = {id: item.producto_id, producto_id: item.producto_id, cantidad: item.cantidad, unidad_medida: item.unidad_medida, producto:item.producto, };
                console.log(newItem);
                this.props.insertItem(newItem);
            }
            this.setState({cargado:true});
        }, err => {
            console.log(err);
        })

    }

    asignarBooleans(estado){
        if(estado === 1) {
            this.setState({produccionEstaEnProceso: false, produccionEstaPendiente: true, produccionEstaTerminada: false,
                produccionSePuedeAnular: true})
        } else if (estado === 2) {
            this.setState({produccionEstaEnProceso: true, produccionEstaPendiente: false, produccionEstaTerminada: false,
                produccionSePuedeAnular: true})
        } else if (estado === 3) {
            this.setState({produccionEstaEnProceso: false, produccionEstaPendiente: false, produccionEstaTerminada: true,
                produccionSePuedeAnular: false})
        } else {
            this.setState({produccionEstaEnProceso: false, produccionEstaPendiente: false, produccionEstaTerminada: true,
                produccionSePuedeAnular: false})
        }
    }

    renderItemProduccion(prod)
    {
        return(
            <Table.Row  key={prod.id}>
            <Table.Cell>
            <Header as='h4'>
                <Header.Content>
                {prod.producto}
                </Header.Content>
            </Header>
            </Table.Cell>
            <Table.Cell textAlign='center'>
            <Input disabled={this.state.produccionEstaEnProceso  || this.state.produccionEstaTerminada} prodseleccionado={prod.id} name="cantidad" type="number" step="any"
                        labelPosition='right' placeholder='Cantidad' label={{ basic: true, content: prod.unidad_medida }}
                        onChange={this.handleChange}
                        defaultValue={(prod.cantidad) ? prod.cantidad : 0}
            >
            </Input>
            </Table.Cell>

            <Table.Cell textAlign='center' content>
            <Button disabled={this.dataToSend.estado === 2  || this.dataToSend.estado === 3 || this.dataToSend.estado === 4} negative circular icon="times"
                    onClick = {() => {
                        // Si el usuario elimina un item de la seleccion lo saco de los detalles a enviar
                        const index = this.detalles.map(e => e.producto_id).indexOf(prod.id);
                        if(index != -1) {
                        this.detalles.splice(index,1);
                        this.props.removeItem(prod);
                        }
                    }}
                    />
            </Table.Cell>
        </Table.Row>
        )

    }

    actualizarProduccion() {

        let detallesFinal = [];
        for(let det of this.detalles) {
            const cantidadInt = parseInt(det.cantidad);
            const detNuevo = null;
            if(det.producto_id) {
                detNuevo = {id: det.id, producto_id: det.producto_id, cantidad: cantidadInt};
            } else {
                detNuevo = {id: det.id, producto_id: det.id, cantidad: cantidadInt};
            }
            detallesFinal.push(detNuevo);
          // this.props.removeItem(det);
        }

        this.dataToSend = { id_produccion: this.props.produccionSeleccionada.id, descripcion: this.dataToSend.descripcion, estado: 1, detalles: detallesFinal };
        console.log(this.dataToSend);
        this.props.postProduccion(this.dataToSend)
        .then(data => {

            this.renderMensaje(1, data);
        }, err => {
          console.log(err);
        });
    }

    registrarProduccionEnProceso() {

        let detallesFinal = [];
        for(let det of this.detalles) {
            const cantidadInt = parseInt(det.cantidad);
            const detNuevo = null;
            if(det.producto_id) {
                detNuevo = {id: det.id, producto_id: det.producto_id, cantidad: cantidadInt};
            } else {
                detNuevo = {id: det.id, producto_id: det.id, cantidad: cantidadInt};
            }
            detallesFinal.push(detNuevo);
          // this.props.removeItem(det);
        }

        this.dataToSend = { id_produccion: this.props.produccionSeleccionada.id, descripcion: this.dataToSend.descripcion, estado: 2, detalles: detallesFinal };
        console.log(this.dataToSend);
        this.props.postProduccion(this.dataToSend)
        .then(data => {

            this.renderMensaje(2, data);
        }, err => {
          console.log(err);
        });
    }

      registrarProduccionTerminada() {

        let detallesFinal = [];
        for(let det of this.detalles) {
            const cantidadInt = parseInt(det.cantidad);
            const detNuevo = null;
            if(det.producto_id) {
                detNuevo = {id: det.id, producto_id: det.producto_id, cantidad: cantidadInt};
            } else {
                detNuevo = {id: det.id, producto_id: det.id, cantidad: cantidadInt};
            }
            detallesFinal.push(detNuevo);
          // this.props.removeItem(det);
        }

        this.dataToSend = { id_produccion: this.props.produccionSeleccionada.id, descripcion: this.dataToSend.descripcion, estado: 3, juanipls: true, detalles: detallesFinal };
        const finalData = { id_produccion: this.props.produccionSeleccionada.id, estado: 3, descripcion: this.dataToSend.descripcion };
        console.log(finalData);

        this.props.postProduccion(finalData)
        .then(data => {

            this.renderMensaje(3, data);
        }, err => {
          console.log(err);
        });
    }

    anularProduccion() {

        let detallesFinal = [];
        for(let det of this.detalles) {
            const cantidadInt = parseInt(det.cantidad);
            const detNuevo = null;
            if(det.producto_id) {
                detNuevo = {id: det.id, producto_id: det.producto_id, cantidad: cantidadInt};
            } else {
                detNuevo = {id: det.id, producto_id: det.id, cantidad: cantidadInt};
            }
            detallesFinal.push(detNuevo);
          // this.props.removeItem(det);
        }

        this.dataToSend = { id_produccion: this.props.produccionSeleccionada.id, descripcion: this.dataToSend.descripcion, estado: 4, juanipls: true, detalles: detallesFinal };
        const finalData = { id_produccion: this.props.produccionSeleccionada.id, estado: 4, descripcion: this.dataToSend.descripcion };
        console.log(finalData);

        this.props.postProduccion(finalData)
        .then(data => {

            this.renderMensaje(4, data);
        }, err => {
          console.log(err);
        });
    }

    render()
    {
        if(this.state.cargado === true) {
            this.itemsProduccionRendered = this.props.itemsTable.map(producto => {
                // Cuando el usuario selecciona una MP la agrego al vector de detalles a enviar solo si no esta repetida
                // Hago este map para poder trackear la mp por su title
                const index = this.detalles.map(e => e.producto_id).indexOf(producto.id);
                if(index == -1) {

                    this.detalles.push(producto);
                }
                return this.renderItemProduccion(producto);
            });

        }
            const estadoProduccion = this.dataToSend.estado;

            return(
                <div>
                    <Grid>
                        <Grid.Row>
                            <Grid.Column width={16}>
                                <Segment loading={!this.state.cargado}>
                                    <Grid container centered>
                                        { (estadoProduccion === 1) ?
                                        <Grid.Row centered>
                                            <Grid.Column width='4'>
                                                <center>
                                                <h4>Agregue productos o modifique la producción seleccionada</h4>
                                                <SearchStandard filtro="ProductosFabricados"/>
                                                </center>
                                            </Grid.Column>
                                        </Grid.Row>
                                        : null
                                        }

                                        <Grid.Row>
                                            <center>
                                                <div>
                                                <h3>Detalles de la producción</h3>
                                                </div>
                                            </center>
                                        </Grid.Row>

                                        <Grid.Row>
                                            <Grid.Column width='16'>
                                                <div style={{overflow: 'auto', maxHeight: 500 }}>
                                                <Table celled color="orange">
                                                    <Table.Header>
                                                    <Table.Row>
                                                        <Table.HeaderCell>Producto </Table.HeaderCell>
                                                        <Table.HeaderCell textAlign='center'>Cantidad</Table.HeaderCell>
                                                        <Table.HeaderCell textAlign='center'>Acciones</Table.HeaderCell>
                                                    </Table.Row>
                                                    </Table.Header>
                                                    <Table.Body>
                                                        {this.itemsProduccionRendered}
                                                    </Table.Body>
                                                </Table>
                                                </div>
                                            </Grid.Column>
                                        </Grid.Row>

                                        <Divider/>

                                        <Grid.Row>
                                            <center>
                                            <h4>Agregue o modifique la descripción de la producción</h4>
                                            </center>
                                        </Grid.Row>

                                        <Grid.Row>
                                            <Grid.Column width={16}>
                                                <center>
                                                <Form.Field
                                                    id='form-field-descripcion'
                                                    name='descripcion'
                                                    control={TextArea}
                                                    defaultValue={this.dataToSend.descripcion}
                                                    onChange={this.handleChange}
                                                    disabled={this.state.produccionEstaEnProceso || this.state.produccionEstaTerminada}
                                                    style={{width:'100%' , bottom:'0px'}}
                                                />
                                                </center>
                                            </Grid.Column>
                                        </Grid.Row>

                                        <Divider/>

                                        <Grid.Row>
                                            <center>
                                            <Button color='blue'
                                                disabled = {!this.state.produccionEstaPendiente}
                                                onClick={this.actualizarProduccion}>
                                                Actualizar
                                            </Button>
                                            <Button color='orange'
                                                disabled = {!this.state.produccionEstaPendiente}
                                                onClick={this.registrarProduccionEnProceso}>
                                                Poner en proceso
                                            </Button>
                                            <Button color='green'
                                                disabled = {!this.state.produccionEstaEnProceso}
                                                onClick={this.registrarProduccionTerminada}>
                                                Terminar producción
                                            </Button>
                                            <Button color='red'
                                                disabled = {!this.state.produccionSePuedeAnular}
                                                onClick={this.showConfirmDialog}>
                                                Anular producción
                                            </Button>
                                            </center>

                                        </Grid.Row>


                                    </Grid>
                                </Segment>
                            </Grid.Column>
                        </Grid.Row>
                    </Grid>
                    

                    {/* Este es el modal para el mensaje */}
                    <Modal
                        open={this.state.messageOpen}
                        onClose={this.handleMessageConfirm}
                        size='small'
                    >
                        <Header icon='browser' content='Aviso' />
                        <Modal.Content>
                        <h3>{this.state.messageInfo}</h3>
                        </Modal.Content>
                        <Modal.Actions>
                        <Button color='green' onClick={this.handleMessageConfirm} inverted>
                            <Icon name='checkmark' /> Aceptar
                        </Button>
                        </Modal.Actions>
                    </Modal>

                    <Confirm
                        open={this.state.open}
                        header='Anular Producción'
                        content='¿Está seguro de que desea anular esta producción?'
                        cancelButton='No'
                        confirmButton='Sí'
                        onCancel={this.handleCancel}
                        onConfirm={this.handleConfirm}
                    />

                    <br/>
                </div>

            )

    }
}

const mapStateToProps = state => ({
    productos: state.productos.items,
    itemsTable: state.tableData.items,
    produccion: state.resultProduccion
  });

  export default withRouter(connect(mapStateToProps, {  getProductos, openModal, closeModal, removeItem, postProduccion, insertItem, getProducciones, getProduccion })(ModalProduccion));
