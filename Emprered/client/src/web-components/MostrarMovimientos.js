import React, { Component } from 'react'
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { getMovimientos, descargarReporteMovimientos } from '../actions/stockActions';
import { Button, Header, Segment, Icon, Grid, Divider } from 'semantic-ui-react'
import {Table} from 'semantic-ui-react';
import MenuPrincipal from './MenuPrincipal';
import MenuEmprendedor from './MenuEmprendedor';
import FiltrosProducciones from './FiltroProducciones.js';
import { getUser } from '../config/configs';

 class MostrarMovimientos extends Component
{
    
    componentWillMount()
    {
      this.userLogeado = (getUser()) ? getUser() : null;
      this.itemsHistorialRendered = [];
      this.state = {
        cargado: false,
        setFiltro: false
      }

      this.props.getMovimientos()
      .then(data => {
        this.obtenerHistorial();
        this.setState({cargado:true});
      }, err => {
        console.log(err);
      });

      this.downloadFile = this.downloadFile.bind(this);
      this.ordenFiltros = { nombreAsc: true, fechaAsc: false, movimientoAsc: false, motivoAsc: true }
    }

    obtenerHistorial() {
        
          let vectorFinalMov = [];
          for (let mov of this.props.movimientos_stock.movimientos_stock) {
            const date = new Date(mov.fecha_hora);
            const dateString= date.toLocaleString();
            const movConFechaFormateada = {...mov, fecha_hora: dateString};
            vectorFinalMov.push(movConFechaFormateada);
          }
          this.itemsHistorialRendered = vectorFinalMov.map(mov => {
            return this.renderItemHistorial(mov);
          })
        }

  renderItemHistorial(mov)
  {
      return(
        <Table.Row  key={mov.id}>
        <Table.Cell>
          <Header as='h4'>
           <Header.Content>
              {(mov.producto)? mov.producto : mov.materia_prima}
           </Header.Content>
          </Header>
        </Table.Cell>
        <Table.Cell textAlign='center'>{mov.fecha_hora}</Table.Cell>
        <Table.Cell textAlign='center'>{mov.tipo_movimiento}</Table.Cell>
        <Table.Cell textAlign='center'>{mov.cantidad}</Table.Cell>
        <Table.Cell textAlign='center'>{mov.unidad_medida}</Table.Cell>
        <Table.Cell textAlign='center'>{(mov.motivo)}</Table.Cell>
      </Table.Row>
      )
  }

  downloadFile() {
    this.props.descargarReporteMovimientos(document)
  }

    renderHistorial()
  {
    return(

      <div style={{overflow: 'auto', maxHeight: 500 }}>
        <Table celled color="orange">
          <Table.Header>
            <Table.Row>
                <Table.HeaderCell>Nombre<Icon link name='sort' onClick={() => this.ordenar('nombre')}/></Table.HeaderCell>
                <Table.HeaderCell textAlign='center'>Fecha (DD/MM/AAAA)<Icon link name='sort' onClick={() => this.ordenar('fecha')}/></Table.HeaderCell>
                <Table.HeaderCell textAlign='center'>Tipo de Movimiento<Icon link name='sort' onClick={() => this.ordenar('movimiento')}/></Table.HeaderCell>
                <Table.HeaderCell textAlign='center'>Cantidad</Table.HeaderCell>
                <Table.HeaderCell textAlign='center'>Unidad de medida</Table.HeaderCell>
                <Table.HeaderCell textAlign='center'>Motivo<Icon link name='sort' onClick={() => this.ordenar('motivo')}/></Table.HeaderCell>
            </Table.Row>
          </Table.Header>
          <Table.Body>
              {this.itemsHistorialRendered}
          </Table.Body>
        </Table>

      </div>
    )
  }

  ordenar(atributo){
    switch(atributo){
        
      case 'nombre':
        if(this.ordenFiltros.nombreAsc){
          this.props.movimientos_stock.movimientos_stock.sort(function(a, b){

                if(a.producto)
                {
                    if(b.producto)
                    {
                      return a.producto.localeCompare(b.producto);
                    }
                    return a.producto.localeCompare(b.materia_prima);
                }
                else
                {
                  if(b.producto)
                  {
                    return a.materia_prima.localeCompare(b.producto);
                  }
                  return a.materia_prima.localeCompare(b.materia_prima);
                }

              });
          } else {
            this.props.movimientos_stock.movimientos_stock.sort(function(a, b){
             
              if(b.producto)
                {
                    if(a.producto)
                    {
                      return b.producto.localeCompare(a.producto);
                    }
                    return b.producto.localeCompare(a.materia_prima);
                }
                else
                {
                  if(a.producto)
                  {
                    return b.materia_prima.localeCompare(a.producto);
                  }
                  return b.materia_prima.localeCompare(a.materia_prima);
                }

           })
         }
        this.ordenFiltros.nombreAsc = !this.ordenFiltros.nombreAsc;
        this.setState({ setFiltro: !this.state.setFiltro })
        break;
       
      case 'fecha':
        if(this.ordenFiltros.fechaAsc) {
          this.props.movimientos_stock.movimientos_stock.sort(function(a, b){
            return b.fecha_hora.localeCompare(a.fecha_hora);
          });
        }
        else {
          this.props.movimientos_stock.movimientos_stock.sort(function(a, b){
            return a.fecha_hora.localeCompare(b.fecha_hora);
          })
        }
        this.ordenFiltros.fechaAsc = !this.ordenFiltros.fechaAsc;
        this.setState({setFiltro: !this.state.setFiltro});
        break;

      case 'movimiento':
        if(this.ordenFiltros.movimientoAsc) {
          this.props.movimientos_stock.movimientos_stock.sort(function(a, b){
            return a.tipo_movimiento.localeCompare(b.tipo_movimiento);
          });
        }
        else {
          this.props.movimientos_stock.movimientos_stock.sort(function(a, b){
            return b.tipo_movimiento.localeCompare(a.tipo_movimiento);
          })
        }
      this.ordenFiltros.movimientoAsc = !this.ordenFiltros.movimientoAsc;
      this.setState({setFiltro: !this.state.setFiltro});
      break;
      
      case 'motivo':
        if(this.ordenFiltros.motivoAsc) {
          this.props.movimientos_stock.movimientos_stock.sort(function(a, b){
            return a.motivo.localeCompare(b.motivo);
          });
        }
        else {
          this.props.movimientos_stock.movimientos_stock.sort(function(a, b){
            return b.motivo.localeCompare(a.motivo);
          })
        }
      this.ordenFiltros.motivoAsc = !this.ordenFiltros.motivoAsc;
      this.setState({setFiltro: !this.state.setFiltro});
      break;
    } 
  }

  render()
  {
    return(
      <div className="DivPrincipal" style={{ overflow: 'auto' }}>
            <div id="MenuTop">
              <MenuPrincipal/>
            </div>
            <div className = "row">
              <div id="MenuLateral">
                { this.userLogeado ? <MenuEmprendedor/> : null}
                <MenuEmprendedor/>
              </div>
              <div style={{"paddingTop":"61px", marginLeft:"210px"}}>
                <Grid centered>
                  <Grid.Row centered>
                    <Grid.Column width={14}>
                      <Grid>
                        <Grid.Row>
                          <Grid.Column width={16}>
                            <center>
                              <h2>
                                  Historial de Movimientos
                              </h2>
                            </center>
                          </Grid.Column>
                        </Grid.Row>

                        <Divider/>

                        <Grid.Row centered>
                          <Grid.Column width={16}>
                            <center>
                              <Button color="orange" circular
                                onClick= {this.downloadFile}
                              >
                                <Icon name='arrow circle down' />
                                Descargar reporte
                              </Button>
                            </center>
                          </Grid.Column>
                        </Grid.Row>

                        <Grid.Row>
                          <Grid.Column width={16}>
                            <Segment loading={!this.state.cargado}>
                              <Grid>
                                <Grid.Row>
                                  <Grid.Column width={16}>
                                    <Grid centered>
                                        <Grid.Row>
                                          <Grid.Column width={16} center>
                                            <FiltrosProducciones filtro='Movimientos'/>
                                          </Grid.Column>
                                        </Grid.Row>
                                      </Grid>
                                  </Grid.Column>
                                </Grid.Row>

                                <Grid.Row>
                                  <Grid.Column width={16}>
                                    {this.renderHistorial()}
                                  </Grid.Column>
                                </Grid.Row>
                              </Grid>
                            </Segment>
                          </Grid.Column>
                        </Grid.Row>

                      </Grid>
                    </Grid.Column>
                  </Grid.Row>
                </Grid>
              </div>
            </div>
        </div>
        )
  }
}

const mapStateToProps = state => ({
    movimientos_stock: state.stock.resultStock
  });

  export default withRouter(connect(mapStateToProps, { getMovimientos, descargarReporteMovimientos })(MostrarMovimientos));