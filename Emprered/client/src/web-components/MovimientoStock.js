import React, { Component } from 'react'
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { getTodasMPEmp } from '../actions/matPrimaEmpActions';
import { getProductos } from '../actions/productosActions';
import ModalManager from './ModalManager';
import { openModal } from '../actions/modalActions';
import FormMov from './FormMov';
import { Modal, Form, Button, Header, Icon, Segment, Grid, FormSelect, Input, Divider, Message } from 'semantic-ui-react'
import {Table} from 'semantic-ui-react';
import MenuPrincipal from './MenuPrincipal';
import MenuEmprendedor from './MenuEmprendedor';
import {getUser} from '../config/configs';

class Movimientos extends Component {

  constructor(props)
  {
    super(props);
    this.state = {
      open: false, messageInfo: null, messageOpen: null, showMP: false, showProd: false, setFiltro: true
    }

    this.objetos = [{ text: 'Materia Prima', value: 'mp' },
    { text: 'Producto', value: 'producto' }]

    this.data = {
      tipo_movimiento: "Ingreso"
    };

    this.ordenFiltros = { nombreAsc: true };
    
  }

  componentWillMount() {
    this.props.getTodasMPEmp();
    this.props.getProductos();
    this.userLogeado = (getUser()) ? getUser() : null;
    this.itemsMovRendered = null;
    this.itemsHistorialRendered = null;
  }

  showConfirmDialog = (id) => this.setState({open: true})
  handleCancel = () => this.setState({open: false})

  handleMessageConfirm = () => {
    this.setState({messageOpen: false});
  }

  handleConfirm = () => {
    this.setState({open: false});
  }

  handleButtonClick = () => this.setState({ });;

  renderMensaje = (info) => {
    let mensaje = '';
    if(info.estado === 'OK') {
        mensaje = 'El movimiento ha sido registrado';
    } else {
        mensaje = info.mensaje
    }
    this.cleanAll();
    this.setState({messageInfo: mensaje, messageOpen: true, estadoResponse:info.estado});
  }

  renderItemProd(a)
  {
    if(!a.es_fabricado)
    {
      return(
        <Table.Row  key={a.nombre}>
        <Table.Cell>
          <Header as='h4'>
            <Header.Content>
              {a.nombre}
            </Header.Content>
          </Header>
        </Table.Cell>
        <Table.Cell textAlign='center'>{a.stock_minimo}</Table.Cell>
        <Table.Cell textAlign='center'>{a.stock_actual}</Table.Cell>
        <Table.Cell textAlign='center'>{a.unidad_medida}</Table.Cell>
        <Table.Cell textAlign='center' content>
                <Button primary circular icon="add" color="green"
                  onClick={() => {
                    this.loadData(a);
                    this.props.openModal({
                    header: `${this.data.tipo_movimiento} de "${a.nombre}"`,
                    content: <FormMov data={this.data}/>
                  })}}>
                Actualizar
                </Button>
        </Table.Cell>
      </Table.Row>
      )
    }
  }
  
  renderItemMP(a)
  {
      return(
        <Table.Row  key={a.nombre}>
        <Table.Cell>
          <Header as='h4'>
            <Header.Content>
              {a.nombre}
            </Header.Content>
          </Header>
        </Table.Cell>
        <Table.Cell textAlign='center'>{a.stock_minimo}</Table.Cell>
        <Table.Cell textAlign='center'>{a.stock_actual}</Table.Cell>
        <Table.Cell textAlign='center'>{a.unidad_medida}</Table.Cell>
        <Table.Cell textAlign='center' content>
                <Button primary circular icon="add" color="green"
                  onClick={() => {
                    this.loadData(a);
                    this.props.openModal({
                    header: `${this.data.tipo_movimiento} de "${a.nombre}"`,
                    content: <FormMov data={this.data}/>
                  });
                }
                }
                >
                Actualizar
                </Button>
        </Table.Cell>
      </Table.Row>
      )
  }

  loadData(a)
  {
    if(this.state.showMP)
    {
      this.data = {
        ...this.data,
        materia_prima_id: a.id,
        unidad_medida: a.unidad_medida
      }
    }
    else
    {
      this.data = {
        ...this.data,
        producto_id: a.id,
        unidad_medida: a.unidad_medida
      }
    }
  }

  handleChange = (e, {name, value, checked}) => {
    if(name==="objeto")
    {
      (value === 'mp') ? this.setState(
        {
          showMP: true,
          showProd: false
        }
      ):
      this.setState(
        {
          showMP: false,
          showProd: true
        }
      )
      
    }
    else {
    this.data = {
       ...this.data,
       [name]: (value) ? value : checked
    }
    }
  }; 

  render() {

     return(
    <div id="DivPrincipal">
        <div id="MenuTop">
          <MenuPrincipal/>
        </div>
        <div className = "row">
          <div id="MenuLateral">
            {(this.userLogeado) ? <MenuEmprendedor/> : null}
           </div>
          <div style={{"paddingTop":"61px", marginLeft:"210px"}}>
            <Grid centered>
              <Grid.Row>
                <Grid.Column width={16}>
                  <center>
                    <h2>
                        Nuevo movimiento de stock
                    </h2>
                  </center>
                </Grid.Column>
              </Grid.Row>

              <Divider/>
            </Grid>
            <Form>
              <Grid container centered>
                <Grid.Row centered>
                  <Grid.Column width='4'>
                    <FormSelect id='tipo_movimiento' name='tipo_movimiento' label='Acción: ' options={[{ text: 'Ingreso', value: 'Ingreso' },
                    { text: 'Egreso', value: 'Egreso' }]} defaultValue='Ingreso' onChange={this.handleChange}/>

                  </Grid.Column>
                  <Grid.Column width='4'>
                    <FormSelect id="objeto" name="objeto" label='Movimientos de: ' options={this.objetos} onChange={this.handleChange}/>
                  </Grid.Column>
                </Grid.Row>

                <Grid.Row>
                  <Grid.Column width={16}>
                    
                    {this.state.showMP ? this.renderMP() : null }
                    {this.state.showProd ? this.renderProd() : null}
                    
                  </Grid.Column>
                </Grid.Row>
              </Grid>
            </Form>      
      
            { (!this.state.showMP && !this.state.showProd) ? this.showDefaultMessage() : null }

          <ModalManager />

          <Modal
            open={this.state.messageOpen}
            onClose={this.handleMessageConfirm}
            size='small'
            href='/compras'
          >
            <Header icon='browser' content='Aviso' />
            <Modal.Content>
            <h3>{this.state.messageInfo}</h3>
            </Modal.Content>
            <Modal.Actions>
            <Button color='green' onClick={this.handleMessageConfirm} inverted>
                <Icon name='checkmark' /> Aceptar
            </Button>
            </Modal.Actions>
          </Modal>
        </div>
      </div>
    </div>
    )
  }

  showDefaultMessage(){
    return(
      <Grid centered>
        <Grid.Row>
          <Grid.Column width={10}>
            <Message warning>
              <Message.Header>Seleccione el tipo de movimiento que desea y se desplegará la tabla correspondiente</Message.Header>
            </Message>
          </Grid.Column>
        </Grid.Row>
      </Grid>
    )
  }

  renderProd()
  { 
    const itemsRendered = this.props.productos.map(p => (
      this.renderItemProd(p))
    );

    return(
      <div id="itemGroup">
      <Table celled>
        <Table.Header>
        <Table.Row>
            <Table.HeaderCell>Nombre<Icon link name='sort' onClick={()=>this.ordenarProd('nombre')}/></Table.HeaderCell>
            <Table.HeaderCell textAlign='center'>Stock Mínimo</Table.HeaderCell>
            <Table.HeaderCell textAlign='center'>Stock actual</Table.HeaderCell>
            <Table.HeaderCell textAlign='center'>Unidad de medida</Table.HeaderCell>
            <Table.HeaderCell textAlign='center'>Acciones</Table.HeaderCell>
        </Table.Row>
        </Table.Header>
        <Table.Body>
          {itemsRendered}
        </Table.Body>
      </Table>
    </div>
    )
  }


  renderMP()
  {
    const itemsRendered = this.props.mpsEmp.map(mp => (
      this.renderItemMP(mp))
    );

    return(
      <div id="itemGroup">
      <Table celled>
        <Table.Header>
        <Table.Row>
            <Table.HeaderCell>Nombre<Icon link name='sort' onClick={()=>this.ordenarMP('nombre')}/></Table.HeaderCell>
            <Table.HeaderCell textAlign='center'>Stock Mínimo</Table.HeaderCell>
            <Table.HeaderCell textAlign='center'>Stock actual</Table.HeaderCell>
            <Table.HeaderCell textAlign='center'>Unidad de medida</Table.HeaderCell>
            <Table.HeaderCell textAlign='center'>Acciones</Table.HeaderCell>
        </Table.Row>
      </Table.Header>
      <Table.Body>
        {itemsRendered}
        </Table.Body>
      </Table>
    </div>
    )
  }

  ordenarMP(atributo) {
    switch(atributo) {
      case 'nombre': 
        if(this.ordenFiltros.nombreAsc) {
          this.props.mpsEmp.sort(function(a, b){
            return a.nombre.localeCompare(b.nombre);
          });
        }
        else {
          this.props.mpsEmp.sort(function(a, b){
            return b.nombre.localeCompare(a.nombre);
          })
        }
        this.ordenFiltros.nombreAsc = !this.ordenFiltros.nombreAsc;
        this.setState({setFiltro: !this.state.setFiltro});
        break;
    }
  }

  ordenarProd(atributo) {
    switch(atributo) {
      case 'nombre': 
        if(this.ordenFiltros.nombreAsc) {
          this.props.productos.sort(function(a, b){
            return a.nombre.localeCompare(b.nombre);
          });
        }
        else {
          this.props.productos.sort(function(a, b){
            return b.nombre.localeCompare(a.nombre);
          })
        }
        this.ordenFiltros.nombreAsc = !this.ordenFiltros.nombreAsc;
        this.setState({setFiltro: !this.state.setFiltro});
        break;
    }
  }


}
 

const mapStateToProps = state => ({
  mpsEmp: state.mpsEmp.items,
  productos: state.productos.items,
  itemsTable: state.tableData.items
});

export default withRouter(connect(mapStateToProps, { getTodasMPEmp, getProductos, openModal})(Movimientos));
