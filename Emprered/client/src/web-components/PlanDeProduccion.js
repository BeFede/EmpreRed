import React, { Component } from 'react'
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { Button, Header, Icon, Image, Confirm, Grid, Input, Label, Card, Message, Modal} from 'semantic-ui-react'
import {Table} from 'semantic-ui-react';
import MenuPrincipal from './MenuPrincipal';
import MenuEmprendedor from './MenuEmprendedor';
import SearchStandard from './SearchStandard';
import { removeItem, insertItem } from '../actions/tableFillActions';
import {getUser} from '../config/configs';
import { postPlanProduccion } from '../actions/planDeProduccionActions';


class PlanDeProduccion extends Component {

    constructor(props){
        super(props);
        // Para habilitar el boton de Guardar uso 2 booleans (itemsAgregados y costoAgregado dentro de state)
        this.itemsAgregados = false;
        this.state = { tienePlan: false, plan: null, open: false, messageOpen: false, messageInfo: null, costoAgregado: false };
        this.userLogeado = (getUser()) ? getUser() : null;
        this.nuevoPlan = this.nuevoPlan.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.dataToSend = { id_producto: this.props.location.state.producto.id, costo_mano_obra: null, detalles: [] };
        this.infoProducto = {
            nombreProducto: this.props.location.state.producto.nombre,
            fotoProducto: 'http://localhost:8000/server/media/' + this.props.location.state.producto.foto,
            unidadMedidaProducto: this.props.location.state.producto.unidad_medida
        }
        this.saveChanges = this.saveChanges.bind(this);
        this.cargoPlan = false;
    }

    componentWillMount() {
        //Si viene con plan le activo el boolean y seteo el plan y el costo mano de obra en la data to send
        if(this.props.location.state.planEncontrado) {
            this.setState({ tienePlan: true, plan: this.props.location.state.planEncontrado, costoAgregado: true });
            this.dataToSend.costo_mano_obra = this.props.location.state.planEncontrado.costo_mano_obra;
        }

        // Si viene ruteando desde Productos, capturo el id del producto seleccionado
        // OJO si tocan F5 habiendo ruteado antes, se mantiene el id de ese producto

        if(this.props.location.state.producto) {
            this.dataToSend.id_producto = this.props.location.state.producto.id;
        }
    }

    showConfirmDialog = () => this.setState({open: true})
    handleCancel = () => this.setState({open: false})

    handleConfirm = () => {
        this.setState({open: false});
        this.saveChanges();
    }

    handleMessageConfirm = () => {
        this.setState({messageOpen: false});
        this.props.history.push(
            {
              pathname: '/productos'
            }
        )
        window.location = '/productos';
    }

    renderMensaje = (info) => {
        let mensaje = '';
        if(info.estado === 'OK') {
            mensaje = 'Se guardó la receta correctamente';
        } else {
            mensaje = info.mensaje
        }
        this.setState({messageInfo: mensaje, messageOpen: true, estadoResponse:info.estado});
    }

    nuevoPlan(){
        this.setState({tienePlan: true});
    }

    handleChange = (e, {name, mpseleccionada, value}) => {
        // Si lo que cambia es el costo_mano_obra lo trato de una forma
        if(name === 'costo_mano_obra') {
            this.dataToSend.costo_mano_obra = value
            if(this.dataToSend.costo_mano_obra !== '') {
                this.setState({costoAgregado:true});
            } else {
                this.setState({costoAgregado:false});
            }
        } else {

            // Si lo que cambia son los detalles:
            // Lo trackeo en los detalles a enviar y le agrego lo que haya escrito el usuario
            const index = this.dataToSend.detalles.map(e => e.title).indexOf(mpseleccionada);

            this.dataToSend.detalles[index] = {
                ...this.dataToSend.detalles[index],
                [name]: value
            }
        }
   };

   saveChanges() {

        // Preparo la peticion final a enviar de acuerdo a lo que me pide la api
        let detalles = [];
        for(let det of this.dataToSend.detalles) {
            const detalle = {
                materia_prima: det.materia_prima,
                costo_merma: det.costo_merma,
                cantidad: det.cantidad
            }
            detalles.push(detalle);
        }

        const dataFinal = {
            id_producto: this.dataToSend.id_producto,
            costo_mano_obra: this.dataToSend.costo_mano_obra,
            descripcion: null,
            detalles: detalles
        }
        this.props.postPlanProduccion(dataFinal)
        .then((data) => {
            this.renderMensaje(data);
        }, err => {
            console.log(err);
        })

   }

    renderItem1(mp)
    {
        this.itemsAgregados = true;
        return(
            <Table.Row  key={mp.title}>
            <Table.Cell>
            <Header as='h4'>

                <Header.Content>
                {mp.title}
                </Header.Content>
            </Header>
            </Table.Cell>
            <Table.Cell textAlign='center' content>
                    <Input mpseleccionada={mp.title} name="cantidad" type="number" step="any" labelPosition='right' placeholder='Cantidad' label={{ basic: true, content: mp.unidad_medida }}
                        onChange={this.handleChange}
                        defaultValue={mp.cantidad}
                    >
                    </Input>
            </Table.Cell>


            <Table.Cell textAlign='center' content>
                    <Button negative circular icon="times"
                    onClick = {() => {
                        // Si el usuario elimina una mp de la seleccion la saco de los detalles a enviar
                        const index = this.dataToSend.detalles.map(e => e.title).indexOf(mp.title);
                        if(index !== -1) {
                            this.dataToSend.detalles.splice(index,1);
                            this.props.removeItem(mp);
                        }
                    }}
                    />
            </Table.Cell>
        </Table.Row>
        )
    }

    convertirDetallesAFormatoCorrecto(detalles) {
        // Este metodo convierte las mp al formato que entiende el search
        let vectorNuevasMP = [];
        for(let det of detalles) {
            // Le agrego un id a la MP porque así puedo darme cuenta cuando viene repetida en la action insertItem de tableFillActions
            let nuevaMP = { id: det.materia_prima_id, materia_prima: det.materia_prima_id, title: det.materia_prima, costo_merma: det.costo_merma, cantidad: det.cantidad, unidad_medida: det.unidad_medida }
            vectorNuevasMP.push(nuevaMP);
        }
        return vectorNuevasMP;
    }

    cargarMatPrimasDePlanPrevio() {
        if(this.state.plan.detalles) {
            // Convierto a formato para que el search los lea bien
            let detallesNuevos = this.convertirDetallesAFormatoCorrecto(this.state.plan.detalles);
            for(let mp of detallesNuevos) {
                this.props.insertItem(mp);
            }
        }
    }

    mostrarFoto(){

        if (this.props.location.state.producto.foto){
        return (
            <Card.Content>
                <center>
                    <Image size="tiny" src={this.infoProducto.fotoProducto} />
                    <Card.Header>
                        <h3>{this.infoProducto.nombreProducto}</h3>
                    </Card.Header>
                </center>
            </Card.Content>
        );
    }
    }

    render() {

        // Si ya tenia plan y no lo cargo aun, cargo las mp a renderizar en la tabla
        if(this.state.tienePlan && this.state.plan && !this.cargoPlan ) {
            this.cargarMatPrimasDePlanPrevio();
            this.cargoPlan = true;
        }

        const itemsRendered = this.props.itemsTable.map(mp => {
            // Cuando el usuario selecciona una MP la agrego al vector de detalles a enviar solo si no esta repetida
            // Hago este map para poder trackear la mp por su title
            const index = this.dataToSend.detalles.map(e => e.title).indexOf(mp.title);
            if(index === -1) {
                this.dataToSend.detalles.push(mp);
            }
            if(this.dataToSend.detalles.length === 0) {
                this.itemsAgregados = false;
            }
            return this.renderItem1(mp);
        });


        if(!this.state.tienePlan) {
            return (
                <div id="DivPrincipal">
                    <div id="MenuTop">
                        <MenuPrincipal/>
                    </div>
                    <div className = "row">
                        <div id="MenuLateral">
                            {(this.userLogeado) ? <MenuEmprendedor/> : null}
                        </div>
                        <div style={{"paddingTop":"61px", marginLeft:"210px"}}>
                            <center>
                                <Message warning>
                                    <Message.Header>Este producto no tiene una receta asociada</Message.Header>
                                    <p>Para crearla haz click en Nueva Receta de Producto</p>
                                </Message>
                                <Button circular color= "green" onClick={this.nuevoPlan}>
                                    <Icon name='add circle' />
                                    Nueva Receta de Producto
                                </Button>
                            </center>
                        </div>

                    </div>

                </div>
            )
        }

        else {
            
            return(
                <div id="DivPrincipal">
                    <div id="MenuTop">
                        <MenuPrincipal/>
                    </div>
                    <div className = "row">
                        <div id="MenuLateral">
                            {(this.userLogeado) ? <MenuEmprendedor/> : null}
                        </div>
                        <div style={{"paddingTop":"61px", marginLeft:"200px"}}>
                            <center>
                                <Grid container centered>
                                    <Grid.Row>
                                        <Grid.Column width={9}>

                                            <Card.Group>
                                                <Card fluid color='orange'>
                                                    {this.mostrarFoto()}
                                                    <Card.Content extra>
                                                        <center>
                                                            <Label size="large">
                                                                Costo de Mano de Obra
                                                            </Label>
                                                            <Input name="costo_mano_obra" type="number" step="any" labelPosition='left' placeholder='Monto'
                                                                label={{ basic: true, content: '$' }}
                                                                onChange={this.handleChange}
                                                                defaultValue={(this.state.plan) ? (this.state.plan.costo_mano_obra) : null}>
                                                            </Input>
                                                        </center>

                                                    </Card.Content>
                                                </Card>
                                            </Card.Group>


                                        </Grid.Column>
                                    </Grid.Row>

                                    <Grid.Row>
                                        <h1>
                                            Agregar Materias Primas
                                        </h1>
                                    </Grid.Row>

                                    <Grid.Row centered>
                                        {/* Componente de busqueda.. le paso por param el tipo de filtro que va a usar */}
                                        <SearchStandard filtro="Materias Primas"/>
                                        <Grid.Column width={9}>
                                            <div id="itemGroup">
                                                <Table celled>
                                                <Table.Header>
                                                <Table.Row>
                                                    <Table.HeaderCell>Nombre</Table.HeaderCell>
                                                    <Table.HeaderCell textAlign='center'>Cantidad</Table.HeaderCell>

                                                    <Table.HeaderCell textAlign='center'>Acciones</Table.HeaderCell>
                                                </Table.Row>
                                                </Table.Header>
                                                <Table.Body>
                                                {itemsRendered}
                                                </Table.Body>
                                                </Table>
                                            </div>
                                        </Grid.Column>

                                    </Grid.Row>

                                    <Grid.Row centered>
                                        <Button.Group>
                                            <Button href="/productos">Cancelar</Button>
                                            <Button.Or text='o' />
                                            <Button positive disabled = {!this.itemsAgregados || !this.state.costoAgregado} onClick={this.showConfirmDialog}>
                                                Guardar
                                            </Button>
                                        </Button.Group>
                                    </Grid.Row>
                                </Grid>
                            </center>

                            <Confirm
                                open={this.state.open}
                                header='Guardar Receta de Producto'
                                content='¿Está seguro de que desea guardar esta Receta de Producto?'
                                cancelButton='No'
                                confirmButton='Sí'
                                onCancel={this.handleCancel}
                                onConfirm={this.handleConfirm}
                            />

                            {/* Este es el modal para el mensaje */}
                            <Modal
                                open={this.state.messageOpen}
                                onClose={this.handleMessageConfirm}
                                size='small'
                            >
                                <Header icon='browser' content='Aviso' />
                                <Modal.Content>
                                <h3>{this.state.messageInfo}</h3>
                                </Modal.Content>
                                <Modal.Actions>
                                <Button color='green' onClick={this.handleMessageConfirm} inverted>
                                    <Icon name='checkmark' /> Aceptar
                                </Button>
                                </Modal.Actions>
                            </Modal>
                        </div>

                    </div>

                </div>

            )
        }

    }
}

const mapStateToProps = state => ({
    itemsTable: state.tableData.items
});

export default withRouter(connect(mapStateToProps, { removeItem, insertItem, postPlanProduccion })(PlanDeProduccion));
