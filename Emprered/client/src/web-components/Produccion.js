import React, { Component } from 'react'
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { getProductos } from '../actions/productosActions';
import ModalManager from './ModalManager';
import { openModal } from '../actions/modalActions';
import { removeItem } from '../actions/tableFillActions';
import { Input, Modal, Button, Header, Icon, Grid, Form, TextArea, Confirm, Segment, Divider, Label} from 'semantic-ui-react'
import {Table} from 'semantic-ui-react';
import MenuPrincipal from './MenuPrincipal';
import MenuEmprendedor from './MenuEmprendedor';
import {getUser} from '../config/configs';
import SearchStandard from './SearchStandard';
import ModalProduccion from './ModalProduccion';
import { postProduccion, getProducciones, descargarReporte } from '../actions/produccionActions';
import '../styles/Produccion.css'
import FiltrosProducciones from './FiltroProducciones.js';


class Produccion extends Component {

  componentWillMount() {
    this.userLogeado = (getUser()) ? getUser() : null;
    this.itemsProduccionRendered = null;
    this.itemsHistorialRendered = null;
    this.obtenerHistorial();
    this.dataToSend = { descripcion: null, estado: null, detalles: [] };
    this.descripcionProduccion = null;
    this.produccionDisabled = true;
    this.registrarProduccionPendiente = this.registrarProduccionPendiente.bind(this);
    this.state = {
        cargado:false, open: false, showHistory: true, tableProduccion: false, messageInfo: null, messageOpen: null,
        setFiltro: false
    };
    this.ordenFiltros = { idAsc: true, fechaAsc: false, estadoAsc: false }
  }


  handleCancel = () => this.setState({open: false})

  showConfirmDialog = () => this.setState({open: true})

  handleChange = (e, {name, prodseleccionado, value}) => {
    // Si lo que cambia es la descripcion lo trato de una forma
    if(name === 'descripcion') {
        this.dataToSend.descripcion = value
    } else {

        // Si lo que cambia son los detalles:
        // Lo trackeo en los detalles a enviar y le agrego lo que haya escrito el usuario
        const index = this.dataToSend.detalles.map(e => e.id).indexOf(prodseleccionado);

        this.dataToSend.detalles[index] = {
            ...this.dataToSend.detalles[index],
            [name]: value
        }
    }
  }

  handleMessageConfirm = () => {
    this.setState({messageOpen: false});
  }

  handleConfirm = () => {
    this.setState({open: false});
    this.anularProduccion();
  }

  handleButtonClick = () => this.setState({ visible: !this.state.visible });;

  renderMensaje = (info) => {
    let mensaje = '';
    if(info.estado === 'OK') {
        mensaje = 'Producción registrada como "Pendiente", para gestionarla acceda a ella desde el historial de producciones';
        this.cleanAll();
        // window.location = '/produccion';
    } else {
        mensaje = info.mensaje
    }
    this.setState({messageInfo: mensaje, messageOpen: true, estadoResponse:info.estado});
  }

  cleanDetallesTabla() {
    for(let det of this.dataToSend.detalles) {
      this.props.removeItem(det);
    }
  }

  cleanAll() {
    for(let item of this.props.itemsTable) {
      this.props.removeItem(item);
    }
    this.dataToSend = { detalles: [] };
    this.itemsProduccionRendered = null;
    this.produccionDisabled = true;
    this.setState({tableProduccion: false, showHistory: true});
  }

  obtenerHistorial() {
    this.props.getProducciones()
    .then(data => {
      this.setState({cargado:true});
    }, err => {
      console.log(err);
    })
  }

  renderItemProduccion(prod)
  {
    this.produccionDisabled = false;

      return(
        <Table.Row  key={prod.id}>
        <Table.Cell>
          <Header as='h4'>
            <Header.Content>
              {prod.title}
            </Header.Content>
          </Header>
        </Table.Cell>
        <Table.Cell textAlign='center'>
          <Input disabled={this.state.produccionEstaEnProceso  || this.state.produccionEstaTerminada} prodseleccionado={prod.id} name="cantidad" type="number" step="any"
                    labelPosition='right' placeholder='Cantidad' label={{ basic: true, content: prod.unidad_medida }}
                    onChange={this.handleChange}
                    defaultValue={(prod.cantidad) ? prod.cantidad : 0}
          >
          </Input>
        </Table.Cell>
        <Table.Cell textAlign='center' content>
        <Button disabled={this.state.produccionEstaEnProceso || this.state.produccionEstaTerminada} negative circular icon="times"
                  onClick = {() => {
                      // Si el usuario elimina un item de la seleccion lo saco de los detalles a enviar
                    const index = this.dataToSend.detalles.map(e => e.id).indexOf(prod.id);
                    if(index != -1) {
                      this.dataToSend.detalles.splice(index,1);
                      this.props.removeItem(prod);
                    }
                  }}
                />
        </Table.Cell>
      </Table.Row>
      )

  }

  renderTableProduccion()
  {
    if(this.state.tableProduccion)
    {
      if(this.dataToSend.detalles.length === 0) {
        this.produccionDisabled = true;
      }
    return(

      <div>

      <Grid container centered>
          <Grid.Row>
            <Grid.Column width='4' floated='left'>

              <h3>Busque los productos a fabricar</h3>
              <SearchStandard filtro="ProductosFabricados" placeholder='Productos'/>
            </Grid.Column>
          </Grid.Row>
          <Divider/>
          <Grid.Row>
            <center>
              <div>
                <h3>Detalles</h3>
              </div>
            </center>
          </Grid.Row>
          <Grid.Row>
          <Grid.Column width='16'>
            <div style={{overflow: 'auto', maxHeight: 600 }}>
              <Table celled color="orange" style={{width:'100%'}}>
                <Table.Header>
                  <Table.Row>
                      <Table.HeaderCell>Producto </Table.HeaderCell>
                      <Table.HeaderCell textAlign='center'>Cantidad</Table.HeaderCell>
                      <Table.HeaderCell textAlign='center'>Acciones</Table.HeaderCell>
                  </Table.Row>
                </Table.Header>
                <Table.Body>
                    {this.itemsProduccionRendered}
                </Table.Body>
              </Table>
            </div>
          </Grid.Column>
          </Grid.Row>
          <Divider/>
          <Grid.Row>
            <Grid.Column width={16}>
              <br/>
              <center>
                <h3>Agregue una descripción de la producción</h3>
              </center>
              <Form.TextArea 
                id='form-field-descripcion'
                name='descripcion'
                control={TextArea}
                placeholder='Descripción'
                defaultValue={this.dataToSend.descripcion}
                onChange={this.handleChange}
                style={{width:'100%' , bottom:'0px'}}
              />
              <br/>
              <div align='center'>
              <Button color='yellow'
                disabled = {this.produccionDisabled || this.state.produccionEstaPendiente || this.state.produccionEstaEnProceso || this.state.produccionEstaTerminada}
                onClick={this.registrarProduccionPendiente}>
                Registrar como Pendiente
              </Button>
              </div>
            </Grid.Column>
          </Grid.Row>

      </Grid>
        
      </div>
    )
  }
  }

  registrarProduccionPendiente() {

    let detallesFinal = [];
    for(let det of this.dataToSend.detalles) {
        const cantidadInt = parseInt(det.cantidad);
        const detNuevo = {producto_id: det.id, cantidad: cantidadInt};
        detallesFinal.push(detNuevo);
      // this.props.removeItem(det);
    }

    this.dataToSend = { descripcion: this.dataToSend.descripcion, estado: 1, detalles: detallesFinal };
    this.props.postProduccion(this.dataToSend)
    .then(data => {
        this.renderMensaje(data);
    }, err => {
      console.log(err);
    });

  }

  render() {

      if(this.state.cargado === true) {
        this.itemsProduccionRendered = this.props.itemsTable.map(producto => {
          // Cuando el usuario selecciona una MP la agrego al vector de detalles a enviar solo si no esta repetida
          // Hago este map para poder trackear la mp por su title
          const index = this.dataToSend.detalles.map(e => e.id).indexOf(producto.id);
          if(index == -1) {
              this.dataToSend.detalles.push(producto);
          }
          return this.renderItemProduccion(producto);
        });


        this.itemsHistorialRendered = this.props.producciones.map(produccion => {
          const date = new Date(produccion.fecha_hora);
          const dateString= date.toLocaleString();
          const produccionConFechaFormateada = {...produccion, fecha_hora: dateString};
          return this.renderItemHistorial(produccionConFechaFormateada);
        })
      }

      return(
      <body style={{overflow:'auto'}}>
        <div id="DivPrincipal">
          <MenuPrincipal/>

              {(this.userLogeado) ? <MenuEmprendedor/> : null}

            <div className='DivSecundario' style={{paddingTop:"20px", marginLeft:"215px"}}>
            <br/>

            <Grid>
            <br/>
              <Grid.Column floated='left' width={5} >

                <Icon name='wrench' size='big'/><Header as='h2' icon name ='wrench'> Producciones</Header>
              </Grid.Column>
              <Grid.Column floated='right' width={5}>
                <div align='right' >
                <br/>
                  <Button style={{marginBottom:'0px'}} color='blue'
                    onClick={()=>this.setState( {
                      showHistory: true,
                      tableProduccion: false,
                    })}>
                    <Icon name='list' />
                      Ver producciones
                  </Button>
                  <Button color='orange' onClick={() => {
                    this.cleanAll();
                    this.setState({
                    tableProduccion: true,
                    showHistory: false
                    })
                    }}>
                      <Icon name='add circle' />
                        Nueva producción
                  </Button>
                </div>
              </Grid.Column>
            </Grid>
            <Divider/>
                 
              <Grid centered>
                  <Grid.Row>
                    <Grid.Column width={14} centered>
                        {(this.state.tableProduccion) ? this.renderTableProduccion() : (this.state.showHistory ? this.renderHistorial() : null)}
                    </Grid.Column>
                  </Grid.Row>
              </Grid>

              <ModalManager />

              {/* Este es el modal para el mensaje */}
              <Modal
                        open={this.state.messageOpen}
                        onClose={this.handleMessageConfirm}
                        size='small'>
                        <Header icon='browser' content='Aviso' />
                        <Modal.Content>
                        <h3>{this.state.messageInfo}</h3>
                        </Modal.Content>
                        <Modal.Actions>
                  <Button color='green' onClick={this.handleMessageConfirm} inverted>
                            <Icon name='checkmark' /> Aceptar
                        </Button>
                        </Modal.Actions>
              </Modal>

              <Confirm
                  open={this.state.open}
                  header='Anular Producción'
                  content='¿Está seguro de que desea anular esta producción?'
                  cancelButton='No'
                  confirmButton='Sí'
                  onCancel={this.handleCancel}
                  onConfirm={this.handleConfirm}
              />


          </div>
      </div>
      </body>
      )
  }

  renderItemHistorial(produccion)
  {
    this.produccionDisabled = false;
      return(
        <Table.Row  key={produccion.id}>
        <Table.Cell>
          <Header as='h5'>
            <Header.Content>
              {produccion.id}
            </Header.Content>
          </Header>
        </Table.Cell>
        <Table.Cell textAlign='center'>

          {produccion.fecha_hora}
        </Table.Cell>
        <Table.Cell textAlign='center'>

          <Label circular color={this.asignarColor(produccion.estado_id)} size='huge'>
            {produccion.estado_descripcion}
          </Label>
        </Table.Cell>

        <Table.Cell textAlign='center'>
          {produccion.descripcion}
        </Table.Cell>


        <Table.Cell textAlign='right' content>
          <div>
            <Button color="orange" circular icon="eye"
              onClick={() =>
                this.props.openModal({
                      header: "Gestión de una producción",
                      content: <ModalProduccion idProduccion={produccion.id} produccionSeleccionada={produccion}/>
                })}/>
              <Button
                circular
                content="Descargar reporte"
                icon = "arrow circle down"
                color="blue"
                onClick={() =>
                  this.props.descargarReporte(produccion.id, "reporte_materias_primas", document)
                 }/>
          </div>

        </Table.Cell>
      </Table.Row>
      )

  }


  renderHistorial()
  {
    if(this.state.showHistory)
    {
    return(
      <div>
        <h2>Historial de producciones</h2>
        <Segment loading={!this.state.cargado}>
          <Grid centered>
              <Grid.Row>
                <Grid.Column width={16} centered>
                  <FiltrosProducciones filtro= 'Producciones'/>
                  <Divider/>
                </Grid.Column>     
              </Grid.Row>
              <Grid.Row>
                <Grid.Column width={16}>
                  <div style={{overflow: 'auto', maxHeight: 600 }}>
                    <Table unstackable color="orange">
                      <Table.Header>
                        <Table.Row>
                        <Table.HeaderCell>ID<Icon link name='sort' onClick={() => this.ordenar('id')}/></Table.HeaderCell>
                            <Table.HeaderCell textAlign='center'>Fecha (DD/MM/AAAA)<Icon link name='sort'  onClick={() => this.ordenar('fecha')}/></Table.HeaderCell>
                            <Table.HeaderCell textAlign='center'>Estado<Icon link name='sort' onClick={() => this.ordenar('estado')}/></Table.HeaderCell>
                            <Table.HeaderCell textAlign='center'>Descripción</Table.HeaderCell>
                            <Table.HeaderCell textAlign='center'>Acciones</Table.HeaderCell>
                        </Table.Row>
                      </Table.Header>
                      <Table.Body>
                          {this.itemsHistorialRendered}
                      </Table.Body>
                    </Table>
                  </div>
                </Grid.Column>
              </Grid.Row>   
          </Grid>   
        </Segment>

        
      </div>
    )
  }
  }

  ordenar(atributo){
     
    switch(atributo){
        
      case 'id':
        if(this.ordenFiltros.idAsc){
          this.props.producciones.sort(function(a, b){
            return a.id - b.id;
            });
          } else {
            this.props.producciones.sort(function(a, b){
            return b.id - a.id;
           })
         }
        this.ordenFiltros.idAsc = !this.ordenFiltros.idAsc;
        this.setState({ setFiltro: !this.state.setFiltro })
        break;
       
      case 'fecha':
        if(this.ordenFiltros.fechaAsc) {
          this.props.producciones.sort(function(a, b){
            return b.fecha_hora.localeCompare(a.fecha_hora);
          });
        }
        else {
          this.props.producciones.sort(function(a, b){
            return a.fecha_hora.localeCompare(b.fecha_hora);
          })
        }
        this.ordenFiltros.fechaAsc = !this.ordenFiltros.fechaAsc;
        this.setState({setFiltro: !this.state.setFiltro});
        break;
      case 'estado':
        if(this.ordenFiltros.estadoAsc) {
          this.props.producciones.sort(function(a, b){
            return a.estado_descripcion.localeCompare(b.estado_descripcion);
          });
        }
        else {
          this.props.producciones.sort(function(a, b){
            return b.estado_descripcion.localeCompare(a.estado_descripcion);
          })
        }
      this.ordenFiltros.estadoAsc = !this.ordenFiltros.estadoAsc;
      this.setState({setFiltro: !this.state.setFiltro});
      break;
    } 
  }

  asignarColor(estado) {
    return (estado === 1) ? 'yellow' : ((estado === 2) ? 'orange' : ((estado === 3) ? 'green' : 'red'));
  }

}

const mapStateToProps = state => ({
  productos: state.productos.items,
  itemsTable: state.tableData.items,
  producciones: state.producciones.resultProducciones
});

export default withRouter(connect(mapStateToProps, {  getProductos, openModal, removeItem, postProduccion, getProducciones, descargarReporte })(Produccion));
