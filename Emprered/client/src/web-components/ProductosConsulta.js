import React,{Component} from 'react';
import { Button, Header, Icon, Image, Accordion, Segment, Grid, Input, Radio, Container, Confirm, Divider, Dropdown} from 'semantic-ui-react'
import {Table} from 'semantic-ui-react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import ModalManager from './ModalManager';
import { openModal } from '../actions/modalActions';
import FormProducto from './FormProducto';
import { getProductos, delProducto } from '../actions/productosActions';
import { getAlicuotas, getCategoriasProducto, getUnidadesMedida } from '../actions/tipificacionesActions';
import MenuPrincipal from './MenuPrincipal';
import MenuEmprendedor from './MenuEmprendedor';
import {getUser} from '../config/configs';
import {getPlanesProduccion} from '../actions/planDeProduccionActions';
import SearchMPCustom from './SearchMP'
import FiltrosMPyProd from './FiltroMPyProd.js';
import { urlMedia } from './../config/configs.js';

  class ProductosConsulta extends Component{

  state = { open: false, idABorrar: null, setFiltro: false};

    componentWillMount()
    {
        this.props.getProductos();
        this.props.getAlicuotas();
        this.props.getCategoriasProducto();
        this.props.getUnidadesMedida();
        this.userLogeado = (getUser()) ? getUser() : null;
        this.ordenFiltros = { stockAsc: false, costoAsc: true, nombreAsc: true }
     }

    showConfirmDialog = (id) => this.setState({open: true, idABorrar: id})
    handleCancel = () => this.setState({open: false})

    handleConfirm = () => {
      this.setState({open: false});
      this.deleteProducto(this.state.idABorrar);

    }

    deleteProducto(id) {
      this.props.delProducto(this.props.productos, id)
      .then(data => {

      }, err => {
        console.log(err);
      })
      ;
    }

    buttonVerPlanProduccion(p){
      if (p.es_fabricado){
      return  (
        <Button color='orange' circular icon='file alternate'
          onClick={() => this.routePlanProduccion(p)}
        >
        </Button>
      )
    }
    }

    routePlanProduccion(producto) {

      this.props.getPlanesProduccion(producto.id)
      .then((data) => {
        if(data.estado === 'OK') {
          this.props.history.push(
            {
              pathname: '/planproduccion',
              state: {producto: producto, planEncontrado: data.datos}
            }
          );
        } else {
          this.props.history.push(
            {
              pathname: '/planproduccion',
              state: {producto: producto}
            }
          );
        }
      }, err => {
        console.log(err);
      });

    }


    renderItem(p)
    {

      let foto = '/static/media/EmprenRed5.f4d3b35d.png';
      if (p.foto){
        foto = urlMedia + p.foto
      }


      return(
        <Table.Row  key={p.nombre}>
        <Table.Cell>
          <Header as='h4' image>
            <Image src={foto} rounded size='mini' />
            <Header.Content>
              {p.nombre}
              <Header.Subheader>
                {p.categoria} - {p.subcategoria}
              </Header.Subheader>
            </Header.Content>
          </Header>
        </Table.Cell>
        <Table.Cell textAlign='center'>{(p.stock_actual <= 0) ? 'No cargado' : p.stock_actual}</Table.Cell>
        <Table.Cell textAlign='center'>{(p.costo <= 0) ? 'No cargado' : p.costo}</Table.Cell>
        <Table.Cell textAlign='center' content>

                {this.buttonVerPlanProduccion(p)}

                <Button primary circular icon="edit"
                  onClick={() => this.props.openModal({
                    header: "Editar producto",
                    content: <FormProducto data={p}/>
                  })}
                />
                <Button negative circular icon="times"
                  onClick = {() => this.showConfirmDialog(p.id)}
                />
        </Table.Cell>
      </Table.Row>

      )
    }
    render() {

      const itemsRendered = this.props.productos.map(p => (
        this.renderItem(p)
      ));


  return (
    <div className="DivPrincipal" style={{overflow:'auto'}}>
        <div id="MenuTop">
          <MenuPrincipal/>
        </div>
        <div className = "row">
          <div id="MenuLateral">
            {(this.userLogeado) ? <MenuEmprendedor/> : null}
          </div>
          <div style={{"paddingTop":"61px", marginLeft:"210px"}}>
              <Segment basic>
                {/* <center>
                <Image src= {empreRed} width='100' height='100'/>
                </center> */}

                {/* ACORDARSE DEL MODAL MANAGER */}
                <ModalManager />

                <Grid>
                  <Grid.Row>
                    <Grid.Column floated='left' width={5}>
                    <Header as='h2'> <Icon name='list' size='tiny'/>Listado de Productos</Header>
                    </Grid.Column>
                    <Grid.Column floated='right' width={5} >
                        <div align='right'>
                        <Button style={{marginBottom:'5px'}} color='green'
                        icon labelPosition='left'
                        onClick={() => this.props.openModal({
                          header: "Nuevo Producto",
                          content: <FormProducto />})}>
                        <Icon name='add circle' />
                        Registrar nuevo producto
                        </Button>
                      </div>
                    </Grid.Column>
                  </Grid.Row>
                  </Grid>
                  <FiltrosMPyProd filtro='Productos'/>
                  <Divider/>
                  <Grid>
                  <Grid.Row>
                    <Grid.Column width={16}>
                      <div id="itemGroup">
                        {/* <Item.Group divided> */}
                        <Table singleLine striped>
                          <Table.Header>
                          <Table.Row>
                              <Table.HeaderCell>Nombre<Icon link name='sort' onClick={()=>this.ordenar('nombre')}/></Table.HeaderCell>
                              <Table.HeaderCell textAlign='center'>Stock<Icon link name='sort' onClick={()=>this.ordenar('stock')}/></Table.HeaderCell>
                              <Table.HeaderCell textAlign='center'>Costo<Icon link name='sort' onClick={()=>this.ordenar('costo')}/></Table.HeaderCell>
                              <Table.HeaderCell textAlign='center'>Acciones</Table.HeaderCell>
                          </Table.Row>
                        </Table.Header>
                        <Table.Body>
                          {itemsRendered}
                          </Table.Body>
                        </Table>
                        {/* </Item.Group> */}
                        <Confirm
                          open={this.state.open}
                          header='Borrar producto'
                          content='¿Está seguro que desea borrar este producto?'
                          cancelButton='No'
                          confirmButton='Si'
                          onCancel={this.handleCancel}
                          onConfirm={this.handleConfirm}
                        />
                      </div>
                    </Grid.Column>
                  </Grid.Row> 
                </Grid>
              </Segment>
          </div>
      </div>
    </div>

    )
  }

  ordenar(atributo) {

    switch(atributo) {
      case 'stock': 
        if(this.ordenFiltros.stockAsc) {
          this.props.productos.sort(function(a, b){
            return a.stock_actual - b.stock_actual;
          });
        } else {
          this.props.productos.sort(function(a, b){
            return b.stock_actual - a.stock_actual;
          });
        }
        this.ordenFiltros.stockAsc = !this.ordenFiltros.stockAsc;
        this.setState({setFiltro: !this.state.setFiltro});
        break;
      case 'nombre': 
        if(this.ordenFiltros.nombreAsc) {
          this.props.productos.sort(function(a, b){
            return a.nombre.localeCompare(b.nombre);
          });
        }
        else {
          this.props.productos.sort(function(a, b){
            return b.nombre.localeCompare(a.nombre);
          })
        }
        this.ordenFiltros.nombreAsc = !this.ordenFiltros.nombreAsc;
        this.setState({setFiltro: !this.state.setFiltro});
        break;
  
      case 'costo': 
        if(this.ordenFiltros.costoAsc) {
          this.props.productos.sort(function(a, b){
            return b.costo - a.costo;
          });
        } else {
          this.props.productos.sort(function(a, b){
            return a.costo - b.costo;
          });
        }
        this.ordenFiltros.costoAsc = !this.ordenFiltros.costoAsc;
        this.setState({setFiltro: !this.state.setFiltro});
        break;
    }
  }
  

renderLogoYBuscador(){
  return(
      <Grid>
        <Grid.Row centered verticalAlign='middle'>
          <Grid.Column width={16}>
             <h3>Buscar</h3>
              <SearchMPCustom/>
          </Grid.Column>   
        </Grid.Row>
      </Grid>
    )
}


}


const mapStateToProps = state => ({
    productos: state.productos.items,
    categoriasProducto: state.tipificaciones.categoriasProducto,
    alicuotas: state.tipificaciones.alicuotas,
    unidadesMedida: state.tipificaciones.unidadesMedida,
    planProduccionInfo: state.planProduccion.resultPlan
});



  export default withRouter(connect(mapStateToProps, { getProductos, openModal, delProducto, getAlicuotas, getCategoriasProducto, getUnidadesMedida, getPlanesProduccion })(ProductosConsulta));
