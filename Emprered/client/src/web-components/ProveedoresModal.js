import React, { Component } from 'react'
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { Button, Form, Grid, Icon, Message, Segment, Accordion, List, Modal, Header, Confirm, Dimmer, Loader} from 'semantic-ui-react'
import { postProveedor, getProveedores, delProveedor } from '../actions/proveedoresActions';
import { selecProveedor, removeSelecProveedor } from '../actions/selecProveedorAction';
import { closeModal } from '../actions/modalActions'

class ProveedoresModal extends Component {

    constructor(props){
        super(props);
        this.state = { cargado: false, open: false, idABorrar: null, messageInfo: null, messageOpen: false, seleccionoProveedor: false, agregarDisabled: true };
        
    }

    componentWillMount() {
        this.itemsRendered = [];
        this.nuevoProveedor = { apellido: 'a' };
        this.handleChange = this.handleChange.bind(this);

        this.props.getProveedores()
        .then(data => {
            this.setState({cargado: true})
        }, err => {
            console.log(err);
        })
    }

    handleCancel = () => this.setState({open: false})

    showConfirmDialog = () => this.setState({open: true})

    handleMessageConfirm = () => {
        this.setState({messageOpen: false});
    }

    handleConfirm = () => {
        this.props.delProveedor(this.props.proveedores, this.state.idABorrar)
        .then(data => {
            console.log(data)
            this.setState({open: false});
            this.renderMensaje(data);
        }, err => {
            console.log(err);
        })
    }

    handleChange = (e, {name, value}) => {
        this.nuevoProveedor = {
            ...this.nuevoProveedor,
            [name]: value
        }
        if(document.getElementById('nombre').value !== '' && document.getElementById('numero_documento').value !== ''
            && document.getElementById('direccion').value !== '' && document.getElementById('telefono').value !== '')
            {
                this.setState({agregarDisabled: false});
            }
            else {
                this.setState({agregarDisabled: true});
            }
    }

    renderMensaje = (mensaje) => {
        this.setState({messageInfo: mensaje, messageOpen: true});
    }

    borrarProveedor(proveedor) {
        this.setState({idABorrar: proveedor.id})
        this.showConfirmDialog();
    }

    agregarProveedor() {
        this.props.postProveedor(this.nuevoProveedor)
        .then(data => {
            this.limpiarCampos();
            this.renderMensaje('Proveedor agregado');
        }, err => {
            console.log(err);
        })
    }

    limpiarCampos() {
        document.getElementById('nombre').value = '';
        document.getElementById('numero_documento').value = '';
        document.getElementById('direccion').value = '';
        document.getElementById('telefono').value = '';
        this.nuevoProveedor = { apellido: 'a' };
        this.setState({agregarDisabled: true});
    }

    seleccionarProveedor(proveedor){
        this.props.removeSelecProveedor();
        this.props.selecProveedor(proveedor);
        this.setState({seleccionoProveedor: true});
        this.props.closeModal();
    }

    renderAgregarProveedor() {
        const NuevoProveedorForm = (
                <Grid>
                    <Grid.Row centered>
                        <Grid.Column width={14}>
                            <Form>
                                <Form.Group widths={4}>
                                    <Form.Input name='nombre' id='nombre' label='Nombre o Razón Social' placeholder='Nombre o Razón Social' required onChange={this.handleChange} />
                                    <Form.Input name='numero_documento' id='numero_documento' label='CUIT' placeholder='CUIT' required onChange={this.handleChange}/>
                                    <Form.Input name='direccion' id='direccion' label='Dirección' placeholder='Dirección' required onChange={this.handleChange}/>
                                    <Form.Input type='number' name='telefono' id='telefono' label='Teléfono' placeholder='Teléfono' required onChange={this.handleChange}/>
                                </Form.Group>
                            </Form>
                        </Grid.Column>
                    </Grid.Row>

                    <Grid.Row centered>
                        <Grid.Column width={14}>
                            <Button.Group>
                                <Button disabled={this.state.agregarDisabled} color='green' icon labelPosition='left' onClick={() => this.agregarProveedor()}>
                                    <Icon name='add circle' />
                                    Agregar
                                </Button>
                                <Button color='blue' icon labelPosition='right' onClick={() => this.limpiarCampos()}>
                                    <Icon name='eraser' />
                                    Limpiar
                                </Button>
                            </Button.Group>
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
        )

        const panels = [
            {
              key: 'Agregar-proveedor',
              title: 'Agregar nuevo proveedor',
              content: {content: NuevoProveedorForm }
            }
        ]

        return(
            <Grid>
                <Grid.Row>
                    <Grid.Column width={16}>
                        <Accordion fluid styled panels={panels}/>
                    </Grid.Column>
                </Grid.Row>
            </Grid>
        )
    }

    renderListaProveedores() {
            
        if(this.props.proveedores.length > 0) {
            this.itemsRendered = this.props.proveedores.map(item =>{
                return this.renderItemIndividual(item)
              });
        }

        return(
            <Segment>
                <Grid>
                    <Grid.Row>
                        <Grid.Column width={16}>
                            {
                                (this.props.proveedores.length > 0)
                                ?
                                <List divided relaxed verticalAlign='middle'>
                                    {this.itemsRendered}
                                </List>
                                :
                                <Message info>
                                    <Message.Header>No hay ningún proveedor cargado aún</Message.Header>
                                </Message>
                            }
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            </Segment>
        )
    }

    renderItemIndividual(item) {
        return(
            <List.Item key={item.id}>
                <List.Icon style={{cursor: "pointer"}} name='times circle outline' color='red' size='large' verticalAlign='middle' onClick={() => this.borrarProveedor(item)} />
                <List.Content>
                    <List.Header as='a' onClick={() => this.seleccionarProveedor(item)}>{item.nombre}</List.Header>
                    <List.Description>CUIT: {item.numero_documento} - Direccion: {item.direccion} - Teléfono: {item.telefono}</List.Description>
                </List.Content>
            </List.Item>
        )
    }


    render() {

        return (
        <div>
            <Segment>
                {
                    (this.state.cargado)
                    ?
                    <Grid centered>
                        <Grid.Row centered>
                            <Grid.Column width={16}>
                                <Grid>
                                    <Grid.Row centered>
                                        <Grid.Column width={16}>
                                            {this.renderAgregarProveedor()}
                                        </Grid.Column>
                                    </Grid.Row>
                                    <Grid.Row centered>
                                        <Grid.Column width={16}>
                                            {this.renderListaProveedores()}
                                        </Grid.Column>
                                    </Grid.Row>
                                </Grid>
                            </Grid.Column>
                        </Grid.Row>
                    </Grid>
                    :
                    <center>
                            <Dimmer active inverted>
                                <Loader active inline='centered'/>
                            </Dimmer>
                    </center>
                }
                
            </Segment>

            {/* Este es el modal para el mensaje */}
            <Modal
                        open={this.state.messageOpen}
                        onClose={this.handleMessageConfirm}
                        size='small'>
                        <Header icon='browser' content='Aviso' />
                        <Modal.Content>
                        <h3>{this.state.messageInfo}</h3>
                        </Modal.Content>
                        <Modal.Actions>
                  <Button color='green' onClick={this.handleMessageConfirm} inverted>
                            <Icon name='checkmark' /> Aceptar
                        </Button>
                        </Modal.Actions>
              </Modal>

            <Confirm
                  open={this.state.open}
                  header='Eliminar proveedor'
                  content='¿Está seguro de que desea eliminar este proveedor?'
                  cancelButton='No'
                  confirmButton='Sí'
                  onCancel={this.handleCancel}
                  onConfirm={this.handleConfirm}
              />
        </div>
        )
    }
}


const mapStateToProps = state => ({
    proveedorSeleccionado: state.proveedorSeleccionado.item,
    proveedores: state.proveedores.proveedores
    
});

export default withRouter(connect(mapStateToProps, { postProveedor, getProveedores, delProveedor, selecProveedor, removeSelecProveedor, closeModal })(ProveedoresModal));