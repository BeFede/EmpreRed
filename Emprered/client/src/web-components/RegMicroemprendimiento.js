import React,{Component} from 'react';
import { Grid, Icon, Modal, Segment, Form, Button, Header, Input } from 'semantic-ui-react';
import MenuPrincipal from './MenuPrincipal';
import ReactTooltip from 'react-tooltip';
import { postMarca } from '../actions/marcasActions';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { urlMedia } from '../config/configs';

class RegMicroemprendimiento extends Component
{

    constructor(props) {
        super(props);
        this.state = {
            showEditU: false,
            messageInfo: null,
            messageOpen: null,
            estadoResponse: null,
        }
        this.server = urlMedia;
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.fotuli = null; // acá se guarda el file de la foto
        this.data = (props.data) ? props.data : null;
    }

    handleChange = (e, {name, value}) => {
        
        if (name == 'logo') {
              // La única forma que encontré para hacerlo andar *shrug*
              this.fotuli = e.target.files[0];
          }

        this.data = {
           ...this.data,
           [name]: (value) ? value : null
        }
    }

    renderMensaje = (info) => {
        let mensaje = '';
        if (info.estado === 'OK') {
            mensaje = 'Se cargó la marca correctamente!';
        } else {
            mensaje = info.mensaje;
        }
        this.setState({ messageInfo: mensaje, messageOpen: true, estadoResponse: info.estado });
    }

    handleMessageConfirm = () => {
        if(this.state.estadoResponse === 'OK') {
            this.setState({ messageOpen: false });
            this.props.history.push('/productos');
        } else {
            this.setState({ messageOpen: false });
        }
    }

     handleSubmit = () => {
        // Aca invoco a la promise y agarro lo que resuelva, sea que haya entrado por then o catch en la action
        if(this.fotuli!=null)
        {
            this.data.logo = this.fotuli;
        }   
        if(this.data)
        {
            this.props.postMarca(this.data)
            .then((data) => {
                console.log(this.data);
                this.renderMensaje(data);
            }, err => {
                console.log(err);
            });
        }  
      }

    render()
    {
        return(
            <div id="MenuPrincipal">
            <div id="MenuTop">
          <MenuPrincipal/>
            </div>
            <div style={{"paddingTop":"65px"}}>
            <center>
            <Header as='h2'>Registro del Microemprendimiento</Header>
            </center>
            <br/>
            <center>
                <Button primary icon='mail forward' content="Saltar paso" href="/productos"></Button>
            </center>

            <Grid.Column centered width={6}>
                <Segment>
                    <Form>
                    <br/>
                        <header as='h3' ><b><u>Registro de marca</u></b></header>             
                        <br/>
                        <Form.Group>
                            <Form.Input name="nombre" label='Nombre de marca' placeholder='Nombre de marca' onChange={this.handleChange}/>
                            <Form.Input name="descripcion" label='Descripción del microemprendimiento' placeholder='Descripción' onChange={this.handleChange}/>
                        </Form.Group>
                        <header as='h3' ><b><u>Logo de la marca</u></b></header>             
                        <br/>
                        <Form.Group>
                            <Form.Field with={8}>
                            <ReactTooltip id='foto_tip' place="top" type="dark" effect="solid">
                            {/* <img src={ this.server + this.data.foto } height="200"></img> */}
                            </ReactTooltip>
                            <Input type="file" id="logo" name='logo' onChange={this.handleChange}/>
                            </Form.Field>
                        </Form.Group>
                        <header as='h3' ><b><u>Slogan de la marca</u></b></header>             
                        <br/>
                        <Form.Group>
                            <Form.Input name="frase" label='Slogan de la marca' placeholder='Slogan' onChange={this.handleChange}/>    
                        </Form.Group>
                        <center>
                            <Button positive  icon='check' content="Guardar" onClick={this.handleSubmit}></Button>
                        </center>
                    </Form>
                </Segment>                
            </Grid.Column>

            {/* Este es el modal para el mensaje */}
            <Modal
                open={this.state.messageOpen}
                onClose={this.handleMessageConfirm}
                size='small'
            >
                <Header icon='browser' content='Aviso' />
                <Modal.Content>
                    <h3>{this.state.messageInfo}</h3>
                </Modal.Content>
                <Modal.Actions>
                    <Button color='green' onClick={this.handleMessageConfirm} inverted>
                        <Icon name='checkmark' /> Aceptar
                        </Button>
                </Modal.Actions>
            </Modal>
            </div>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    marca: state.marcas.resultMarca
});

export default withRouter(connect(mapStateToProps, { postMarca })(RegMicroemprendimiento));