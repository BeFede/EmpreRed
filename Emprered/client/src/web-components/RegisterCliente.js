import React, {Component} from 'react'
import { Form, Segment, Button, Modal, Header, Icon, Container } from 'semantic-ui-react'
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom'
import { registrarCliente } from '../actions/clienteActions'

class RegisterC extends Component{

  constructor(props) {
    super(props);
    this.data = (props.data) ? props.data : null;
    this.state = { messageOpen: false, messageInfo: null, estadoResponse: null }
    this.registrar = this.registrar.bind(this);
}
handleConfirm = () => {
  this.setState({messageOpen: false});

  // Si el usuario fue registrado redirecciono
  
  if(this.state.estadoResponse === 'OK') {
    this.props.history.push('/');
  } 
}

handleChange = (e, {name, value, checked}) => {
     this.data = {
        ...this.data,
        [name]: (value) ? value : checked
     }
};

registrar(){
  this.props.registrarCliente(this.data)
  .then((data) => {
    if(data.estado === 'OK') {
      data.mensaje = "Se registró como cliente correctamente";
      this.renderMensaje(data);
    } 
    else {
      this.renderMensaje(data);
    }
  }, err => {
  console.log(err);
  });
}

handleSubmit = () => {
  this.registrar()
}

renderMensaje = (info) => {
  this.setState({messageInfo: info.mensaje, messageOpen: true, estadoResponse: info.estado});
}

render(){

  return (
    <div>
      <Container >
      <br></br>
        <Segment >
        <Form onSubmit={this.handleSubmit}>
          <Form.Group widths='equal'>
            <Form.Input name="nombre"  label='Nombre' placeholder='Nombre' onChange={this.handleChange} required/>
            <Form.Input name="apellido" label='Apellido' placeholder='Apellido' onChange={this.handleChange} required/>
            </Form.Group>
          <Form.Group widths='equal'>
          
            <Form.Input name="numero_documento" placeholder='CUIL' label='CUIL'  onChange={this.handleChange} required/>
              <Form.Input name="fecha_nacimiento" type='date' label='Fecha de nacimiento' onChange={this.handleChange} required />
          </Form.Group>
          <Form.Group widths='equal'>
            
            <Form.Input name="telefono" label='Teléfono' placeholder='Teléfono' onChange={this.handleChange} required/>
          </Form.Group>
            <center>
              <Button positive  icon='check' content="Registrar"/>
            </center>
          </Form>
          
        
        <Modal
            open={this.state.messageOpen}
            onClose={this.handleConfirm}
            size='small'
        >
            <Header icon='browser' content='Aviso' />
            <Modal.Content>
            <h3>{this.state.messageInfo}</h3>
            </Modal.Content>
            <Modal.Actions>
            <Button color='green' onClick={this.handleConfirm} inverted> 
                <Icon name='checkmark' /> Aceptar
            </Button>
            </Modal.Actions>
        </Modal>
        </Segment>

      </Container>
    </div>
    )
  } 
}


const mapStateToProps = state => ({
});

export default withRouter(connect(mapStateToProps, { registrarCliente })(RegisterC));

