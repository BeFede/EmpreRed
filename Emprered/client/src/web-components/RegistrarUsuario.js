import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import React, { Component, View } from 'react';
import { Menu, Button, Image, Icon, Segment, Modal, Container, Header, Form, Divider, Grid } from 'semantic-ui-react'
import FormUser from './FormUser';
import ModalManager from './ModalManager';
import { openModal } from '../actions/modalActions';
import { GoogleLogin } from 'react-google-login';
import { registroGoogle } from '../actions/usuarioActions';
import LogoE from '../assets/EmpreRedLogoE.png'
import  '../styles/RegistroUsuario.css'
import Footer2 from './Footer2.js'
import { Column, Row } from 'simple-flexbox';
import Emp2 from '../assets/EmpreRedRectangular.png'

const MenuSup = ()=>(
    <Menu fixed="top" inverted
    style= {{ "marginBottom": "0px",
    "display": "flex",
    "flexDirection": "row",
    "flex":1
    }}
>
    <Button className="item" onClick={this.handleButtonClick}>
        <i className="sidebar icon" />
    </Button>
    <Menu.Menu position= 'left'>
        <Menu.Item
            name='Home'
            active={true}
            href='/'
        >
        <Icon name='home'/>
        </Menu.Item>
    </Menu.Menu>
</Menu>
)
class RegUser extends Component {

    constructor(props) {
        super(props);
        this.state = { messageOpen: false, messageInfo: null, estadoResponse: null }
    }

    handleConfirm = () => {
        this.setState({messageOpen: false});

        // Si el usuario fue registrado redirecciono

        if(this.state.estadoResponse === 'OK') {
          this.props.history.push('/Ingreso');
        }
      }


    responseSuccessRegisterGoogle = (response) => {

        this.props.registroGoogle(response.tokenId)
        .then( (data) => {
            if(data.estado === 'OK') {
                this.renderMensaje(data)
            } else {
                this.renderMensaje(data);
            }
        }, err => {
            console.log(err);
        });
    }

    renderMensaje = (info) => {
        this.setState({messageInfo: info.mensaje, messageOpen: true, estadoResponse:info.estado});
    }

    responseFailureRegisterGoogle = (response) => {
        console.log(response);
    }

  render() {
    return (
        <body>
        <div className='Fondo'>
        <div className="primer-div">
        <br/><br/><br/><br/><br/><br/>

        </div>

        
        
       
            <ModalManager/>
            <MenuSup/>
            <center>
            <div className='contenedor'>
            <Container>
            
                <Grid>
               
                    <Grid.Row columns={2}>
                    
                        <Grid.Column>
                            <div className='logo'>
                                <center >
                                <Image src={LogoE}  />
                                </center>
                                <br/>
                            </div>
                        </Grid.Column>
                        
                        
                        <Grid.Column>
                        <br/>
                            <Segment raised center>
                                <Form>
                                    <Form.Field>
                                    <div>
                                        <h1> Unite a nuestra Red <br/>
                                         
                                        </h1>
                                    </div>
                                    </Form.Field>
                                    <Form.Field>
                                        <Button primary style={{"width": "190px", "padding-top": "10px","padding-bottom": "10px",
                                            "border-radius": "2px","border": "1px solid transparent","font-size": "16px", "font-weight": "bold","font-family": "Roboto"}}
                                            onClick={() => this.props.openModal({
                                            header: "Crear usuario",
                                            content: <FormUser/>
                                            })}>
                                            Crear cuenta
                                            </Button>
                                    </Form.Field>
                                    <Form.Field>
                                        <GoogleLogin
                                            clientId="216986984118-creegjhvfh6p3rpsm38t1qao8306jera.apps.googleusercontent.com"
                                            buttonText="Google"
                                            onSuccess={this.responseSuccessRegisterGoogle}
                                            onFailure={this.responseFailureRegisterGoogle}                                        
                                        />
                                    </Form.Field>
                                </Form>
                            </Segment>
                        </Grid.Column>    
                               
                    </Grid.Row>
                    
                   
                </Grid>
                
            </Container>
            </div>
     
            </center>

            {/* Este es el modal para el mensaje */}
            <Modal
                open={this.state.messageOpen}
                onClose={this.handleConfirm}
                size='small'
            >
                <Header icon='browser' content='Aviso' />
                <Modal.Content>
                <h3>{this.state.messageInfo}</h3>
                </Modal.Content>
                <Modal.Actions>
                <Button color='green' onClick={this.handleConfirm} inverted>
                    <Icon name='checkmark' /> Aceptar
                </Button>
                </Modal.Actions>
            </Modal>

            <Container>
            
                <Grid>
               
                   
        
                    
                   
                </Grid>
                
            </Container>
            
        
        
        </div>
        </body>
        


    )
  }
}
const mapStateToProps = state => ({

  });

export default withRouter(connect(mapStateToProps,{ openModal, registroGoogle })(RegUser));
