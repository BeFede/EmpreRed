import React, {Component} from 'react'
import { Form, Segment, Button, Modal, Header, Icon, Loader } from 'semantic-ui-react'
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom'
import { getCondicionesFrenteAIVA } from '../actions/tipificacionesActions';
import { registrarEmprendedor } from '../actions/emprendedoresActions';

  class DatosPersonales extends Component {

      constructor(props) {
          super(props);
          this.data = (props.data) ? props.data : null;
          //this.state = { cargado: false, messageOpen: false, messageInfo: null, estadoResponse: null, redirect: '/productos' };
          this.state = { cargado: false, messageOpen: false, messageInfo: null, estadoResponse: null }
          this.registrar = this.registrar.bind(this);
          this.optionsCondicionesIVA = [];
      }


    componentWillMount() {
      this.props.getCondicionesFrenteAIVA()
      .then(data => {
        this.setState({cargado:true});
      }, err => {
          console.log(err);
      });
      
    }

    handleConfirm = () => {
      this.setState({messageOpen: false});
  
      // Si el usuario fue registrado redirecciono
      
      if(this.state.estadoResponse === 'OK') {
        this.props.history.push('/');
      } 
    }

    getOptions() {
      for (const cond of this.props.condiciones_iva) {
          const opt = {key: cond.id, text: cond.descripcion, value: cond.id}
          this.optionsCondicionesIVA.push(opt);
      }
    }

    handleChange = (e, {name, value, checked}) => {
         this.data = {
            ...this.data,
            [name]: (value) ? value : checked
         }
    };

    registrar(){
      this.props.registrarEmprendedor(this.data)
      .then((data) => {
        if(data.estado === 'OK') {
          data.mensaje = "Se registró como microemprendedor correctamente";
          this.renderMensaje(data);
        } 
        else {
          this.renderMensaje(data);
        }
      }, err => {
      console.log(err);
      });
    }

    handleSubmit = () => {
      this.registrar()
    }

    renderMensaje = (info) => {
      this.setState({messageInfo: info.mensaje, messageOpen: true, estadoResponse: info.estado});
    }

    render() {

        if(this.state.cargado === true) {

          this.optionsCondicionesIVA = [];
          this.optionsTiposDocumento = [];
          this.getOptions();
        }

          return (
            <Segment loading={!this.state.cargado}>
            <Form onSubmit={this.handleSubmit}>
              <Form.Group widths='equal'>
                <Form.Input name="nombre"  label='Nombre' placeholder='Nombre' onChange={this.handleChange} required/>
                <Form.Input name="apellido" label='Apellido' placeholder='Apellido' onChange={this.handleChange} required/>
                </Form.Group>
              <Form.Group widths='equal'>
              
                <Form.Input name="numero_documento" placeholder='CUIL' label='CUIL'  onChange={this.handleChange} required/>
                  <Form.Input name="fecha_nacimiento" type='date' label='Fecha de nacimiento' onChange={this.handleChange} required />
              </Form.Group>
              <Form.Group widths='equal'>
                
                <Form.Select name="condicion_frente_IVA" label='Condición frente a IVA' onChange={this.handleChange} options={this.optionsCondicionesIVA} required/>
                <Form.Input name="telefono" label='Teléfono' placeholder='Teléfono' onChange={this.handleChange} required/>
               </Form.Group>
                <center>
                  <Button positive  icon='check' content="Registrar"/>
                </center>
              </Form>
              
            
            <Modal
                open={this.state.messageOpen}
                onClose={this.handleConfirm}
                size='small'
            >
                <Header icon='browser' content='Aviso' />
                <Modal.Content>
                <h3>{this.state.messageInfo}</h3>
                </Modal.Content>
                <Modal.Actions>
                <Button color='green' onClick={this.handleConfirm} inverted> 
                    <Icon name='checkmark' /> Aceptar
                </Button>
                </Modal.Actions>
            </Modal>
            </Segment>
          )
        }
    }

const mapStateToProps = state => ({
  condiciones_iva: state.tipificaciones.condiciones_iva,
  response: state.emps.mensaje
});

export default withRouter(connect(mapStateToProps, { getCondicionesFrenteAIVA, registrarEmprendedor })(DatosPersonales));
