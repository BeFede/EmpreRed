import React, {Component} from 'react'
import { Form, Segment } from 'semantic-ui-react'

  class Ubicacion extends Component {
      
    render() {
        return (
        <Segment inverted>    
        <Form inverted>
          <Form.Group widths='equal'>
            <Form.Input  label='Pais' placeholder='Pais' required/>
            <Form.Input  label='Provincia' placeholder='Provincia' required/>
          </Form.Group>
          <Form.Group widths='equal'>  
          <Form.Input  label='Localidad' placeholder='Localidad' required/>
          <Form.Input  label='Calle' placeholder='Calle' required/>
          </Form.Group>
          <Form.Group widths='equal'>
          <Form.Input  label='Código postal' placeholder='Código postal' required/> 
          <Form.Input  label='Número' placeholder='Número' required/>
          <Form.Input  label='Piso' placeholder='Piso'/>
          <Form.Input  label='Nro dto' placeholder='Nro dto'/>
          </Form.Group>
          <Form.Button>Siguiente</Form.Button>
        </Form>
        </Segment>
      )
    }
}

export default Ubicacion;