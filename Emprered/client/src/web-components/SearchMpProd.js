import _ from 'lodash';
import React, { Component } from 'react';
import { Search, Grid } from 'semantic-ui-react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types'
import { Item, Icon, Button } from 'semantic-ui-react'
import './../styles/searchStandard.css';
import { getProductos } from '../actions/productosActions';
import { getTodasMPEmp } from '../actions/matPrimaEmpActions';
import { selectMP, selectProd, removeMP, removeProd } from '../actions/mpOrProdSelectActions';

class SearchMPBasic extends Component {

  constructor(props) {
    super(props);
    this.source = [];
    }

  componentWillMount() {
    this.resetComponent();
    this.source = [];
    // Llamar al metodo que llene el source
  }
  


  obtenerItems() {
    
    let vectorPublicacionesConFormatoSearch = [];
    switch(this.props.item)
    {
      case 'Producto':
      this.props.getProductos()
      .then((data) => {
            for(let prod of data) {
              let prodConFormatoSearch = {
                // Siempre agregar un id si no lo tiene así la action puede darse cuenta cuando NO agregar una repetida
                id: prod.id,
                title: prod.nombre,
                tipo: 'Producto'
              }
              vectorPublicacionesConFormatoSearch.push(prodConFormatoSearch);
            }
            this.source = vectorPublicacionesConFormatoSearch;
        }, err => {
          this.source = [];
          console.log(err);
        })
      break;
      case 'Materia Prima':
      this.props.getTodasMPEmp()
      .then((data) => {
        for(let mp of data) {
          let mpConFormatoSearch = {
            // Siempre agregar un id si no lo tiene así la action puede darse cuenta cuando NO agregar una repetida
            id: mp.id,
            title: mp.nombre,
            tipo: 'Materia Prima'
          }
          vectorPublicacionesConFormatoSearch.push(mpConFormatoSearch);
        }
        this.source = vectorPublicacionesConFormatoSearch;
    }, err => {
      this.source = [];
      console.log(err);
    })
      break;
    }
  }

  resetComponent = () => this.setState({ isLoading: false, results: [], value: '' })

  handleResultSelect = (e, { result }) => {
    // Limpiamos si habia algo en MP o Prod seleccionado
    this.props.removeProd();
    this.props.removeMP();
    
    // Llamo al método que corresponda si es MP o Producto lo que se selecciona, esto deja la variable en redux
    if(result.tipo === 'Producto') {
      this.props.selectProd(result.id)
    } else {
      this.props.selectMP(result.id)
    }
    this.setState({ value: result.title });
  }

  handleSearchChange = (e, { value }) => {
    this.setState({ isLoading: true, value })

    setTimeout(() => {
      if (this.state.value.length < 1) return this.resetComponent()

      const re = new RegExp(_.escapeRegExp(this.state.value), 'i')
      const isMatch = result => re.test(result.title)

      this.setState({
        isLoading: false,
        results: _.filter(this.source, isMatch),
      })
    }, 300)
  }

  render() {
    
    const { isLoading, value, results } = this.state
    this.obtenerItems();
    
    return (
      <Grid>
        <Grid.Row centered>
          <Grid.Column width={16}>
            <Grid>
              <Grid.Row>
                <Grid.Column width={11}>
                  <Search
                    placeholder='Seleccione el item a buscar'
                    icon={ <Icon name='search' link onClick={() => this.selectItem(this.state.value)}/> }
                    noResultsMessage='No se encontraron resultados'
                    onKeyPress={this.handleKeyPress}
                    fluid 
                    input={{ fluid: true }}
                    id = "search"
                    className = "search"
                    loading={isLoading}
                    onResultSelect={this.handleResultSelect}
                    onSearchChange={_.debounce(this.handleSearchChange, 500, { leading: true })}
                    results={results}
                    value={value}
                    {...this.props}
                  />
                </Grid.Column>
                <Grid.Column width={4}>
                  <Button size='tiny' color='blue' onClick={()=>this.resetComponent()}>Limpiar</Button>
                </Grid.Column>
              </Grid.Row>
            </Grid>
          </Grid.Column>
        </Grid.Row>
      </Grid>
          
    )
  }
}

const resultRenderer = ({ title }) => 
    <Item>
        <Item.Content verticalAlign='middle'>
        <Item.Header>
            <Icon name='search' />
              {title}
        </Item.Header>
        </Item.Content>
    </Item>

resultRenderer.propTypes = {
  title: PropTypes.string
}

const SearchMPCustom = () => <SearchMPBasic resultRenderer={resultRenderer} />

const mapStateToProps = state => ({
  // itemMPorProd: state.mpOrProd
});

export default withRouter(connect(mapStateToProps, {getProductos, getTodasMPEmp, selectProd, selectMP, removeMP, removeProd })(SearchMPBasic, SearchMPCustom));
