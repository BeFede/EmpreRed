import _ from 'lodash'
import React, { Component } from 'react'
import { Search, Grid } from 'semantic-ui-react'
import { insertItem } from '../actions/tableFillActions';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { getTodasMPEmp } from '../actions/matPrimaEmpActions';
import { getProductos } from '../actions/productosActions';
import { urlMedia } from '../config/configs';
import '../styles/searchStandard.css';

class SearchStandard extends Component {

  constructor(props) {
    super(props);
    // Recibe por param el tipo de filtro a usar
    this.filtro = props.filtro;
    this.source = [];
  }

  componentWillMount() {
    this.resetComponent();
    // Hay que hacer un if para cada tipo de filtro que necesitemos, con un metodo que obtenga de las actions lo que tiene que llenar el search y lo ponga en
    // formato correcto, luego ese filtro hay que pasarselo por props al invocar al componente
    if(this.filtro === "Materias Primas") {
      this.obtenerTodasMatPrimas();
    } else if(this.filtro === "ProductosFabricados") {
      this.obtenerTodosProductosFabricados();
    } else if(this.filtro === "Productos") {
      this.obtenerTodosProductos();
    }
  }

  obtenerTodasMatPrimas() {

    let vectorMatPrimasConFormatoSearch = [];

    this.props.getTodasMPEmp()
    .then((data) => {
        for(let mp of data) {
            let mpConFormatoSearch = {
                // Siempre agregar un id si no lo tiene así la action puede darse cuenta cuando NO agregar una repetida
                id: mp.id,
                title: mp.nombre,
                materia_prima: mp.id,
                description: mp.observaciones,
                unidad_medida: mp.unidad_medida,
                price: mp.costo
            }
            vectorMatPrimasConFormatoSearch.push(mpConFormatoSearch);
        }
        this.source = vectorMatPrimasConFormatoSearch;
    }, err => {
      this.source = [];
      console.log(err);
    })
  }

  obtenerTodosProductosFabricados() {

    let vectorProductosConFormatoSearch = [];
    this.props.getProductos()
    .then((data) => {
        for(let prod of data) {
          if(prod.es_fabricado) {
              let prodConFormatoSearch = {
                // Siempre agregar un id si no lo tiene así la action puede darse cuenta cuando NO agregar una repetida
                id: prod.id,
                title: prod.nombre,
                producto: prod.nombre,
                description: prod.observaciones,
                unidad_medida: prod.unidad_medida,
                price: prod.costo,
                image: urlMedia + prod.foto
              }
            vectorProductosConFormatoSearch.push(prodConFormatoSearch);
          }
        }
        this.source = vectorProductosConFormatoSearch;
    }, err => {
      this.source = [];
      console.log(err);
    })
  }

  obtenerTodosProductos() {

    let vectorProductosConFormatoSearch = [];
    this.props.getProductos()
    .then((data) => {
        for(let prod of data) {
          let prodConFormatoSearch = {
            // Siempre agregar un id si no lo tiene así la action puede darse cuenta cuando NO agregar una repetida
            id: prod.id,
            title: prod.nombre,
            producto: prod.nombre,
            description: prod.observaciones,
            unidad_medida: prod.unidad_medida,
            price: prod.costo,
            image: urlMedia + prod.foto,
            foto: urlMedia + prod.foto,
            categoria: prod.categoria,
            subcategoria: prod.subcategoria
          }
          vectorProductosConFormatoSearch.push(prodConFormatoSearch);
        }
        this.source = vectorProductosConFormatoSearch;
    }, err => {
      this.source = [];
      console.log(err);
    })
  }

  resetComponent = () => this.setState({ isLoading: false, results: [], value: '' })

  handleResultSelect = (e, { result }) => {
    this.setState({ value: result.title });
    this.props.insertItem(result);
  }

  handleSearchChange = (e, { value }) => {
    this.setState({ isLoading: true, value })

    setTimeout(() => {
      if (this.state.value.length < 1) return this.resetComponent()

      const re = new RegExp(_.escapeRegExp(this.state.value), 'i')
      const isMatch = result => re.test(result.title)

      this.setState({
        isLoading: false,
        results: _.filter(this.source, isMatch),
      })
    }, 300)
  }

  render() {
    const { isLoading, value, results } = this.state

    return (
        <Grid.Column width={3}>
          <Search className="search"
            noResultsMessage='No se encontraron resultados'
            loading={isLoading}
            onResultSelect={this.handleResultSelect}
            onSearchChange={_.debounce(this.handleSearchChange, 500, { leading: true })}
            results={results}
            value={value}
            {...this.props}
          />
        </Grid.Column>
    )
  }
}

const mapStateToProps = state => ({
  items: state.tableData.items
});

export default withRouter(connect(mapStateToProps, { insertItem, getTodasMPEmp, getProductos })(SearchStandard));
