import React, { Component } from 'react';
import { Card, Segment, Message, Grid, Confirm, Button, Divider } from 'semantic-ui-react'
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import MenuPrincipal from './../MenuPrincipal';
import MenuEmprendedor from '../MenuEmprendedor';
import {getUser} from '../../config/configs';
import { openModal } from '../../actions/modalActions';
import { getEventos, postEvento, deleteEvento } from '../../actions/eventosActions';
import ModalEvento from '../eventos/ModalEvento';
import ModalManager from '../ModalManager';


class MisTareas extends Component {

    constructor(props){
        super(props);
        this.state = {open: false, idABorrar: null, cargado:false, tieneTareasPendientes:false, tienetareasVencidas: false};
        this.userLogeado = (getUser()) ? getUser() : null;
    }

    componentWillMount() {
        this.tareasPendientesRendered = [];
        this.tareasVencidasRendered = [];
        this.obtenerEventos();
    }

    showConfirmDialog = (id) => this.setState({open: true, idABorrar: id});
    handleCancel = () => this.setState({open: false});

    handleConfirm = () => {
        this.setState({open: false});
        this.deleteTarea(this.state.idABorrar);
    }

    deleteTarea(id) {
        this.props.deleteEvento(this.props.tareas.eventosPendientes, id);
    }

    obtenerEventos() {
        this.props.getEventos()
        .then(data => {
            const tieneTareasPendientes = (data.datos.eventosPendientes.length > 0) ? true : false;
            const tienetareasVencidas = (data.datos.eventosVencidos.length > 0) ? true : false;
            this.setState({cargado: true, tieneTareasPendientes: tieneTareasPendientes, tienetareasVencidas: tienetareasVencidas});
        }, err => {
            console.log(err);
        })
    }

    renderTareasPendientes() {
        return(
            <Card.Group itemsPerRow={1}>
                {this.tareasPendientesRendered}
            </Card.Group>
        )
    }

    renderTareasVencidas() {
        return(
            <Card.Group itemsPerRow={1}>
                {this.tareasVencidasRendered}
            </Card.Group>
        )
    }

    renderTarea(tarea, pendiente) {
        const options = { year: 'numeric', month: 'long', day: 'numeric'};
        const fechaAMostrar = new Date(tarea.eventos_meta[0].repeat_start).toLocaleDateString("es-ES", options);
        const horaAMostrar = new Date(tarea.eventos_meta[0].repeat_start).toLocaleTimeString("es-ES", {hour: '2-digit', minute:'2-digit'});
        // Transformo a días los segundos que vienen del back
        const diasRepeticion = (tarea.eventos_meta[0].repeat_interval) ? (tarea.eventos_meta[0].repeat_interval / 86400) : null;
        return(
            <Card
            style={(pendiente) ? {backgroundColor:'#A9F5BC'} : {backgroundColor:'#F78181'}}
            header={`${fechaAMostrar} - ${horaAMostrar} hs`}
            meta={(diasRepeticion) ? `Se repite cada ${diasRepeticion} días` : 'No se repite'}
            description={tarea.nombre}
            extra={ (pendiente) ?
                <div>
                    <Button content='Borrar' circular icon='times' labelPosition='left' size='mini' color='red' floated='right' onClick={()=>this.showConfirmDialog(tarea.id)}/>
                    <Button content='Editar' circular icon='edit' labelPosition='left' size='mini' color='blue' floated='right'
                    onClick={() => this.props.openModal({
                        header: "Programación de tarea",
                        content: <ModalEvento data={tarea}/>
                    })}
                    />
                </div>
                :
                null
            }
            />
        )
    }

    render() {

        if(this.state.cargado) {
            this.tareasPendientesRendered = this.props.tareas.eventosPendientes.map(tarea =>{
                return this.renderTarea(tarea, true)
            });

            this.tareasVencidasRendered = this.props.tareas.eventosVencidos.map(tarea =>{
                return this.renderTarea(tarea, false)
            });
        }

        return (
            <div className="DivPrincipal" style={{overflow:'auto', backgroundColor:'#f2f2f2'}}>
                <div id="MenuTop">
                    <MenuPrincipal/>
                </div>
                <div  className = "row">
                    <div id="MenuLateral">
                        {(this.userLogeado) ? <MenuEmprendedor/> : null}
                    </div>
                    <div style={{"paddingTop":"61px", marginLeft:"210px"}}>
                        <Grid>
                            <Grid.Row centered>
                                <Grid.Column width={12}>
                                    <Segment loading={!this.state.cargado}>
                                        <Grid>
                                            <Grid.Row centered>
                                                <Grid.Column width={16}>
                                                    <center><h1>Mis tareas</h1></center>
                                                </Grid.Column>
                                            </Grid.Row>

                                            <Divider/>

                                            <Grid.Row centered>
                                                <Grid.Column width={16}>
                                                    <center>
                                                        <Button content='Programar tarea' circular icon='tasks' labelPosition='left' size='huge' color='blue'
                                                        onClick={() => this.props.openModal({
                                                            header: "Programación de tarea",
                                                            content: <ModalEvento/>
                                                        })}
                                                        />
                                                    </center>
                                                </Grid.Column>
                                            </Grid.Row>

                                            <Divider/>

                                            <Grid.Row centered>
                                                <Grid.Column width={8}>
                                                    <Segment color='orange'>
                                                        <Grid>
                                                            <Grid.Row centered>
                                                                <Grid.Column width={16}>
                                                                    <center>
                                                                        <h1>Tareas Próximas</h1>
                                                                    </center>
                                                                </Grid.Column>
                                                            </Grid.Row>

                                                            <Divider/>
                                                            
                                                            {
                                                                (this.props.tareas && this.props.tareas.eventosPendientes.length > 0)
                                                                ?
                                                                <Grid.Row>
                                                                    <Grid.Column width={16}>
                                                                        {this.renderTareasPendientes()}
                                                                    </Grid.Column>
                                                                </Grid.Row>
                                                                :
                                                                <Grid.Row>
                                                                    <Grid.Column width={16}>
                                                                        <center>
                                                                            <Message size='massive' color='orange' compact>No posee tareas próximas</Message>
                                                                        </center>
                                                                    </Grid.Column>
                                                                </Grid.Row>
                                                            }
                                                            
                                                        </Grid>
                                                    </Segment>
                                                </Grid.Column>

                                                <Divider vertical></Divider>

                                                <Grid.Column width={8}>
                                                    <Segment color='orange'>
                                                        <Grid>
                                                            <Grid.Row centered>
                                                                <Grid.Column width={16}>
                                                                    <center>
                                                                        <h1>Tareas Vencidas (últimos 7 días)</h1>
                                                                    </center>
                                                                </Grid.Column>
                                                            </Grid.Row>

                                                            <Divider/>

                                                            {
                                                                (this.state.tienetareasVencidas)
                                                                ?
                                                                <Grid.Row>
                                                                    <Grid.Column width={16}>
                                                                        {this.renderTareasVencidas()}
                                                                    </Grid.Column>
                                                                </Grid.Row>
                                                                :
                                                                <Grid.Row>
                                                                    <Grid.Column width={16}>
                                                                        <center>
                                                                            <Message size='massive' color='orange' compact>No posee tareas vencidas en los últimos 7 días</Message>
                                                                        </center>
                                                                    </Grid.Column>
                                                                </Grid.Row>
                                                            }
                                                        </Grid>
                                                    </Segment>
                                                </Grid.Column>
                                            </Grid.Row>
                                        </Grid>
                                    </Segment>
                                </Grid.Column>
                            </Grid.Row>
                        </Grid>

                        <ModalManager/>

                        <Confirm
                            open={this.state.open}
                            header='Borrar tarea programada'
                            content='¿Está seguro que desea borrar esta tarea?'
                            cancelButton='No'
                            confirmButton='Si'
                            onCancel={this.handleCancel}
                            onConfirm={this.handleConfirm}
                        />
                    </div> 
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    tareas: state.eventos.resultEventos
});
  
export default withRouter(connect(mapStateToProps, { openModal, getEventos, deleteEvento })(MisTareas));