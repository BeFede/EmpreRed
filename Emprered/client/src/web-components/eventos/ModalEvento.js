import React, { Component } from 'react';
import { Form, TextArea, Button, Grid, Container, Modal, Header, Icon, Radio, Message } from 'semantic-ui-react';
import { connect } from 'react-redux';
import { closeModal } from '../../actions/modalActions';
import { postEvento } from '../../actions/eventosActions';

class ModalEvento extends Component {

    constructor(props) {
        super(props);
        this.data = (props.data) ? props.data : null;
        this.state = { repiteDisabled:(this.data && this.data.eventos_meta[0].repeat_interval !== null ? false : true), 
            errorFecha: false, 
            esModificacion:false,
            messageInfo: null,
            messageOpen: false
        };
    }

    componentWillMount(){
        if(this.data) {
            // LA COREANADA QUE TENGO QUE HACER PARA TRANSFORMAR LO QUE VIENE DEL BACK EN UNA FECHA Y HORA ARGENTINA PARA PASARLE AL INPUT
            // PORQUE EL MUY PUTO NO DIFERENCIA ENTRE Z Y NO Z CONCHUDO DEL ORTO ME CAGO EN TODOS
            let fechaEvento = new Date(this.data.eventos_meta[0].repeat_start.substring(0,19) + '+0300');
            let fechaHasta = (this.data.eventos_meta[0].repeat_until) ? new Date(this.data.eventos_meta[0].repeat_until.substring(0,19) + '+0300') : null;
            this.data.fecha_hora = fechaEvento.toISOString().substring(0,19);
            // Transformo a días los minutos que vienen del back
            this.data.repeticion = (this.data.eventos_meta[0].repeat_interval) ? this.data.eventos_meta[0].repeat_interval / 86400 : null;
            this.data.repetir_hasta = (this.data.eventos_meta[0].repeat_until) ? fechaHasta.toISOString().substring(0,19) : null;
            this.setState({esModificacion:true});
        }
    }

    renderMensaje = (info) => {
        let mensaje = '';
        if(info.estado === 'OK') {
            mensaje = 'Tarea programada';
        } else {
            mensaje = info.mensaje
        }
        this.setState({ messageInfo: mensaje, messageOpen: true });
    }

    handleMessageConfirm = () => {
        this.setState({messageOpen: false});    
        this.props.closeModal();   
    }

    handleChange = (e, {name, value, checked}) => {
        this.data = {
           ...this.data,
           [name]: (value) ? value : checked
        }
        if(name === 'se_repite') {
            this.setState({repiteDisabled:!checked});
        }

        if(name === 'fecha_hora') {
            const fechaHoraActual = new Date();
            const fechaHoraSeteada = new Date(value);
            if(fechaHoraSeteada <= fechaHoraActual) {
                this.setState({errorFecha:true});
            } else {
                this.setState({errorFecha:false});
            }
        }

        if(name === 'repetir_hasta') {
            let fechaEvento = null;
            if(document.getElementById("fecha_hora").value !== '') {
                fechaEvento = new Date(document.getElementById("fecha_hora").value);
            }
            const fechaHoraActual = new Date();
            const fechaHoraSeteada = new Date(value);
            if(fechaHoraSeteada <= fechaHoraActual) {
                this.setState({errorFecha:true});
            } else {
                // Si ya me setearon la fecha del evento y esta que quiero setear es menor a esa, tiro error
                if(fechaEvento && fechaHoraSeteada < fechaEvento) {
                    this.setState({errorFecha:true});
                } else {
                    this.setState({errorFecha:false});
                }
            }
        }

        
    };

    handleSubmit = () => {
        // Fijarse que pasa si es modificacion y si no
        // Cuando paso la repeticion, la multiplico por 86400 para pasarla a segundos, porque esta en dias
        let dataFinal = {};

        // Si es modificacion la data tenia que tener un id
        if(this.data.id) {
            dataFinal.id = this.data.id;
        }

        dataFinal.descripcion = 'a';
        dataFinal.nombre = this.data.nombre;
        dataFinal.prioridad = 4;
        dataFinal.enviar_mail = (this.data.enviar_mail) ? this.data.enviar_mail : false;
        dataFinal.enviar_notificacion = true;
        // Si esta en disabled el repite le envio en null esos datos, sino los paso si tienen value y sino null
        if(this.state.repiteDisabled) {
            dataFinal.eventos_meta = [
                {
                    activo: true,
                    repeat_start: this.data.fecha_hora,
                    repeat_interval: null,
                    repeat_until: null
    
                }
            ]
        } else {
            dataFinal.eventos_meta = [
                {
                    activo: true,
                    repeat_start: this.data.fecha_hora,
                    repeat_interval: (this.data.repeticion) ? (this.data.repeticion * 86400) : null,
                    repeat_until: (this.data.repetir_hasta) ? this.data.repetir_hasta : null
    
                }
            ]
        }
        console.log(dataFinal);
        this.props.postEvento(dataFinal)
        .then(data => {
            this.renderMensaje(data);
        }, err => {
            console.log(err);
        })
    }

    renderModalMensaje() {
        return (
            <Modal
                open={this.state.messageOpen}
                onClose={this.handleMessageConfirm}
                size='small'
            >
                <Header icon='browser' content='Aviso' />
                <Modal.Content>
                <h3>{this.state.messageInfo}</h3>
                </Modal.Content>
                <Modal.Actions>
            <Button color='green' onClick={this.handleMessageConfirm} inverted>
                    <Icon name='checkmark' /> Aceptar
                </Button>
                </Modal.Actions>
            </Modal>
        );
    }

    render() {

        if (!this.data){
            this.data = {
              
            }
        }

        return (
        <div>
            <Grid>
                <Grid.Row>
                    <Grid.Column width={16}>
                        <Container style={{padding: '20px',  marginBottom:'30px'}}>
                            <Form onSubmit={this.handleSubmit} error={this.state.errorFecha}>
                                
                                <Form.Field width={6}>
                                    <label>Fecha y Hora de la tarea (MM/DD/AAAA)</label>
                                    <Form.Input id="fecha_hora" error={this.state.errorFecha} type="datetime-local" name='fecha_hora' defaultValue={this.data.fecha_hora} onChange={this.handleChange} required/>
                                </Form.Field>

                                <Form.Group >
                                    <Form.Field width={2}>
                                        <label>¿Se repite?</label>
                                        <Radio name='se_repite' toggle defaultChecked={(this.data.repeticion)} onChange={this.handleChange}/>
                                    </Form.Field>
                                    <Form.Field  disabled={this.state.repiteDisabled} width={4}>
                                        <label>¿Cada cuantos días se repite?</label>
                                        <Form.Input  type="number" name='repeticion' defaultValue={this.data.repeticion} onChange={this.handleChange} required={!this.state.repiteDisabled}/>
                                    </Form.Field>
                                    <Form.Field  disabled={this.state.repiteDisabled} width={6}>
                                        <label>Repetir hasta</label>
                                        <Form.Input error={this.state.errorFecha} type="datetime-local" name='repetir_hasta' defaultValue={this.data.repetir_hasta} onChange={this.handleChange} required={!this.state.repiteDisabled}/>
                                    </Form.Field>
                                </Form.Group>

                                <Form.Field width={5}>
                                    <label>Enviar e-mail de notificación</label>
                                    <Radio name='enviar_mail' toggle defaultChecked={(this.data.enviar_mail)} onChange={this.handleChange}/>
                                </Form.Field>
                                
                                <Form.Field
                                    id='form-textarea-nombre'
                                    name='nombre'
                                    control={TextArea}
                                    defaultValue={this.data.nombre}
                                    label='Descripción de la tarea'
                                    onChange={this.handleChange}
                                    required
                                />

                                <Message
                                    error
                                    header='Fecha inválida'
                                    content='Ingrese una fecha y hora superiores a la actual / definida'
                                />

                                <Button positive disabled={this.state.errorFecha} icon='check' content="Confirmar" floated='right'></Button>
                            </Form>
                            <Button negative  icon='cancel' content="Cancelar" floated='right' onClick={this.props.closeModal}></Button>
                        </Container>
                    </Grid.Column>
                </Grid.Row>
            </Grid>
            {this.renderModalMensaje()}
        </div>
        )
    }
}

const mapStateToProps = state => ({
    
});

export default connect(mapStateToProps, { closeModal, postEvento })(ModalEvento);
