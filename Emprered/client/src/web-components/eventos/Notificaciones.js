import React, { Component } from 'react';
import { Card, Segment, Message, Grid, Icon, Button, Divider, Transition, List } from 'semantic-ui-react'
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import MenuPrincipal from './../MenuPrincipal';
import MenuEmprendedor from '../MenuEmprendedor';
import {getUser} from '../../config/configs';
import { getEventos, postEvento, deleteEvento } from '../../actions/eventosActions';

class Notificaciones extends Component {

    constructor(props){
        super(props);
        this.state = { cargado:true, tieneNotificaciones: false };
        this.userLogeado = (getUser()) ? getUser() : null;
    }

    componentWillMount() {
        this.notificacionesRendered = [];
        this.notificaciones = [
            { descripcion: 'Se venció la tarea "Comprar enanos" el día 3/4 a las 17hs', id:1, fecha: '2019-04-03T17:00:00Z' },
            { descripcion: 'Se venció la tarea "Tu vieja" el día 4/4 a las 15hs', id:2, fecha: '2019-04-04T15:00:00Z' }
        ]
        // this.obtenerNotificaciones();
    }

    handleRemove = (id) => {
        for (var i = 0; i < this.notificaciones.length; i++) {
            if (id === this.notificaciones[i].id){
                this.notificaciones.splice(i,1);
            }
        }
        this.setState({tieneNotificaciones:true})
        console.log(this.notificaciones);
    }
    
    obtenerNotificaciones() {
        // this.props.getEventos()
        // .then(data => {
        //     const tieneTareasPendientes = (data.datos.eventosPendientes.length > 0) ? true : false;
        //     const tienetareasVencidas = (data.datos.eventosVencidos.length > 0) ? true : false;
        //     this.setState({cargado: true, tieneTareasPendientes: tieneTareasPendientes, tienetareasVencidas: tienetareasVencidas});
        // }, err => {
        //     console.log(err);
        // })
    }

    renderNotificaciones() {
        return(
            <Transition.Group itemsPerRow={1} as={Card.Group} duration={500} animation='scale' size='huge' verticalAlign='middle'>
                    {this.notificacionesRendered}
            </Transition.Group>
        )
    }

    renderNotificacion(notif) {
        const options = { year: 'numeric', month: 'long', day: 'numeric'};
        const fechaAMostrar = new Date(notif.fecha).toLocaleDateString("es-ES", options);
        const horaAMostrar = new Date(notif.fecha).toLocaleTimeString("es-ES", {hour: '2-digit', minute:'2-digit'});
        return(
            <Card
            style={{backgroundColor:'#ffcc99'}}
            >

                <Card.Content>
                    <Icon style={{float:'right'}} size='huge' name='calendar alternate outline' />
                    <Card.Header><h4>{`${fechaAMostrar} - ${horaAMostrar} hs`}</h4></Card.Header>
                    <Card.Description >
                        <p style={{'font-size':'180%', "overflow": "hidden", "word-wrap": "break-word"}}>{notif.descripcion}</p>
                    </Card.Description>
                </Card.Content>
                <Card.Content extra>
                    <Button circular icon='check circle' color='blue' floated='right' onClick={() => this.handleRemove(notif.id)}/>
                </Card.Content>
            
            </Card>
        )
    }

    render() {

        if(this.state.cargado) {
            this.notificacionesRendered = this.notificaciones.map(notif =>{
                return this.renderNotificacion(notif)
            });
        }

        return (
            <div className="DivPrincipal" style={{overflow:'auto', backgroundColor:'#f2f2f2'}}>
                <div id="MenuTop">
                    <MenuPrincipal/>
                </div>
                <div  className = "row">
                    <div id="MenuLateral">
                        {(this.userLogeado) ? <MenuEmprendedor/> : null}
                    </div>
                    <div style={{"paddingTop":"61px", marginLeft:"210px"}}>
                        <Grid>
                            <Grid.Row centered>
                                <Grid.Column width={8}>
                                    <Segment loading={!this.state.cargado}>
                                        <Grid>
                                            <Grid.Row centered>
                                                <Grid.Column width={16}>
                                                    <center><h1>Notificaciones</h1></center>
                                                </Grid.Column>
                                            </Grid.Row>

                                            <Divider/>

                                            <Grid.Row centered>
                                                <Grid.Column width={16}>
                                                    <Segment color='orange'>
                                                        <Grid>
                                                            {
                                                                (this.notificaciones && this.notificaciones.length > 0)
                                                                ?
                                                                <Grid.Row>
                                                                    <Grid.Column width={16}>
                                                                        {this.renderNotificaciones()}
                                                                    </Grid.Column>
                                                                </Grid.Row>
                                                                :
                                                                <Grid.Row>
                                                                    <Grid.Column width={16}>
                                                                        <Message icon='check circle' size='massive' info compact content='No posee notificaciones' />
                                                                    </Grid.Column>
                                                                </Grid.Row>
                                                            }
                                                        </Grid>
                                                    </Segment>
                                                </Grid.Column>

                                            </Grid.Row>
                                        </Grid>
                                    </Segment>
                                </Grid.Column>
                            </Grid.Row>
                        </Grid>
                    </div> 
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    tareas: state.eventos.resultEventos
});
  
export default withRouter(connect(mapStateToProps, { })(Notificaciones));