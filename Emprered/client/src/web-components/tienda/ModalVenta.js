import React, { Component } from 'react';
import { Grid, Divider, Segment, Card, Input, Button, Item, Label, Icon } from 'semantic-ui-react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { closeModal } from '../../actions/modalActions';
import { urlMedia } from '../../config/configs';
import logoMercadoPago from '../../assets/MercadoPago.png';
import { obtenerLinkMercadoPago } from '../../actions/tiendaActions';

export class ModalVenta extends Component {

    constructor(props) {
        super(props);
        this.handleChange = this.handleChange.bind(this);
        this.data = (props.data) ? props.data : null;
        if(this.data) {
            this.data = {...this.data, cantidad: 1}
        }
        this.state = {cambioData:false};
    }

    componentWillMount(){
         
    }

    handleChange = (e, {name, value}) => {

        this.data = {
            ...this.data,
            [name]: (value) ? parseInt(value) : 0
        }
        // Seteo state aca para que se me actualice el label de precio
        this.setState({cambioData:true});
    };

    routeMercadoPago() {
        this.props.obtenerLinkMercadoPago(this.data.id, this.data.cantidad)
        .then(data => {
            this.props.closeModal();
            console.log(data.datos);
            window.location = data.datos;
        }, err => {
            console.log(err);
        })
    }

    render() {
        return (
            <div>
                <Grid>
                    <Grid.Row>
                        <Grid.Column width={16}>
                            <Segment size ='big'>
                                <Grid verticalAlign='middle'>
                                    <Grid.Row centered>
                                        <Grid.Column width={8}>
                                            {this.renderImagenConDatos()}
                                        </Grid.Column>

                                        <Divider vertical ></Divider>

                                        <Grid.Column width={8}>
                                            {this.renderDatosCompra()}
                                        </Grid.Column>
                                    </Grid.Row>
                                </Grid>
                            </Segment>
                        </Grid.Column>
                    </Grid.Row>

                </Grid>
            </div>
        )
    }

    renderImagenConDatos(){
        const foto = urlMedia + this.data.imagenes[0].foto;
        const titulo = this.data.titulo;
        const categoria = this.data.producto.categoria;
        const subcategoria = this.data.producto.subcategoria;
        const emprendedor = this.data.emprendedor;
        const precio = this.data.precio_venta;

        const extra = (
            <center>
                <h2>
                ${precio}
                </h2>
            </center>
        );

        return (
            <Grid columns={1}>
                <Grid.Column width={16}>
                    <Segment>
                        <Card
                            color='orange'
                            fluid
                            image={foto}
                            header={titulo}
                            meta={categoria + ' - ' + subcategoria}
                            descripcion={'De: ' + emprendedor.nombre + ' ' + emprendedor.apellido }
                            extra={extra}
                        />
                    </Segment>
                </Grid.Column>
            </Grid>
        )
    }

    renderDatosCompra(){
        return (
            <Grid centered verticalAlign='middle' columns={1}>
                <Grid.Column width={16}>
                    <Segment>
                        <Grid centered verticalAlign='middle'>
                            <Grid.Row centered>
                                <Grid.Column width={16}>
                                    <h2>Selecciona la cantidad a comprar</h2>
                                    <Input size='small' type="number"  step="any" min={0} name='cantidad' defaultValue={1} onChange={this.handleChange}/>
                                </Grid.Column>
                            </Grid.Row>

                            <Grid.Row centered>
                                <Grid.Column width={16}>
                                    <h2>Total de su compra</h2>
                                    <Label size='massive' color='orange'>${this.data.cantidad * this.data.precio_venta}</Label>
                                </Grid.Column>
                            </Grid.Row>

                            <Grid.Row centered>
                                <Grid.Column width={16}>
                                    <Segment>
                                        <Item.Group>
                                            <Item>
                                                <Item.Image size='tiny' src={logoMercadoPago} />
                                                <Item.Content verticalAlign='middle'>
                                                    <Item.Header><b>Pagar con Mercado Pago</b></Item.Header>
                                                    <Item.Description>
                                                        <Button color='blue' icon labelPosition='left' onClick={() => this.routeMercadoPago()}>
                                                            Continuar
                                                            <Icon name='shopping cart' />
                                                        </Button>
                                                    </Item.Description>
                                                </Item.Content>
                                            </Item>
                                        </Item.Group>
                                    </Segment>
                                </Grid.Column>
                            </Grid.Row>
                        </Grid>
                    </Segment>
                </Grid.Column>
            </Grid>
        )
    }
}

const mapStateToProps = state => ({
    
});

export default withRouter(connect(mapStateToProps, { closeModal, obtenerLinkMercadoPago })(ModalVenta));
