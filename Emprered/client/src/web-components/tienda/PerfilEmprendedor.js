import React, { Component } from 'react';
import { Card, Segment, Image, Grid,  Loader, Dimmer, Icon, Label, Button, Divider, Header, Item, Tab, Message, Rating } from 'semantic-ui-react'
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import MenuPrincipal from './../MenuPrincipal';
import { getCatalogoTienda } from '../../actions/tiendaActions';
import { urlMedia } from '../../config/configs';
import imageNotFound from '../../assets/imageNotFound.jpeg';
import Avatar from '../../assets/default-avatar.png';
import {getUser} from '../../config/configs';

  class PerfilEmprendedor extends Component
  {
    constructor(props)
    {
        super(props)    
        this.emprendedor = (this.props.location.state.e) ? this.props.location.state.e : null;
        this.marca = (this.props.location.state.m) ? this.props.location.state.m : null;
        this.catalogos = (this.props.location.state.e.catalogos) ? this.props.location.state.e.catalogos : null;
        this.state = {cargado: false, catalogo: null, pagina_actual: 1};
        this.userLogeado = (getUser()) ? getUser() : null;
    }

    componentWillMount() {
      // Le paso el id del catalogo minorista por ahora y el numero de pagina actual, por defecto trae 5 resultados desde el action
      this.catalogoMinorista = this.catalogos[0];
      this.obtenerCatalogo(this.emprendedor.id, this.state.pagina_actual);
    }

    obtenerCatalogo(idCatalogo, paginaActual) {
      this.props.getCatalogoTienda(idCatalogo,paginaActual)
      .then(data => {
        this.setState({catalogo: data.datos, cargado: true, pagina_actual: paginaActual});
      }, err => {
        console.log(err);
      })
    }

    routeDetalleCatalogo(idDetalle) {
      this.props.history.push(
        {
        pathname: '/publicacion',
        state: {catalogo: this.catalogoMinorista.id, idDetalle: idDetalle}
        }
    );
    }

    render()
    {
        if(this.emprendedor)
        {
        return(
            <div className="DivPrincipal" style={{overflow:'auto', backgroundColor:'#f2f2f2'}}>
            <div id="MenuTop">
                <MenuPrincipal/>
            </div>
            <div  className = "row">
                <div style={{"paddingTop":"61px"}}>
                  <Grid>
                    <Grid.Row centered>
                      <Grid.Column width={10}>
                        <Segment>
                          <h1>Perfil de emprendedor</h1>  
                          {
                            (this.state.cargado) ?
                            <Grid>
                              <Grid.Row>
                                <Grid.Column width={16}>
                                  <Segment color='orange'>
                                    {this.renderDatosEmprendedor()}
                                  </Segment>

                                  <Divider/>

                                  <Grid>
                                    <Grid.Row>
                                      <Grid.Column width={16}>
                                        <h2>Catálogos</h2>
                                      </Grid.Column>
                                    </Grid.Row>
                                  </Grid>

                                  <Segment color='orange'>
                                    {this.renderCatalogos()}
                                  </Segment>
                                </Grid.Column>
                              </Grid.Row>
                            </Grid>
                            :
                            <Dimmer active inverted>
                              <Loader active inline='centered'/>
                            </Dimmer>
                          }  
                        </Segment>
                      </Grid.Column>
                    </Grid.Row>
                  </Grid>               
             </div>
            </div>
          </div>   
        )
    }
    else{
        return(null)
    }
  }

  renderDatosEmprendedor(){
    return(
      <Grid>
        <Grid.Row>
          <Grid.Column width={4} >
            {this.renderFotoEmprendedor()}
          </Grid.Column>   
            
          <Grid.Column width={12}>              
              {this.renderDatosYMarca()}
          </Grid.Column>   
        </Grid.Row>
      </Grid>
    )
  }

  renderFotoEmprendedor(){
    return(
      <Grid>
        <Grid.Column width={16}>
          <Segment>
            <Image size='medium' src={(this.emprendedor.foto !== "") ? urlMedia+this.emprendedor.foto : Avatar} />
          </Segment>
        </Grid.Column>
      </Grid>
    )
  }

  renderDatosYMarca(){
    return(
      <Grid>
        <Grid.Row>
            <Grid.Column width={16}>
              <Grid>
                <Grid.Row>
                  <Grid.Column width={16}>
                    {(this.userLogeado !== null)
                      ?
                      <Segment>
                        <Header as='h2'>
                          <Icon name='user' />
                          <Header.Content>{this.emprendedor.nombre} {this.emprendedor.apellido}</Header.Content>
                        </Header>

                        <Header as='h4'>
                          <Header.Content>Contacto: {(this.emprendedor.telefono!=="") ? this.emprendedor.telefono: "N/A" }</Header.Content>
                        </Header>
                      </Segment>
                      :
                      <Segment>
                        <Message warning>
                          <Message.Header>Debe estar logeado para poder visualizar la información personal del emprendedor</Message.Header>
                        </Message>
                      </Segment>
                      }
                    
                  </Grid.Column>     
                </Grid.Row>

                <Grid.Row>
                  <Grid.Column  width={16}>
                    <Segment>
                      <Item.Group>
                        <Item>
                          <Item.Image size='tiny' src={(this.marca.logo !== "") ? urlMedia+this.marca.logo : imageNotFound} />

                          <Item.Content verticalAlign='middle'>
                            <Item.Header>
                              <Icon name='tags' />                              
                                Marca:
                            </Item.Header>
                            <br/>
                            <br/>
                            <Item.Header as='a'>
                              <Label size='medium' color='orange' image>
                                {this.marca.nombre}
                              </Label>  
                            </Item.Header>
                          </Item.Content>
                        </Item>
                      </Item.Group>
                    </Segment>
                  </Grid.Column>
                </Grid.Row>        
              </Grid>    
            </Grid.Column>   
        </Grid.Row>  
      </Grid>
    )
  }

  renderCatalogos(){
    const panes = [
      { menuItem: 'Catálogo del emprendedor', render: () => <Tab.Pane>{this.renderItemsCatalogo()}</Tab.Pane> },
    ];

    return (
      <Grid>
        <Grid.Column width={16}>
          <Tab menu={{ fluid: true, vertical: true }} menuPosition='left' panes={panes} />
        </Grid.Column>
      </Grid>
    )

  }

  renderItemIndividual(item){
    return(
      <Item>
              <Item.Image size='tiny' src={urlMedia + item.producto.foto} />

              <Item.Content>
                <Item.Header as='a' onClick={() => this.routeDetalleCatalogo(item.id)}>
                  {item.titulo}
                </Item.Header>

                <Item.Meta>
                  <Label as='a' size="large" color='orange' tag>
                    {item.producto.categoria}
                  </Label>
                  <Label as='a' color='orange' tag>
                    {item.producto.subcategoria}
                  </Label>
                </Item.Meta>
                <Item.Description>
                  <p>
                    <Icon name='eye'/>
                    {item.visitas} visitas &nbsp;&nbsp;
                  </p>
                  <Rating disabled maxRating={5} defaultRating={item.valoracion} icon='star' size='mini' />
                </Item.Description>
                <Item.Extra>
                  <Label size='large'>{(item.precio_venta <= 0) ? "N/A" : "$"+item.precio_venta }</Label>
                </Item.Extra>
              </Item.Content>
      </Item>
    )
  }

  renderItemsCatalogo() {
    this.itemsRendered = this.state.catalogo.detalles_catalogo.data.map(item =>{
      return this.renderItemIndividual(item)
    });

    return(
      <Grid>
        <Grid.Column width={16}>
          <Grid>
            <Grid.Row centered>
              <Grid.Column width={4}>
                <center>
                  {this.renderButtonLeft()}
                </center>
              </Grid.Column>
              <Grid.Column width={4}>
                <center>
                  {this.renderButtonNext()}
                </center>
              </Grid.Column>
            </Grid.Row>

            <Grid.Row centered>
              <Grid.Column width={16}>
                <center>
                  <h4>Página {this.state.pagina_actual} de {this.state.catalogo.detalles_catalogo.cant_pagina}</h4>
                </center>
              </Grid.Column>
            </Grid.Row>

            <Grid.Row>
              <Grid.Column width={16}>
                <Segment>
                  <Item.Group divided>
                    {this.itemsRendered}            
                  </Item.Group>
                </Segment>
              </Grid.Column>
            </Grid.Row>
          </Grid>
          
        </Grid.Column>
      </Grid>
    )
  }

  renderButtonNext()
  {
    return (
      <Button disabled={!(this.state.catalogo.detalles_catalogo.cant_pagina > this.state.catalogo.detalles_catalogo.pagina_actual)} circular color='blue' icon='chevron right' size='massive' onClick={()  => {
        this.setState(
        {
          cargado: false 
        });
        this.obtenerCatalogo(this.emprendedor.id, this.state.pagina_actual +1);
        }
    }/>
    )
  }

  renderButtonLeft()
  {
    return (
      <Button disabled={( this.state.catalogo.detalles_catalogo.pagina_actual === 1  )} circular color='blue' icon='chevron left' size='massive' onClick={()  => {
        this.setState(
        { 
          cargado: false 
        });
        this.obtenerCatalogo(this.emprendedor.id, this.state.pagina_actual -1);
        }}/>
      
    )
  }

}

const mapStateToProps = state => ({
  catalogo: state.tienda.resultCatalogo
});

export default withRouter(connect(mapStateToProps, { getCatalogoTienda })(PerfilEmprendedor));