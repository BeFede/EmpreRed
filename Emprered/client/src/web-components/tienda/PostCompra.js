import React, { Component } from 'react';
import { Grid, Rating, Segment, Button, Message, Icon, Modal, Header } from 'semantic-ui-react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import MenuPrincipal from './../MenuPrincipal';
import { valorarDetalle } from '../../actions/tiendaActions'

class PostCompra extends Component {

    constructor(props) {
        super(props);
        this.handleRate = this.handleRate.bind(this);
        const url_string = window.location.href; 
        const url = new URL(url_string);
        this.id_detalle = url.searchParams.get("id_detalle");
        this.data = {id_detalle: this.id_detalle, valoracion: null};
        this.state = { messageInfo: null, messageOpen: false }
    }

    componentWillMount() {
         
    }

    renderMensaje = (estado, mensajeResponse) => {
        let mensaje = '';
        if(estado === 'OK') {
            mensaje = '¡Gracias por su valoración!';
        } else {
            mensaje = mensajeResponse;
        }
        this.setState({ messageInfo: mensaje, messageOpen: true });
    }

    handleMessageConfirm = () => {
        this.setState({messageOpen: false});
        this.routeTienda();
    }

    routeTienda() {
        this.props.history.push(
            {
              pathname: '/tienda',
              state: { }
            }
        )
    }

    handleRate = (e, { rating, maxRating }) => {
        this.data.valoracion = rating;
        this.valorarDetalle();
    };

    valorarDetalle() {
        console.log(this.data);
        this.props.valorarDetalle(this.data.id_detalle, this.data.valoracion)
        .then(data => {
            this.renderMensaje(data.estado, data.mensaje);
        }, err => {
            console.log(err);
        })
    }

    renderModalMensaje() {
        return (
            <Modal
                open={this.state.messageOpen}
                onClose={this.handleMessageConfirm}
                size='small'
            >
                <Header icon='browser' content='Aviso' />
                <Modal.Content>
                <h3>{this.state.messageInfo}</h3>
                </Modal.Content>
                <Modal.Actions>
            <Button color='green' onClick={this.handleMessageConfirm} inverted>
                    <Icon name='checkmark' /> Aceptar
                </Button>
                </Modal.Actions>
            </Modal>
        );
    }

    render() {
        return (
        <div className="DivPrincipal" style={{overflow:'auto', backgroundColor:'#f2f2f2'}}>
            <div id="MenuTop">
                <MenuPrincipal/>
            </div>
            <div  className = "row">
                <div style={{"paddingTop":"61px"}}>
                    <Grid centered verticalAlign='middle'>
                        <Grid.Row centered>
                            <Grid.Column width={10}>
                                <Segment color='orange' size='massive'>
                                    <Grid centered verticalAlign='middle'>

                                        <Grid.Row centered verticalAlign='middle'>
                                            <Grid.Column width={16}>
                                                <center>
                                                    <Icon size='massive' color='green' name='check circle outline'>
                                                    </Icon>
                                                </center>
                                            </Grid.Column>
                                        </Grid.Row>
                                        
                                        <Grid.Row centered verticalAlign='middle'>
                                            <Grid.Column width={16}>
                                                <Message size='huge' info>
                                                    <Message.Header>
                                                        <center>
                                                            Su compra se ha realizado con éxito
                                                        </center>
                                                    </Message.Header>
                                                    <Message.Content>
                                                        <center>
                                                            Para acceder a la información sobre la misma o valorar el producto en otro momento ingrese al panel de "Mis compras"
                                                        </center>
                                                    </Message.Content>
                                                </Message>
                                            </Grid.Column>
                                        </Grid.Row>

                                        <Grid.Row centered verticalAlign='middle'>
                                            <Grid.Column width={16}>
                                                <center>
                                                    <h1>Valorar producto</h1>
                                                    <Rating icon='star' size='massive' name='valoracion' defaultRating={0} maxRating={5} onRate={this.handleRate}>
                                                    </Rating>
                                                </center>
                                            </Grid.Column>
                                        </Grid.Row>

                                        <Grid.Row centered verticalAlign='middle'>
                                            <Grid.Column width={16}>
                                                <center>
                                                    <Button size='massive' color='blue' onClick={() => this.routeTienda()}>
                                                        Volver a la tienda
                                                    </Button>
                                                </center>
                                            </Grid.Column>
                                        </Grid.Row>
                                    </Grid>
                                </Segment>
                            </Grid.Column>
                        </Grid.Row>
                    </Grid>
                    {this.renderModalMensaje()}
                </div>
            </div>
        </div>
        )
    }
}

const mapStateToProps = state => ({
    
});

export default withRouter(connect(mapStateToProps, { valorarDetalle })(PostCompra));
