import React, { Component } from 'react';
import { Grid, Segment, Button, Message, Icon } from 'semantic-ui-react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import MenuPrincipal from './../MenuPrincipal';

class PostCompraError extends Component {

    constructor(props) {
        super(props);
        const url_string = window.location.href; 
        const url = new URL(url_string);
        this.id_detalle = url.searchParams.get("id_detalle");
        console.log(this.id_detalle);
    }

    componentWillMount() {
         
    }

    routePublicacion() {

    }

    render() {
        return (
        <div className="DivPrincipal" 
            style={{overflow:'auto', backgroundColor:'#f2f2f2', display: 'flex',
            'flex-direction': 'column',
            'justify-content': 'center',
            'align-items': 'center',
            'text-align': 'center'}}
        >
            <div id="MenuTop">
                <MenuPrincipal/>
            </div>
            <div  className = "row">
                <div style={{"paddingTop":"61px"}}>
                    <Grid centered verticalAlign='middle'>
                        <Grid.Row centered>
                            <Grid.Column width={10}>
                                <Segment color='orange' size='massive'>
                                    <Grid centered verticalAlign='middle'>
                                        <Grid.Row centered verticalAlign='middle'>
                                            <Grid.Column width={16}>
                                                <center>
                                                    <Icon size='massive' color='orange' name='exclamation circle'>
                                                    </Icon>
                                                </center>
                                            </Grid.Column>
                                        </Grid.Row>
                                        <Grid.Row centered verticalAlign='middle'>
                                            <Grid.Column width={16}>
                                                <Message size='massive' warning>
                                                    <Message.Header>
                                                        <center>
                                                            Se produjo un error en su compra
                                                        </center>
                                                    </Message.Header>
                                                </Message>
                                            </Grid.Column>
                                        </Grid.Row>
                                        <Grid.Row centered verticalAlign='middle'>
                                            <Grid.Column width={16}>
                                                <center>
                                                    <Button size='massive' color='blue'>
                                                        Volver a publicación
                                                    </Button>
                                                </center>
                                            </Grid.Column>
                                        </Grid.Row>
                                    </Grid>
                                </Segment>
                            </Grid.Column>
                        </Grid.Row>
                    </Grid>
                </div>
            </div>
        </div>
        )
    }
}

const mapStateToProps = state => ({
    
});

export default withRouter(connect(mapStateToProps, { })(PostCompraError));
