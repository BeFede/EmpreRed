import React, { Component } from 'react'
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { Button, Icon, Image, Confirm, Grid, Label, Tab, Comment, Header, Divider, Statistic, Item, Rating, Form, TextArea, Message, Modal, Segment, Dimmer, Loader } from 'semantic-ui-react'
import MenuPrincipal from '../MenuPrincipal';
import ModalManager from '../ModalManager';
import { openModal } from '../../actions/modalActions';
import { removeItem, insertItem } from '../../actions/tableFillActions';
import {getUser} from '../../config/configs';
import { getDetalleCatalogoTienda, postComentarioTienda } from '../../actions/tiendaActions';
import Logo from '../../assets/EmpreRedRectangular.png';
import Logo2 from '../../assets/EmpreRedLogoE.png';
import Avatar from '../../assets/default-avatar.png';
import { urlMedia } from '../../config/configs';
import ModalVenta from './ModalVenta';

class Publicacion extends Component {

    constructor(props){
        super(props);
        this.state = { etiquetaSeleccionada: 'Etiqueta', cargoDetalle: false, catalogo: null, 
            detalle: null, messageOpen: false, messageInfo: null, arrayFotos: [] };
        this.userLogeado = (getUser()) ? getUser() : null;
        this.detalleSeleccionado = null;
        this.handleChange = this.handleChange.bind(this);
        this.dataToSend = null;
        this.commentsRendered = null;
        this.nuevoComentario = null;
    }

    componentWillMount() {

        if(this.props.location.state.idDetalle) {
            this.setState({ catalogo: this.props.location.state.catalogo });
            this.obtenerDetalle(this.props.location.state.idDetalle);
            //this.obtenerDetalle(1);
        }

    }

    handleMessageConfirm = () => {
        this.setState({messageOpen: false});
    }

    renderMensaje = (info) => {
        let mensaje = '';
        if(info.estado === 'OK') {
            mensaje = 'Cambios guardados';
        } else {
            mensaje = info.mensaje
        }
        this.setState({ messageInfo: mensaje, messageOpen: true });
    }

    obtenerDetalle(id) {
        this.props.getDetalleCatalogoTienda(id)
        .then(data => {
            this.detalleSeleccionado = data.datos;
            let fotosActual = this.state.arrayFotos;
            if(data.datos.imagenes.length > 0) {
                for( let foto of data.datos.imagenes) {
                    const objFoto = {url: foto.foto}
                    fotosActual.push(objFoto);
                }               
            }
            this.setState({ detalle: data.datos, cargoDetalle: true, arrayFotos: fotosActual });
        }, err => {
            console.log(err);
        })
    }

    handleChange = (e, {name, value, checked}) => {
        if(name === 'nuevoComentario') {
            this.nuevoComentario = value;
        }      
    };

    routeEmprendedor(emprendedor, marca) {
        this.props.history.push(
            {
              pathname: '/perfil',
              state: {e: emprendedor, m: marca}
            }
        )
    }

    postComment() {
        let comentarioCompleto = {id_detalle: this.detalleSeleccionado.id, titulo: `comentario en detalle ${this.detalleSeleccionado.id}`, descripcion: null};
        if(this.nuevoComentario != null) {
            comentarioCompleto.descripcion = this.nuevoComentario;
            this.props.postComentarioTienda(comentarioCompleto).
            then(data => {
                this.nuevoComentario = null;
                document.getElementById("nuevoComentario").value = "";
                document.getElementById("headerComments").scrollIntoView();
            }, err => {
                console.log(err);
            });
            
        }
    }

    iniciarVenta() {
        if (this.userLogeado) {
            this.props.openModal({
                header: "Su compra",
                content: <ModalVenta data={this.detalleSeleccionado}/>
            })
        } else {
            this.props.history.push(
                {
                  pathname: '/ingreso',
                  state: { redireccionaDePublicacion: this.detalleSeleccionado.id }
                }
            )
        }
    }

    render() {
        return (
            <div className="DivPrincipal" style={{overflow:'auto', backgroundColor:'#f2f2f2'}}>
                <div id="MenuTop">
                    <MenuPrincipal/>
                </div>
                <div className = "row">
                        <div style={{"paddingTop":"61px"}}>
                            
                            {
                            (this.state.cargoDetalle)
                            ?
                            <Grid>
                                <Grid.Row centered>
                                    <Grid.Column width={10}>
                                        <Segment size ='massive'>
                                            <Grid>
                                                <Grid.Row centered>
                                                    <Grid.Column width={16}>
                                                        <h2>
                                                            Publicación
                                                        </h2>
                                                    </Grid.Column>
                                                </Grid.Row>
                                                
                                                <Divider />

                                                <Grid.Row centered>
                                                    
                                                    <Grid.Column width={8}>
                                                        <Segment size='massive' color='orange'>
                                                            {this.renderTitulo()}
                                                            {this.renderEtiqueta()}
                                                            {this.renderValoracionVisitasPrecio()}
                                                            {this.renderInfoEmprendedor()}
                                                            {this.renderBotones()}
                                                            {this.renderDescripcion()}     
                                                        </Segment>        
                                                    </Grid.Column>

                                                    <Divider vertical ></Divider>

                                                    <Grid.Column width={8}>
                                                        <Segment size='massive' color='orange'>
                                                            {this.renderImagenes()}
                                                        </Segment> 
                                                    </Grid.Column>
                                                </Grid.Row>

                                                <Divider />

                                                <Grid.Row centered>
                                                        {this.renderComentarios()}
                                                </Grid.Row>
                                            </Grid>
                                        </Segment>
                                    </Grid.Column>
                                </Grid.Row>
                            </Grid>
                            :
                            <center>
                                <Dimmer active inverted>
                                    <Loader active inline='centered'/>
                                </Dimmer>
                            </center>
                            }
                            {this.renderModalMensaje()}
                            <ModalManager/>
                        </div>
                </div>
            </div>
        
        )
    }

    renderTitulo() {
        return(
            <Grid columns={1}>
                <Grid.Column width={16}>
                    <h3>{this.state.detalle.titulo}</h3>
                </Grid.Column>
            </Grid>
        );
    }

    renderEtiqueta() {
        return(
            <Grid columns={1}>               
                <Grid.Column width={16}>
                    <Label as='a' size="large" color='orange' tag>
                        {this.state.detalle.producto.categoria}
                    </Label>
                    <Label as='a' color='orange' tag>
                        {this.state.detalle.producto.subcategoria}
                    </Label>
                </Grid.Column>
            </Grid>
        );
    }

    renderValoracionVisitasPrecio() {
        return(
            <Grid columns={2}>
                <Grid.Column width={8}>
                    <Rating icon='star' size='huge' disabled defaultRating={((this.state.detalle.valoracion) ? this.state.detalle.valoracion : 0)} maxRating={5}>
                    </Rating>
                    <br/>
                    <Statistic size='small'>
                        <Statistic.Value>
                            {((this.state.detalle.visitas) ? this.state.detalle.visitas : 0)}
                        </Statistic.Value>
                        <Statistic.Label>Visitas</Statistic.Label>
                    </Statistic>
                </Grid.Column>
                <Grid.Column width={8}>
                    <h1>${(this.state.detalle.precio_venta ? this.state.detalle.precio_venta : 0)}</h1>
                </Grid.Column>
            </Grid>
        );
    }

    renderInfoEmprendedor(){
        const srcFoto = (this.state.detalle.emprendedor.foto !== '') ? urlMedia + this.state.detalle.emprendedor.foto : Logo2;
        const srcFotoMarca = (this.state.detalle.marca.logo !== '') ? urlMedia + this.state.detalle.marca.logo : Logo2;
        const emprendedor = this.state.detalle.emprendedor;
        const nombre = this.state.detalle.emprendedor.nombre;
        const apellido = this.state.detalle.emprendedor.apellido;
        const marca = this.state.detalle.marca;
        const nombreMarca = this.state.detalle.marca.nombre;
        return(
            <Grid columns={2}>
                    <Grid.Column width={16}>
                        <h4>Información del Emprendedor</h4>
                        <Segment>
                            <Item.Group>
                            <Item>
                                <Item.Image size='mini' src={srcFoto} />

                                <Item.Content verticalAlign='middle'>
                                    <Item.Header as='a' onClick={() => this.routeEmprendedor(emprendedor, marca)}>
                                    <h3>{nombre + ' ' + apellido}</h3>
                                    </Item.Header>
                                    {
                                        (nombreMarca !== '') ?
                                        <Item.Extra>
                                            <Label as='a' size='medium' color='orange' image>
                                                <img src={srcFotoMarca} />
                                                {nombreMarca}
                                            </Label>
                                        </Item.Extra>
                                        :
                                        null
                                    }  
                                </Item.Content>
                            </Item>
                            </Item.Group>
                        </Segment>
                    </Grid.Column>
            </Grid>
        );
    }

    renderBotones(){
        return(
            <Grid columns={1}>
                <Grid.Row centered>
                    <Grid.Column width={5}>
                        <center>
                        <Button animated='fade' size='huge' color='blue' onClick={() => this.iniciarVenta()}>
                            <Button.Content hidden>Comprar</Button.Content>
                            <Button.Content visible>
                                <Icon name='shopping bag' />
                            </Button.Content>
                        </Button>
                        </center>
                    </Grid.Column>
                </Grid.Row>
                
            </Grid>
        );
    }

    renderDescripcion() {
        return(
            <Grid columns={1}>
                <Grid.Column width={16}>
                    <h4>Descripción de la publicación</h4>
                    <Segment>
                        {(this.state.detalle.descripcion)}
                    </Segment>
                </Grid.Column>
            </Grid>
        );
    }

    renderComentarioUnico(com) {
        let avatar = "";
        if(com.foto === "") {
            avatar = Avatar;
        } else {
            avatar = urlMedia + com.foto;
        }
        const date = new Date(com.fecha_hora);
        const dateString= date.toLocaleString();
        return(
            <Comment key={com.fecha_hora}>
                <Comment.Avatar src={avatar} />
                <Comment.Content>
                    <Comment.Author as='a'>{com.creador}</Comment.Author>
                    <Comment.Metadata>
                    <div>{dateString}</div>
                    </Comment.Metadata>
                    <Comment.Text>{com.descripcion}</Comment.Text>
                </Comment.Content>
            </Comment>
        )
    }

    renderComentarios() {

        if(this.state.detalle) {
            this.commentsRendered = this.props.detalle.comentarios.map(com => {
                return this.renderComentarioUnico(com)
            })
        }
        return( 
            <Grid.Column width={16}>
                <Segment size='massive' color='orange'>
                    <Comment.Group size='large'>
                        <Header id= 'headerComments' as='h3' dividing>
                        Comentarios
                        </Header>
                        
                        {this.commentsRendered}

                        <Form reply>
                        <Form.TextArea id='nuevoComentario' name='nuevoComentario' onChange = {this.handleChange}/>
                        <Button circular disabled={this.userLogeado === null} content='Publicar comentario' labelPosition='left' icon='edit' primary onClick={() => this.postComment()}/>
                        </Form>
                    </Comment.Group>
                </Segment>
            </Grid.Column>
        );
    }

    renderImageTab(srcFoto, count) {
        const tab = { 
            menuItem: `${(count !== undefined) ? `Imagen ${count}` : 'Imagen no disponible'}`, render: () => 
            <Tab.Pane>
                {
                    (count !== undefined) 
                    ?
                    <div>
                        <Segment attached size='massive'>
                            <Image src={srcFoto} fluid />
                        </Segment>
                    </div>
                    :
                    <Segment size='massive'>
                        <Message color='orange' compact>No hay imágenes para mostrar</Message>  
                    </Segment>
                }
                
            </Tab.Pane> 
            }
            
        return tab;
    }

    renderImagenes() {
        let panes = [];
        let count = 0;
        if(this.state.arrayFotos.length > 0) {
            for(let foto of this.state.arrayFotos) {
                count ++;
                let srcFoto = urlMedia + foto.url;
                
                const tabFoto = this.renderImageTab(srcFoto, count);
                panes.push(tabFoto);
            }
        } else {
            // Si es una nueva carga de detalle o si no tiene foto el detalle consultado pongo por defecto el logo
            panes.push(this.renderImageTab(Logo, 1))
        }

        return(
            <Grid columns={1}>
                <Grid.Column width={16}>
                    <Tab panes={panes} />
                </Grid.Column>
            </Grid>
        );
    }

    renderModalMensaje() {
        return (
            <Modal
                open={this.state.messageOpen}
                onClose={this.handleMessageConfirm}
                size='small'
            >
                <Header icon='browser' content='Aviso' />
                <Modal.Content>
                <h3>{this.state.messageInfo}</h3>
                </Modal.Content>
                <Modal.Actions>
            <Button color='green' onClick={this.handleMessageConfirm} inverted>
                    <Icon name='checkmark' /> Aceptar
                </Button>
                </Modal.Actions>
            </Modal>
        );
    }
}

const mapStateToProps = state => ({
    detalle: state.catalogo.resultDetalleCatalogo
});

export default withRouter(connect(mapStateToProps, { insertItem, removeItem, getDetalleCatalogoTienda, postComentarioTienda, openModal })(Publicacion));