import React, { Component } from 'react';
import { Segment, Image, Grid, Loader, Dimmer, Icon, Divider, Label, Pagination, Button, Item, Message, Rating, Dropdown } from 'semantic-ui-react'
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import MenuPrincipal from './../MenuPrincipal';
import { urlMedia } from '../../config/configs';
import imageNotFound from '../../assets/imageNotFound.jpeg';
import logoEmpreRed from '../../assets/EmpreRedLogoE.png';
import SearchTiendaCustom from './SearchTienda';
import { getPublicacionesPorFrase, getPublicacionesPorFiltros, getPublicacionesPorSubcategoria } from '../../actions/tiendaActions';
import '../../styles/Tienda.css'

const optionsResult = [
  { key: 'relevante', text: 'más relevante', value: 'relevante' },
  { key: 'lowcost', text: 'menor precio', value: 'lowcost' },
  { key: 'highcost', text: 'mayor precio', value: 'highcost' },
  { key: 'valoration', text: 'más valorada', value: 'valoration' },
  { key: 'visit', text: 'más visitada', value: 'visit' }
]


class ResultPage extends Component {
  constructor(props) {
    super(props)

    this.frase = (this.props.location.state.frase) ? this.props.location.state.frase : null;

    // Recibe un array con una o varias subcategorias
    this.busquedaPorCategoria = (this.props.location.state.busquedaPorCategoria) ? this.props.location.state.busquedaPorCategoria : null;
    this.catOSubcatClickeado = (this.props.location.state.categoriaOSubcategoria) ? this.props.location.state.categoriaOSubcategoria : null;

    this.state = {
      cargado: false, infoBusqueda: null, results: null, pagina_actual: 1, columns: true, changeOrder: false, changePrecio: false, change_filter: false,
      filtradoPrecio: false, filtradoCategoria: false
    };

    this.precios = { precioHasta: 0, precioDe: 0, precioA: 0, precioDesde: 0 };

    this.categorias = [];

    this.categoriasRendered = null;

    this.filtros = { categorias: [], precio_desde: '', precio_hasta: '' }

    this.cantidad_resultados = null;

  }

  componentWillMount() {
    // Diferencio si buscó por categoria o en el buscador
    if (this.busquedaPorCategoria) {
      this.obtenerResultadosPorSubategoria(this.busquedaPorCategoria, this.state.pagina_actual)
    } else if (this.frase) {
      this.obtenerResultadosBusqueda(this.frase, this.state.pagina_actual);
    }
  }

  obtenerResultadosBusqueda(frase, paginaActual) {
    this.props.getPublicacionesPorFrase(frase, paginaActual)
      .then(data => {
        let resultados = [];
        // Si la búsqueda no arroja resultados, no va a haber un data.datos.data (los productos)
        if (data.datos.data) {
          resultados = data.datos.data
        }
        this.cantidad_resultados = data.datos.cantidad_resultados_totales;
        this.setState({ cargado: true, infoBusqueda: data.datos, results: resultados, pagina_actual: paginaActual });
      }, err => {
        console.log(err);
      })
  }

  obtenerResultadosPorSubategoria(subcategorias, paginaActual) {
    this.props.getPublicacionesPorSubcategoria(subcategorias, paginaActual)
      .then(data => {
        let resultados = [];
        // Si la búsqueda no arroja resultados, no va a haber un data.datos.data (los productos)
        if (data.datos.data) {
          resultados = data.datos.data
          this.cantidad_resultados = data.datos.cantidad_resultados_totales;
        }
        this.setState({ cargado: true, infoBusqueda: data.datos, results: resultados, pagina_actual: paginaActual });
      }, err => {
        console.log(err);
      })
  }

  obtenerResultadosBusquedaFiltros(frase, paginaActual, filtros) {

    this.props.getPublicacionesPorFiltros(frase, paginaActual, filtros)
      .then(data => {
        let resultados = [];
        // Si la búsqueda no arroja resultados, no va a haber un data.datos.data (los productos)
        if (data.datos.data) {
          resultados = data.datos.data
        }
        this.cantidad_resultados = data.datos.cantidad_resultados_totales;
        this.setState({
          cargado: true, infoBusqueda: data.datos, results: resultados,
          pagina_actual: paginaActual, change_filter: false
        });
      }, err => {
        console.log(err);
      })
  }

  routeDetalleCatalogo(idCatalogoMinorista, idDetalle) {
    this.props.history.push(
      {
        pathname: '/publicacion',
        state: { catalogo: idCatalogoMinorista, idDetalle: idDetalle }
      }
    );
  }

  render() {

    if (this.frase || this.busquedaPorCategoria) {
      this.chargePrice();
      this.renderCategorias();
      if (this.state.change_filter) {
        this.obtenerResultadosBusquedaFiltros(this.frase, this.state.pagina_actual, this.filtros);
      }
      return (
        <div className="DivPrincipal" style={{ backgroundColor: '#f2f2f2' }}>
          <div id="MenuTop">
            <MenuPrincipal />
          </div>
          <div className="row">
            <div className="DivPrincipalResult">
              <Grid>
                <Grid.Column width={4} >
                  {this.renderBarFilter()}
                </Grid.Column>
                <Grid.Column width={10}>
                  <Grid.Row centered>
                    <Grid.Column width={10}>
                      <Segment>
                        <br />
                        {this.renderLogoYBuscador()}
                        <br />
                        <br />
                        <h2>Resultados de la búsqueda: "{(this.frase) ? this.frase : this.catOSubcatClickeado}"</h2>
                        <h2 style={{ color: 'grey' }}>
                          {this.state.filtradoPrecio ?
                            ((this.filtros.precio_desde !== '') ? ' Desde: $' + this.filtros.precio_desde : '')
                            + ((this.filtros.precio_hasta !== '') ? ' Hasta: $' + this.filtros.precio_hasta : '')
                            + (this.state.filtradoCategoria ? ' - Categoría: ' + this.filtros.categorias[0].name : '')
                            :
                            (this.state.filtradoCategoria ? 'Categoría: ' + this.filtros.categorias[0].name : '')
                          }
                        </h2>
                        {
                          (this.state.cargado) ?
                            <Grid>
                              <Grid.Row>
                                <Grid.Column width={16}>
                                  {
                                    (!(this.cantidad_resultados > 0)) ?
                                      <Grid>
                                        <Grid.Row>
                                          <Grid.Column width={16}>
                                            <Segment color='orange'>
                                              <Message warning>
                                                <Message.Header>No se encontraron resultados para su búsqueda</Message.Header>
                                              </Message>
                                            </Segment>
                                          </Grid.Column>
                                        </Grid.Row>
                                      </Grid>
                                      :
                                      <Grid>
                                        <Grid.Row>
                                          <Grid.Column width={16}>
                                            <h3>{this.cantidad_resultados} publicación/es encontrada/s</h3>
                                          </Grid.Column>
                                        </Grid.Row>
                                        <Grid.Row>
                                          <Grid.Column width={16}>

                                            {this.renderItemsResultado()}

                                          </Grid.Column>
                                        </Grid.Row>
                                      </Grid>
                                  }
                                </Grid.Column>
                              </Grid.Row>

                            </Grid>
                            :
                            <Dimmer active inverted>
                              <Loader active inline='centered' />
                            </Dimmer>
                        }
                      </Segment>
                    </Grid.Column>
                  </Grid.Row>
                </Grid.Column>
              </Grid>
            </div>
          </div>
        </div>
      )
    }
    else {
      return (null)
    }
  }

  renderLogoYBuscador() {
    return (
      <Grid>
        <Grid.Row centered verticalAlign='middle'>
          <Grid.Column width={1}>
            <Image src={logoEmpreRed} size="tiny" />
          </Grid.Column>

          <Grid.Column width={9}>
            <SearchTiendaCustom />
          </Grid.Column>
        </Grid.Row>
      </Grid>
    )
  }

  cargarFiltros(str) {
    if (!this.state.filtradoPrecio) {
      switch (str) {
        case 'precio_desde':
          this.filtros.precio_desde = this.precios.precioDesde;
          break;
        case 'precio_hasta':
          this.filtros.precio_hasta = this.precios.precioHasta;
          break;
        case 'ambas':
          this.filtros.precio_desde = this.precios.precioDe;
          this.filtros.precio_hasta = this.precios.precioA;
          break;
      }
      this.setState({
        change_filter: true,
        filtradoPrecio: true
      });
    }
  }

  renderItemIndividual(item) {

    const idCatalogoMinorista = item.emprendedor.catalogos[0];

    return (
      <Item>
        <Item.Image size='tiny' src={(item.producto.foto !== '') ? urlMedia + item.producto.foto : imageNotFound} />
        <Item.Content as='a'>
          <Segment>

            <Item.Header as='a' onClick={() => this.routeDetalleCatalogo(idCatalogoMinorista, item.id)}>
              <h3>
                {item.titulo}
              </h3>
            </Item.Header>

            <Item.Meta>
              <Label size='large'>{(item.precio_venta <= 0) ? "N/A" : "$" + item.precio_venta}</Label>
              <Label as='a' size="large" color='orange' attached='bottom right'>
                {item.producto.categoria}
              </Label>

            </Item.Meta>


            <Item.Description>
              <h4>
                de <a onClick={() => { this.toEmprendedor(item) }} >{item.marca.nombre ? item.marca.nombre : item.emprendedor.nombre}</a>
              </h4>
              <p>
                <Icon name='eye' />
                {item.visitas} visitas &nbsp;&nbsp;
                </p>
              <Rating disabled maxRating={5} defaultRating={item.valoracion} icon='star' size='mini' />
            </Item.Description>
          </Segment>

        </Item.Content>
      </Item>
    )
  }

  renderBarFilter() {
    return (
      <div >
        <Grid>
          <Grid.Column width={16}>

            <Grid.Row centered>
              <center>
                <h1>{this.frase ? this.frase : this.catOSubcatClickeado}</h1>
                <label>
                  {this.cantidad_resultados ? this.cantidad_resultados : 0} Resultados obtenidos
                </label>
              </center>
              <Divider />
            </Grid.Row>

            <Grid.Row>
              <Grid>
                <Grid.Column>
                  {this.state.filtradoCategoria ?
                    <Grid>
                      <Grid.Row centered>
                        <Label color='red' tag>
                          Categoría
                              <Icon name='close' onClick={() => this.limpiarCategoria()} />
                        </Label>
                      </Grid.Row>
                    </Grid>
                    :
                    null
                  }
                  {this.state.filtradoPrecio ?
                    <Grid>
                      <Grid.Row centered>
                        <Label tag color='red' >
                          Precio
                            <Icon name='close' onClick={() => this.limpiarPrecio()} />
                        </Label>
                      </Grid.Row>
                    </Grid>
                    :
                    null
                  }
                </Grid.Column>
              </Grid>
            </Grid.Row>
            {(this.cantidad_resultados > 0) ?
              <Grid>
                <Grid.Row centered>
                  <Grid centered>
                    <Grid.Row centered>
                      <label>
                        <h3>
                          Ordenar por <Dropdown defaultValue='visit' options={optionsResult} name='optionsResult' onChange={this.handleChange} />
                        </h3>
                      </label>
                    </Grid.Row>


                    <center>


                      {(!this.state.filtradoPrecio) ?

                        <Grid>

                          <Grid.Row centered>


                            <Grid.Column textAlign='center'>
                              <Grid.Row centered>
                                <h3>
                                  Precios
                              </h3>
                              </Grid.Row>
                              <Grid.Row centered>
                                <center>
                                  <a as={'a'} size='big' circular color="blue" onClick={() => this.cargarFiltros('precio_hasta')}>Hasta ${this.precios.precioHasta}</a>
                                </center>
                              </Grid.Row>

                              <Grid.Row>
                                <center>
                                  <a as={'a'} size='big' circular color="blue" onClick={() => this.cargarFiltros('ambas')}>${this.precios.precioDe} a ${this.precios.precioA}</a>
                                </center>
                              </Grid.Row>

                              <Grid.Row>
                                <center>
                                  <a as={'a'} size='big' circular color="blue" onClick={() => this.cargarFiltros('precio_desde')}>Más de ${this.precios.precioDesde}</a>
                                </center>
                              </Grid.Row>
                            </Grid.Column>

                          </Grid.Row>
                        </Grid>

                        :
                        null
                      }

                      {!this.state.filtradoCategoria ?
                        <Grid>

                          <Grid.Row centered>
                            <Grid>
                              <Grid.Column>
                                <Grid.Row centered>
                                  <h3>
                                    Categoría/s
                                  </h3>
                                </Grid.Row>
                                <Grid.Row>

                                {this.categoriasRendered}
                                </Grid.Row>
                              </Grid.Column>
                            </Grid>
                          </Grid.Row>
                        </Grid>
                        :
                        null
                      }
                    </center>

                  </Grid>
                </Grid.Row>
              </Grid>
              : null}


          </Grid.Column>
        </Grid>
      </div>
    )
  }

  limpiarCategoria() {
    this.filtros.categorias = [];
    this.setState({
      filtradoCategoria: false,
      change_filter: true
    })
  }


  limpiarPrecio() {
    this.filtros.precio_desde = '';
    this.filtros.precio_hasta = '';
    this.setState({
      filtradoPrecio: false,
      change_filter: true
    })
  }

  handleChange = (e, { name, value, checked }) => {
    if (name === "optionsResult") {
      this.orderBy(value);
    }
    else {
      this.data = {
        ...this.data,
        [name]: (value) ? value : checked
      }
    }
  };

  renderCategorias() {
    if (this.state.cargado && !this.categoriasRendered) {
      this.categoriasRendered = this.categorias.map(category => {
        return this.renderItemCategoria(category);
      })
    }
  }


  renderItemCategoria(category) {
    return (

      <Label as={'a'} color='blue' key={category.id} onClick={() => this.chargeCategory(category)}>
        {category.name}
      </Label>

    )
  }

  chargeCategory(category) {
    const arrayCat = [category];
    this.filtros.categorias = arrayCat;
    this.setState({
      change_filter: true,
      filtradoCategoria: true
    })
  }

  chargePrice() {
    var prom = 0;
    if (this.state.cargado) {
      for (const r in this.state.results) {
        prom = prom + parseFloat(this.state.results[r].precio_venta);
        if (this.categorias.map(c => c.id).indexOf(this.state.results[r].producto.id_categoria) == -1) {
          this.categorias.push({ id: this.state.results[r].producto.id_categoria, name: this.state.results[r].producto.categoria });
        }
      }
      prom = parseFloat(prom / this.state.results.length);
    }
    if (!this.state.filtradoPrecio) {
      this.precios = {
        ...this,
        precioHasta: (prom - prom / 2),
        precioDe: (prom - prom / 2),
        precioA: (prom + prom / 2),
        precioDesde: (prom + prom / 2)
      }
    }
  }

  orderBy(v) {

    switch (v) {

      case 'lowcost':
        this.state.results.sort(function (a, b) {
          return a.precio_venta.localeCompare(b.precio_venta);
        });
        break;

      case 'highcost':
        this.state.results.sort(function (a, b) {
          return b.precio_venta.localeCompare(a.precio_venta);
        });
        break;

      case 'valoration':
        this.state.results.sort(function (a, b) {
          if (a.valoracion > b.valoracion) return -1;
          if (b.valoracion > a.valoracion) return 1;
          return 0;
        });
        break;
      case 'relevante':
        this.state.results.sort(function (a, b) {
          var aAux = (a.valoracion * 0.8) + (a.visitas * 0.2);
          var bAux = (b.valoracion * 0.8) + (b.visitas * 0.2);

          if (aAux > bAux) return -1;
          if (bAux > aAux) return 1;
          return 0;
        });
        break;
      case 'visit':
        this.state.results.sort(function (a, b) {
          if (a.visitas > b.visitas) return -1;
          if (b.visitas > a.visitas) return 1;
          return 0;
        });
        break;

    }
    this.setState(
      {
        orderBy: !this.state.orderBy
      }
    )
  }

  renderItemsResultado() {

    if (this.state.results.length > 0) {
      this.itemsRendered = this.state.results.map(item => {
        return this.renderItemIndividual(item)
      });
    }

    return (

      <Grid>
        <Grid.Column width={16}>
          <Grid>


            <Grid.Row centered>
              <Grid.Column width={16}>
                <center>
                  <h4>Página {this.state.pagina_actual} de {this.state.infoBusqueda.cant_pagina}</h4>
                </center>
              </Grid.Column>
            </Grid.Row>

            <Grid.Row>
              <Grid.Column width={16}>

                <Item.Group divided>
                  {this.itemsRendered}
                </Item.Group>


              </Grid.Column>
            </Grid.Row>
            <Grid.Row centered>
              <Pagination
                activePage={this.state.pagina_actual}
                onPageChange={(e, data) => this.changePage(data.activePage)}
                totalPages={this.state.infoBusqueda.cant_pagina}
              />
            </Grid.Row>
          </Grid>

        </Grid.Column>
      </Grid>
    )
  }

  renderButtonNext() {
    return (
      <Button disabled={!(this.state.infoBusqueda.cant_pagina > this.state.infoBusqueda.pagina_actual)} circular color='blue' icon='chevron right' size='massive' onClick={() => {
        this.setState(
          {
            cargado: false
          });
        if (this.state.filtradoCategoria || this.state.filtradoPrecio) {
          this.obtenerResultadosBusquedaFiltros(this.frase, this.state.pagina_actual + 1, this.filtros);
        }
        else {
          this.obtenerResultadosBusqueda(this.frase, this.state.pagina_actual + 1);
        }
      }
      } />
    )
  }

  renderButtonLeft() {
    return (
      <Button disabled={(this.state.infoBusqueda.pagina_actual === 1)} circular color='blue' icon='chevron left' size='massive' onClick={() => {
        this.setState(
          {
            cargado: false
          });
        if (this.state.filtradoCategoria || this.state.filtradoPrecio) {
          this.obtenerResultadosBusquedaFiltros(this.frase, this.state.pagina_actual - 1, this.filtros);
        }
        else {
          this.obtenerResultadosBusqueda(this.frase, this.state.pagina_actual - 1);
        }
      }} />

    )
  }

}

const mapStateToProps = state => ({
  catalogo: state.tienda.resultDetallesCatalogo
});

export default withRouter(connect(mapStateToProps, { getPublicacionesPorFrase, getPublicacionesPorFiltros, getPublicacionesPorSubcategoria })(ResultPage));