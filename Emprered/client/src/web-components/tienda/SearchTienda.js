import _ from 'lodash';
import React, { Component } from 'react';
import { Search, Grid } from 'semantic-ui-react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types'
import { getTitulosPublicaciones } from '../../actions/tiendaActions';
import { Item, Icon } from 'semantic-ui-react'
import '../../styles/searchStandard.css';

class SearchTiendaBasic extends Component {

  constructor(props) {
    super(props);
    this.source = [];
  }

  componentWillMount() {
    this.resetComponent();
    this.source = [];
    // Llamar al metodo que llene el source
    this.obtenerTitulosPublicaciones();
  }

  obtenerTitulosPublicaciones() {

    let vectorPublicacionesConFormatoSearch = [];
    this.props.getTitulosPublicaciones()
    .then((data) => {
        for(let pub of data.datos) {
          let pubConFormatoSearch = {
            // Siempre agregar un id si no lo tiene así la action puede darse cuenta cuando NO agregar una repetida
            id: pub.id,
            title: pub.titulo,
          }
          vectorPublicacionesConFormatoSearch.push(pubConFormatoSearch);
        }
        this.source = vectorPublicacionesConFormatoSearch;
    }, err => {
      this.source = [];
      console.log(err);
    })
  }

  routePublicacion(idDetalle) {
    return(
      this.props.history.push(
        {
          pathname: '/publicacion',
          state: {idDetalle: idDetalle}
        }
      )
    )
  }

  routeResultPage(frase) {
      this.props.history.push(
        {
          pathname: '/resultado_busqueda',
          state: {frase: frase}
        }
      )
      window.location = '/resultado_busqueda';
  }

  handleKeyPress = (event) => {
    if(event.key == 'Enter'){
      if(this.state.value != '') {
        this.routeResultPage(this.state.value);
        
      }
    }
  }

  resetComponent = () => this.setState({ isLoading: false, results: [], value: '' })

  handleResultSelect = (e, { result }) => {
    this.setState({ value: result.title });
    // Llamar al metodo que redirige a la pagina de la publicacion
    this.routePublicacion(result.id);
  }

  handleSearchChange = (e, { value }) => {
    this.setState({ isLoading: true, value })

    setTimeout(() => {
      if (this.state.value.length < 1) return this.resetComponent()

      const re = new RegExp(_.escapeRegExp(this.state.value), 'i')
      const isMatch = result => re.test(result.title)

      this.setState({
        isLoading: false,
        results: _.filter(this.source, isMatch),
      })
    }, 300)
  }

  render() {
    const { isLoading, value, results } = this.state

    return (
        <Grid.Column width={3}>
          <Search 
            placeholder='Presione Enter o haga click en la lupa para obtener varios resultados'
            icon={ <Icon name='search' link onClick={() => this.routeResultPage(this.state.value)}/> }
            noResultsMessage='No se encontraron publicaciones'
            onKeyPress={this.handleKeyPress}
            fluid 
            input={{ fluid: true }}
            className="search"
            loading={isLoading}
            onResultSelect={this.handleResultSelect}
            onSearchChange={_.debounce(this.handleSearchChange, 500, { leading: true })}
            results={results}
            value={value}
            {...this.props}
          />
        </Grid.Column>
    )
  }
}

const resultRenderer = ({ title }) => 
    <Item>
        <Item.Content verticalAlign='middle'>
        <Item.Header>
            <Icon name='search' />
            {title}
        </Item.Header>
        </Item.Content>
    </Item>

resultRenderer.propTypes = {
  title: PropTypes.string
}

const SearchTiendaCustom = () => <SearchTiendaBasic resultRenderer={resultRenderer} />

const mapStateToProps = state => ({
  publicaciones: state.tienda.resultDetallesCatalogo
});

export default withRouter(connect(mapStateToProps, { getTitulosPublicaciones })(SearchTiendaBasic, SearchTiendaCustom));
