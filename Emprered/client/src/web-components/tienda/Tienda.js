import React, { Component } from 'react';
import { Card, Segment, Form, Image, Grid, Header, Pagination, Dimmer, Icon, Label, Button, Rating, Item, Accordion, List, Divider, GridColumn } from 'semantic-ui-react'
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import MenuPrincipal from './../MenuPrincipal';
import logoEmpreRed from '../../assets/EmpreRedLogoE.png';
import { getProductosVisita } from '../../actions/prodTiendaVisitasActions.js';
import { getSubcategoriasDeCategoria, getCategoriasProducto } from '../../actions/tipificacionesActions';
import { urlMedia } from '../../config/configs';
import imageNotFound from '../../assets/imageNotFound.jpeg'
import SearchTiendaCustom from './SearchTienda';
import '../../styles/Tienda.css'

class Tienda extends Component {

  constructor(props) {
    super(props);
    this.state = {
      cargoProductos: false,
      cargoCategorias: false,
      pagina_actual: 1
    }
    this.data = null;
    this.categorias = [];
    this.rootPanels = [];
    this.productosRendered = null;
  }

  componentWillMount() {
    this.props.getProductosVisita(this.state.pagina_actual)
      .then(data => {
        this.data = data.datos;
        this.setState({ cargoProductos: true });
      }, err => {
        console.log(err);
      });

    this.props.getCategoriasProducto()
      .then(data => {
        this.categorias = data;
        this.llenarAccordion();
      }, err => {
        console.log(err);
      });
  }

  renderItemSubcat(subcat) {
    // Armamos un array de un solo elemento para pasarle a la peticion que recibe varias subcategorias una sola
    const arrayIDsSubcats = [subcat.id];
    return (
      <List.Item as='a' onClick={() => this.routeResultPage(arrayIDsSubcats, subcat.descripcion)} key={subcat.id}>
        <List.Content>
          <List.Header>{subcat.descripcion}</List.Header>
        </List.Content>
      </List.Item>
    )
  }

  llenarAccordion() {
    for (let cat of this.categorias) {
      const SubLevelContent = null;
      let subcatsRendered = []
      this.props.getSubcategoriasDeCategoria(cat.id)
        .then(subcats => {
          let arrayIDsSubcats = [];
          subcatsRendered = subcats.map(subcat => {
            arrayIDsSubcats.push(subcat.id);
            return this.renderItemSubcat(subcat)
          });

          SubLevelContent = (
            <div>
              <List celled selection animated verticalAlign='middle'>
                {subcatsRendered}
              </List>
            </div>
          );

          const catPanel = {
            key: cat.descripcion,
            title: {
              content: <Label color='orange' onClick={() => this.routeResultPage(arrayIDsSubcats, cat.descripcion)} content={cat.descripcion} />,
            },
            content: { content: SubLevelContent }
          };

          this.rootPanels.push(catPanel);

          this.setState({ cargoCategorias: true });
        }, err => {
          console.log(err);
        });

    }

  }


  renderProd() {

    if (this.data) {
      return (
        <Grid className="grid-conteiner2">


          <Grid.Row centered>
          <Grid.Column width={16}>

            <center>
              <h4>Página {this.state.pagina_actual} de {this.data.productos.cant_pagina}</h4>
            
            </center>
          </Grid.Column>
          </Grid.Row>
          <Grid.Row loading={!this.state.cargoProductos} attached>
            
           

              <Item.Group divided className="item-group">
                {this.productosRendered}
              </Item.Group>
            
          </Grid.Row>
          <Grid.Row >
            <Grid columns={2} verticalAlign='middle'>
              <Grid.Column>
                <Pagination
                  activePage={this.state.pagina_actual}
                  onPageChange={(e, data) => this.changePage(data.activePage)}
                  totalPages={this.data.productos.cant_pagina}
                />
              </Grid.Column>
            </Grid>
          </Grid.Row>
        </Grid>
      )
    }
  }

  changePage(activePage) {
    this.props.getProductosVisita(activePage)
      .then(data => {
        this.data = data.datos;
        this.setState({ pagina_actual: activePage, cargoProductos: true });
      }, err => {
        console.log(err);
      });
  }

  toEmprendedor(data) {
    return (
      this.props.history.push(
        {
          pathname: '/perfil',
          state: { e: data.emprendedor, m: data.marca }
        }
      )
    )
  }

  routePublicacion(idDetalle) {
    return (
      this.props.history.push(
        {
          pathname: '/publicacion',
          state: { idDetalle: idDetalle }
        }
      )
    )
  }

  routeResultPage(subcategorias, catOSubcatClickeado) {
    // Le pasamos un array de una o varias categorias
    this.props.history.push(
      {
        pathname: '/resultado_busqueda',
        state: { busquedaPorCategoria: subcategorias, categoriaOSubcategoria: catOSubcatClickeado }
      }
    )
    // window.location = '/resultado_busqueda';
  }

  renderProducto(item) {
    return (

      <Item className="itemsP"  >
        <Item.Image as='a' floated='left' size='small' onClick={() => this.routePublicacion(item.id)} src={(item.producto.foto !== "") ? urlMedia + item.producto.foto : imageNotFound} />
        <Item.Content as='a' >
          <Segment>
            <Item.Header style={{ "overflow": "hidden", "word-wrap": "break-word" }}>
              <h3>
                {item.titulo}
              </h3>
            </Item.Header>
            <Item.Meta>
              <Label size='big' style={{ backgroundColor: '#e6e6e6' }}>
                {(item.precio_venta <= 0) ? "N/A" : "$" + parseInt(item.precio_venta, 10)}
              </Label>
              <Label color='orange' attached='bottom right' >
                {item.producto.categoria}
              </Label>
              <h4>
                de <a onClick={() => { this.toEmprendedor(item) }} >{item.marca.nombre ? item.marca.nombre : item.emprendedor.nombre}</a>
              </h4>
              <p>
                <Icon name='eye' />&nbsp;&nbsp;
              {item.visitas}   Visitas &nbsp;&nbsp;
            </p>
              <Rating disabled maxRating={5} defaultRating={item.valoracion} icon='star' size='small' />
            </Item.Meta>
          </Segment>
        </Item.Content>
      </Item>

    )
  }

  renderProductosVisitas() {
    if (this.state.cargoProductos) {
      this.productosRendered = this.data.productos.data.map(p => {
        return this.renderProducto(p)
      })
    }
  }

  renderLogoYBuscador() {
    return (
      <Grid>
        <Grid.Row centered verticalAlign='middle'>
          <Grid.Column width={1}>
            <Image src={logoEmpreRed} size="tiny" />
          </Grid.Column>

          <Grid.Column width={6} >
            {/* <Form.Input fluid label='' placeholder='Buscar productos' icon={{ name: 'search', circular: true, link: true }}/> */}
            <SearchTiendaCustom />
          </Grid.Column>
        </Grid.Row>
      </Grid>
    )
  }

  renderCategorias() {
    return (
      <Grid Column width={4}>
        <Grid.Row centered>
          <Grid.Column width={10}>
            <center>
              <h2>Buscar por categorías</h2>
            </center>

          </Grid.Column>
        </Grid.Row>

        <Grid.Row centered>
          <Grid.Column width={10}>

            <Grid loading={!this.state.cargoCategorias}>
              <Grid.Row verticalAlign='middle'>
                <Grid.Column width={16}>
                  <Accordion className="Accordion" fluid panels={this.rootPanels} />
                </Grid.Column>
              </Grid.Row>
            </Grid>

          </Grid.Column>
        </Grid.Row>
      </Grid>

    )
  }

  render() {
    this.renderProductosVisitas();
    return (

      <body>
        <Header>
          <MenuPrincipal />
        </Header>
        <div className="DivPrincipalTienda" >


          {this.renderLogoYBuscador()}



          <Grid className="grid-conteiner">
            <Grid.Row>
              <Grid.Column width={4} >
                {this.renderCategorias()}
              </Grid.Column>
              <Grid.Column className="column-products" width={10}>
                <Segment className="segment-conteiner" loading={!(this.state.cargoProductos && this.state.cargoCategorias)}>
                  <center>
                    {this.renderProd()}
                  </center>
                </Segment>

              </Grid.Column>
            </Grid.Row>
          </Grid>
        </div>
      </body>
    )
  }
}

const mapStateToProps = state => ({
  itemsTable: state.tableData.items,
  categoriasProducto: state.tipificaciones.categoriasProducto,
});

export default withRouter(connect(mapStateToProps, { getProductosVisita, getCategoriasProducto, getSubcategoriasDeCategoria })(Tienda));

