Documentación del cliente
--------------------------

Recordá agregar lo siguiente en este archivo para que la documentación escrita
pueda visualizarse aquí.

.. code-block:: rest

   .. automodule:: script
      :members:

Donde *script* es el nombre del archivo cuya documentación debe indexarse.
Sphinx indexa los archivos del cliente ubicados debajo de EmpreRed/client/src.
Si querés indexar un archivo *bar* que a su vez esté dentro de otra carpeta,
por ejemplo *EmpreRed/client/src/foo/*, deberías hacer lo siguiente:

.. code-block:: rest

   .. automodule:: foo.bar
      :members:


Todas las clases y métodos nuevos que sean agregados a los archivos ya indexados
no requieren de ninguna acción extra.

Si algo no es parseado como debería, probablemente sea porque falta una línea en
blanco entre directiva y directiva, o en el docstring.
