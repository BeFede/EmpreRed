.. EmpreRed documentation master file, created by
   sphinx-quickstart on Wed May  2 17:46:02 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Bienvenido a la documentación de EmpreRed!
==========================================

Requerimientos:

El sistema requiere los paquetes Sphinx, m2r y sphinx_rtd_theme para
generar la documentación.

Autores: Alberteam

Contenidos:
-----------

.. toctree::
   :maxdepth: 2

   intro
   project
   server
   client


Índices y tablas
----------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
