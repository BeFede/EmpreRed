

Cómo escribir la documentación
------------------------------

La documentación se puede escribir en markdown o en rst.
Importante: las directivas de rest deben escribirse en archivos rst, de otro modo
no funcionan.

Para usar markdown es necesario utilizar m2r.

Se pueden agregar archivos usando la directiva automodule:

.. code-block:: rest

   .. automodule:: script
      :members:

Donde script es un archivo (script.py) debajo alguna de las carpetas de EmpreRed
que estén cargadas en conf.py

Con lo cual queda así:

.. automodule:: script
   :members:

Para generar la documentación y abrir el archivo en el navegador:

.. code-block:: bash

    $ cd EmpreRed/docs
    $ make clean && make html && chromium _build/html/index.html


Cheatsheet de ReST
-------------------

http://docutils.sourceforge.net/docs/user/rst/cheatsheet.txt
