import { Component, ViewChild } from '@angular/core';
import {Nav, NavController, Platform} from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';


import { PerfilPage } from '../pages/perfil/perfil';
import { LoginPage } from "../pages/login/login";
import {LogoutPage} from "../pages/logout/logout";
import { Storage } from '@ionic/storage';
import { Events } from 'ionic-angular';
import {EmprendedoresPage} from "../pages/emprendedores/emprendedores";
import {RegistroRolPage} from "../pages/registro-rol/registro-rol";



@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = HomePage;

  pages: Array<{title: string, component: any}>;

  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen,
              private storage : Storage, public events : Events) {
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Artículos', component: HomePage },
      { title: 'Emprendedores', component: EmprendedoresPage },


    ];

    events.subscribe('user:login', () => {
      this.login()
    });

    events.subscribe('user:logout', () => {
      this.logout()
    });

    this.storage.get('usuario').then(
      (usuario) => {


          if (usuario == null){
            this.pages.push({title: 'Login', component: LoginPage})

          }
          else {
            console.log(usuario)
            if (usuario.es_activo == true) {
              //this.pages.push({ title: 'Compras', component: ComrpasPage })
              this.pages.push({title: 'Perfil', component: PerfilPage})
              this.pages.push({title: 'Cerrar sesión', component: LogoutPage})
            }
            else {
              this.pages.push({title: 'Registro rol', component: RegistroRolPage})
            }
          }



      }
    )

  }

  borrarEntrada(title){
    for (let i = 0; i < this.pages.length; i++){
      if (this.pages[i].title == title){
        this.pages.splice(i, 1);
        break;
      }
    }
  }

  login(){
    this.pages.push({ title: 'Perfil', component: PerfilPage })
    this.pages.push({ title: 'Cerrar sesión', component: LogoutPage })
    this.borrarEntrada('Login')
  }

  logout(){
    this.borrarEntrada('Perfil')
    this.borrarEntrada('Cerrar sesión')
    this.pages.push({title: 'Login', component: LoginPage})
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }

}
