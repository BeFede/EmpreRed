import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { StarRatingModule } from 'ionic3-star-rating';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { PerfilPage } from '../pages/perfil/perfil';
import { LoginPage } from '../pages/login/login';
import { LogoutPage} from "../pages/logout/logout";
import { EmprendedoresPage } from "../pages/emprendedores/emprendedores";
import { EmprendedorPage } from '../pages/emprendedor/emprendedor';
import { ProductosListPage} from "../pages/productos-list/productos-list";

import { DetalleCatalogoPageModule} from "../pages/detalle-catalogo/detalle-catalogo.module";
import {ModalFiltrosPageModule} from '../pages/modal-filtros/modal-filtros.module';
import {ModalCompraProductoPageModule} from '../pages/modal-compra-producto/modal-compra-producto.module';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { UsuariosProvider } from '../providers/usuarios/usuarios';
import { HttpClientModule } from '@angular/common/http';
import { IonicStorageModule } from '@ionic/storage';
import { EmprendedoresProvider } from '../providers/emprendedores/emprendedores';
import { TiendaProvider } from '../providers/tienda/tienda';
import {ComponentsModule} from '../components/components.module'

import  {InAppBrowser} from '@ionic-native/in-app-browser/ngx'
import {EmprendedoresPageModule} from "../pages/emprendedores/emprendedores.module";
import {EmprendedorPageModule} from "../pages/emprendedor/emprendedor.module";
import {RegistroUsuarioPage} from '../pages/registro-usuario/registro-usuario';
import {RegistroUsuarioPageModule} from "../pages/registro-usuario/registro-usuario.module";
import {RegistroRolPageModule} from "../pages/registro-rol/registro-rol.module";
import {RegistroEmprendedorPageModule} from "../pages/registro-emprendedor/registro-emprendedor.module";
import {RegistroClientePageModule} from "../pages/registro-cliente/registro-cliente.module";


@NgModule({
  declarations: [
    MyApp,
    HomePage,    
    PerfilPage,
    LoginPage,

    ProductosListPage,
    LogoutPage,

  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot(),
    ComponentsModule,
    StarRatingModule,
    ModalFiltrosPageModule,
    ModalCompraProductoPageModule,
    DetalleCatalogoPageModule,
    EmprendedoresPageModule,
    EmprendedorPageModule,
    RegistroUsuarioPageModule,
    RegistroRolPageModule,
    RegistroClientePageModule,
    RegistroEmprendedorPageModule,


  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LoginPage,
    LogoutPage,
    PerfilPage,
    EmprendedoresPage,
    EmprendedorPage,
    ProductosListPage,

    
  ],
  providers: [
    StatusBar,
    SplashScreen,
    UsuariosProvider,
    EmprendedoresProvider,
    TiendaProvider,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    InAppBrowser,

  ]
})
export class AppModule {}
