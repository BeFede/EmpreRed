import { NgModule } from '@angular/core';
import { DetalleCatalogoListItemComponent } from './detalle-catalogo-list-item/detalle-catalogo-list-item';
import {IonicModule} from "ionic-angular";
import { DetalleCatalogoListComponent } from './detalle-catalogo-list/detalle-catalogo-list';

@NgModule({
	declarations: [DetalleCatalogoListItemComponent,
    DetalleCatalogoListComponent],
	imports: [IonicModule],
	exports: [DetalleCatalogoListItemComponent,
    DetalleCatalogoListComponent]
})
export class ComponentsModule {}
