import {Component, Input} from '@angular/core';
import {Config} from "../../providers/config";
import {NavController, NavParams} from "ionic-angular";
import {DetalleCatalogoPage} from '../../pages/detalle-catalogo/detalle-catalogo';


/**
 * Generated class for the DetalleCatalogoListItemComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'detalle-catalogo-list-item',
  templateUrl: 'detalle-catalogo-list-item.html'
})
export class DetalleCatalogoListItemComponent {

  text: string;
  urlMedia: string;
  @Input() detalleCatalogo : any;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.urlMedia = Config.apiUrlMedia;
  }


  verDetalleCatalogo(id){
    this.navCtrl.push(DetalleCatalogoPage, {
      idDetalle: id
    });
  }

}

