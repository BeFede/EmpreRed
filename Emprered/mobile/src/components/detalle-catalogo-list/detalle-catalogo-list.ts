import {Component, Input} from '@angular/core';
import {TiendaProvider} from "../../providers/tienda/tienda";
import {Config} from "../../providers/config";
import {ModalController} from 'ionic-angular';
import {ModalFiltrosPage} from '../../pages/modal-filtros/modal-filtros';

/**
 * Generated class for the DetalleCatalogoListComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'detalle-catalogo-list',
  templateUrl: 'detalle-catalogo-list.html'
})
export class DetalleCatalogoListComponent {

  @Input() detallesCatalogo: Array<object>;
  @Input() idEmprendedor : number;
  pag_actual: number;
  urlMedia : string;
  nombre : string
  public filtros : any;

  constructor(public tiendaProvider : TiendaProvider, public controladorModal : ModalController) {
    this.filtros = {

    };
    this.nombre = "";
    this.pag_actual = 1;
    this.urlMedia = Config.apiUrlMedia;
    if (this.idEmprendedor != undefined){
      this.filtros.idEmprendedor = this.idEmprendedor
    }
  }



abrirModalFiltros(){
  let modal = this.controladorModal.create(ModalFiltrosPage, {nombre: this.nombre});
  modal.present();

  modal.onDidDismiss((filtros)=>{



    if (filtros != undefined){

      if (filtros.nombre != undefined) this.nombre = filtros.nombre;
      this.filtros = filtros;


    this.detallesCatalogo = [];
    this.getProductos();
    }

  })
}


  cargarMasDetalles(event){

  this.pag_actual = this.pag_actual + 1;
  setTimeout(() => {

     this.getProductos()
    event.complete();
  }, 1000);

}




buscarProductos(nombre){

      this.nombre = nombre;
      this.filtros.nombre = nombre;
      this.pag_actual = 1;
      this.tiendaProvider.getListaProductos("visitas",this.filtros, this.pag_actual).then(
      (res) => {
        this.detallesCatalogo = res.datos.productos.data;
      }
    ).catch(
      (err) => console.log(err)
    )



}

getProductos(){

    this.tiendaProvider.getListaProductos("visitas",this.filtros, this.pag_actual).then(
      (res) => {


          if (res.datos.productos.data != undefined){

          if (this.detallesCatalogo == undefined ){
            this.detallesCatalogo = res.datos.productos.data;
          }
          else {
            for (let i = 0; i < res.datos.productos.data.length; i++){
            this.detallesCatalogo.push(res.datos.productos.data[i]);
          }
          }

        }




      }
    ).catch(
      (err) => console.log(err)
    )
}



}
