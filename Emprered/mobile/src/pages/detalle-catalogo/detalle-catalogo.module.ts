import { NgModule } from '@angular/core';
 import { IonicPageModule } from 'ionic-angular';
 import { DetalleCatalogoPage } from './detalle-catalogo';
import {StarRatingModule} from "ionic3-star-rating";

@NgModule({
  declarations: [
     DetalleCatalogoPage,
  ],
  imports: [
    IonicPageModule.forChild(DetalleCatalogoPage),
    StarRatingModule,
  ],
})
export class DetalleCatalogoPageModule {}
