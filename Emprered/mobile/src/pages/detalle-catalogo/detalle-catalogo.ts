import { Component } from '@angular/core';
import {IonicPage, ModalController, NavController, NavParams} from 'ionic-angular';
import {TiendaProvider} from "../../providers/tienda/tienda";
import {Config} from "../../providers/config";
import {Storage} from "@ionic/storage";


import {ModalCompraProductoPage} from "../modal-compra-producto/modal-compra-producto";



/**
 * Generated class for the DetalleCatalogoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-detalle-catalogo',
  templateUrl: 'detalle-catalogo.html',
})
export class DetalleCatalogoPage {

  urlMedia : string;
  idDetalle : string;
  imagenes: object[] ;
  comentarios: object[];
  titulo: string;
  descripcion: string;
  precio: string;
  etiqueta: string;
  valoracion: number;
  visitas: string;
  emprendedor_nombre: string;
  emprendedor_foto: string;
  emprendedor_id: string;
  categoria: string;
  subcategoria: string;
  inputComentario: string;


  constructor(public navCtrl: NavController, public navParams: NavParams, public tiendaProvider : TiendaProvider, public storage : Storage, public controladorModal : ModalController) {
    this.urlMedia = Config.apiUrlMedia;
    let idDetalle = navParams.get('idDetalle');
    this.getDetalleCatalogo(idDetalle)
  }

  ionViewDidLoad() {
  }

  getDetalleCatalogo(idDetalle){
    this.tiendaProvider.getDetalleCatalogo(idDetalle)
      .then(
        (res) => {
          this.setData(res.datos.detalle_catalogo)
        }
      )
      .catch(
        (err) => console.log(err)
      )
  }

  setData(detalle){
    this.idDetalle = detalle.id;
    this.imagenes = detalle.imagenes;
    this.comentarios = detalle.comentarios;
    this.titulo = detalle.titulo;
    this.descripcion = detalle.descripcion;
    this.precio = detalle.precio_venta;
    this.valoracion = parseInt(detalle.valoracion);
    this.visitas = detalle.visitas;
    this.emprendedor_nombre = detalle.emprendedor.nombre + " " + detalle.emprendedor.apellido;
    this.emprendedor_foto = detalle.emprendedor.foto;
    this.emprendedor_id = detalle.emprendedor.id;
    this.categoria = detalle.producto.categoria;
    this.subcategoria = detalle.producto.subcategoria;

  }


  enviarComentario(){
    let textComentario = this.inputComentario;
    this.storage.get('usuario').then(
      (usuario) => {
        if (usuario != null){
          this.storage.get('token').then(
            (token) => {
              this.tiendaProvider.agregarComentario(this.idDetalle, textComentario, token).then(
                (res) => {
                  this.getDetalleCatalogo(this.idDetalle);
                  this.inputComentario = "";
                }

              ).catch(
                (err) => console.log(err)
              )

            }
          ).catch(
            (err) => console.log(err)
          )
        }
        else {
          console.log("Debe estar logueado para hacer un comentario")
        }
      }
    ).catch(
      (err) => console.log(err)
    )
  }

  abrirModalCompra(){
    let modal = this.controladorModal.create(ModalCompraProductoPage, {titulo: this.titulo, idDetalle: this.idDetalle, imagenes: this.imagenes, descripcion: this.descripcion, precio:this.precio});
      modal.present();

      modal.onDidDismiss((compra)=>{

        console.log(compra)
  })



  }

}
