import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EmprendedorPage } from './emprendedor';
import {DetalleCatalogoListComponent} from "../../components/detalle-catalogo-list/detalle-catalogo-list";
import {ComponentsModule} from "../../components/components.module";

@NgModule({
  declarations: [
    EmprendedorPage,

  ],
  imports: [
    IonicPageModule.forChild(EmprendedorPage),
    ComponentsModule,
  ],
})
export class EmprendedorPageModule {}
