import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {TiendaProvider} from "../../providers/tienda/tienda";
import {Config} from "../../providers/config";

/**
 * Generated class for the EmprendedorPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()


@Component({
  selector: 'page-emprendedor',
  templateUrl: 'emprendedor.html'


})
export class EmprendedorPage {

  emprendedor : object;
  urlMedia : string;
  detalles_catalogo : object;
  cant_page : number;
  pag_actual : number;

  constructor(public navCtrl: NavController, public navParams: NavParams, public tiendaProvider : TiendaProvider) {
    this.urlMedia = Config.apiUrlMedia;
    this.emprendedor = navParams.get("emprendedor");
    this.getCatalogo(this.emprendedor);
  }

  ionViewDidLoad() {
    console.log();
  }

  getCatalogo(emprendedor){
    this.tiendaProvider.getCatalogoMinorista(emprendedor.id).then(
      (res) => {
        this.detalles_catalogo = res.datos.catalogo.detalles_catalogo.data;
        this.cant_page = res.datos.catalogo.detalles_catalogo.cant_pagina;
        this.pag_actual = res.datos.catalogo.detalles_catalogo.pagina_actual;
        console.log(this.detalles_catalogo)
      }
    ).catch(
      (err) => console.log(err)
    )

  }

}
