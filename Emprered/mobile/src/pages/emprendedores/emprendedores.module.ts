import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EmprendedoresPage } from './emprendedores';

@NgModule({
  declarations: [
    EmprendedoresPage,
  ],
  imports: [
    IonicPageModule.forChild(EmprendedoresPage),
  ],
})
export class EmprendedoresPageModule {}
