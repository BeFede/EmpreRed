import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {EmprendedoresProvider} from '../../providers/emprendedores/emprendedores';
import {Config} from '../../providers/config';
import { EmprendedorPage } from '../emprendedor/emprendedor';

/**
 * Generated class for the EmprendedoresPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-emprendedores',
  templateUrl: 'emprendedores.html',
})
export class EmprendedoresPage {

  nombre : string;

  constructor(public navCtrl: NavController, public navParams: NavParams, public emprendedoresProvider: EmprendedoresProvider) {
    this.getEmprendedores()
  }

  emprendedores = [] // array<any>
  urlMedia = Config.apiUrlMedia;

  getEmprendedores(){
    this.emprendedoresProvider.getEmprendedores().then(
      
      (data) => {
        console.log(data.datos.emprendedores)
        this.emprendedores = data.datos.emprendedores
      }

    ).catch(

      (err) => console.log(err)
    )
  }

  ionViewDidLoad() {}

  verEmprendedor(emprendedor){
    this.navCtrl.push(EmprendedorPage, {
      emprendedor: emprendedor
    })
  }

  buscarEmprendedores(){
    let filtros = {
      "nombre": this.nombre
    };

      this.emprendedoresProvider.getEmprendedores(filtros).then(
      (res) => {
        this.emprendedores = res.datos.emprendedores;
      }
    ).catch(
      (err) => console.log(err)
    )



  }





}
