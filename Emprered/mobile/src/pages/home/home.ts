import  {Component} from "@angular/core";
import {NavController, PopoverController, MenuController} from "ionic-angular";
import {Config} from "../../providers/config";
import {TiendaProvider} from "../../providers/tienda/tienda";



@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})

export class HomePage {
  // search condition
  public search = {

    date: new Date().toISOString()
  }


  urlMedia : string;
  detalles_catalogo : object;
  cant_page : number;
  pag_actual : number;

  constructor(public nav: NavController, public popoverCtrl: PopoverController, public menu: MenuController, public tiendaProvider : TiendaProvider) {
    this.menu.swipeEnable(false);
        this.urlMedia = Config.apiUrlMedia;
    this.getProductos("visitas");
  }
  getProductos(criterio= "visitas", nombre = ""){
    this.tiendaProvider.getListaProductos(criterio,nombre).then(
      (res) => {
        console.log(res)
        this.detalles_catalogo = res.datos.productos.data;
        this.cant_page = res.datos.productos.cant_pagina;
        this.pag_actual = res.datos.productos.pagina_actual;

      }
    ).catch(
      (err) => console.log(err)
    )
  }



}

//
