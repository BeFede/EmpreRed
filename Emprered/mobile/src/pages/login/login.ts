import {Component} from "@angular/core";
import {NavController, AlertController, MenuController} from "ionic-angular";
import {HomePage} from "../home/home";
import { UsuariosProvider } from '../../providers/usuarios/usuarios';
import {Storage} from "@ionic/storage";
import { Events } from 'ionic-angular';
import {RegistroUsuarioPage} from "../registro-usuario/registro-usuario"
import {RegistroRolPage} from "../registro-rol/registro-rol"

@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {

  constructor(
    public navCtrl: NavController, 
    public menu: MenuController, 
    public usuariosProvider: UsuariosProvider,
    private alertCtrl: AlertController,
    private storage : Storage,
    public events : Events
     ) {
    this.menu.swipeEnable(false);
  }

  // go to register page
  registrar() {
     this.navCtrl.push(RegistroUsuarioPage)
  }

  // login and go to home page
  login(email, password) {

    this.usuariosProvider.login(email, password)
    .then(
      (data) =>{
        console.log(data);          

        this.storage.set('usuario', data.datos.usuario);
        this.storage.set('token', data.datos.token)
        this.events.publish('user:login');

        if (data.datos.usuario.es_activo == true){
          this.navCtrl.setRoot(HomePage)
        }
        else {
          this.navCtrl.setRoot(RegistroRolPage)
        }


      }
    ).catch(
      (data) => {        
        if (data.error){
          this.mostrarMensajeErrorLogin(data.error.mensaje)

        }
      }
    )
    
  }


  olvidoClave() {
    console.log("Not implemented")
  }

  mostrarMensajeErrorLogin(mensaje){
    
      let alert = this.alertCtrl.create({
        title: 'Error en el ingreso',
        subTitle: mensaje,
        buttons: ['Aceptar']
      });
      alert.present();
    
  }

}
