import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ModalCompraProductoPage } from './modal-compra-producto';

@NgModule({
  declarations: [
    ModalCompraProductoPage,
  ],
  imports: [
    IonicPageModule.forChild(ModalCompraProductoPage),
  ],
})
export class ModalCompraProductoPageModule {}
