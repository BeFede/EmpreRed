import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, AlertController, LoadingController } from 'ionic-angular';
import {Config} from "../../providers/config";
import {Storage} from "@ionic/storage";
import {TiendaProvider} from "../../providers/tienda/tienda";
import {InAppBrowser, InAppBrowserEvent, InAppBrowserObject, InAppBrowserOptions} from "@ionic-native/in-app-browser/ngx";

/**
 * Generated class for the ModalCompraProductoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-modal-compra-producto',
  templateUrl: 'modal-compra-producto.html',
})
export class ModalCompraProductoPage {


  urlMedia : string;
  idDetalle : string;
  imagenes: object[] ;
  titulo: string;
  descripcion: string;
  precio: number;
  precio_unitario: number;
  cantidad : string;
  browser : InAppBrowserObject;



  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl : ViewController, public storage : Storage, public tiendaProvider : TiendaProvider, private alertCtrl: AlertController, public loadingController: LoadingController, public inAppBrower : InAppBrowser) {
    this.urlMedia = Config.apiUrlMedia;
    this.titulo = navParams.get('titulo')
    this.idDetalle = navParams.get('idDetalle')
    this.imagenes = navParams.get('imagenes')
    this.descripcion = navParams.get('descripcion')
    this.precio_unitario = navParams.get('precio')
    this.precio = this.precio_unitario
    this.cantidad = "1.00"
  }

  ionViewDidLoad() {

  }

  comprarProducto(){
    let loader = this.loadingController.create({
          content: "Procesando ..."
        });
    loader.present();

    if (this.cantidad != undefined && parseFloat(this.cantidad) > 0) {
      this.storage.get('usuario').then(
        (usuario) => {
          if (usuario != null) {
            this.storage.get('token').then(
              (token) => {
                this.tiendaProvider.comprarProducto(this.idDetalle, parseFloat(this.cantidad), token).then(
                  (res) => {
                    loader.dismiss()

                    let idVenta = res.datos.venta_id;
                    console.log(res.datos);

                    let options :InAppBrowserOptions = {
                      location: 'yes'
                    }


                    this.browser = this.inAppBrower.create(res.datos.link_de_pago, '_blank', options);


                    //browser.show();


                    this.browser.on("loadstart").subscribe((event : InAppBrowserEvent) => {



                        if (event.url.includes("mercado_pago_exito/exito")){
                          console.log("asdas")
                          console.log(event.url.split("?")[1])
                        }
                        else if (event.url.includes("mercado_pago/error")){
                          console.log("asdas3423423")
                          console.log(event.url.split("?")[1])
                          this.titulo = event.url;
                        }


                    });


                    this.browser.on('loaderror').subscribe(() => {
                      this.browser.close();
                    })


                    /*
                   this.browser.on("loadstart").subscribe((event: InAppBrowserEvent) => {

                        //pull code from url and store in variable

                        if (event.url.includes("mercado_pago_exito/exito")){
                          console.log("asdas")
                          console.log(event.url.split("?")[1])
                        }
                        else if (event.url.includes("mercado_pago/error")){
                          console.log("asdas3423423")
                          console.log(event.url.split("?")[1])
                          this.titulo = (event.url.split("?")[1])

                        }
                        else {
                          this.titulo = event.url.split("?")[1]
                        }

                        this.descripcion = (event.url.indexOf("mercado_pago/error").toString())



                        this.browser.close();
                        //let token = event.url.split('=')[1].split('&')[0];
                    });*/



                  }
                ).catch(
                  (err) => loader.dismiss()
                )

              }
            ).catch(
              (err) => loader.dismiss()
            )
          } else {
            this.mostrarMensaje("Error","Debe estar logueado para hacer un comentario")
          }
        }
      ).catch(
        (err) => loader.dismiss()
      )
    }
    else{
      loader.dismiss()
      this.mostrarMensaje('Error','La cantidad debe ser positiva')
    }
  }

  actualizarPrecio(cantidad){
    if (cantidad != undefined){
      this.precio = this.precio_unitario * parseFloat(this.cantidad);
    } else{
      this.precio = this.precio_unitario;
    }
  }

  validarCompra(idVenta: number){
   let loader = this.loadingController.create({
          content: "Procesando ..."
        });
    loader.present();


    this.storage.get('usuario').then(
      (usuario) => {
        if (usuario != null) {
          this.storage.get('token').then(
            (token) => {
                 this.tiendaProvider.consultarVenta(idVenta, token).then(
                  (res) => {
                    loader.dismiss()
                   this.mostrarMensaje("Datos", "volvio")

                  }
                ).catch(
                  (err) => loader.dismiss()
                )
              }

          ).catch(
            (err) => {
              console.log(err)
            }
          )
        } else {
          this.mostrarMensaje("Error", "Debe estar logueado para hacer consultar la venta")

        }
      }
    ).catch(
      (err) => {
            console.log(err)
      }
    )

    
  }



  mostrarMensaje(titulo, mensaje){

      let alert = this.alertCtrl.create({
        title: titulo,
        subTitle: mensaje,
        buttons: ['Aceptar']
      });
      alert.present();

  }


   cerrar(){
    this.viewCtrl.dismiss()
  }


}





















/*
  urlMedia : string;
  idDetalle : string;
  imagenes: object[] ;
  titulo: string;
  descripcion: string;
  precio: string;
  cantidad: number;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    //this.urlMedia = Config.apiUrlMedia;
    this.idDetalle = navParams.get('idDetalle');
    this.imagenes = navParams.get('imagenes');
    this.titulo = navParams.get('titulo');
    this.descripcion = navParams.get('descripcion');
    this.precio = navParams.get('precio');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ModalCompraProductoPage');
  }


  comprarProducto(){
    this.storage.get('usuario').then(
      (usuario) => {
        if (usuario != null){
          this.storage.get('token').then(
            (token) => {
              this.tiendaProvider.agregarComentario(this.idDetalle, textComentario, token).then(
                (res) => {
                  this.getDetalleCatalogo(this.idDetalle);
                  this.inputComentario = "";
                }

              ).catch(
                (err) => console.log(err)
              )

            }
          ).catch(
            (err) => console.log(err)
          )
        }
        else {
          console.log("Debe estar logueado para hacer un comentario")
        }
      }
    ).catch(
      (err) => console.log(err)
    )



    //const browser = this.inAppBrowser.create()

  }
}
*/
