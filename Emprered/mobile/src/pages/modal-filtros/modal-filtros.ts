import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';

/**
 * Generated class for the ModalFiltrosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-modal-filtros',
  templateUrl: 'modal-filtros.html',
})
export class ModalFiltrosPage {

  nombre: string;
   precioDesde : number;
   precioHasta : number;
   categorias : Array<any>;
   subCategorias : Array<any>;
   emprendedor : string;


   marca : string;

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl : ViewController) {
    this.nombre = navParams.get('nombre');
  }

  ionViewDidLoad() {

  }

  buscar(){
    this.viewCtrl.dismiss({
      "nombre": this.nombre,
      "precioDesde": this.precioDesde,
      "precioHasta": this.precioHasta,
      "emprendedor": this.emprendedor,
      "marca": this.marca,
      "categorias": this.categorias,
      "subCategorias": this.subCategorias
    })
  }

   cerrar(){
    this.viewCtrl.dismiss()
  }

}
