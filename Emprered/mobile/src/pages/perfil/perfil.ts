import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {Storage} from "@ionic/storage";

/**
 * Generated class for the PerfilPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-perfil',
  templateUrl: 'perfil.html',
})
export class PerfilPage {

  nombre : string;
  constructor(public navCtrl: NavController, public navParams: NavParams, private storage : Storage) {
    this.storage.get('usuario').then(
      (usuario) => {
        console.log(usuario)
      }
    ).catch(
      (err) => console.log(err)
    )
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PerfilPage');
  }

}
