import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ProductosListPage } from './productos-list';


@NgModule({
  declarations: [
    ProductosListPage,

  ],
  imports: [
    IonicPageModule.forChild(ProductosListPage),
  ],
})
export class ProductosListPageModule {}
