import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {TiendaProvider} from "../../providers/tienda/tienda";
import {Config} from "../../providers/config";


/**
 * Generated class for the ProductosListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-productos-list',
  templateUrl: 'productos-list.html',
})
export class ProductosListPage {

  urlMedia : string;
  detalles_catalogo : object;
  cant_page : number;
  pag_actual : number;

  constructor(public navCtrl: NavController, public navParams: NavParams, public tiendaProvider : TiendaProvider) {
    this.urlMedia = Config.apiUrlMedia;
    this.getProductos("visitas");
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProductosListPage');
  }

  getProductos(criterio= "visitas", nombre = ""){
    this.tiendaProvider.getListaProductos(criterio,nombre).then(
      (res) => {
        console.log(res)
        this.detalles_catalogo = res.datos.productos.data;
        this.cant_page = res.datos.productos.cant_pagina;
        this.pag_actual = res.datos.productos.pagina_actual;

      }
    ).catch(
      (err) => console.log(err)
    )
  }

}
