import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RegistroEmprendedorPage } from './registro-emprendedor';

@NgModule({
  declarations: [
    RegistroEmprendedorPage,
  ],
  imports: [
    IonicPageModule.forChild(RegistroEmprendedorPage),
  ],
})
export class RegistroEmprendedorPageModule {}
