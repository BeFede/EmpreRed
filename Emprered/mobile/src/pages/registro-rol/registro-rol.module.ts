import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RegistroRolPage } from './registro-rol';

@NgModule({
  declarations: [
    RegistroRolPage,
  ],
  imports: [
    IonicPageModule.forChild(RegistroRolPage),
  ],
})
export class RegistroRolPageModule {}
