import { Component } from '@angular/core';
import {IonicPage, NavController, NavParams, Platform} from 'ionic-angular';
import {RegistroEmprendedorPage} from "../registro-emprendedor/registro-emprendedor";
import {RegistroClientePage} from "../registro-cliente/registro-cliente";

/**
 * Generated class for the RegistroRolPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-registro-rol',
  templateUrl: 'registro-rol.html',
})
export class RegistroRolPage {


  widthScreen :number;
  heighScreen :number;

  constructor(public navCtrl: NavController, public navParams: NavParams, platform: Platform) {



    platform.ready().then((readySource) => {
      this.widthScreen = platform.width();
      this.heighScreen =  platform.height();
      console.log(this.heighScreen);
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegistroRolPage');
  }


  registrarEmprendedor(){
    this.navCtrl.push(RegistroEmprendedorPage);
  }

  registrarCliente(){
      this.navCtrl.push(RegistroClientePage);
  }

}
