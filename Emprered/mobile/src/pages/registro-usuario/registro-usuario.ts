import { Component } from '@angular/core';
import {IonicPage, NavController, NavParams, AlertController, LoadingController} from 'ionic-angular';
import {UsuariosProvider} from "../../providers/usuarios/usuarios";
import {LoginPage} from "../login/login";


/**
 * Generated class for the RegistroUsuarioPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-registro-usuario',
  templateUrl: 'registro-usuario.html',
})
export class RegistroUsuarioPage {

  email :string;
  username: string;
  password : string;
  password_repetida : string;

  constructor(public navCtrl: NavController, public navParams: NavParams, public usuarioProvider : UsuariosProvider,
              public alertCtrl : AlertController, public loadingController : LoadingController) {

  }

  registrarUsuario() {


   let loader = this.loadingController.create({
          content: "Registrando ..."
        });
    loader.present();

    this.usuarioProvider.registrarUsuario(this.email, this.username, this.password, this.password_repetida)
      .then((e) => {
        console.log(e)
        let alert = this.alertCtrl.create({
          title: 'Registro exitoso',
          subTitle: "Revise su casilla de correo para validar su cuenta",
          buttons: ['Aceptar']
        });
        loader.dismiss()
        alert.present();
        this.navCtrl.push(LoginPage)

      })
      .catch((err) => {
        console.log(err)
        let alert = this.alertCtrl.create({
          title: 'Error',
          subTitle: err.error.mensaje,
          buttons: ['Aceptar']
        });
        loader.dismiss()
        alert.present()
      })
  }





}
