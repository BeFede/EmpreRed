import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Config } from '../config';

/*
  Generated class for the EmprendedoresProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class EmprendedoresProvider {

  constructor(public http: HttpClient) {
       
  }

  getEmprendedores(filtros = {"nombre":""}) : Promise<any>{

    let parametros = "";
    if (filtros.nombre){
      parametros += "&nombre="+filtros.nombre
    }

    const promise = new Promise<any>((resolve, reject) => {


      this.http.get(Config.apiUrl + "emprendedores/?pag_actual=1"+parametros)
        .subscribe(res => {
          resolve(res);
          
        }, (err) => {
          reject(err);
        });

    });

    return promise;

  }


  }


