import {HttpClient, HttpHeaders} from '@angular/common/http';
import { Injectable } from '@angular/core';
import {Config} from "../config";
import { Storage } from '@ionic/storage';
import {AlertController} from "ionic-angular";

//import {RequestOptions} from "@angular/http";

/*
  Generated class for the TiendaProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class TiendaProvider {

  constructor(public http: HttpClient, public storage : Storage, public alerctCtrl : AlertController) {
  }


  getCatalogoMinorista(idEmprendedor) : Promise<any>{

    const promise = new Promise<any>((resolve, reject) => {
      this.http.get(Config.apiUrl + "tienda/catalogos?id=" + idEmprendedor)
        .subscribe(res => {
          console.log(res)
          resolve(res);

        }, (err) => {
          reject(err);
        });

    });

    return promise;

  }



  getDetalleCatalogo(idDetalle) : Promise<any>{

    const promise = new Promise<any>((resolve, reject) => {
      this.http.get(Config.apiUrl + "tienda/detalles_catalogo?id=" + idDetalle)
        .subscribe(res => {
          resolve(res);

        }, (err) => {
          reject(err);
        });

    });

    return promise;

  }


  getListaProductos(criterio, filtros, numero_pagina = 1) : Promise<any>{

    let parametros = ""


    if (filtros.nombre)
       parametros += "&nombre="+filtros.nombre
    if (filtros.precioDesde)
       parametros += "&precio_desde="+filtros.precioDesde
    if (filtros.precioHasta)
       parametros += "&precio_hasta="+filtros.precioHasta
    

    if (numero_pagina)
       parametros += "&pagina_actual="+numero_pagina
    const promise = new Promise<any>((resolve, reject) => {

      this.http.get(Config.apiUrl + "tienda/productos?criterio=" + criterio + parametros)
        .subscribe(res => {
          resolve(res);

        }, (err) => {
          reject(err);
        });

    });

    return promise;

  }

  agregarComentario(idDetalle, descripcion, token) {



      const headers = new HttpHeaders().append('Authorization', token);

      const promise = new Promise<any>((resolve, reject) => {

        this.http.post(Config.apiUrl + "catalogos_emprendedor/administrar_detalle/agregar_comentario", {
          id_detalle: idDetalle,
          titulo: "Comentario en detalle " + idDetalle,
          descripcion: descripcion

        }, {
          headers: headers
        })
          .subscribe(res => {
            resolve(res);

          }, (err) => {
            reject(err);
          });

      });

      return promise;

    }


  comprarProducto(idDetalle, cantidad, token){

     const headers = new HttpHeaders().append('Authorization', token);

      const promise = new Promise<any>((resolve, reject) => {

        this.http.post(Config.apiUrl + "registrar_venta/", {
          id_detalle_catalogo: idDetalle,
          cantidad: cantidad,
          pago_mobile: 1,
          url_exito: Config.urlClienteWeb + 'compra_exito',
          url_error: Config.urlClienteWeb + 'compra_error'
        }, {
          headers: headers
        })
          .subscribe(res => {

            resolve(res);


          }, (err) => {
            reject(err);
          });

      });

      return promise;
  }


  consultarVenta(idVenta, token) : Promise<any> {


    const headers = new HttpHeaders().append('Authorization', token);
    const promise = new Promise<any>((resolve, reject) => {

          this.http.post(Config.apiUrl + "ventas/", {
              'venta_id': idVenta
          }, {
            headers: headers
          })
            .subscribe(res => {
              resolve(res);

            }, (err) => {
              reject(err);
            });

        });
      return promise;


  }








}
