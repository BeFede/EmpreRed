import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Config } from '../config';
import { Storage } from '@ionic/storage';


/*  
  Generated class for the UsuariosProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class UsuariosProvider {

  constructor(
    public http: HttpClient, 
    public storage: Storage,
    ) {
    
  }

  private setUsuario(data){
    this.storage.set("usuario", data.datos.usuario);
    this.storage.set("token", data.datos.token);
  }

   
   public login(user, pass): Promise<any> {
    
    let datos = {
      "username": user,
      "password": pass,
      "tipo_autenticacion": "emprered"
    }     
    const promise = new Promise<any>((resolve, reject) => {      
        this.http.post(Config.apiUrl + 'login', JSON.stringify(datos))
          .subscribe(res => {          
            this.setUsuario(res);                          
            resolve(res);
          }, (err) => {              
              reject(err);
            });

    });
      
    return promise;
    

  
  }

   public registrarUsuario(email, user, pass, pass_repetida): Promise<any> {

    let datos = {
      "usuario": {
        "email": email,
        "username": user,
        "password": pass,
        "password_repetida": pass_repetida,
      },
      "tipo_autenticacion": 'emprered'

    }
    const promise = new Promise<any>((resolve, reject) => {
        this.http.post(Config.apiUrl + 'registro', JSON.stringify(datos))
          .subscribe(res => {
            resolve(res);
          }, (err) => {
              reject(err);
            });

    });

    return promise;



  }




  /*
    this.http.post(this.apiUrl+'/users', JSON.stringify(data), {
    headers: new HttpHeaders().set('Authorization', 'my-auth-token'),
    params: new HttpParams().set('id', '3'),
  })
  .subscribe(res => {
    resolve(res);
  }, (err) => {
    reject(err);
  });
  */ 


}

