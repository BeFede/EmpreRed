from django.contrib import admin

from .models.emprendedores import Emprendedores, Marcas
from .models.clientes import Clientes
from .models.productos import DatosComercialesProducto, Productos
from .models.materias_primas import DatosComercialesMateriaPrima, MateriasPrimas
from .models.tipificaciones import UnidadesMedida, CondicionesFrenteIVA, TiposDocumento, AlicuotasIVA
from .models.direccion import Direcciones
from .models.usuarios import Perfil
from .models.planes_produccion import PlanProduccion, DetallePlanProduccion
from .models.compras import Compra, DetalleCompra, IvaCompras
from .models.producciones import Produccion, DetalleProduccion
from .models.movimientos_stock import MovimientosStock
from .models.catalogo import Catalogo, DetalleCatalogo, Comentario, Valoracion, Etiqueta, ImagenesDetalleCatalogo
from .models.busquedas import Busquedas
from .models.categorias import Categoria, Subcategoria
from .models.eventos import Eventos, EventosMeta, NotificacionesEvento
from .models.notificaciones import Notificaciones


# Register your models here.

admin.site.register(Emprendedores)
admin.site.register(Clientes)
admin.site.register(DatosComercialesProducto)
admin.site.register(Productos)

admin.site.register(DatosComercialesMateriaPrima)
admin.site.register(MateriasPrimas)
admin.site.register(UnidadesMedida)
admin.site.register(Categoria)
admin.site.register(Subcategoria)
admin.site.register(CondicionesFrenteIVA)
admin.site.register(TiposDocumento)
admin.site.register(AlicuotasIVA)
admin.site.register(Perfil)
admin.site.register(Marcas)
admin.site.register(PlanProduccion)
admin.site.register(DetallePlanProduccion)
admin.site.register(Compra)
admin.site.register(DetalleCompra)
admin.site.register(IvaCompras)
admin.site.register(Produccion)
admin.site.register(DetalleProduccion)
admin.site.register(MovimientosStock)
admin.site.register(Catalogo)
admin.site.register(DetalleCatalogo)
admin.site.register(Comentario)
admin.site.register(Valoracion)
admin.site.register(Etiqueta)
admin.site.register(ImagenesDetalleCatalogo)
admin.site.register(Busquedas)
admin.site.register(Eventos)
admin.site.register(EventosMeta)
admin.site.register(NotificacionesEvento)
admin.site.register(Notificaciones)
