
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('server', '0006_initial_data_20180911_1936'),
    ]

    operations = [
        migrations.AddField(
            model_name='produccion',
            name='estado',
            field=models.PositiveSmallIntegerField(blank=True, choices=[(1, 'Pendiente'), (2, 'En Proceso'), (3, 'Terminada'), (4, 'Anulada')], null=True),

        ),
    ]
