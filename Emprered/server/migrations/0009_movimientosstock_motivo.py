# Generated by Django 2.0.2 on 2018-09-23 18:42

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('server', '0008_auto_20180923_1646'),
    ]

    operations = [
        migrations.AddField(
            model_name='movimientosstock',
            name='motivo',
            field=models.CharField(default='', max_length=200),
        ),
    ]
