# Generated by Django 2.0.2 on 2018-10-11 23:22

from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('server', '0013_auto_20181011_2248'),
    ]

    operations = [
        migrations.CreateModel(
            name='Catalogo',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('fecha_hora_publicacion', models.DateTimeField(default=django.utils.timezone.now)),
                ('tipo_catalogo', models.PositiveSmallIntegerField(choices=[(1, 'Mayorista'), (2, 'Minorista')], default=2)),
                ('estado', models.PositiveSmallIntegerField(choices=[(1, 'Visible'), (2, 'No visible')], default=1)),
                ('emprendedor', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='server.Emprendedores')),
            ],
        ),
        migrations.CreateModel(
            name='Comentario',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('titulo', models.CharField(max_length=128)),
                ('descripcion', models.TextField()),
            ],
        ),
        migrations.CreateModel(
            name='DetalleCatalogo',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('valoracion_promedio', models.DecimalField(decimal_places=2, max_digits=2)),
                ('visitas', models.IntegerField()),
                ('estado', models.PositiveSmallIntegerField(choices=[(1, 'Visible'), (2, 'No visible')], default=1)),
                ('catalogo', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='server.Catalogo')),
                ('producto', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='server.Productos')),
            ],
        ),
        migrations.CreateModel(
            name='Valoracion',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('puntaje', models.IntegerField()),
                ('detalle_catalogo', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='server.DetalleCatalogo')),
                ('perfil_creador', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to='server.Perfil')),
            ],
        ),
        migrations.AddField(
            model_name='comentario',
            name='detalle_catalogo',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='server.DetalleCatalogo'),
        ),
        migrations.AddField(
            model_name='comentario',
            name='perfil_creador',
            field=models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to='server.Perfil'),
        ),
    ]
