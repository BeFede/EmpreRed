# Generated by Django 2.0.2 on 2019-02-19 21:21

import datetime
from django.db import migrations, models
import django.db.models.deletion
from django.utils.timezone import utc
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('server', '0038_auto_20190123_0022'),
    ]

    operations = [
        migrations.CreateModel(
            name='DetallesVenta',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('precio_individual', models.DecimalField(decimal_places=2, max_digits=10)),
                ('cantidad', models.DecimalField(decimal_places=2, max_digits=10)),
                ('producto', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='server.Productos')),
            ],
        ),
        migrations.CreateModel(
            name='Pago',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('id_preferencia', models.CharField(max_length=64)),
                ('transaccion_mercado_pago', models.CharField(max_length=64)),
                ('monto', models.DecimalField(decimal_places=2, max_digits=10)),
                ('estado', models.PositiveSmallIntegerField(choices=[(2, 'Realizado'), (1, 'No realizado')], default=1)),
            ],
        ),
        migrations.CreateModel(
            name='Venta',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('fecha_hora_registro', models.DateTimeField(default=django.utils.timezone.now)),
                ('estado', models.PositiveSmallIntegerField(choices=[(1, 'Pendiente de pago'), (2, 'Confirmada'), (3, 'Cancelada')], default=1)),
            ],
        ),
        migrations.AddField(
            model_name='emprendedores',
            name='access_token_mercado_pago',
            field=models.CharField(default='', max_length=128),
        ),
        migrations.AddField(
            model_name='emprendedores',
            name='ultima_actualizacion_token_mercado_pago',
            field=models.CharField(default='', max_length=128),
        ),
        migrations.AlterField(
            model_name='perfil',
            name='expiracion_clave',
            field=models.DateTimeField(default=datetime.datetime(2019, 2, 19, 21, 21, 57, 77378, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='perfil',
            name='expiracion_clave_recuperacion',
            field=models.DateTimeField(default=datetime.datetime(2019, 2, 19, 21, 21, 57, 77432, tzinfo=utc)),
        ),
        migrations.AddField(
            model_name='venta',
            name='comprador',
            field=models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to='server.Perfil'),
        ),
        migrations.AddField(
            model_name='venta',
            name='emprendedor',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='server.Emprendedores'),
        ),
        migrations.AddField(
            model_name='pago',
            name='venta',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='server.Venta'),
        ),
        migrations.AddField(
            model_name='detallesventa',
            name='venta',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='server.Venta'),
        ),
    ]
