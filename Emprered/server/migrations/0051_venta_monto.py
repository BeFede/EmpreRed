# Generated by Django 2.0.2 on 2019-03-12 18:56

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('server', '0050_auto_20190312_1646'),
    ]

    operations = [
        migrations.AddField(
            model_name='venta',
            name='monto',
            field=models.DecimalField(decimal_places=2, default=0.0, max_digits=10),
            preserve_default=False,
        ),
    ]
