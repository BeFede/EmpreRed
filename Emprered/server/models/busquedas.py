from django.db import models
from django.contrib.auth.models import User
from django.utils.timezone import now

class Busquedas(models.Model):
    query = models.TextField()
    boost = models.TextField(default="", null=True)
    model_name = models.TextField(default="", null=True)
    fecha_hora = models.DateTimeField(default=now)
    id_usuario = models.ForeignKey(User, on_delete=models.PROTECT, null=True, blank=True)

    def __str__(self):
        return "Query: {}; Boost: {}; DateTime: {}; User ID:{}".format(self.query, self.boost, self.fecha_hora, self.id_usuario)
