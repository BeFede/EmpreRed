from django.utils.timezone import now
from django.db import models
from .productos import Productos
from .emprendedores import Emprendedores
from .usuarios import Perfil

class Catalogo(models.Model):
    MAYORISTA = 1
    MINORISTA = 2
    VISIBLE = 1
    NO_VISIBLE = 2
    TIPOS_CATALOGO = (
        (MAYORISTA, 'Mayorista'),
        (MINORISTA, 'Minorista')
    )
    ESTADOS = (
        (VISIBLE, 'Visible'),
        (NO_VISIBLE, 'No visible')
    )

    fecha_hora_publicacion = models.DateTimeField(default=now)
    tipo_catalogo = models.PositiveSmallIntegerField(choices=TIPOS_CATALOGO, default=MINORISTA)
    estado = models.PositiveSmallIntegerField(choices=ESTADOS, default=VISIBLE)
    emprendedor = models.ForeignKey(Emprendedores, on_delete=models.CASCADE)

    def get_tipo_catalogo(self):
        return self.TIPOS_CATALOGO[self.tipo_catalogo-1][1]


    def get_estado(self):
        return self.ESTADOS[self.estado-1][1]

    def __str__(self):
        return "Catálogo {0} del emprendedor {1}".format(self.get_tipo_catalogo(), self.emprendedor.nombre)



class Etiqueta(models.Model):
    descripcion = models.CharField(max_length=128)
    def __str__(self):
        return self.descripcion

class DetalleCatalogo(models.Model):
    # Defino estados distintos para el detalle por si difieren en el futuro
    VISIBLE = 1
    NO_VISIBLE = 2
    ESTADOS = (
        (VISIBLE, 'Visible'),
        (NO_VISIBLE, 'No visible')
    )

    catalogo = models.ForeignKey(Catalogo, on_delete=models.CASCADE)
    producto = models.ForeignKey(Productos, on_delete=models.CASCADE)
    valoracion_promedio = models.DecimalField(max_digits=10, decimal_places=2)
    visitas = models.IntegerField()
    estado = models.PositiveSmallIntegerField(choices=ESTADOS, default=VISIBLE)
    titulo = models.CharField(max_length=128, default="")
    descripcion = models.TextField(default="")
    precio = models.DecimalField(max_digits=10, decimal_places=2)
    etiqueta = models.ForeignKey(Etiqueta, on_delete=models.DO_NOTHING, default=None, null=True)

    def get_estado(self):
        return self.ESTADOS[self.estado-1][1]

    def __str__(self):
        return "Producto {0}".format(self.producto.nombre)


class ImagenesDetalleCatalogo(models.Model):
    detalle_catalogo = models.ForeignKey(DetalleCatalogo, on_delete=models.CASCADE)
    imagen = models.ImageField(upload_to='imagenes/{}'.format(detalle_catalogo), null=True)

class Comentario(models.Model):
    detalle_catalogo = models.ForeignKey(DetalleCatalogo, on_delete=models.CASCADE)
    fecha_hora_publicacion = models.DateTimeField(default=now)
    titulo = models.CharField(max_length=128)
    descripcion = models.TextField()
    perfil_creador = models.ForeignKey(Perfil, on_delete=models.DO_NOTHING)
    # Por si alguna vez pinta hacer respuestaas
    comentario_padre = models.ForeignKey("self", on_delete=models.CASCADE, default = None, blank=True, null=True)
    def __str__(self):
        return self.titulo


class Valoracion(models.Model):
    detalle_catalogo = models.ForeignKey(DetalleCatalogo, on_delete=models.CASCADE)
    perfil_creador = models.ForeignKey(Perfil, on_delete=models.DO_NOTHING)
    puntaje = models.IntegerField()
