from django.db import models

class Categoria(models.Model):
    descripcion = models.CharField(max_length=50, default="Otros")

    def __str__(self):
        return self.descripcion

class Subcategoria(models.Model):
    descripcion = models.CharField(max_length=50, default="Otros")
    categoria = models.ForeignKey(Categoria, default=1, on_delete=models.PROTECT)

    def __str__(self):
        return self.descripcion
