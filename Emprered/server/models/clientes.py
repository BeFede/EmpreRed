from django.db import models
from .tipificaciones import TiposDocumento #, CondicionesFrenteIVA
from django.contrib.auth.models import User
from .direccion import Direcciones

# Clientes
class Clientes(models.Model):
    nombre = models.CharField(max_length = 40)
    apellido = models.CharField(max_length = 40, default="")
    id_tipo_documento = models.ForeignKey(TiposDocumento, on_delete=models.PROTECT)
    numero_documento = models.CharField(max_length=15)
    fecha_nacimiento = models.DateField(auto_now = False, auto_now_add=False)
    telefono = models.CharField(max_length = 15, null = True, blank= True)
    id_direccion  = models.ForeignKey(Direcciones, on_delete=models.PROTECT, null=True, blank=True)
    # id_condicion_frente_IVA = models.ForeignKey(CondicionesFrenteIVA, on_delete=models.PROTECT)
    id_usuario = models.ForeignKey(User, on_delete=models.PROTECT, null=True,blank=True)

    def __str__(self):
        return self.nombre
