
from django.utils.timezone import now
from django.db import models
from .productos import Productos
from .materias_primas import MateriasPrimas
from .emprendedores import Emprendedores
from .proveedores import Proveedores

class Compra(models.Model):
    fecha_hora = models.DateTimeField(default=now)
    monto = models.DecimalField(max_digits=10, decimal_places=2)
    emprendedor = models.ForeignKey(Emprendedores, on_delete=models.DO_NOTHING, default=None)
    comprobante_referencia = models.CharField(max_length=15)
    proveedor = models.ForeignKey(Proveedores, on_delete=models.DO_NOTHING, default=None)

class DetalleCompra(models.Model):
    producto = models.ForeignKey(Productos, on_delete=models.DO_NOTHING, null=True)
    materia_prima = models.ForeignKey(MateriasPrimas, on_delete=models.DO_NOTHING, null=True)
    cantidad = models.DecimalField(max_digits=10,decimal_places=2, null=True)
    monto = models.DecimalField(max_digits=10, decimal_places=2)
    descuento = models.DecimalField(max_digits=4, decimal_places=2, null=True)
    compra = models.ForeignKey(Compra, on_delete=models.CASCADE, default=None)

class IvaCompras(models.Model):
    compra = models.ForeignKey(Compra, on_delete=models.PROTECT)
    iva0 = models.DecimalField(max_digits=10, decimal_places=2, default=0)
    iva105 = models.DecimalField(max_digits=10, decimal_places=2, default=0)    
    iva21 = models.DecimalField(max_digits=10, decimal_places=2, default=0)
    
   