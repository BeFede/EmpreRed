from django.db import models


# Modelos para el patrón de dirección
class Paises(models.Model):
    nombre = models.CharField(max_length = 50)

class Provincias(models.Model):
    id_pais = models.ForeignKey(Paises, on_delete=models.PROTECT)
    nombre = models.CharField(max_length = 50)

class Ciudades(models.Model):
    id_provincia = models.ForeignKey(Provincias, on_delete=models.PROTECT)
    nombre = models.CharField(max_length = 50)

class Direcciones(models.Model):
    id_ciudad = models.ForeignKey(Ciudades, on_delete=models.PROTECT, blank = True, null= True)
    codigo_postal = models.IntegerField(blank = True, null= True)
    barrio = models.CharField(max_length = 60, blank = True, null= True)
    calle = models.CharField(max_length = 60, blank = True, null= True)
    numero_calle = models.IntegerField(blank = True, null= True)
    
