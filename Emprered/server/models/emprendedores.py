from django.db import models
from .tipificaciones import TiposDocumento, CondicionesFrenteIVA
from django.contrib.auth.models import User
from .direccion import Direcciones

# Emprendedor
class Emprendedores(models.Model):
    nombre = models.CharField(max_length = 40)
    apellido = models.CharField(max_length = 40, default="")
    id_tipo_documento = models.ForeignKey(TiposDocumento, on_delete=models.PROTECT)
    numero_documento = models.CharField(max_length=15)
    fecha_nacimiento = models.DateField(auto_now = False, auto_now_add=False)
    telefono = models.CharField(max_length = 15, null = True, blank= True)
    id_direccion  = models.ForeignKey(Direcciones, on_delete=models.PROTECT, null=True, blank=True)
    id_condicion_frente_IVA = models.ForeignKey(CondicionesFrenteIVA, on_delete=models.PROTECT)
    id_usuario = models.ForeignKey(User, on_delete=models.PROTECT, null=True,blank=True)

    access_token_mercado_pago = models.CharField(max_length=128, default="")
    ultima_actualizacion_token_mercado_pago = models.CharField(max_length=128, default="")
    refresh_token_mercado_pago = models.CharField(max_length=128, default="")
    redirect_uri_luego_vinculacion = models.CharField(max_length=128, default="")

    def __str__(self):
        return self.nombre

class Marcas(models.Model):
    nombre = models.CharField(max_length=30)
    logo = models.ImageField(upload_to='imagenes/', null=True)
    descripcion = models.TextField(null=True, default="")
    frase = models.CharField(max_length=512, default="")
    emprendedor = models.OneToOneField(Emprendedores, on_delete=models.PROTECT, primary_key=True)
