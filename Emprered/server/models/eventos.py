from django.utils.timezone import now
from django.db import models
from .emprendedores import Emprendedores

class Eventos(models.Model):
    BAJA = 1
    MEDIA = 2
    ALTA = 3
    MUY_ALTA = 4
    PRIORIDAD_CHOICES = (
        (BAJA, 'Baja'),
        (MEDIA, 'Media'),
        (ALTA, 'Alta'),
        (MUY_ALTA, 'Muy alta')
    )

    nombre = models.CharField(max_length=50)
    descripcion = models.TextField(null=True)
    emprendedor = models.ForeignKey(Emprendedores, on_delete=models.DO_NOTHING, default=None)
    prioridad = models.PositiveSmallIntegerField(choices=PRIORIDAD_CHOICES, null=True, blank=True, default=MEDIA)
    activo = models.BooleanField(default=True)


class EventosMeta(models.Model):
    evento = models.ForeignKey(Eventos, on_delete=models.DO_NOTHING, default=None)
    repeat_start = models.DateTimeField()
    repeat_interval = models.PositiveIntegerField(blank=True, null=True)
    repeat_until = models.DateTimeField(blank=True, null=True)
    last_repeated = models.DateTimeField(blank=True, null=True)


class NotificacionesEvento(models.Model):
    # Indica las notificaciones que debe tener un evento (mandar mail y/o notificación)
    evento = models.ForeignKey(Eventos, on_delete=models.DO_NOTHING, default=None)
    enviar_mail = models.BooleanField()
    enviar_notificacion = models.BooleanField()
