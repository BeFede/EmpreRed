from django.db import models
from .emprendedores import Emprendedores
from .tipificaciones import AlicuotasIVA, UnidadesMedida
from .categorias import Categoria, Subcategoria


# Materia prima
class DatosComercialesMateriaPrima(models.Model):
    costo = models.DecimalField(max_digits=8,decimal_places=2, null=True)
    stock_actual = models.DecimalField(max_digits=6,decimal_places=2, null=True)
    stock_minimo = models.DecimalField(max_digits=6,decimal_places=2, null=True)

    def __str__(self):
        return "Costo: " +  str(self.costo) + " | Stock: " +  str(self.stock_actual) + " | Stock minimo: " +  str(self.stock_minimo)

class MateriasPrimas(models.Model):
    id_emprendedor = models.ForeignKey(Emprendedores, on_delete=models.CASCADE)
    nombre = models.CharField(max_length=50)
    id_datos_comerciales = models.ForeignKey(DatosComercialesMateriaPrima, on_delete=models.PROTECT)
    id_alicuota_IVA = models.ForeignKey(AlicuotasIVA, on_delete=models.PROTECT, default=1)
    id_unidad_medida = models.ForeignKey(UnidadesMedida, on_delete=models.PROTECT)
    observaciones = models.TextField(null=True)
    activo = models.BooleanField(default = True)

    def __str__(self):
        return self.nombre
