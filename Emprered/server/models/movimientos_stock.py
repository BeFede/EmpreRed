from django.utils.timezone import now
from django.db import models
from .productos import Productos
from .materias_primas import MateriasPrimas
from .emprendedores import Emprendedores

class MovimientosStock(models.Model):
    INGRESO = 1
    EGRESO = 2
    TIPOS_MOVIMIENTOS = (
        (INGRESO, 'Ingreso'),
        (EGRESO, 'Egreso')        
    )
    fecha_hora = models.DateTimeField(default=now)
    producto = models.ForeignKey(Productos, on_delete=models.DO_NOTHING, null=True, blank=True)
    materia_prima = models.ForeignKey(MateriasPrimas, on_delete=models.DO_NOTHING, null=True, blank=True)
    cantidad = models.DecimalField(max_digits=10,decimal_places=2, default=1)
    tipo_movimiento = models.PositiveSmallIntegerField(choices=TIPOS_MOVIMIENTOS, default=1)
    emprendedor = models.ForeignKey(Emprendedores, on_delete=models.DO_NOTHING, default=None)
    motivo = models.CharField(max_length = 200, default="")
    
    def __str__(self):
        return self.TIPOS_MOVIMIENTOS[self.tipo_movimiento-1][1] + " de " + (self.producto.nombre if self.producto else self.materia_prima.nombre) + " por " + self.motivo



def registrar_movimiento_stock(emprendedor, fecha_hora, producto, materia_prima, cantidad, tipo_movimiento, motivo):
    
    # Efectivizamos los movimientos de stock        
    if producto:
        if tipo_movimiento == MovimientosStock.INGRESO:
            # Ingreso de producto
            producto.id_datos_comerciales.stock_actual += cantidad
        elif tipo_movimiento == MovimientosStock.EGRESO:
            # Egreso de producto
            # Validamos que no se quiera registrar un egreso mayor al nivel de stock
            if ((producto.id_datos_comerciales.stock_actual - cantidad) >= 0):
                producto.id_datos_comerciales.stock_actual -= cantidad
            else:
                return False, "El stock no puede quedar en negativo"    
        else:
            return False, "No se especifica el tipo de movimiento"

        producto.id_datos_comerciales.save()
        producto.save()

    elif materia_prima:
        if tipo_movimiento == MovimientosStock.INGRESO:
            #Ingreso de materia prima
            materia_prima.id_datos_comerciales.stock_actual += cantidad
        elif tipo_movimiento == MovimientosStock.EGRESO:
            # Egreso de materia prima
            # Validamos que no se quiera registrar un egreso mayor al nivel de stock
            if ((materia_prima.id_datos_comerciales.stock_actual - cantidad) >= 0):
                materia_prima.id_datos_comerciales.stock_actual -= cantidad
            else:
                return False, "El stock no puede quedar en negativo"    
            
        else:
            return False, MovimientosStock.INGRESO

        materia_prima.id_datos_comerciales.save()
        materia_prima.save()

    else:
        return False, "Faltan parámetros obligatorios"

    # Registramos el movimiento de stock

    movimiento_stock = MovimientosStock()
    movimiento_stock.fecha_hora = fecha_hora
    movimiento_stock.producto = producto
    movimiento_stock.materia_prima = materia_prima
    movimiento_stock.cantidad = cantidad    
    movimiento_stock.tipo_movimiento = tipo_movimiento
    movimiento_stock.emprendedor = emprendedor
    movimiento_stock.motivo = motivo
    movimiento_stock.save()
    
    return True, "El movimiento se ha registrado exitosamente"

