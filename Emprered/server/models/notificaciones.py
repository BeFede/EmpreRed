from django.utils.timezone import now
from django.db import models
from .emprendedores import Emprendedores
from .eventos import Eventos

class Notificaciones(models.Model):
    # Indica las notificaciones generadas para cada emprendedor
    titulo = models.CharField(max_length=50)
    fecha_hora_creacion = models.DateTimeField(default=now)
    fecha_hora_vista = models.DateTimeField(blank=True, null=True, default=None)
    emprendedor = models.ForeignKey(Emprendedores, on_delete=models.DO_NOTHING, default=None)
    # y para que sepamos a dónde ir cuando se hace click sobre la notificacion
    evento = models.ForeignKey(Eventos, on_delete=models.DO_NOTHING, default=None)
