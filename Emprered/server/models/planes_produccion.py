from django.db import models
from .productos import Productos
from .materias_primas import MateriasPrimas

class PlanProduccion(models.Model):
    producto = models.ForeignKey(Productos, on_delete=models.CASCADE)
    costo_mano_obra = models.DecimalField(max_digits=10,decimal_places=2, null=True)
    descripcion = models.TextField(null=True)

class DetallePlanProduccion(models.Model):
    plan_produccion = models.ForeignKey(PlanProduccion, on_delete=models.CASCADE)
    materia_prima = models.ForeignKey(MateriasPrimas, on_delete=models.CASCADE)
    costo_merma = models.DecimalField(max_digits=10, decimal_places=2, default=0, null=True)
    cantidad = models.DecimalField(max_digits=10, decimal_places=2)
