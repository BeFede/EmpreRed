from django.db import models
from .productos import Productos
from django.utils.timezone import now
from .emprendedores import Emprendedores
from .planes_produccion import PlanProduccion, DetallePlanProduccion
from .materias_primas import MateriasPrimas
from taggit.managers import TaggableManager


class Produccion(models.Model):
    PENDIENTE = 1
    EN_PROCESO = 2
    TERMINADA = 3
    ANULADA = 4
    STATUS_CHOICES = (
        (PENDIENTE, 'Pendiente'),
        (EN_PROCESO, 'En Proceso'),
        (TERMINADA, 'Terminada'),
        (ANULADA, 'Anulada')

    )
    emprendedor = models.ForeignKey(Emprendedores, on_delete=models.DO_NOTHING, default=None)
    fecha_hora = models.DateTimeField(default=now)
    descripcion = models.TextField(null=True)
    estado = models.PositiveSmallIntegerField(choices=STATUS_CHOICES, null=True, blank=True, default=PENDIENTE)

    # Taggit
    hashtags = TaggableManager()

    def validar_cambio_estado(self, nuevo_estado):

        # Si el nuevo estado es igual al actual no seteamos nada
        if (self.estado == nuevo_estado):
            return True


        # Producción nueva
        if not self.estado:
            return (nuevo_estado == self.PENDIENTE)

        # En estado pendiente se puede pasar al estado en proceso o anulada
        if (self.estado == self.PENDIENTE):
            return (nuevo_estado == self.EN_PROCESO or nuevo_estado == self.ANULADA)

        # Estando en proceso, se puede pasar a estado terminada o anulada
        if (self.estado == self.EN_PROCESO):
            return (nuevo_estado == self.TERMINADA or nuevo_estado == self.ANULADA)

        # Estando terminada o anulada no se puede pasar a otro estado
        if (self.estado == self.TERMINADA or self.estado == self.ANULADA):
            raise CambioDeEstadosException("No se pueden editar las producciones anuladas o terminadas")

        raise CambioDeEstadosException("El cambio de estado no es válido")


    def set_estado(self, nuevo_estado):

        mensaje_exito = "El cambio de estados se realizó con éxito"
        mensaje_error = "El cambio de estado no es válido"

        # Si el nuevo estado es igual al actual no seteamos nada
        if (self.estado == nuevo_estado):
            return True, mensaje_exito

        # Producción nueva
        if not self.estado:
            if (nuevo_estado == self.PENDIENTE):
                self.estado = self.PENDIENTE
                return True, mensaje_exito

        # En estado pendiente se puede pasar al estado en proceso o anulada
        if (self.estado == self.PENDIENTE):
            if (nuevo_estado == self.EN_PROCESO or nuevo_estado == self.ANULADA):
                self.estado = nuevo_estado
                return True, mensaje_exito

        # Estando en proceso, se puede pasar a estado terminada o anulada
        if (self.estado == self.EN_PROCESO):
            if (nuevo_estado == self.TERMINADA or nuevo_estado == self.ANULADA):
                self.estado = nuevo_estado
                return True, mensaje_exito


        # Si ninguna operación se realizó, entonces el cambio de estados no es válido
        return False, mensaje_error


    def actualizar_detalles(self, detalles):
        # Si la producción pasa a pendiente o pasa a en proceso podemos modificar los detalles de la producción

        if (self.estado == self.PENDIENTE or self.estado == self.EN_PROCESO):
            # Borramos los detalles actuales de la producción
            DetalleProduccion.objects.filter(produccion=self).delete()
            for detalle in detalles:
                id_producto = detalle.get('producto_id', None)
                cantidad = detalle.get('cantidad', None)

                if (id_producto and cantidad):
                    detalle_produccion = DetalleProduccion()
                    try:
                        # Busco el producto con el id que nos pasan y valido de que pertenezca
                        # al emprendedor registrado
                        producto = Productos.objects.filter(id=id_producto).filter(id_emprendedor=self.emprendedor)[0]
                        if not producto:
                            return False, 'Error. El producto {0} no existe'.format(id_producto)

                        # También valido que sea un producto que puede ser fabricado
                        if not producto.es_fabricado:
                            return False, 'Uno o más productos están catalogados como no fabricables'

                        detalle_produccion.produccion = self
                        detalle_produccion.producto = producto
                        detalle_produccion.cantidad = cantidad
                        detalle_produccion.save()


                    except IndexError as ie:
                        return False, 'No posee producto con id {}'.format(id_producto)

                    except Exception as e:
                        return False, str(e)

                else:
                    return False, "Faltan parámetros obligatorios"

            return True, "Éxito"

        else:
            return False, "No puede realizar esta operación si no se trata de una producción pendiente o en proceso"

    def resumir_produccion(self):
        detalles_produccion = DetalleProduccion.objects.filter(produccion=self)

        produccion_list = []
        materias_primas_list = []
        detalle_produccion_list = []
        materias_primas_necesarias = {}

        for detalle_produccion in detalles_produccion:

            materias_primas = []
            materias_primas_list = []
            plan_produccion = PlanProduccion.objects.get(producto=detalle_produccion.producto)
            detalles_plan_produccion = DetallePlanProduccion.objects.filter(plan_produccion=plan_produccion)


            for detalle_plan_produccion in detalles_plan_produccion:
                materia_prima = []
                materia_prima = {
                    "id_materia_prima": detalle_plan_produccion.materia_prima.id,
                    "materia_prima": detalle_plan_produccion.materia_prima.nombre,
                    "cantidad_receta": detalle_plan_produccion.cantidad,
                    "cantidad_total": detalle_plan_produccion.cantidad * detalle_produccion.cantidad,
                }
                materias_primas_list.append(materia_prima)

                # Si no existe agregamos la materia prima al diccionario
                if not detalle_plan_produccion.materia_prima.id in materias_primas_necesarias:
                    materias_primas_necesarias[detalle_plan_produccion.materia_prima.id] = {
                        "id": detalle_plan_produccion.materia_prima.id,
                        "nombre":  detalle_plan_produccion.materia_prima.nombre,
                        "cantidad" : materia_prima['cantidad_total']
                    }

                # Si existía la mat prima en el dict, entoncs le sumamos la cantidad
                else:
                    materias_primas_necesarias[detalle_plan_produccion.materia_prima.id] = {
                        "id": detalle_plan_produccion.materia_prima.id,
                        "nombre":  detalle_plan_produccion.materia_prima.nombre,
                        "cantidad": materias_primas_necesarias[detalle_plan_produccion.materia_prima.id]['cantidad'] + materia_prima['cantidad_total']
                    }



            producto = {
                "producto": detalle_produccion.producto.nombre,
                "cantidad": detalle_produccion.cantidad,
                "materias_primas": materias_primas_list,
            }
            detalle_produccion_list.append(producto)



        # Hago esto para recorrerlo facilmente desde el html del reporte porque la librería es una pija y bastante limitada
        materias_primas_necesarias_list = []
        for materia_prima in materias_primas_necesarias:
            materias_primas_necesarias_list.append(materias_primas_necesarias[materia_prima])


        produccion_list = {
            "id": self.id,
            "detalles_produccion": detalle_produccion_list,
            "materias_primas": materias_primas_necesarias_list
        }

        return produccion_list

    # Verificamos si se posee la materia prima necesaria para realizar una producción
    def validar_disponibilidad_materia_prima(self):
        produccion = self.resumir_produccion()

        materias_primas_necesarias = produccion['materias_primas']
        materias_primas_faltantes = []

        for materia_prima in materias_primas_necesarias:
            cantidad_necesaria = materia_prima['cantidad']
            cantidad_disponible = MateriasPrimas.objects.get(id = materia_prima['id']).id_datos_comerciales.stock_actual
            if (cantidad_necesaria > cantidad_disponible):
                materias_primas_faltantes.append(materia_prima['nombre'])

        estado = True
        mensaje = "Éxito"

        if materias_primas_faltantes != []:
            estado = False
            mensaje = "No posee suficiente stock para llevar a cabo la producción. \n Materias primas faltantes: {0}".format(', '.join(materias_primas_faltantes))


        return estado, mensaje


class DetalleProduccion(models.Model):
    produccion = models.ForeignKey(Produccion, on_delete=models.CASCADE)
    producto = models.ForeignKey(Productos, on_delete=models.CASCADE)
    cantidad = models.DecimalField(max_digits=10, decimal_places=2)


class CambioDeEstadosException(Exception):
    pass
