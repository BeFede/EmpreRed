from django.db import models
from .emprendedores import Emprendedores
from .tipificaciones import AlicuotasIVA, UnidadesMedida
from .categorias import Subcategoria

# Producto
class DatosComercialesProducto(models.Model):
    costo = models.DecimalField(max_digits=10,decimal_places=2)
    precio = models.DecimalField(max_digits=10,decimal_places=2)
    stock_actual = models.DecimalField(max_digits=10,decimal_places=2)
    stock_minimo = models.DecimalField(max_digits=10,decimal_places=2)
    utilidad = models.DecimalField(max_digits=5,decimal_places=2)

class Productos(models.Model):
    id_emprendedor = models.ForeignKey(Emprendedores, on_delete=models.CASCADE)
    nombre = models.CharField(max_length=50)
    foto = models.ImageField(upload_to='imagenes/', null=True)
    id_datos_comerciales = models.ForeignKey(DatosComercialesProducto, on_delete=models.PROTECT)
    id_alicuota_IVA = models.ForeignKey(AlicuotasIVA, on_delete=models.PROTECT, default=1)
    id_unidad_medida = models.ForeignKey(UnidadesMedida, on_delete=models.PROTECT, null=True)
    cantidad_mayorista = models.DecimalField(max_digits=8,decimal_places=2, null=True)
    es_fabricado = models.BooleanField(default=False)
    observaciones = models.TextField(null=True)
    activo = models.BooleanField(default = True)
    subcategoria = models.ForeignKey(Subcategoria, null=True, on_delete=models.PROTECT)


    def __str__(self):
        return self.nombre
