from django.db import models
from .tipificaciones import TiposDocumento
from .emprendedores import Emprendedores
from django.contrib.auth.models import User


# proveedor
class Proveedores(models.Model):
    nombre = models.CharField(max_length = 40)
    apellido = models.CharField(max_length = 40, default="")
    id_tipo_documento = models.ForeignKey(TiposDocumento, on_delete=models.PROTECT)
    numero_documento = models.CharField(max_length=15)

    telefono = models.CharField(max_length = 15, null = True, blank= True)
    direccion  = models.CharField(max_length=64)
    id_usuario = models.ForeignKey(User, on_delete=models.PROTECT, null=True,blank=True)
    emprendedor = models.ForeignKey(Emprendedores, on_delete=models.CASCADE)

    def __str__(self):
        return self.nombre

