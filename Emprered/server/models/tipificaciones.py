from django.db import models


# Modelos de tipificación
class UnidadesMedida(models.Model):
    descripcion = models.CharField(max_length = 30)
    def __str__(self):
        return self.descripcion

class CondicionesFrenteIVA(models.Model):
    descripcion = models.CharField(max_length = 30)
    def __str__(self):
        return self.descripcion

# Le agrego id para respetar el mismo que utiliza AFIP
class TiposDocumento(models.Model):
    id = models.IntegerField(primary_key = True)
    descripcion = models.CharField(max_length = 30)
    def __str__(self):
        return self.descripcion

class AlicuotasIVA(models.Model):
    porcentaje_IVA = models.CharField(max_length = 30)
    def __str__(self):
        return self.porcentaje_IVA
