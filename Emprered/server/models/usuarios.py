
from django.db import models
from django.contrib.auth.models import User, Group
import datetime
from django.utils.timezone import now



class Perfil(models.Model):
    EMPRENDEDOR = 1
    CLIENTE = 2
    ANONIMO = 3
    ROLE_CHOICES = (
        (EMPRENDEDOR, 'Emprendedor'),
        (CLIENTE, 'Cliente'),
        (ANONIMO, 'Anónimo'),
    )
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    foto = models.ImageField(upload_to='imagenes/', null=True)
    rol = models.PositiveSmallIntegerField(choices=ROLE_CHOICES, null=True, blank=True)
    es_activo = models.BooleanField(default=False)
    tipo_registro = models.CharField(max_length = 30, default = "emprered")

    es_email_confirmado = models.BooleanField(default=False)
    clave_activacion = models.CharField(max_length=512, default="")
    expiracion_clave = models.DateTimeField(default=now)

    clave_recuperacion = models.CharField(max_length=512, default="")
    expiracion_clave_recuperacion = models.DateTimeField(default=now)

    def __str__(self):  # __unicode__ for Python 2
        return self.user.username
