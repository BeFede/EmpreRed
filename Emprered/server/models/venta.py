from django.utils.timezone import now
from django.db import models

from Emprered.server.models.catalogo import DetalleCatalogo
from Emprered.server.models.emprendedores import Emprendedores
from Emprered.server.models.productos import Productos
from Emprered.server.models.usuarios import Perfil



class Venta(models.Model):
    PENDIENTE_DE_PAGO = 1
    CONFIRMADA = 2
    CANCELADA = 3

    ESTADOS = (
        (PENDIENTE_DE_PAGO, 'Pendiente de pago'),
        (CONFIRMADA, 'Confirmada'),
        (CANCELADA, 'Cancelada')
    )

    fecha_hora_registro = models.DateTimeField(default=now)
    emprendedor = models.ForeignKey(Emprendedores, on_delete=models.CASCADE)
    comprador = models.ForeignKey(Perfil, on_delete=models.DO_NOTHING)
    estado = models.PositiveSmallIntegerField(choices=ESTADOS, default=PENDIENTE_DE_PAGO)
    monto = models.DecimalField(max_digits=10, decimal_places=2, default = 0.0)

    def get_estado(self):
        return self.ESTADOS[self.estado-1][1]


class Tarjeta(models.Model):
    tipo = models.CharField(max_length=32, default="")
    ultimos_cuatro_digitos = models.CharField(max_length=5, default="")
    dni_titular = models.CharField(max_length=12, default="")
    nombre_titular = models.CharField(max_length=32, default="")

class Pago(models.Model):
    NO_REALIZADO = 1
    REALIZADO = 2

    ESTADOS = (
        (NO_REALIZADO, 'No realizado'),
        (REALIZADO, 'Realizado'),
    )

    venta = models.ForeignKey(Venta, on_delete=models.CASCADE)
    id_preferencia = models.CharField(max_length=64)

    monto = models.DecimalField(max_digits=10, decimal_places=2)
    estado = models.PositiveSmallIntegerField(choices=ESTADOS, default=NO_REALIZADO)
    url_exito = models.CharField(max_length=256, default="")
    url_error = models.CharField(max_length=256, default="")
    pago_mobile = models.PositiveSmallIntegerField(default = 0)

    id_collection = models.CharField(max_length=64, default="")
    id_orden_mercado_pago = models.CharField(max_length=64, default="")
    estado_collection = models.CharField(max_length=16, default="")
    metodo_pago = models.CharField(max_length=16, default="")

    tarjeta = models.ForeignKey(Tarjeta, on_delete=models.DO_NOTHING, default="", blank=True, null=True)

    def get_estado(self):
        return self.ESTADOS[self.estado - 1][1]



class DetallesVenta(models.Model):
    venta = models.ForeignKey(Venta, on_delete=models.CASCADE)
    producto = models.ForeignKey(Productos, on_delete=models.PROTECT)
    precio_individual = models.DecimalField(max_digits=10, decimal_places=2)
    cantidad = models.DecimalField(max_digits=10, decimal_places=2)
    id_publicacion = models.ForeignKey(DetalleCatalogo, on_delete=models.PROTECT, default=None, null=True)