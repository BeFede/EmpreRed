import datetime
from haystack import indexes
from celery_haystack.indexes import CelerySearchIndex
from Emprered.server.models.catalogo import DetalleCatalogo
from Emprered.server.models.emprendedores import Emprendedores, Marcas

class DetalleCatalogoIndex(CelerySearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    # author = indexes.CharField(model_attr='user')
    # pub_date = indexes.DateTimeField(model_attr='pub_date')

    def get_model(self):
        return DetalleCatalogo

    def index_queryset(self, using=None):
        """Used when the entire index for model is updated."""
        return self.get_model().objects.all() # filter(pub_date__lte=datetime.datetime.now())


class EmprendedoresIndex(CelerySearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    # author = indexes.CharField(model_attr='user')
    # pub_date = indexes.DateTimeField(model_attr='pub_date')

    def get_model(self):
        return Emprendedores

    def index_queryset(self, using=None):
        """Used when the entire index for model is updated."""
        return self.get_model().objects.all() # filter(pub_date__lte=datetime.datetime.now())


class MarcasIndex(CelerySearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    # author = indexes.CharField(model_attr='user')
    # pub_date = indexes.DateTimeField(model_attr='pub_date')

    def get_model(self):
        return Marcas

    def index_queryset(self, using=None):
        """Used when the entire index for model is updated."""
        return self.get_model().objects.all() # filter(pub_date__lte=datetime.datetime.now())
