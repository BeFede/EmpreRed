from django.core.mail import send_mail
from django.core.mail import EmailMessage



class ServicioEmail:
    
    @staticmethod
    def enviar_email(asunto, cuerpo, destinatarios, files = []):
        if asunto and cuerpo and destinatarios:
            email = EmailMessage(asunto, cuerpo, 'empreredproject@gmail.com', destinatarios)

            email.content_subtype = "html"
            if files:
                for file in files:                                      
                    email.attach(file['filename'], file['file'].getvalue(), 'application/pdf')
            email.send()
