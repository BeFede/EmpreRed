import requests

from Emprered.settings import APP_ID_MERCADO_PAGO, SECRET_KEY_MERCADO_PAGO, DEBUG, WEB_PATH, ACCESS_TOKEN_MERCADO_PAGO, ACCESS_TOKEN_MERCADO_PAGO_TESTING
import mercadopago


class ServicioMercadoPago:

    def generar_link_pago(self, access_token, detalle_catalogo, cantidad, venta):

        if (access_token == ""):
            if (DEBUG):
                mp = mercadopago.MP(ACCESS_TOKEN_MERCADO_PAGO_TESTING)
            else:
                mp = mercadopago.MP(ACCESS_TOKEN_MERCADO_PAGO)

        else:
            mp = mercadopago.MP(access_token)

        url_exito = WEB_PATH + 'mercado_pago/exito'
        url_error = WEB_PATH + 'mercado_pago/error'
        precio = float(detalle_catalogo.precio)

        preference = {
            "items": [
                {
                    "title": detalle_catalogo.titulo,
                    "quantity": cantidad,
                    "currency_id": "ARS",
                    "unit_price": precio
                }
            ],
            "back_urls": {
                "success": url_exito,
                "failure": url_error
            },
            "auto_return": "approved",
            "external_reference": venta.id,
            "payer": {
                "email": venta.comprador.user.email
             }
        }



        preferenceResult = mp.create_preference(preference)

        #print(preferenceResult)

        if (preferenceResult['status'] == 200 or preferenceResult['status'] == 201):
            return preferenceResult['status'], preferenceResult['response']
        else:

            return preferenceResult['status'], preferenceResult['response']['message']



    def consultar_transaccion(self, venta):


        access_token = venta.emprendedor.access_token_mercado_pago

        if (access_token == ""):
            if (DEBUG):
                access_token = ACCESS_TOKEN_MERCADO_PAGO_TESTING
            else:
                access_token = ACCESS_TOKEN_MERCADO_PAGO



        url = "https://api.mercadopago.com/v1/payments/search?access_token={0}&external_reference={1}".format(access_token, venta.id)
        response = requests.get(url)

        searchResult = response.json()



        pago_dic = {
            "monto_final": searchResult['results'][0]['transaction_details']['total_paid_amount'],
            "tarjeta": {
                "tipo": searchResult['results'][0]['payment_method_id'],
                "ultimos_cuatro_digitos": searchResult['results'][0]['card']['last_four_digits'],
                "titular": {
                    "nombre": searchResult['results'][0]['card']['cardholder']['name'],
                    "dni": searchResult['results'][0]['card']['cardholder']['identification']['number'],
                }
            }
        }

        return pago_dic



