from io import BytesIO, StringIO

from django.http import HttpResponse
from django.template.loader import get_template
import xhtml2pdf.pisa as pisa


class ServicioReportes:

    @staticmethod
    def reporte_pdf(path: str, params: dict, filename = "file_emprered.pdf"):
        template = get_template(path)
        html = template.render(params)
        response = BytesIO()
        pdf = pisa.pisaDocument(BytesIO(html.encode("UTF-8")), response)
        if not pdf.err:
            response = HttpResponse(response.getvalue(),
                                 content_type='application/pdf')
            response['Content-Disposition'] = 'attachment; filename=reporte.pdf' 

            return response
            #return HttpResponse(response.getvalue(), , filename=name)
        else:
            return HttpResponse("Error Rendering PDF", status=400)



    @staticmethod
    def reporte_pdf_file(path: str, params: dict, filename = "file_emprered.pdf"):
        template = get_template(path)
        html = template.render(params)
        result = BytesIO()
        pdf = pisa.pisaDocument(BytesIO(html.encode("UTF-8")), result)
        if not pdf.err:
            response = HttpResponse(result.getvalue(),
                                 content_type='application/pdf')
            response['Content-Disposition'] = 'attachment; filename=reporte.pdf' 

            return response, result
            #return HttpResponse(response.getvalue(), , filename=name)
        else:
            return HttpResponse("Error Rendering PDF", status=400), None


