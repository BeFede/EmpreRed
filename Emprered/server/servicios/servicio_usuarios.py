from django.contrib.auth.models import User, Group
from django.db import transaction
from django.contrib.auth import authenticate
from Emprered.server.models.emprendedores import Emprendedores
from Emprered.server.models.clientes import Clientes

from re import sub
from rest_framework_jwt.settings import api_settings
from rest_framework.authentication import get_authorization_header
from rest_framework_jwt.authentication import BaseJSONWebTokenAuthentication
from rest_framework_jwt.utils import jwt_decode_handler
from django.core.exceptions import ValidationError
from django.core.validators import EmailValidator
from Emprered.utils import RespuestaHTTP, validar_no_vacio, get_datos_token_google
from Emprered.server.servicios.servicio_email import ServicioEmail

from Emprered.settings import WEB_PATH


class ServicioUsuarios:

    def login(self, nombre_usuario, clave):
        token = None
        usuario = None
        # La sesión se puede iniciar con username o email
        try:

            usuario = User.objects.all().filter(username=nombre_usuario)
            if (len(usuario) == 0):
                usuario = User.objects.all().filter(email=nombre_usuario)
                usuario = usuario[0]
            else:
                usuario = usuario[0]

        except:
            usuario = None

        if usuario:

            if usuario.check_password(clave):
                jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
                jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER
                payload = jwt_payload_handler(usuario)
                token = jwt_encode_handler(payload)
            else:
                usuario = None


        return usuario, token


    def login_google(self, request):
        usuario = None
        token_google = request.META.get('HTTP_AUTHORIZATION', None)[7:]

        user = get_datos_token_google(token_google)

        try:
            usuario = User.objects.all().get(email=user.get('email'))
            return self.actualizar_token(usuario)
        except:
            pass

        return None, None


    def actualizar_token(self, usuario):
        jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
        jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER
        payload = jwt_payload_handler(usuario)
        token = jwt_encode_handler(payload)
        return usuario, token


    def get_rol_by_username(self, username):
        try:
            Emprendedores.objects.get(id_usuario = User.objects.get(username = username))
            return "emprendedor"
        except Exception as e:
            pass
        try:
            Clientes.objects.get(id_usuario = User.objects.get(username = username))
            return "cliente"
        except Exception as e:
           pass

        return "anonimo"


    def get_rol(self, usuario):

        try:
            emprendedor = Emprendedores.objects.get(id_usuario = User.objects.get(id = usuario.id))
            return emprendedor, "emprendedor"
        except Exception as e:
            pass
        try:
            u = User.objects.get(id = usuario.id)

            cliente = Clientes.objects.get(id_usuario = u.id)
            return cliente, "cliente"
        except Exception as e:
            pass

        return None, "anonimo"


    def get_usuario_jwt(self, request):
        usuario = request.user  # Usuario anónimo.

        jwt = request.META.get('HTTP_AUTHORIZATION', None)

        if jwt is None:
            cabecera_auth = request.META.get('HTTP_AUTHORIZATION', None)
            if cabecera_auth is not None:
                try:
                    jwt = sub('JWT ', '', cabecera_auth)
                except Exception as e:
                    print(e)
        else:
            try:
                jwt = jwt[7:]
                payload = jwt_decode_handler(jwt)
                auth = BaseJSONWebTokenAuthentication()
                usuario = auth.authenticate_credentials(payload)

            except Exception as e:
                print(e)
        return usuario


    def validar_existencia_username(self, username):
        try:
            User.objects.all().get(username=username)
            return True
        except Exception as e:
            return False



    def validar_formato_mail(self, email):
        validador = EmailValidator()
        try:
            validador(email)
            return True
        except ValidationError:
            return False


    def validar_existencia_mail(self, email):
        try:
            User.objects.all().get(email=email)
            return True
        except Exception as e:
            return False


    def validar_clave(self, clave, clave_repetida):

        if (clave != clave_repetida):
            return False, "Las contraseñas no coinciden"
        else:
            if len(clave) < 8:
                return False, "La contraseña es muy débil"

        return True, "Ok"


    def validar_username_except(self, nuevo_username, actual_username):
        if nuevo_username == actual_username:
            return False
        return self.validar_existencia_username(nuevo_username)


    def get_usuario_to_dic(self, perfil):
        rol = "Anonimo"
        if perfil.rol == perfil.EMPRENDEDOR:
            rol = "Emprendedor"
        elif perfil.rol == perfil.CLIENTE:
            rol = "Cliente"



        usuario_dic = {
            "username" : perfil.user.username,
            "email": perfil.user.email,
            "es_activo": perfil.es_activo,
            "rol": rol,
            "foto": perfil.foto.url if perfil.foto else "",
            "tipo_registro": perfil.tipo_registro,
            "confirmo_email": perfil.es_email_confirmado
        }


        if (rol == 'Emprendedor'):
            emprendedor = Emprendedores.objects.get(id_usuario = perfil.user)
            es_autorizado_mercado_pago = not (emprendedor.access_token_mercado_pago == "")
            usuario_dic['es_autorizado_mercado_pago'] = es_autorizado_mercado_pago
        return usuario_dic

    def enviar_confirmacion_email(self, perfil):
        servicio_email = ServicioEmail
        url = WEB_PATH + "confirmar_email/" + perfil.user.username + "/" + perfil.clave_activacion
        email_body = '''
        <html>
            <body>
                <h2>Gracias por registrarse en Emprered</h2>
                
                <h3>Una nueva forma de emprender</h3><p></p>
                <p>Para confirmar la cuenta y poder finalizar el proceso de registro, por favor, presione el siguiente enlace en menos de 48 horas</p>
                <a href="''' + url + '''">¡Click aquí!</a>
                
            </body>
        </html>
        '''


        servicio_email.enviar_email("¡¡Bienvenidos a Emprered!!", email_body, [perfil.user.email], None)

    def enviar_bienvenida_email(self, perfil):
        servicio_email = ServicioEmail
        url = WEB_PATH + "confirmar_email/" + perfil.user.username + "/" + perfil.clave_activacion
        email_body = '''
        <html>
            <body>
                <h2>Gracias por registrarse en Emprered</h2>

                <h3>Una nueva forma de emprender</h3><p></p>
                <p>Desde Emprered esperamos que tenga una muy buena experiencia y aproveche todas las herramientas brindadas por la aplicación</p>
                

            </body>
        </html>
        '''

        servicio_email.enviar_email("¡¡Bienvenidos a Emprered!!", email_body, [perfil.user.email], None)

    def enviar_email_recuperacion_clave(self, perfil):
        servicio_email = ServicioEmail
        url = WEB_PATH + "recuperar_clave/" + perfil.user.username + "/" + perfil.clave_recuperacion
        email_body = '''
        <html>
            <body>
                <h2>Recuperar cuenta</h2>

                
                <p>Para recuperar la cuenta y cambiar la clave, presione el siguiente enlace en menos de 48 horas</p>
                <a href="''' + url + '''">¡Click aquí!</a>

            </body>
        </html>
        '''

        servicio_email.enviar_email("¡¡Emprered - Olvido de clave!!", email_body, [perfil.user.email], None)
