# Módulo de búsquedas: Haystack + Whoosh

El módulo de búsquedas está compuesto por [Haystack], que nos una API unificada y muy similar a las views de Django, a la cual podemos «enchufar» backends de búsqueda como Solr, Elasticsearch, Xapian, etc.
En nuestro caso usamos [Whoosh], que es pequeño, liviano, y más que suficiente para nuestros propósitos.

> Aquí supondremos que ya están instalados django-haystack y whoosh (ambos disponibles por pip) y que ambos están configurados adecuadamente en settings.py (véase [settings])

## Creando nuestra búsqueda
Primero elegimos uno de los modelos de la base de datos que querramos indexar para luego poder buscarlo en nuestro sitio. Para este caso de ejemplo vamos a utilizar el modelo de `DetalleCatalogo`.

Nota: las líneas comentadas se refieren al uso de autor y fecha de publicación de los catálogos, que de momento no estamos utilizando, pero podrían servir para no mostrarle a los usuarios catálogos que no estén publicados aún.

### Creando el indexador de la búsqueda
```python
# File: Emprered/server/search_indexes.py

# import datetime
from haystack import indexes
from Emprered.server.models.catalogo import DetalleCatalogo

class DetalleCatalogoIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True) # por defecto se usa este nombre, y debe haber *uno* de estos CharField declarado como documento.
    # author = indexes.CharField(model_attr='user')
    # pub_date = indexes.DateTimeField(model_attr='pub_date')

    def get_model(self):
        return DetalleCatalogo

    def index_queryset(self, using=None):
        """Used when the entire index for model is updated."""
        return self.get_model().objects.all() # filter(pub_date__lte=datetime.datetime.now())
```

#### Generando el template
Una vez que ya tengamos este archivo (al que obviamente podemos agregarle más modelos en nuevas clases) procedemos a generar el template que definimos en `use_template=True`. El template es en realidad lo que se indexa (por lo tanto, nuestro motor de búsqueda buscará aquí) y contiene la información sobre el objeto que nosotros consideremos relevante.

Esto lo vamos a hacer en el archivo `Emprered/server/templates/search/indexes/server/detallecatalogo_text.txt`. Es importante que, si mantemos la configuración default, los índices se ubique ahí, y que tengan un nombre como `<model_name>_text.txt`.

```python
{{ object.titulo }}
{{ object.estado }}
{{ object.descripcion }}
```

### Exponiendo la view al resto del mundo

Una vez que tenemos todo esto, es inútil a menos que expongamos una API que permita que el resto de nuestra aplicación pueda utilizar la búsqueda. Lo que vamos a hacer para esto es exponer una view como cualquier otra de Django.

> Esta es una muestra simplificada y no functional de la view. El ejemplo completo se puede encontrar en: [search_query_view]

```python
# File: Emprered/server/views/search_query_view.py

# Lots of common django imports here...
from Emprered.server.servicios.servicio_usuarios import ServicioUsuarios
from haystack.query import SearchQuerySet

# Hay un montón de cosas, lo crucial es esto:
def get(self, request, *args, **kwargs):
    query = kwargs.get('query')
    raw_results = SearchQuerySet().models(*model).filter(content=query)

    resultados = []

    for r in raw_results:
        obj = r.object
        if isinstance(obj, DetalleCatalogo):
            res = {
                'titulo': obj.titulo,
                'descripcion': obj.descripcion,
                #  Todos los datos que querramos traer de DetalleCatalogo, aquí ya trabajamos con el modelo
                }
    resultados.append(res)

    # Devolver resultados a quien pidió la búsqueda
```

## Próximos pasos
En [search_query_view] se puede ver cómo realizar búsquedas sobre distintos modelos según lo que el cliente nos pida. De este modo podemos agregar más modelos a buscar, y podemos buscarlos por separado (o en conjunto) pero con la ventaja de estar utilizando la misma view.
Además, se guardan los datos de búsqueda para luego poder observarlos y estudiar la factibilidad de obtención de analíticas.


## Importante
No olvides exponer las url en tu urls.py para que tu API pueda ser accedida desde el exterior, por ejemplo:
```python
urlpatterns = [
# ...
url(r'^buscar/(?P<model>productos)?/?(?P<query>[\w+\s+]+)?/?$', SearchQueryView.as_view()),
## Cuando tenemos varios modelos y además ofrecemos la posibilidad de darle más prioridad a una palabra (boost), la url puede ser algo más compleja:
url(r'^buscar/(?P<model>productos|emprendedores|marcas)?/?(?P<query>[\w+\s+]+)?/?(?P<boost>[\w+\s+]+)?/?$', SearchQueryView.as_view()),
# ...
]
```

[//]:# (Links. This won't be seen after it's interpreted.)
[Haystack]: <https://haystacksearch.org/>
[Whoosh]: <https://whoosh.readthedocs.io/en/latest/index.html>
[settings]: <https://gitlab.com/BeFede/EmpreRed/blob/tienda-categorias/Emprered/settings.py>
[search_query_view]: <https://gitlab.com/BeFede/EmpreRed/blob/tienda-categorias/Emprered/server/views/search_query_view.py>