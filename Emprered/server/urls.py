from django.conf.urls import url
from django.urls import include, path
from rest_framework_jwt.views import obtain_jwt_token, refresh_jwt_token, verify_jwt_token
from django.views.static import serve

from django.conf import settings

from Emprered.server.views.autorizacion_vendedor_mercado_pago_view import AutorizacionEmprendedorMercadoPagoView
from Emprered.server.views.emprendedor_view import EmprendedorView
from Emprered.server.views.cliente_view import ClienteView
from Emprered.server.views.mercado_pago_view import MercadoPagoView
from Emprered.server.views.tienda_view import TiendaView
from Emprered.server.views.tipificacion_view import TiposDocumentoView, AlicuotasIvaView, CondicionesFrenteIVAView, UnidadesMedidaView, EtiquetasView
from Emprered.server.views.materias_primas_view import MateriasPrimasView
from Emprered.server.views.productos_view import ProductosView
from Emprered.server.views.plan_produccion_view import PlanProduccionView
from Emprered.server.models.planes_produccion import PlanProduccion, DetallePlanProduccion
from Emprered.server.views.login_view import LoginView
from Emprered.server.views.registro_view import RegistroView, ConfirmarEmailView, RenovarConfirmacionEmailView, OlvidoClaveView, RecuperarClaveView
from Emprered.server.views.editar_perfil_view import EditarPerfilView
from Emprered.server.views.compras_view import ComprasView
from Emprered.server.views.proveedores_view import ProveedoresView
from Emprered.server.views.producciones_view import ProduccionesView
from Emprered.server.views.marcas_view import MarcasView
from Emprered.server.views.movimientos_stock_view import MovimientosStockView
from Emprered.server.views.reportes_movimientos_stock_view import ReportesMovimientosStock
from Emprered.server.views.reportes_produccion_view import ReportesProduccion
from Emprered.server.views.catalogo_emprendedor_view import CatalogoEmprendedorView
from Emprered.server.views.detalle_catalogo_emprendedor_view import DetalleCatalogoEmprendedorView, ComentarioDetalleCatalogoEmprendedorView
from Emprered.server.views.search_query_view import SearchQueryView
from Emprered.server.views.categorias_view import CategoriasView
from Emprered.server.views.valoracion_view import ValoracionView
from Emprered.server.views.ventas_view import VentasView
from Emprered.server.views.eventos_view import EventosView
from Emprered.server.views.notificaciones_view import NotificacionesView


urlpatterns = [

    # Usuarios
    url(r'^login/?$', LoginView.as_view()),
    url(r'^registro/?$', RegistroView.as_view()),
    url(r'^editar_perfil/?$', EditarPerfilView.as_view()),

    # Confirmación de email
    url(r'^confirmar_email/?(?P<username>[a-zA-Z0-9_]+)/?(?P<token_activacion>[a-zA-Z0-9_]+)?/?$', ConfirmarEmailView.as_view()),
    url(r'^renovar_confirmacion_cuenta/?(?P<username>[a-zA-Z0-9_]+)/?$', RenovarConfirmacionEmailView.as_view()),

    # Olvide mi contraseña
    url(r'^olvido_clave/?(?P<username>[a-zA-Z0-9_]+)/?$', OlvidoClaveView.as_view()),
    url(r'^recuperar_clave/?(?P<username>[a-zA-Z0-9_]+)/?(?P<token_recuperacion>[a-zA-Z0-9_]+)?/?$', RecuperarClaveView.as_view()),



    url(r'^media/(?P<path>.*)$', serve, {'document_root': settings.MEDIA_ROOT, }, name="media"),

    # Tipificaciones
    url(r'^tipos_documento/?$', TiposDocumentoView.as_view()),
    url(r'^alicuotas_iva/?$', AlicuotasIvaView.as_view()),
    url(r'^condiciones_frente_iva/?$', CondicionesFrenteIVAView.as_view()),
    url(r'^unidades_medida/?$', UnidadesMedidaView.as_view()),
    url(r'^etiquetas/?$', EtiquetasView.as_view()),


    # Emprendedores
    url(r'^emprendedores/?$', EmprendedorView.as_view()),
    url(r'^emprendedores/datos/?(?P<action>[a-zA-Z_]+)/?(?P<id>[0-9]+)?/?$', EmprendedorView.as_view()),
    # url(r'^emprendedores/(?P<id>[0-9]+)/?$', EmprendedorView.as_view()),

    # Clientes
    url(r'^clientes/?$', ClienteView.as_view()),
    url(r'^clientes/datos/?(?P<action>[a-zA-Z_]+)/?(?P<id>[0-9]+)?/?$', ClienteView.as_view()),

    #Marca
    url(r'^marcas/?$', MarcasView.as_view()),


    # Materias Primas
    url(r'^emprendedores/materias_primas/?$', MateriasPrimasView.as_view()),
    url(r'^emprendedores/materias_primas/(?P<id_materia_prima>[0-9]+)/?$', MateriasPrimasView.as_view()),


    # Productos

    url(r'^emprendedores/productos/?$', ProductosView.as_view()),
    url(r'^emprendedores/productos/(?P<id_producto>\d+)/?$', ProductosView.as_view()),

    url(r'^emprendedores/productos/(?P<id_producto>\d+)/plan_produccion/?$', PlanProduccionView.as_view()),
    url(r'^emprendedores/productos/plan_produccion/?$', PlanProduccionView.as_view()),


    # Compras
    url(r'^compras/?$', ComprasView.as_view()),
    url(r'^compras/(?P<id_compra>\d+)/$', ComprasView.as_view()),


    # Proveedores
    url(r'^proveedores/?$', ProveedoresView.as_view()),
    url(r'^proveedores/(?P<id_proveedor>\d+)/?$', ProveedoresView.as_view()),

    # Producciones
    url(r'^producciones/?$', ProduccionesView.as_view()),
    # No tengo ni idea de por qué, pero no funciona si le agrego trailing slash,
    # (la / antes de $) así que se lo saco.
    url(r'^producciones/(?P<id_produccion>\d+)/?$', ProduccionesView.as_view()),

    # Movimientos de stock
    url(r'^movimientos_stock/?$', MovimientosStockView.as_view()),
    url(r'^movimientos_stock/(?P<id_movimiento_stock>\d+)/$', MovimientosStockView.as_view()),


    # Reportes

    # Movimientos Stock
    url(r'^reportes_movimientos_stock/$', ReportesMovimientosStock.as_view()),
    url(r'^reportes_produccion/$', ReportesProduccion.as_view()),

    # Ver todos los catálogos del emprendedor registrado
    # Crear un nuevo catálogo
    url(r'^catalogos_emprendedor/?$', CatalogoEmprendedorView.as_view()),

    # Ver o editar un catálogo en particular
    url(r'^catalogos_emprendedor/(?P<id_catalogo>\d+)/?$', CatalogoEmprendedorView.as_view()),


    # Agregar un detalle a un catálogo
    # Consultar un detalle a un catálogo
    # Editar un detalle de un catálogo
    # Borrar un detalle de un catálogo
    url(r'^catalogos_emprendedor/administrar_detalle/?$', DetalleCatalogoEmprendedorView.as_view()),
    url(r'^catalogos_emprendedor/administrar_detalle/(?P<id_detalle>\d+)/?$', DetalleCatalogoEmprendedorView.as_view()),

    url(r'^catalogos_emprendedor/administrar_detalle/agregar_comentario/?$', ComentarioDetalleCatalogoEmprendedorView.as_view()),


    url(r'^tienda/(?P<recurso>detalles_catalogo|catalogos|productos)$', TiendaView.as_view()),

    #url(r'^tienda/(?P<recurso>productos)/?(?P<criterio>\w*)/?', TiendaView.as_view()),

    # Haystack
    #url(r'^search/', include('haystack.urls')),
    # En el grupo <model> deberían estar todos los modelos que queremos permitir que se busquen
    url(r'^buscar/(?P<model>productos|emprendedores|marcas)?/?(?P<query>[\w+\s+]+)?/?(?P<boost>[\w+\s+]+)?/?$', SearchQueryView.as_view()),

    url(r'^categorias/?$', CategoriasView.as_view()),
    url(r'^categoria/(?P<id_categoria>\d+)/?$', CategoriasView.as_view()),
    url(r'^subcategoria/(?P<id_subcategoria>\d+)/?$', CategoriasView.as_view()),

    url(r'^registrar_venta/?$', VentasView.as_view()),
    url(r'^ventas/(?P<rol>vendedor|comprador)/?$', VentasView.as_view()),
    url(r'^ventas/?$', VentasView.as_view()),


    url(r'^mercado_pago/(?P<resultado_pago>exito|error)/?$', MercadoPagoView.as_view()),

    url(r'^mercado_pago/autorizar_emprendedor/(?P<action>vincular_cuenta|codigo_autorizacion|token_emprendedor)/?$', AutorizacionEmprendedorMercadoPagoView.as_view()),

    url(r'^valorar_detalle_catalogo/?$', ValoracionView.as_view()),
    url(r'^valoracion_detalle_catalogo/?$', ValoracionView.as_view()),

    url(r'^eventos/(?P<id_evento>\d+)?/?$', EventosView.as_view()),

    url(r'^notificaciones/(?P<id_notificacion>\d+)?/?$', NotificacionesView.as_view()),
]
