import time
from decimal import Decimal

import requests
from django.db import transaction
from django.shortcuts import redirect
from django.views import View

from Emprered.server.models.catalogo import DetalleCatalogo, Catalogo
from Emprered.server.models.emprendedores import Emprendedores
from Emprered.server.models.usuarios import Perfil
from Emprered.server.models.venta import Venta, Pago, DetallesVenta
from Emprered.utils import RespuestaHTTP
from Emprered.server.servicios.servicio_usuarios import ServicioUsuarios
import mercadopago
import json
import sys
from Emprered.settings import APP_ID_MERCADO_PAGO, SECRET_KEY_MERCADO_PAGO, DEBUG, WEB_PATH


class AutorizacionEmprendedorMercadoPagoView(View):

    def get(self, request, *args, **kwargs):
        respuesta = RespuestaHTTP()
        servicio = ServicioUsuarios()

        action = kwargs.get('action')



        if (action == 'vincular_cuenta'):
            user = servicio.get_usuario_jwt(request)
            usuario, rol = servicio.get_rol(user)

            if (rol == 'emprendedor'):
                emprendedor_id = usuario

                redirect_uri = request.GET.get('redirect_uri')

                if (emprendedor_id and redirect_uri):

                    emprendedor = Emprendedores.objects.get(id = emprendedor_id)
                    emprendedor.redirect_uri_luego_vinculacion = redirect_uri

                    emprendedor.save()
                    respuesta.tipo_ok()
                    return redirect("https://auth.mercadopago.com.ar/authorization?client_id="+APP_ID_MERCADO_PAGO+"&response_type=code&platform_id=mp&redirect_uri="+WEB_PATH + "mercado_pago/autorizar_emprendedor/codigo_autorizacion?id_emprendedor="+emprendedor_id)
            else:
                respuesta.set_mensaje("Debe ser un emprendedor para realizar esta operación")

        elif (action == 'codigo_autorizacion'):
            codigo_autorizacion = request.GET.get('code')
            emprendedor_id = request.GET.get('id_emprendedor')
            estado = request.GET.get('estado')
            if (emprendedor_id):
                
               
               url = "https://api.mercadopago.com/oauth/token"
               data = json.dumps({'client_id': APP_ID_MERCADO_PAGO, 'client_secret': SECRET_KEY_MERCADO_PAGO, 'grant_type': 'authorization_code','code':codigo_autorizacion, 'redirect_uri': WEB_PATH + 'mercado_pago/autorizar_emprendedor/codigo_autorizacion?id_emprendedor='+emprendedor_id})
               response = requests.post(url, data)

               
               #print(response.json())
               response_json = response.json()
               emprendedor = Emprendedores.objects.get(id = emprendedor_id)
               emprendedor.access_token_mercado_pago = response_json['access_token']
               emprendedor.ultima_actualizacion_token_mercado_pago = time.strftime("%y-%m-%d")
               emprendedor.refresh_token_mercado_pago = response_json['refresh_token']

               emprendedor.save()
               return redirect(emprendedor.redirect_uri_luego_vinculacion)


        elif (action == 'token_emprendedor'):
            print("token emprendedor")
        return respuesta.get_respuesta_http()


    def post(self, request, *args, **kwargs):
        respuesta = RespuestaHTTP()
        servicio = ServicioUsuarios()
        return respuesta.get_respuesta_http()


    def put(self, request, *args, **kwargs):
        respuesta = RespuestaHTTP()
        servicio = ServicioUsuarios()
        return respuesta.get_respuesta_http()



    def delete(self, request, *args, **kwargs):
        respuesta = RespuestaHTTP()
        return respuesta.get_respuesta_http()



