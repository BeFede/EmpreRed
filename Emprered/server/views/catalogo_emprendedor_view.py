import json
import sys

from django.http import HttpResponse, QueryDict
from django.views import View
from rest_framework import viewsets
from django.conf import settings
from Emprered.utils import RespuestaHTTP, validar_no_vacio

from Emprered.server.servicios.servicio_usuarios import ServicioUsuarios
from Emprered.server.models.productos import Productos
from Emprered.server.models.catalogo import Catalogo, DetalleCatalogo, Comentario

from django.db.models.query import RawQuerySet
from django.db import transaction
import datetime
from django.db.models import Sum


class CatalogoEmprendedorView(View):


    def get_catalogo(self, id_catalogo, emprendedor, filtros = {}, orden ={}):



        catalogo = Catalogo.objects.filter(id = id_catalogo).filter(emprendedor=emprendedor)[0]

        detalles_catalogo = DetalleCatalogo.objects.filter(catalogo = catalogo)

        if filtros.get('titulo'):
            detalles_catalogo = detalles_catalogo.filter(titulo__icontains=filtros.get('titulo'))
        if filtros.get('precio_desde'):
            detalles_catalogo = detalles_catalogo.filter(precio__gte=filtros.get('precio_desde'))
        if filtros.get('precio_hasta'):
            detalles_catalogo = detalles_catalogo.filter(precio__lte=filtros.get('precio_hasta'))
        if filtros.get('stock_desde'):
            detalles_catalogo = detalles_catalogo.filter(producto__id_datos_comerciales__stock_actual__gte=filtros.get('stock_desde'))
        if filtros.get('stock_hasta'):
            detalles_catalogo = detalles_catalogo.filter(producto__id_datos_comerciales__stock_actual__lte=filtros.get('stock_hasta'))
        if filtros.get('unidad_medida'):
            detalles_catalogo = detalles_catalogo.filter(producto__id_unidad_medida=filtros.get('unidad_medida'))
        if filtros.get('id_producto'):
            detalles_catalogo = detalles_catalogo.filter(producto__id=filtros.get('id_producto'))
        if filtros.get('nombre_producto'):
            detalles_catalogo = detalles_catalogo.filter(producto__nombre__icontains=filtros.get('nombre_producto'))

        if filtros.get('visible'):

            if (filtros.get('visible') == '1'):
                detalles_catalogo = detalles_catalogo.filter(estado=DetalleCatalogo.VISIBLE)
            elif (filtros.get('visible') == '0'):
                print('asde')
                detalles_catalogo = detalles_catalogo.filter(estado=DetalleCatalogo.NO_VISIBLE)

        detalles_catalogo = detalles_catalogo.order_by('{0}{1}'.format(orden.get('criterio'), orden.get('atributo')))


        detalles_catalogo_list = []

        for detalle_catalogo in detalles_catalogo:


            producto = {
                "id": detalle_catalogo.producto.id,
                "nombre": detalle_catalogo.producto.nombre,
                "foto": detalle_catalogo.producto.foto.url if detalle_catalogo.producto.foto else "",
                "subcategoria": detalle_catalogo.producto.subcategoria.descripcion,
                "categoria": detalle_catalogo.producto.subcategoria.categoria.descripcion,
                "stock": detalle_catalogo.producto.id_datos_comerciales.stock_actual,
                "unidad_medida": detalle_catalogo.producto.id_unidad_medida.descripcion
            }

            detalle_catalogo_dic = {
                "id": detalle_catalogo.id,
                "titulo": detalle_catalogo.titulo,
                "descripcion": detalle_catalogo.descripcion,
                "producto": producto,
                "precio_venta": detalle_catalogo.precio,
                "valoracion": detalle_catalogo.valoracion_promedio,
                "visitas": detalle_catalogo.visitas,
                "estado": detalle_catalogo.get_estado(),
                "etiqueta": detalle_catalogo.etiqueta.descripcion if detalle_catalogo.etiqueta else ""

            }

            if filtros.get('stock_bajo'):
                if filtros.get('stock_bajo') == '1':
                    if detalle_catalogo.producto.id_datos_comerciales.stock_actual < detalle_catalogo.producto.id_datos_comerciales.stock_minimo:
                        detalles_catalogo_list.append(detalle_catalogo_dic)
                elif filtros.get('stock_bajo') == '0':
                    if detalle_catalogo.producto.id_datos_comerciales.stock_actual >= detalle_catalogo.producto.id_datos_comerciales.stock_minimo:
                        detalles_catalogo_list.append(detalle_catalogo_dic)
            else:
                detalles_catalogo_list.append(detalle_catalogo_dic)


        catalogo_dic = {
            "id_catalogo" : catalogo.id,
            "fecha_hora_publicacion" : catalogo.fecha_hora_publicacion,
            "tipo_catalogo" : catalogo.get_tipo_catalogo(),
            "estado": catalogo.get_estado(),
            "detalles_catalogo" : detalles_catalogo_list,
            "cantidad_detalles": len(detalles_catalogo),
            "cantidad_visitas": detalles_catalogo.aggregate(Sum('visitas'))
        }

        return catalogo_dic

    def get_catalogos(self, emprendedor):

        catalogos = Catalogo.objects.filter(emprendedor=emprendedor).order_by("-fecha_hora")
        catalogos_list = []

        for catalogo in catalogos:
            detalles_catalogo = DetalleCatalogo.objects.filter(catalogo = catalogo)
            catalogo_dic = {
                "id":catalogo.id,
                "fecha_hora_publicacion" : catalogo.fecha_hora,
                "tipo_catalogo" : catalogo.tipo_catalogo,
                "estado": catalogo.estado,
                "cantidad_detalles": len(detalles_catalogo)
            }
            catalogos_list.append(catalogo_dic)

        return catalogos_list


    def get_catalogo_minorista(self, emprendedor, filtros = {}, orden = {}):

        catalogos = Catalogo.objects.filter(emprendedor=emprendedor)

        catalogo = None # catálogo minorista

        for cat in catalogos:
            if cat.tipo_catalogo == Catalogo.MINORISTA:
                catalogo = cat
                break

        if not catalogo:
            return None


        return self.get_catalogo(catalogo.id, emprendedor, filtros, orden)



    def get(self, request, *args, **kwargs):
        respuesta = RespuestaHTTP()
        servicio = ServicioUsuarios()

        try:

            user = servicio.get_usuario_jwt(request)
            usuario, rol = servicio.get_rol(user)

            if (rol == 'emprendedor'):
                emprendedor = usuario
                id_catalogo = kwargs['id_catalogo'] if 'id_catalogo' in kwargs else None

                #if not id_catalogo:
                #    respuesta.agregar_dato('catalogos', self.get_catalogos(emprendedor))
                #else:
                #    respuesta.agregar_dato('catalogo', self.get_catalogo(id_catalogo, emprendedor))

                # Devolvemos por defecto el catálogo minorista hasta que se agregue la opción de
                # seleccionar catálogo

                orden = request.GET.get('orden', 'titulo')
                criterio = request.GET.get('criterio', '-')
                titulo = request.GET.get('titulo', '')
                id_producto = request.GET.get('id_producto', "")
                precio_desde = request.GET.get('precio_desde', "")
                precio_hasta = request.GET.get('precio_hasta', "")
                stock_desde = request.GET.get('stock_desde', "")
                stock_hasta = request.GET.get('stock_hasta', "")
                stock_bajo = request.GET.get('stock_bajo', "")
                nombre_producto = request.GET.get('nombre_producto', "")
                visible = request.GET.get('visible', "")


                subcategorias = request.GET.get('subcategorias', [])
                if subcategorias:
                    subcategorias = subcategorias.split(",")

                categorias = request.GET.get('categorias', [])
                if categorias:
                    categorias = categorias.split(",")

                orden = {
                    "atributo": orden,
                    "criterio": criterio
                }

                filtros = {
                    'id_producto': id_producto,
                    'precio_desde': precio_desde,
                    'precio_hasta': precio_hasta,
                    'categorias': [int(c) for c in categorias if c.isdigit()],
                    'subcategorias': [int(sc) for sc in subcategorias if sc.isdigit()],
                    'stock_desde': stock_desde,
                    'stock_hasta': stock_hasta,
                    'stock_bajo': stock_bajo,
                    'titulo': titulo,
                    'nombre_producto': nombre_producto,
                    'visible': visible
                }



                respuesta.agregar_dato('catalogo', self.get_catalogo_minorista(emprendedor, filtros, orden))

                respuesta.tipo_ok()

            else:
                respuesta.tipo_error()
                respuesta.set_mensaje("Solo un emprendedor puede hacer esta operación")

        except Exception as e:
            print(e)
            respuesta.tipo_error()
            respuesta.set_mensaje(str(e))

            print('Error on line {}'.format(sys.exc_info()[-1].tb_lineno))


        return respuesta.get_respuesta_http()



    def post(self, request, *args, **kwargs):
        respuesta = RespuestaHTTP()
        servicio = ServicioUsuarios()

        try:

            user = servicio.get_usuario_jwt(request)
            usuario, rol = servicio.get_rol(user)

            if (rol == 'emprendedor'):

                emprendedor = usuario
                json_request = json.loads(request.body.decode('utf-8'))

                with transaction.atomic():

                    id_catalogo = kwargs['id_catalogo'] if 'id_catalogo' in kwargs else None
                    tipo_catalogo = json_request.get('tipo_catalogo', Catalogo.MINORISTA)
                    estado = json_request.get('estado', Catalogo.VISIBLE)

                    if not id_catalogo:
                        # Nuevo catálogo
                        catalogo = Catalogo()
                        catalogo.fecha_hora_publicacion = datetime.datetime.now()
                        catalogo.emprendedor = emprendedor
                    else:
                        # editar catálogo
                        catalogo = Catalogo.objects.get(id=id_catalogo)

                    catalogo.tipo_catalogo = tipo_catalogo
                    catalogo.estado = estado
                    catalogo.save()
                    respuesta.agregar_dato("id_catalogo", catalogo.id)
                    respuesta.tipo_ok()
            else:
                respuesta.tipo_error()
                respuesta.set_mensaje("Solo un emprendedor puede hacer esta operación")
        except Exception as exc:
            respuesta.tipo_error()
            respuesta.set_mensaje(str(exc))
            print(str(exc))

        return respuesta.get_respuesta_http()
