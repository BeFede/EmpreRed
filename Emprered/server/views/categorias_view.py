import sys
import json
from django.http import HttpResponse, QueryDict
from django.views import View
from django.core.exceptions import MultipleObjectsReturned
from rest_framework import viewsets
from django.conf import settings
from Emprered.utils import RespuestaHTTP
from Emprered.server.models.categorias import Categoria, Subcategoria

from django.db.models.query import RawQuerySet
from django.db import transaction

class CategoriasView(View):

    def get_categorias(self):
        """Devuelve todas las categorías"""
        categorias = Categoria.objects.all()
        categorias_list = []
        for categorias in categorias:
            categorias_dic = {
                "id": categorias.id,
                "descripcion": categorias.descripcion,
            }
            categorias_list.append(categorias_dic)
        return categorias_list

    def get_categoria(self, id_categoria):
        categoria = Categoria.objects.get(id=id_categoria)
        subcategorias = Subcategoria.objects.filter(categoria=id_categoria)
        subcategorias_list = []
        for sc in subcategorias:
            subcategorias_dic = {
                "id": sc.id,
                "descripcion": sc.descripcion,
            }
            subcategorias_list.append(subcategorias_dic)

        return {
            'descripcion': categoria.descripcion,
            'id': categoria.id,
            'subcategorias': subcategorias_list,
        }

    def get_subcategoria(self, id_subcategoria):
        subcategoria =  Subcategoria.objects.get(id=id_subcategoria)
        id_categoria = subcategoria.categoria.id

        categoria = Categoria.objects.get(id=id_categoria)

        return {
            "categoria": {
                "id": categoria.id,
                "descripcion": categoria.descripcion
            },
            "id": subcategoria.id,
            "descripcion": subcategoria.descripcion,
        }


    def get(self, request, *args, **kwargs):
        respuesta = RespuestaHTTP()

        try:
            if kwargs.get('id_categoria'):
                id_categoria = kwargs.get('id_categoria')
                respuesta.set_mensaje("Estas son las subcategorías que pertenecen a la categoría con id {}.".format(id_categoria))
                respuesta.agregar_dato('categoria', self.get_categoria(id_categoria))
            elif kwargs.get('id_subcategoria'):
                id_subcategoria = kwargs.get('id_subcategoria')
                respuesta.set_mensaje("Esta es la categoría a la que pertenece la subcategoría con id {}.".format(id_subcategoria))
                respuesta.agregar_dato('subcategoria', self.get_subcategoria(id_subcategoria ))
            else:
                respuesta.agregar_dato('categorias', self.get_categorias())
            respuesta.tipo_ok()

        except Categoria.DoesNotExist:
            respuesta.tipo_error()
            respuesta.set_mensaje("La categoría no existe.")
        except Subcategoria.DoesNotExist:
            respuesta.tipo_error()
            respuesta.set_mensaje("La subcategoría no existe.")

        except Exception as e:
            print('Error on line {}'.format(sys.exc_info()[-1].tb_lineno))
            print(e)
            respuesta.tipo_error()
            respuesta.set_mensaje(str(e))
        return respuesta.get_respuesta_http()
