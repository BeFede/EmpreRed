import json
import sys
from django.http import HttpResponse, QueryDict
from django.views import View
from rest_framework import viewsets
from django.conf import settings
from Emprered.utils import RespuestaHTTP, validar_no_vacio, NotAuthorizedException
from Emprered.server.models.clientes import Clientes
from Emprered.server.models.tipificaciones import TiposDocumento, CondicionesFrenteIVA
from Emprered.server.models.direccion import Direcciones
from django.db.models.query import RawQuerySet
from django.db import transaction
from Emprered.server.models.usuarios import Perfil
from django.contrib.auth.models import User
from Emprered.server.servicios.servicio_usuarios import ServicioUsuarios


class ClienteView(View):

    def get_clientes(self):
        clientes = Clientes.objects.all()
        clientes_dic = []
        for cliente in clientes:
            cliente_dic = {
                "id": cliente.id,
                "nombre": cliente.nombre,
                "apellido": cliente.apellido,
                "tipo_documento": cliente.id_tipo_documento.id,
                "numero_documento":cliente.numero_documento,
                "fecha_nacimiento": cliente.fecha_nacimiento,
                "telefono": cliente.telefono
            }
            clientes_dic.append(cliente_dic)
        return clientes_dic

    def get_cliente(self, id_cliente):
        cliente = Clientes.objects.get(id=id_cliente)
        cliente_dic = {
            "id": cliente.id,
            "nombre": cliente.nombre,
            "apellido": cliente.apellido,
            "tipo_documento": cliente.id_tipo_documento.id,
            "numero_documento":cliente.numero_documento,
            "fecha_nacimiento": cliente.fecha_nacimiento,
            "telefono": cliente.telefono
        }
        return cliente_dic


    def get(self, request, *args, **kwargs):
        respuesta = RespuestaHTTP()
        servicio = ServicioUsuarios()

        action = kwargs.get('action')
        try:
            if action:
                if action == 'consultar':
                    id = kwargs.get('id')
                    respuesta.agregar_dato('clientes', self.get_cliente(id))
                elif action == 'perfil':

                    user = servicio.get_usuario_jwt(request)
                    if user.is_anonymous:
                        raise NotAuthorizedException()
                    usuario, rol = servicio.get_rol(user)
                    if rol == 'cliente':
                        respuesta.agregar_dato('clientes', self.get_cliente(usuario.id))
            else:
                respuesta.agregar_dato('clientes', self.get_clientes())
            respuesta.tipo_ok()
        except Exception as e:
            print('Error on line {}'.format(sys.exc_info()[-1].tb_lineno))
            print(e)
            respuesta.tipo_error()
            respuesta.set_mensaje(str(e))
        return respuesta.get_respuesta_http()


    def post(self, request, *args, **kwargs):
        respuesta = RespuestaHTTP()
        servicio = ServicioUsuarios()

        try:
            user = servicio.get_usuario_jwt(request)
            if user.is_anonymous:
                raise NotAuthorizedException()
            perfil = Perfil.objects.all().get(user = user)

            #Validamos que el usuario que ingresa no está activo
            if not perfil.es_activo:

                json_request = json.loads(request.body.decode('utf-8'))
                cliente = Clientes()

                with transaction.atomic():
                    perfil.es_activo = True
                    nombre = json_request.get('nombre', None)
                    apellido = json_request.get('apellido', None)
                    #id_tipo_documento = json_request.get('tipo_documento', None)
                    numero_documento = json_request.get('numero_documento', None)
                    fecha_nacimiento = json_request.get('fecha_nacimiento', None)
                    telefono = json_request.get('telefono', None)
                    id_direccion = json_request.get('id_direccion', None)
                    id_condicion_frente_IVA = json_request.get('condicion_frente_iva', None)


                    # En caso de registro o de cambio de direccion
                    #id_ciudad = json_request.get('id_ciudad', None)
                    #codigo_postal = json_request.get('codigo_postal', None)
                    #barrio = json_request.get('barrio', None)
                    #calle = json_request.get('calle', None)
                    #numero_calle = json_request.get('numero_calle', None)


                    campos_validar = [nombre, apellido, numero_documento]

                    if validar_no_vacio(campos_validar):

                        if id_direccion is not None:
                            direccion = Direcciones.objects.get(id=id_direccion)
                        else:
                            direccion = Direcciones()

                        #direccion.id_ciudad = id_ciudad
                        #direccion.codigo_postal = codigo_postal
                        #direccion.barrio = barrio
                        #direccion.numero_calle = numero_calle
                        direccion.save()

                        tipo_documento = TiposDocumento.objects.get(descripcion="CUIL")
                        user.save()
                        cliente.nombre = nombre
                        cliente.apellido = apellido
                        cliente.numero_documento = numero_documento
                        cliente.id_tipo_documento = tipo_documento
                        cliente.fecha_nacimiento = fecha_nacimiento
                        cliente.telefono = telefono
                        cliente.direccion = direccion
                        cliente.id_usuario = user
                        perfil.rol = Perfil.CLIENTE
                        perfil.user.first_name = nombre
                        perfil.user.last_name = apellido
                        perfil.user.save()

                        perfil.save()
                        cliente.save()

                        respuesta.tipo_ok()
                        respuesta.agregar_dato("usuario", servicio.get_usuario_to_dic(perfil))
                        respuesta.set_mensaje("Se ha insertado correctamente el nuevo cliente, con id {}.".format(cliente.id))

                    else:
                        respuesta.tipo_error()
                        respuesta.set_mensaje("Faltan parámetros obligatorios.")
            else:
                respuesta.tipo_error()
                respuesta.set_mensaje("El usuario ya se encuentra activo")

        except Exception as exc:
            print('Error on line {}'.format(sys.exc_info()[-1].tb_lineno))
            respuesta.tipo_error()
            respuesta.set_mensaje(str(exc))

        return respuesta.get_respuesta_http()


    def put(self, request, *args, **kwargs):
        respuesta = RespuestaHTTP()
        servicio = ServicioUsuarios()

        try:

            user = servicio.get_usuario_jwt(request)
            if user.is_anonymous:
                raise NotAuthorizedException()
            usuario, rol = servicio.get_rol(user)

            if (rol == 'cliente'):
                cliente = usuario

                json_request = json.loads(request.body.decode('utf-8'))

                with transaction.atomic():
                    #perfil.es_activo = True
                    nombre = json_request.get('nombre', None)
                    apellido = json_request.get('apellido', None)
                    #id_tipo_documento = json_request.get('tipo_documento', None)
                    numero_documento = json_request.get('numero_documento', None)
                    fecha_nacimiento = json_request.get('fecha_nacimiento', None)
                    telefono = json_request.get('telefono', None)
                    id_direccion = json_request.get('id_direccion', None)
                    id_condicion_frente_IVA = json_request.get('condicion_frente_iva', None)


                    # En caso de registro o de cambio de direccion
                    #id_ciudad = json_request.get('id_ciudad', None)
                    #codigo_postal = json_request.get('codigo_postal', None)
                    #barrio = json_request.get('barrio', None)
                    #calle = json_request.get('calle', None)
                    #numero_calle = json_request.get('numero_calle', None)


                    campos_validar = [nombre, apellido, numero_documento]

                    if validar_no_vacio(campos_validar):

                        if id_direccion is not None:
                            direccion = Direcciones.objects.get(id=id_direccion)
                        else:
                            direccion = Direcciones()

                        #direccion.id_ciudad = id_ciudad
                        #direccion.codigo_postal = codigo_postal
                        #direccion.barrio = barrio
                        #direccion.numero_calle = numero_calle
                        direccion.save()

                        tipo_documento = TiposDocumento.objects.get(descripcion="CUIL")
                        #condicion_frente_IVA = CondicionesFrenteIVA.objects.get(id=id_condicion_frente_IVA)
                        user.save()
                        cliente.nombre = nombre
                        cliente.apellido = apellido
                        cliente.numero_documento = numero_documento
                        cliente.id_tipo_documento = tipo_documento
                        cliente.fecha_nacimiento = fecha_nacimiento
                        cliente.telefono = telefono
                        #cliente.id_condicion_frente_IVA = condicion_frente_IVA
                        cliente.direccion = direccion
                        cliente.id_usuario = user

                        cliente.save()

                        respuesta.tipo_ok()
                        respuesta.set_mensaje("Se ha modificado el cliente")

                    else:
                        respuesta.tipo_error()
                        respuesta.set_mensaje("Faltan parámetros obligatorios.")
            else:
                respuesta.tipo_forbidden()
                respuesta.set_mensaje("Solo un cliente puede hacer esta operación")

        except Exception as exc:
            print('Error on line {}'.format(sys.exc_info()[-1].tb_lineno))
            respuesta.tipo_error()
            respuesta.set_mensaje(str(exc))

        return respuesta.get_respuesta_http()



    def delete(self, request, *args, **kwargs):
        respuesta = RespuestaHTTP()
        id_cliente_a_borrar = kwargs['id_cliente'] if 'id_cliente' in kwargs else None

        try:
            if id_cliente_a_borrar:
                cliente = Clientes.objects.get(id=id_cliente_a_borrar)
                cliente.delete()
                respuesta.tipo_ok()
                respuesta.set_mensaje("Se ha borrado correctamente el cliente con id {}.".format(id_cliente_a_borrar))
            else:
                respuesta.tipo_error()
                respuesta.set_mensaje("Faltan parámetros obligatorios: id_cliente")
        except Exception as exc:
            print('Error on line {}'.format(sys.exc_info()[-1].tb_lineno))
            respuesta.tipo_error()
            respuesta.set_mensaje(str(exc))

        return respuesta.get_respuesta_http()
