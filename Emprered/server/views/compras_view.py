import json
from decimal import Decimal

from django.http import HttpResponse, QueryDict
from django.views import View
from rest_framework import viewsets
from django.conf import settings
from Emprered.utils import RespuestaHTTP, validar_no_vacio

from Emprered.server.servicios.servicio_usuarios import ServicioUsuarios
from Emprered.server.models.compras import Compra, DetalleCompra
from Emprered.server.models.productos import Productos
from Emprered.server.models.materias_primas import MateriasPrimas
from Emprered.server.models.proveedores import Proveedores
from Emprered.server.models.movimientos_stock import MovimientosStock, registrar_movimiento_stock

from django.db.models.query import RawQuerySet
from django.db import transaction
import time


class ComprasView(View):


    def get_compra(self, id_compra, emprendedor):

        compra = Compra.objects.filter(id = id_compra).filter(emprendedor=emprendedor)[0]

        detalles_compra = DetalleCompra.objects.filter(compra = compra)

        detalles_compra_list = []

        for detalle_compra in detalles_compra:
            detalle_compra_dic = {
                "id": detalle_compra.id,
                "materia_prima": detalle_compra.materia_prima.nombre if detalle_compra.materia_prima else None,
                "producto": detalle_compra.producto.nombre if detalle_compra.producto else None,
                "cantidad": detalle_compra.cantidad,
                "monto": detalle_compra.monto,
                "descuento": detalle_compra.descuento,
                "unidad_medida": detalle_compra.materia_prima.id_unidad_medida.descripcion if detalle_compra.materia_prima else detalle_compra.producto.id_unidad_medida.descripcion
            }
            detalles_compra_list.append(detalle_compra_dic)

        compra_dic = {
            "fecha_hora" : compra.fecha_hora,
            "monto" : compra.monto,
            "proveedor_id": compra.proveedor.id,
            "proveedor": compra.proveedor.nombre,
            "comprobante": compra.comprobante_referencia,
            "detalles" : detalles_compra_list
        }

        return compra_dic

    def get_compras(self, emprendedor, filtros, orden):

        compras = Compra.objects.all().filter(emprendedor = emprendedor)\

        if filtros.get('numero'):
            compras = compras.filter(id=filtros.get('numero'))
        if filtros.get('comprobante'):
            compras = compras.filter(comprobante_referencia=filtros.get('comprobante'))
        #if filtros.get('descripcion'):
        #    compras = compras.filter(descripcion__contains=filtros.get('descripcion'))
        if filtros.get('fecha_desde'):
            compras = compras.filter(fecha_hora__gte=filtros.get('fecha_desde'))
        if filtros.get('fecha_hasta'):
            compras = compras.filter(fecha_hora__lte=filtros.get('fecha_hasta'))
        if filtros.get('proveedor'):
            compras = compras.filter(proveedor__id=filtros.get('proveedor'))
        #if filtros.get('estado'):
        #    producciones = producciones.filter(estado=filtros.get('estado'))

        if filtros.get('producto'):
            compras_temp = []
            for compra in compras:
                detalles = DetalleCompra.objects.all().filter(compra=compra)

                for detalle in detalles:

                    if detalle.producto and Decimal(detalle.producto.id) == Decimal(filtros.get('producto')):
                        compras_temp.append(compra)
                        break
            compras = compras.filter(id__in=[com.id for com in compras_temp])
        if filtros.get('materia_prima'):
            compras_temp = []
            for compra in compras:
                detalles = DetalleCompra.objects.all().filter(compra=compra)

                for detalle in detalles:

                    if detalle.materia_prima and Decimal(detalle.materia_prima.id) == Decimal(filtros.get('materia_prima')):
                        compras_temp.append(compra)
                        break
            compras = compras.filter(id__in=[com.id for com in compras_temp])

        compras = compras.order_by('{0}{1}'.format(orden.get('criterio'), orden.get('atributo')))
        compras_list = []

        for compra in compras:
            compra_dic = {
                "id": compra.id,
                "fecha_hora": compra.fecha_hora,
                "monto": compra.monto,
                "proveedor_id": compra.proveedor.id,
                "proveedor": compra.proveedor.nombre, 
                "comprobante": compra.comprobante_referencia
            }
            compras_list.append(compra_dic)

        return compras_list


    def get(self, request, *args, **kwargs):
        respuesta = RespuestaHTTP()
        servicio = ServicioUsuarios()

        try:

            user = servicio.get_usuario_jwt(request)
            usuario, rol = servicio.get_rol(user)

            if (rol == 'emprendedor'):
                emprendedor = usuario
                id_compra = kwargs['id_compra'] if 'id_compra' in kwargs else None

                if not id_compra:
                    orden = request.GET.get('orden', 'id')
                    criterio = request.GET.get('criterio', 'desc')
                    numero = request.GET.get('numero', "")
                    descripcion = request.GET.get('descripcion', "")
                    producto = request.GET.get('producto', "")
                    materia_prima = request.GET.get('materia_prima', "")
                    fecha_desde = request.GET.get('fecha_desde', "")
                    fecha_hasta = request.GET.get('fecha_hasta', "")
                    comprobante = request.GET.get('comprobante', "")
                    proveedor = request.GET.get('proveedor', "")

                    filtros = {
                        'numero': numero,
                        'descripcion': descripcion,
                        'producto': producto,
                        'materia_prima': materia_prima,
                        'fecha_desde': fecha_desde,
                        'fecha_hasta': fecha_hasta,
                        'comprobante': comprobante,
                        'proveedor': proveedor

                    }

                    criterio = '-' if criterio == 'desc' else ''

                    orden = {
                        'atributo': orden,
                        'criterio': criterio
                    }

                    respuesta.agregar_dato('compras', self.get_compras(emprendedor, filtros = filtros, orden = orden))
                else:
                    respuesta.agregar_dato('compra', self.get_compra(id_compra, emprendedor))
                respuesta.tipo_ok()

            else:
                respuesta.tipo_error()
                respuesta.set_mensaje("Solo un emprendedor puede hacer esta operación")

        except Exception as e:
            print(e)
            respuesta.tipo_error()
            respuesta.set_mensaje(str(e))
        return respuesta.get_respuesta_http()


    def post(self, request, *args, **kwargs):
        respuesta = RespuestaHTTP()
        servicio = ServicioUsuarios()

        try:

            user = servicio.get_usuario_jwt(request)
            usuario, rol = servicio.get_rol(user)

            if (rol == 'emprendedor'):

                emprendedor = usuario
                json_request = json.loads(request.body.decode('utf-8'))

                with transaction.atomic():
                    detalles = json_request.get('detalles', [])
                    proveedor = json_request.get('proveedor', None)
                    comprobante = json_request.get('comprobante', None)
                    monto_total = 0
                    fecha_hora = time.strftime("%Y-%m-%d %H:%M:%S")

                    proveedor_obj = Proveedores.objects.filter(id = proveedor).filter(emprendedor_id = emprendedor)[0]

                    compra = Compra()
                    compra.fecha_hora = fecha_hora
                    compra.monto = monto_total
                    compra.emprendedor = emprendedor
                    compra.proveedor = proveedor_obj
                    compra.save()

                    for detalle in detalles:
                        materia_prima = detalle['id_materia_prima'] if 'id_materia_prima' in detalle else None
                        producto = detalle['id_producto'] if 'id_producto' in detalle else None
                        monto = detalle['monto'] if 'monto' in detalle else None
                        cantidad = detalle['cantidad'] if 'cantidad' in detalle else None
                        descuento = detalle['descuento'] if 'descuento' in detalle else None

                        if validar_no_vacio([monto, cantidad]):
                            #print(materia_prima)
                            detalle_compra = DetalleCompra()
                            monto_total = monto_total + (monto * cantidad)
                            try:
                                # Busco la materia prima con el id que nos pasan y valido de que pertenezca
                                # al emprendedor registrado
                                if materia_prima:
                                    materia_prima = MateriasPrimas.objects.filter(id = materia_prima).filter(id_emprendedor=emprendedor)[0]
                                    exito, mensaje = registrar_movimiento_stock(emprendedor, fecha_hora, None, materia_prima, cantidad, MovimientosStock.INGRESO, "Compra")
                                    if exito:
                                        materia_prima.id_datos_comerciales.costo = monto
                                        materia_prima.id_datos_comerciales.save()
                                        materia_prima.save()
                                        detalle_compra.materia_prima = materia_prima

                                elif producto:
                                    producto = Productos.objects.filter(id = producto).filter(id_emprendedor=emprendedor)[0]
                                    exito, mensaje = registrar_movimiento_stock(emprendedor, fecha_hora, producto, None, cantidad, MovimientosStock.INGRESO, "Compra")
                                    if exito:
                                        producto.id_datos_comerciales.costo = monto
                                        producto.id_datos_comerciales.precio = monto * (1 + (producto.id_datos_comerciales.utilidad / 100))
                                        producto.id_datos_comerciales.save()
                                        producto.save()
                                        detalle_compra.producto = producto


                                detalle_compra.monto = monto
                                detalle_compra.descuento = descuento
                                detalle_compra.cantidad = cantidad
                                detalle_compra.compra = Compra.objects.all().get(id=compra.id)
                                detalle_compra.save()
                            except Exception as ex:
                                respuesta.tipo_error()
                                respuesta.set_mensaje(ex)
                                return respuesta.get_respuesta_http()
                        else:
                            respuesta.tipo_error()
                            respuesta.set_mensaje("Faltan parámetros obligatorios")
                            return respuesta.get_respuesta_http()
                    compra.emprendedor = emprendedor
                    compra.fecha_hora = fecha_hora
                    compra.monto = monto_total
                    compra.proveedor = proveedor_obj
                    compra.comprobante_referencia = comprobante
                    compra.save()
                    respuesta.tipo_ok()
            else:
                respuesta.tipo_error()
                respuesta.set_mensaje("Solo un emprendedor puede hacer esta operación")
        except Exception as exc:
            respuesta.tipo_error()
            respuesta.set_mensaje(str(exc))
            print(str(exc))

        return respuesta.get_respuesta_http()


    def delete(self, request, *args, **kwargs):
        respuesta = RespuestaHTTP()
        servicio = ServicioUsuarios()
        id_compra_a_borrar = kwargs.get('id_compra', None)

        if id_compra_a_borrar:
            try:
                user = servicio.get_usuario_jwt(request)
                usuario, rol = servicio.get_rol(user)

                if (rol == 'emprendedor'):
                    emprendedor = usuario
                    compra = Compra.objects.filter(id=id_compra_a_borrar).filter(emprendedor=emprendedor)[0]
                    # No hace falta buscar los detalles porque se borrarán en cascada al borrar la compra.
                    compra.delete()
                    respuesta.tipo_ok()
                    respuesta.set_mensaje("Se ha borrado correctamente la compra con id {}".format(id_compra_a_borrar))
                else:
                    respuesta.tipo_error()
                    respuesta.set_mensaje("Solo un emprendedor puede hacer esta operación")
            except IndexError as ie:
                respuesta.tipo_error()
                respuesta.set_mensaje('No posee una compra con id {}'.format(id_compra_a_borrar))
            except Exception as exc:
                respuesta.tipo_error()
                respuesta.set_mensaje(str(exc))
        else:
            respuesta.tipo_error()
            respuesta.set_mensaje("Faltan parámetros obligatorios.")

        return respuesta.get_respuesta_http()
