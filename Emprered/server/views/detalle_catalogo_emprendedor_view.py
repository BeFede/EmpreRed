import json
from django.http import HttpResponse, QueryDict
from django.views import View
from rest_framework import viewsets
from django.conf import settings
from Emprered.utils import RespuestaHTTP, validar_no_vacio

from Emprered.server.servicios.servicio_usuarios import ServicioUsuarios
from Emprered.server.models.productos import Productos
from Emprered.server.models.catalogo import Catalogo, DetalleCatalogo, Comentario, Etiqueta, ImagenesDetalleCatalogo
from decimal import Decimal
from django.db.models.query import RawQuerySet
from django.db import transaction
import time
from Emprered.server.models.usuarios import Perfil


class DetalleCatalogoEmprendedorView(View):


    def get_detalle_catalogo(self, id_detalle_catalogo,emprendedor):

        # Traemos el detalle que nos pasan 
        detalle_catalogo = DetalleCatalogo.objects.get(id = id_detalle_catalogo)

        if not detalle_catalogo:
            return []

        #validamos si el detalle pertence al emprendedor
        if detalle_catalogo.catalogo.emprendedor.id != emprendedor.id:
            return None

        comentarios = Comentario.objects.filter(detalle_catalogo = detalle_catalogo).order_by("-fecha_hora_publicacion")

        comentarios_list = []

        for comentario in comentarios:
            comentario_dic = {
                "creador": comentario.perfil_creador.user.username,
                "foto": comentario.perfil_creador.foto.url if comentario.perfil_creador.foto else "",
                "titulo": comentario.titulo,
                "descripcion": comentario.descripcion,
                "fecha_hora": comentario.fecha_hora_publicacion
            }
            comentarios_list.append(comentario_dic)

        imagenes = ImagenesDetalleCatalogo.objects.filter(detalle_catalogo = detalle_catalogo)

        imagenes_list = []
        if detalle_catalogo.producto.foto:
            imagenes_list.append(
                {
                    "foto": detalle_catalogo.producto.foto.url
                }
            )
        for imagen in imagenes:
            imagen_dic = {
                "foto": imagen.imagen.url
            }
            imagenes_list.append(imagen_dic)


        producto = {
            "id": detalle_catalogo.producto.id,
            "nombre": detalle_catalogo.producto.nombre,
            "foto": detalle_catalogo.producto.foto.url if detalle_catalogo.producto.foto else "",                
            "subcategoria": detalle_catalogo.producto.subcategoria.descripcion,
            "categoria": detalle_catalogo.producto.subcategoria.categoria.descripcion
        }

        detalle_catalogo_dic = {
            "id": detalle_catalogo.id,
            "titulo": detalle_catalogo.titulo,
            "descripcion": detalle_catalogo.descripcion,
            "producto": producto,
            "precio_venta": detalle_catalogo.precio,
            "valoracion": detalle_catalogo.valoracion_promedio,
            "visitas": detalle_catalogo.visitas,
            "estado": detalle_catalogo.get_estado(),
            "etiqueta": detalle_catalogo.etiqueta.descripcion if detalle_catalogo.etiqueta else "",
            "etiqueta_id": detalle_catalogo.etiqueta.id if detalle_catalogo.etiqueta else "",
            "comentarios": comentarios_list,
            "imagenes": imagenes_list
        }

        return detalle_catalogo_dic




    def get(self, request, *args, **kwargs):
        respuesta = RespuestaHTTP()
        servicio = ServicioUsuarios()

        try:

            user = servicio.get_usuario_jwt(request)
            usuario, rol = servicio.get_rol(user)

            if (rol == 'emprendedor'):
                emprendedor = usuario             
                id_detalle = kwargs['id_detalle'] if 'id_detalle' in kwargs else None              

                if id_detalle:
                    respuesta.agregar_dato("detalle", self.get_detalle_catalogo(id_detalle, emprendedor))
                respuesta.tipo_ok()

            else:
                respuesta.tipo_error()
                respuesta.set_mensaje("Solo un emprendedor puede hacer esta operación")

        except Exception as e:
            print(e)
            respuesta.tipo_error()
            respuesta.set_mensaje(str(e))
        return respuesta.get_respuesta_http()


    
    def post(self, request, *args, **kwargs):
        respuesta = RespuestaHTTP()
        servicio = ServicioUsuarios()

        try:

            user = servicio.get_usuario_jwt(request)
            usuario, rol = servicio.get_rol(user)

            if (rol == 'emprendedor'):

                emprendedor = usuario
                json_request = request.POST.dict()
                #json_request = json.loads(request.body.decode('utf-8'))
                

                with transaction.atomic():
                    id_detalle = kwargs['id_detalle'] if 'id_detalle' in kwargs else None

                    try:
                        if not id_detalle:
                            # nuevo detalle
                            detalle_catalogo = DetalleCatalogo()

                            catalogo = json_request.get('catalogo', None)
                            
                            producto = json_request.get('producto', None)
                            estado = json_request.get('estado', DetalleCatalogo.VISIBLE)
                            titulo = json_request.get('titulo', None)
                            descripcion = json_request.get('descripcion', None)
                            precio = json_request.get('precio_venta', None)
                            etiqueta_id = json_request.get('etiqueta', None)
                            etiqueta = None
                            if etiqueta_id:
                                etiqueta = Etiqueta.objects.get(id = etiqueta_id)
                            
                            fotos = request.FILES.getlist('fotosNuevas')                        
                            
                        

                            # validamos si el catálogo es del emprendedor
                            catalogo = Catalogo.objects.filter(id = catalogo) #.filter(emprendedor__id = emprendedor.id)
                            
                            if len(catalogo) > 0:
                                catalogo = catalogo[0]
                                
                                parametros_obligatorios = [catalogo, producto, titulo, descripcion]

                                if validar_no_vacio(parametros_obligatorios):


                                    # Traemos el producto y validamos que sea del emprendedor
                                    producto = Productos.objects.filter(id = producto).filter(id_emprendedor=emprendedor)
                                    if (len(producto) > 0):
                                        producto = producto[0]
                                        if not precio:
                                            # Si no nos pasan el precio, utilizamos el precio del producto por defecto
                                            precio = producto.id_datos_comerciales.precio

                                        
                                        detalle_catalogo.catalogo = catalogo
                                        detalle_catalogo.producto = producto
                                        detalle_catalogo.estado = estado
                                        detalle_catalogo.titulo = titulo
                                        detalle_catalogo.descripcion = descripcion
                                        
                                        detalle_catalogo.precio = precio
                                        detalle_catalogo.valoracion_promedio = 0.0
                                        detalle_catalogo.visitas = 0
                                        detalle_catalogo.etiqueta = etiqueta if etiqueta else None
                                        detalle_catalogo.save()

                                        
                                        
                                        for foto in fotos:                                    
                                            imagen = ImagenesDetalleCatalogo()
                                            imagen.detalle_catalogo = detalle_catalogo
                                            imagen.imagen = foto
                                            imagen.save()

                                        
                                        respuesta.tipo_ok()

                                    else:
                                        respuesta.tipo_error()
                                        respuesta.set_mensaje("El producto no pertenece al microemprendedor")   
                                else:
                                    respuesta.tipo_error()
                                    respuesta.set_mensaje("Faltan campos obligatorios")

                            else:
                                respuesta.tipo_error()
                                respuesta.set_mensaje("El emprendedor no posee el catálogo especificado")


                        else:
                            # Edicion del catálogo
                            detalle_catalogo = DetalleCatalogo.objects.get(id = id_detalle)
                        
                            
                            # Validamos que el detalle pertenezca a un catálogo dell emprendedor logueado
                            #catalogo = Catalogo.objects.filter(id = detalle_catalogo).filter(emprendedor = emprendedor)

                            catalogo = Catalogo.objects.get(id = detalle_catalogo.catalogo.id, emprendedor__id=emprendedor.id)
                        
                            
                            if catalogo:                                
                                
                                estado = json_request.get('estado', DetalleCatalogo.VISIBLE)
                                titulo = json_request.get('titulo', None)
                                descripcion = json_request.get('descripcion', None)
                                precio = json_request.get('precio_venta', None)
                                
                                etiqueta_id = json_request.get('etiqueta', 1)
                                etiqueta_id = json_request.get('etiqueta', None)
                                etiqueta = None
                                if etiqueta_id:
                                    etiqueta = Etiqueta.objects.get(id = etiqueta_id)
                                
                                fotosABorrar = request.POST.getlist('fotosABorrar')
                                fotos = request.FILES.getlist('fotosNuevas')
                                
                                

                                parametros_obligatorios = [titulo, descripcion]

                                if validar_no_vacio(parametros_obligatorios):
                                    detalle_catalogo.estado = estado
                                    detalle_catalogo.titulo = titulo
                                    detalle_catalogo.descripcion = descripcion
                                    detalle_catalogo.precio = precio
                                    detalle_catalogo.etiqueta = etiqueta

                                    imagenes_anteriores = ImagenesDetalleCatalogo.objects.filter(detalle_catalogo=detalle_catalogo)
                                    imgs_anteriores_dict = {}
                                    
                                    
                                    for img in imagenes_anteriores:                                        
                                        imgs_anteriores_dict[img.imagen.url] = img


                                    for img in fotosABorrar:
                                        try:                                                                            
                                            img_borrar = imgs_anteriores_dict.get(img, None)                                        
                                            if img_borrar:
                                                img_borrar.delete()
                                        except:
                                            pass
                                        
                                    

                                    for foto in fotos:
                                        imagen = ImagenesDetalleCatalogo()
                                        imagen.detalle_catalogo = detalle_catalogo
                                        imagen.imagen = foto                                        
                                        imagen.save()

                                    

                                    detalle_catalogo.save()
                                    respuesta.tipo_ok()
                                

                                else:
                                    respuesta.tipo_error()
                                    respuesta.set_mensaje("Faltan campos obligatorios")


                            else:
                                respuesta.tipo_error()
                                respuesta.set_mensaje("El emprendedor no posee el catálogo especificado")



                        
                    except Exception as e:
                        print(e)
                        respuesta.tipo_error()
                        respuesta.set_mensaje(str(e))                    
            else:
                respuesta.tipo_error()
                respuesta.set_mensaje("Solo un emprendedor puede hacer esta operación")
        except Exception as exc:
            respuesta.tipo_error()
            respuesta.set_mensaje(str(exc))
            print(str(exc))

        return respuesta.get_respuesta_http()


    
    def delete(self, request, *args, **kwargs):
        print("ssda")
        respuesta = RespuestaHTTP()
        servicio = ServicioUsuarios()

        try:

            user = servicio.get_usuario_jwt(request)
            usuario, rol = servicio.get_rol(user)

            if (rol == 'emprendedor'):
                emprendedor = usuario             
                id_detalle = kwargs['id_detalle'] if 'id_detalle' in kwargs else None              

                catalogos = Catalogo.objects.filter(emprendedor = emprendedor)
                detalle_catalogo = DetalleCatalogo.objects.get(id = id_detalle)
                estado = False

                for catalogo in catalogos:
                    # Validamos que el detalle sea de un catálogo del emprendedor que consulta
                    if (detalle_catalogo.catalogo.id == catalogo.id):
                        estado = True
                        break

                if estado:
                    comentarios = Comentario.objects.filter(detalle_catalogo = detalle_catalogo)

                    for comentario in comentarios:
                        comentario.delete()
                    
                    detalle_catalogo.delete()

                    respuesta.tipo_ok()

                else:
                    respuesta.tipo_error()
                    respuesta.set_mensaje("Esta publicación no le pertenece")


            else:
                respuesta.tipo_error()
                respuesta.set_mensaje("Solo un emprendedor puede hacer esta operación")

        except Exception as e:
            print(e)
            respuesta.tipo_error()
            respuesta.set_mensaje(str(e))
        return respuesta.get_respuesta_http()



class ComentarioDetalleCatalogoEmprendedorView(View):

    def post(self, request, *args, **kwargs):
        respuesta = RespuestaHTTP()
        servicio = ServicioUsuarios()

        try:

            user = servicio.get_usuario_jwt(request)
            usuario, rol = servicio.get_rol(user)


            if rol != 'anonimo':
                
                json_request = json.loads(request.body.decode('utf-8'))
                id_detalle = json_request.get('id_detalle', None)
                titulo = json_request.get('titulo', None)
                descripcion = json_request.get('descripcion', None)
                

                if validar_no_vacio([id_detalle, titulo, descripcion]):
                    

                    detalle_catalogo = DetalleCatalogo.objects.get(id = id_detalle)
                    perfil = Perfil.objects.all().get(user = user)
                    comentario = Comentario()

                    comentario.detalle_catalogo = detalle_catalogo
                    comentario.fecha_hora_publicacion = time.strftime("%Y-%m-%d %H:%M:%S")
                    comentario.titulo = titulo
                    comentario.descripcion = descripcion
                    comentario.perfil_creador = perfil    

                    comentario.save()

                    respuesta.tipo_ok()

                
            else:
                respuesta.tipo_error()
                respuesta.set_mensaje("Solo usuarios registrados pueden hacer comentarios")

        except Exception as exc:
            respuesta.tipo_error()
            respuesta.set_mensaje(str(exc))
            print(str(exc))

        return respuesta.get_respuesta_http()
    

