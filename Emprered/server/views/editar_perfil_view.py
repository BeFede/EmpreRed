import json
from django.http import HttpResponse, QueryDict
from django.views import View
from rest_framework import viewsets
from django.conf import settings
from Emprered.utils import RespuestaHTTP, validar_no_vacio

from Emprered.server.servicios.servicio_usuarios import ServicioUsuarios
from Emprered.server.models.usuarios import Perfil
from django.db.models.query import RawQuerySet
from django.db import transaction
from django.contrib.auth.models import User
from Emprered.utils import TIPO_AUTENTICACION_EMPRERED

class EditarPerfilView(View):


    def post(self, request, *args, **kwargs):
        respuesta = RespuestaHTTP()
        servicio = ServicioUsuarios()

        try:
            user = servicio.get_usuario_jwt(request)

            perfil = Perfil.objects.all().get(user=user)


            json_request = request.POST.dict()
            respuesta.tipo_error()

            with transaction.atomic():
                foto = request.FILES.get('foto', None)
                # False siempre que haya foto, pero si no vino la foto, no siempre implica borrar.
                borrar_foto = False if foto else json_request.get('borrar_foto', False)
                clave_actual = json_request.get('clave_actual', None)
                nueva_clave = json_request.get('nueva_clave', None)
                repetir_clave = json_request.get('repetir_clave', None)
                username = json_request.get('username', None)

                campos_validar = [username]

                if validar_no_vacio(campos_validar):

                    if nueva_clave:
                        if (user.check_password(clave_actual)):
                            state,msg = servicio.validar_clave(nueva_clave, repetir_clave)
                            if not state:
                                respuesta.tipo_error()
                                respuesta.set_mensaje(msg)
                                return respuesta.get_respuesta_http()
                        else:
                            respuesta.tipo_error()
                            respuesta.set_mensaje("La clave es incorrecta")
                            return respuesta.get_respuesta_http()

                    # Validamos que el username pasado no esté siendo usado

                    if not servicio.validar_username_except(username, user.username):
                            user.username = username
                            if foto or borrar_foto:
                                # Si hay foto -> modificarla
                                # Si borrar_foto es True -> perfil.foto = None
                                # En ese caso foto es None, ergo se borra la foto
                                perfil.foto = foto

                            if (nueva_clave):
                                if (perfil.tipo_registro == TIPO_AUTENTICACION_EMPRERED):
                                    user.set_password(nueva_clave)
                                else:
                                    respuesta.set_mensaje("No se puede setear la clave a un usuario registrado por redes sociales")


                            perfil.save()
                            user.save()

                            usuario, token = servicio.actualizar_token(user)
                            respuesta.agregar_dato("token", "Bearer " + token)
                            respuesta.agregar_dato("usuario", servicio.get_usuario_to_dic(perfil))
                            
                            respuesta.tipo_ok()
                    else:
                        respuesta.tipo_error()
                        respuesta.set_mensaje("El usuario ya está en uso")

        except Exception as exc:
            respuesta.tipo_error()
            respuesta.set_mensaje(str(exc))
            print(str(exc))



        return respuesta.get_respuesta_http()
