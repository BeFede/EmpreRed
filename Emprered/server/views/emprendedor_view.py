import json
import sys

from django.db.models import Sum, Count
from django.http import HttpResponse, QueryDict
from django.views import View
from rest_framework import viewsets
from django.conf import settings

from Emprered.server.models.catalogo import DetalleCatalogo
from Emprered.settings import IMAGEN_PERFIL_DEFECTO
from Emprered.utils import RespuestaHTTP, validar_no_vacio

from Emprered.server.models.emprendedores import Emprendedores, Marcas
from Emprered.server.models.tipificaciones import TiposDocumento, CondicionesFrenteIVA
from Emprered.server.models.productos import Productos

from Emprered.server.models.direccion import Direcciones
from django.db.models.query import RawQuerySet
from django.db import transaction
from Emprered.server.models.usuarios import Perfil
from django.contrib.auth.models import User
from Emprered.server.servicios.servicio_usuarios import ServicioUsuarios


class EmprendedorView(View):

    def get_emprendedores(self, filtros = {}):
        emprendedores = Emprendedores.objects.all()



        emprendedores_dic = []

        for emprendedor in emprendedores:
            perfil = Perfil.objects.get(user__id = emprendedor.id_usuario.id)
            marca = Marcas.objects.all().filter(emprendedor_id = emprendedor)

            if (filtros['nombre']):
                marca = marca.filter(nombre__icontains=filtros['nombre'])

            if len(marca) > 0:
                marca = marca[0]
            else:
                continue





            emprendedor_dic = {
                "id": emprendedor.id,
                "nombre": emprendedor.nombre,
                "apellido": emprendedor.apellido,
                "tipo_documento": emprendedor.id_tipo_documento.id,
                "numero_documento":emprendedor.numero_documento,
                "fecha_nacimiento": emprendedor.fecha_nacimiento,
                "telefono": emprendedor.telefono,
                "condicion_frente_IVA": emprendedor.id_condicion_frente_IVA.id,
                "foto": perfil.foto.url if perfil.foto else IMAGEN_PERFIL_DEFECTO,
                "nombre_marca": marca.nombre if marca else "",
                "descripcion_marca": marca.descripcion if marca else "",
                "foto_marca": marca.logo.url if marca and marca.logo else IMAGEN_PERFIL_DEFECTO,
                "email": perfil.user.email,
                "visitas": DetalleCatalogo.objects.all().filter(catalogo__emprendedor = emprendedor).aggregate(Sum('visitas')).get('visitas__sum', 0)
            }
            emprendedores_dic.append(emprendedor_dic)
        return emprendedores_dic

    def get_emprendedor(self, id_emprendedor):
        emprendedor = Emprendedores.objects.get(id=id_emprendedor)
        perfil = Perfil.objects.get(user__id = emprendedor.id_usuario.id)
        # marca = Marcas.objects.all().get(id_emprendedor = emprendedor)
        emprendedor_dic = {
            "id": emprendedor.id,
            "nombre": emprendedor.nombre,
            "apellido": emprendedor.apellido,
            "tipo_documento": emprendedor.id_tipo_documento.id,
            "numero_documento":emprendedor.numero_documento,
            "fecha_nacimiento": emprendedor.fecha_nacimiento,
            "telefono": emprendedor.telefono,
            "condicion_frente_IVA": emprendedor.id_condicion_frente_IVA.id
        }
        return emprendedor_dic


    def get(self, request, *args, **kwargs):
        respuesta = RespuestaHTTP()
        servicio = ServicioUsuarios()

        action = kwargs.get('action')
        try:
            if (action):

                if (action == 'consultar'):
                    id = kwargs.get('id')
                    respuesta.agregar_dato('emprendedor', self.get_emprendedor(id))
                elif(action == 'perfil'):
                    user = servicio.get_usuario_jwt(request)
                    usuario, rol = servicio.get_rol(user)
                    if (rol == 'emprendedor'):
                        respuesta.agregar_dato('emprendedores', self.get_emprendedor(usuario.id))

            else:
                nombre = request.GET.get('nombre', "")

                filtros = {
                    'nombre': nombre,
                }

                respuesta.agregar_dato('emprendedores', self.get_emprendedores(filtros = filtros))
            respuesta.tipo_ok()
        except Exception as e:
            print(e)
            respuesta.tipo_error()
            respuesta.set_mensaje(str(e))
        return respuesta.get_respuesta_http()


    def post(self, request, *args, **kwargs):
        respuesta = RespuestaHTTP()
        servicio = ServicioUsuarios()

        try:
            user = servicio.get_usuario_jwt(request)
            perfil = Perfil.objects.all().get(user = user)


            #Validamos que el usuario que ingresa no está activo
            if perfil.es_email_confirmado:
                if not perfil.es_activo:

                    json_request = json.loads(request.body.decode('utf-8'))
                    emprendedor = Emprendedores()

                    with transaction.atomic():
                        perfil.es_activo = True
                        nombre = json_request.get('nombre', None)
                        apellido = json_request.get('apellido', None)
                        #id_tipo_documento = json_request.get('tipo_documento', None)
                        numero_documento = json_request.get('numero_documento', None)
                        fecha_nacimiento = json_request.get('fecha_nacimiento', None)
                        telefono = json_request.get('telefono', None)
                        id_direccion = json_request.get('id_direccion', None)
                        id_condicion_frente_IVA = json_request.get('condicion_frente_IVA', None)


                        # En caso de registro o de cambio de direccion
                        #id_ciudad = json_request.get('id_ciudad', None)
                        #codigo_postal = json_request.get('codigo_postal', None)
                        #barrio = json_request.get('barrio', None)
                        #calle = json_request.get('calle', None)
                        #numero_calle = json_request.get('numero_calle', None)


                        campos_validar = [nombre, apellido, numero_documento]

                        if validar_no_vacio(campos_validar):

                            if id_direccion is not None:
                                direccion = Direcciones.objects.get(id=id_direccion)
                            else:
                                direccion = Direcciones()

                            #direccion.id_ciudad = id_ciudad
                            #direccion.codigo_postal = codigo_postal
                            #direccion.barrio = barrio
                            #direccion.numero_calle = numero_calle
                            direccion.save()

                            tipo_documento = TiposDocumento.objects.get(descripcion="CUIL")
                            condicion_frente_IVA = CondicionesFrenteIVA.objects.get(id=id_condicion_frente_IVA)
                            user.save()
                            emprendedor.nombre = nombre
                            emprendedor.apellido = apellido
                            emprendedor.numero_documento = numero_documento
                            emprendedor.id_tipo_documento = tipo_documento
                            emprendedor.fecha_nacimiento = fecha_nacimiento
                            emprendedor.telefono = telefono
                            emprendedor.id_condicion_frente_IVA = condicion_frente_IVA
                            emprendedor.direccion = direccion
                            emprendedor.id_usuario = user
                            perfil.rol = Perfil.EMPRENDEDOR
                            perfil.user.first_name = nombre
                            perfil.user.last_name = apellido
                            perfil.user.save()

                            perfil.save()
                            emprendedor.save()

                            respuesta.tipo_ok()


                            respuesta.agregar_dato("usuario", servicio.get_usuario_to_dic(perfil))

                            respuesta.set_mensaje("Se ha insertado correctamente el nuevo emprendedor, con id {}.".format(emprendedor.id))

                        else:
                            respuesta.tipo_error()
                            respuesta.set_mensaje("Faltan parámetros obligatorios.")
                else:
                    respuesta.tipo_error()
                    respuesta.set_mensaje("El usuario ya se encuentra activo")
            else:
                respuesta.tipo_error()
                respuesta.set_mensaje("La cuenta no ha sido confirmada. No puede registrarse.")

        except Exception as exc:
            print('Error on line {}'.format(sys.exc_info()[-1].tb_lineno))
            respuesta.tipo_error()
            respuesta.set_mensaje(str(exc))

        return respuesta.get_respuesta_http()


    def put(self, request, *args, **kwargs):
        respuesta = RespuestaHTTP()
        servicio = ServicioUsuarios()

        try:
            user = servicio.get_usuario_jwt(request)
            usuario, rol = servicio.get_rol(user)

            if (rol == 'emprendedor'):
                emprendedor = usuario

                json_request = json.loads(request.body.decode('utf-8'))

                with transaction.atomic():
                    #perfil.es_activo = True
                    nombre = json_request.get('nombre', None)
                    apellido = json_request.get('apellido', None)
                    #id_tipo_documento = json_request.get('tipo_documento', None)
                    numero_documento = json_request.get('numero_documento', None)
                    fecha_nacimiento = json_request.get('fecha_nacimiento', None)
                    telefono = json_request.get('telefono', None)
                    id_direccion = json_request.get('id_direccion', None)
                    id_condicion_frente_IVA = json_request.get('condicion_frente_IVA', None)


                    # En caso de registro o de cambio de direccion
                    #id_ciudad = json_request.get('id_ciudad', None)
                    #codigo_postal = json_request.get('codigo_postal', None)
                    #barrio = json_request.get('barrio', None)
                    #calle = json_request.get('calle', None)
                    #numero_calle = json_request.get('numero_calle', None)


                    campos_validar = [nombre, apellido, numero_documento]

                    if validar_no_vacio(campos_validar):

                        if id_direccion is not None:
                            direccion = Direcciones.objects.get(id=id_direccion)
                        else:
                            direccion = Direcciones()

                        #direccion.id_ciudad = id_ciudad
                        #direccion.codigo_postal = codigo_postal
                        #direccion.barrio = barrio
                        #direccion.numero_calle = numero_calle
                        direccion.save()

                        tipo_documento = TiposDocumento.objects.get(descripcion="CUIL")
                        condicion_frente_IVA = CondicionesFrenteIVA.objects.get(id=id_condicion_frente_IVA)
                        user.save()
                        emprendedor.nombre = nombre
                        emprendedor.apellido = apellido
                        emprendedor.numero_documento = numero_documento
                        emprendedor.id_tipo_documento = tipo_documento
                        emprendedor.fecha_nacimiento = fecha_nacimiento
                        emprendedor.telefono = telefono
                        emprendedor.id_condicion_frente_IVA = condicion_frente_IVA
                        emprendedor.direccion = direccion
                        emprendedor.id_usuario = user

                        emprendedor.save()

                        respuesta.tipo_ok()
                        respuesta.set_mensaje("Se ha modificado el emprendedor")

                    else:
                        respuesta.tipo_error()
                        respuesta.set_mensaje("Faltan parámetros obligatorios.")
            else:
                respuesta.tipo_forbidden()
                respuesta.set_mensaje("Solo un emprendedor puede hacer esta operación")

        except json.decoder.JSONDecodeError as e:
            print('Error on line {}'.format(sys.exc_info()[-1].tb_lineno))
            print(e)
            respuesta.tipo_error()
            respuesta.set_mensaje('El json está malformado')

        except Exception as exc:
            print(type(exc))
            print('Error on line {}'.format(sys.exc_info()[-1].tb_lineno))
            respuesta.tipo_error()
            respuesta.set_mensaje(str(exc))

        return respuesta.get_respuesta_http()



    def delete(self, request, *args, **kwargs):
        respuesta = RespuestaHTTP()
        id_emprendedor_a_borrar = kwargs['id_emprendedor'] if 'id_emprendedor' in kwargs else None

        try:
            if id_emprendedor_a_borrar:
                emprendedor = Emprendedores.objects.get(id=id_emprendedor_a_borrar)
                emprendedor.delete()
                respuesta.tipo_ok()
                respuesta.set_mensaje("Se ha borrado correctamente el emprendedor con id {}.".format(id_emprendedor_a_borrar))
            else:
                respuesta.tipo_error()
                respuesta.set_mensaje("Faltan parámetros obligatorios: id_emprendedor")
        except Exception as exc:
            respuesta.tipo_error()
            respuesta.set_mensaje(str(exc))

        return respuesta.get_respuesta_http()
