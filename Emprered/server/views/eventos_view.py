import json
import time
import sys
from django.http import HttpResponse, QueryDict
from django.views import View
from rest_framework import viewsets
from django.conf import settings

from Emprered.server.models.eventos import Eventos, EventosMeta, NotificacionesEvento
from Emprered.server.models.notificaciones import Notificaciones
from Emprered.utils import RespuestaHTTP, validar_no_vacio, Paginador, NotAuthorizedException

from Emprered.server.servicios.servicio_usuarios import ServicioUsuarios

from django.db.models.query import RawQuerySet
from django.db import transaction
import datetime
from django.db.models import Sum
from django.utils.timezone import now

class EventosView(View):

    def get(self, request, *args, **kwargs):
        respuesta = RespuestaHTTP()
        servicio = ServicioUsuarios()
        try:
            user = servicio.get_usuario_jwt(request)
            if user.is_anonymous:
                raise NotAuthorizedException()

            usuario, rol = servicio.get_rol(user)

            if (rol == 'emprendedor'):
                emprendedor = usuario
                id_evento = kwargs.get('id_evento')

                if id_evento:
                    # respuesta.agregar_dato(
                    #     'eventos', self.get_evento(id_produccion, emprendedor)
                    # )
                    respuesta.tipo_error()
                    respuesta.set_mensaje(u'Operación no soportada \U0001F937')
                else:
                    respuesta.agregar_dato(
                        'eventos', self.get_eventos(emprendedor)
                    )
                respuesta.tipo_ok()


            else:
                respuesta.tipo_error()
                respuesta.set_mensaje("Solo un emprendedor puede hacer esta operación")

        except IndexError as ie:
            print('Error on line {}'.format(sys.exc_info()[-1].tb_lineno))
            respuesta.tipo_error()
            respuesta.set_mensaje('Este evento no existe.')
        except Exception as e:
            print(e)
            respuesta.tipo_error()
            respuesta.set_mensaje(str(e))
        return respuesta.get_respuesta_http()


    def get_evento(self, id_evento, emprendedor):
        return []


    def get_eventos(self, emprendedor):

        eventos = Eventos.objects.filter(emprendedor=emprendedor).filter(activo=True)
        eventos_list = []

        for evento in eventos:
            # Vamos a usar únicamente una notificación por evento
            notificaciones = NotificacionesEvento.objects.filter(evento=evento)[0]
            eventos_meta = EventosMeta.objects.filter(evento=evento)
            eventos_meta_list = []

            for meta in eventos_meta:
                meta_dic = {
            		"repeat_start": meta.repeat_start,
            		"repeat_interval": meta.repeat_interval,
            		"repeat_until": meta.repeat_until,
            		"last_repeated": meta.last_repeated,
                }
                eventos_meta_list.append(meta_dic)

            evento_dic = {
                "id": evento.id,
                "nombre": evento.nombre,
                "descripcion": evento.descripcion,
            	"prioridad": evento.prioridad,
                "activo": evento.activo,
            	"enviar_mail": notificaciones.enviar_mail,
            	"enviar_notificacion": notificaciones.enviar_notificacion,
                "eventos_meta": eventos_meta_list,
            }
            eventos_list.append(evento_dic)
        return eventos_list


    def post(self, request, *args, **kwargs):
        respuesta = RespuestaHTTP()
        servicio = ServicioUsuarios()

        try:
            user = servicio.get_usuario_jwt(request)
            if user.is_anonymous:
                raise NotAuthorizedException()
            usuario, rol = servicio.get_rol(user)

            if (rol == 'emprendedor'):
                emprendedor = usuario
                json_request = json.loads(request.body.decode('utf-8'))

                id_evento_a_modificar = kwargs.get("id_evento", -1)

                try:
                    evento = Eventos.objects.get(id=id_evento_a_modificar)
                except Eventos.DoesNotExist:
                    evento = Eventos()


                with transaction.atomic():
                    nombre = json_request.get('nombre')
                    descripcion = json_request.get('descripcion')
                    prioridad = json_request.get('prioridad')
                    enviar_mail = json_request.get('enviar_mail', True)
                    enviar_notificacion = json_request.get('enviar_notificacion', True)
                    eventos_meta = json_request.get('eventos_meta')

                    if validar_no_vacio([nombre, eventos_meta]):

                        evento.nombre = nombre
                        evento.descripcion = descripcion
                        evento.prioridad = prioridad
                        evento.emprendedor = emprendedor
                        evento.activo = True
                        evento.save()

                        # Borro las NotificacionesEvento asociados a este evento
                        NotificacionesEvento.objects.filter(evento = evento).delete()
                        # Para crearlas de nuevo
                        notificaciones = NotificacionesEvento()
                        notificaciones.evento = evento
                        notificaciones.enviar_mail = enviar_mail
                        notificaciones.enviar_notificacion = enviar_notificacion
                        notificaciones.save()

                        # Borro los EventosMeta viejos asociados a este evento
                        EventosMeta.objects.filter(evento = evento).delete()
                        # Y los creo de vuelta
                        for em in eventos_meta:
                            activo = em.get('activo')
                            repeat_start = em.get('repeat_start')
                            repeat_interval = em.get('repeat_interval')
                            repeat_until = em.get('repeat_until')

                            if repeat_start:
                                evento_meta = EventosMeta()
                                evento_meta.evento = evento
                                evento_meta.activo = activo
                                evento_meta.repeat_start = repeat_start
                                evento_meta.repeat_interval = repeat_interval
                                evento_meta.repeat_until = repeat_until
                                evento_meta.save()
                            else:
                                respuesta.tipo_error()
                                respuesta.set_mensaje("Falta un campo obligatorio: ¿cuándo es el evento?")

                        respuesta.tipo_ok()
                    else:
                        respuesta.tipo_error()
                        respuesta.set_mensaje("Faltan campos obligatorios")

            else:
                respuesta.tipo_error()
                respuesta.set_mensaje("Solo un emprendedor puede hacer esta operación")

        except Exception as exc:
            respuesta.tipo_error()
            respuesta.set_mensaje(str(exc))
            print(str(exc))

        return respuesta.get_respuesta_http()


    def delete(self, request, *args, **kwargs):
        respuesta = RespuestaHTTP()
        servicio = ServicioUsuarios()
        id_evento= kwargs.get('id_evento')

        try:
            user = servicio.get_usuario_jwt(request)
            usuario, rol = servicio.get_rol(user)

            if (rol == 'emprendedor'):
                emprendedor = usuario
                if id_evento:
                    evento = Eventos.objects.filter(id=id_evento).filter(emprendedor=emprendedor)[0]
                    if not evento.activo:
                        raise Exception("El evento ya se encuentra borrado.")
                    else:
                        evento.activo = False

                    respuesta.tipo_ok()
                    respuesta.set_mensaje("El evento se ha borrado correctamente")
                    evento.save()
                else:
                    respuesta.tipo_error()
                    respuesta.set_mensaje("Faltan parámetros obligatorios: id_evento")
            else:
                respuesta.tipo_error()
                respuesta.set_mensaje("Solo un emprendedor puede hacer esta operación")
        except Exception as exc:
            respuesta.tipo_error()
            respuesta.set_mensaje(str(exc))

        return respuesta.get_respuesta_http()


    def write_notification(evento):
        notificacion = Notificaciones()

        with transaction.atomic():
            notificacion.titulo = evento.nombre
            notificacion.emprendedor = evento.emprendedor
            notificacion.evento = evento
            notificacion.save()


    def deactivate_events():
        # Si está activo y el repeat_until ya pasó, hay que desactivarlo
        # Esto va a ahorrar tiempo en las queries de check_upcoming_events
        eventos = EventosMeta.objects.exclude(repeat_until__isnull=True)
        eventos = eventos.filter(activo=True).filter(repeat_until__lt=now)

        try:
            with transaction.atomic():
                for evento in eventos:
                    evento.activo = False
                    evento.save()
            return True
        except:
            print('Error on line {}'.format(sys.exc_info()[-1].tb_lineno))
            return False


    def check_upcoming_events():
        # Buscamos los eventos activos y mayores a now()
        ahora = now()
        activos_list = [evt.id for evt in Eventos.objects.filter(activo=True)]
        eventos_meta_activos = EventosMeta.objects.filter(evento__in=activos_list) #.filter(repeat_start__gte=ahora)

        for em in eventos_meta_activos:
            delta = abs(ahora - em.repeat_start)
            # Si es un evento de única vez
            if not em.repeat_interval:
                # Y si ocurrió en los últimos 60 segundos
                if delta < datetime.timedelta(seconds=60):
                    print("Se encontró el evento: {}\n{}".format(em.evento.nombre, em.evento.descripcion))
                    EventosView.write_notification(em.evento)
            else:
                # Si se repetía en los últimos 60 segundos y corresponde con el intervalo
                if delta.total_seconds() % em.repeat_interval < 60:
                    print("Se encontró el evento: {}\n{}".format(em.evento.nombre, em.evento.descripcion))
                    EventosView.write_notification(em.evento)
                    with transaction.atomic():
                        em.last_repeated = ahora
                        em.save()
