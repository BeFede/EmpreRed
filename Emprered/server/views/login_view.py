import json
import jwt
from django.views import View
from Emprered.utils import RespuestaHTTP, validar_no_vacio
from django.contrib.auth.models import User
from django.contrib.auth import authenticate
from Emprered.server.models.usuarios import Perfil
from django.conf import settings
from Emprered.server.servicios.servicio_usuarios import ServicioUsuarios
from Emprered.utils import CLAVE_GOOGLE, TIPO_AUTENTICACION_GOOGLE, TIPO_AUTENTICACION_EMPRERED


class LoginView(View):

    def post(self, request, *args, **kwargs):
        respuesta = RespuestaHTTP()
        json_request = json.loads(request.body.decode('utf-8'))
        servicio = ServicioUsuarios()
        usuario = None
        try:
            tipo_autenticacion = json_request.get('tipo_autenticacion', None)            
            if (tipo_autenticacion == TIPO_AUTENTICACION_EMPRERED):
                nombre_usuario = json_request.get('username', None)
                clave = json_request.get('password', None)                
                usuario, token = servicio.login(nombre_usuario, clave)
            else:
                if (tipo_autenticacion == TIPO_AUTENTICACION_GOOGLE):                    
                    usuario, token = servicio.login_google(request)

            if usuario:
                try:
                    perfil = Perfil.objects.all().get(user = usuario)
                    if perfil.es_email_confirmado:
                        respuesta.agregar_dato("token", "Bearer " + str(token))
                        respuesta.agregar_dato("usuario", servicio.get_usuario_to_dic(perfil))
                        respuesta.tipo_ok()
                        response = respuesta.get_respuesta_http()
                        return response
                    else:
                        respuesta.tipo_error()
                        respuesta.set_mensaje("La cuenta aún no ha sido confirmada")
                        return respuesta.get_respuesta_http()
                except Exception as e:
                    raise e
            else:
                respuesta.tipo_error()
                respuesta.set_mensaje("El usuario o la clave es incorrecto")
                return respuesta.get_respuesta_http()

        except KeyError:
            respuesta.set_mensaje("ERROR")
            return respuesta.get_respuesta_http()

   