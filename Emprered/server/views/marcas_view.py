import json
from django.http import HttpResponse, QueryDict
from django.views import View
from django.core.exceptions import MultipleObjectsReturned
from rest_framework import viewsets
from django.conf import settings
from Emprered.utils import RespuestaHTTP, validar_no_vacio
from Emprered.server.models.emprendedores import Emprendedores, Marcas
from Emprered.server.servicios.servicio_usuarios import ServicioUsuarios

from django.db.models.query import RawQuerySet
from django.db import transaction


class MarcasView(View):

    def get(self, request, *args, **kwargs):
        respuesta = RespuestaHTTP()
        servicio = ServicioUsuarios()

        try:

            user = servicio.get_usuario_jwt(request)
            usuario, rol = servicio.get_rol(user)

            if (rol == 'emprendedor'):
                emprendedor = usuario
                try:
                    marca = Marcas.objects.all().get(emprendedor_id = emprendedor)

                    marca_dic = {
                        "nombre": marca.nombre,
                        "frase": marca.frase,
                        "descripcion": marca.descripcion,
                        "logo": marca.logo.url if marca.logo else ""
                    }

                    respuesta.agregar_dato('marca', marca_dic)
                    respuesta.tipo_ok()

                except Marcas.DoesNotExist:
                    respuesta.tipo_error()
                    respuesta.set_mensaje("El emprendedor no tiene una marca asociada")
                except MultipleObjectsReturned:
                    respuesta.tipo_error()
                    respuesta.set_mensaje("Existe más de una marca para este emprendedor. Esta situación no debería ocurrir jamás.")

            else:
                respuesta.tipo_error()
                respuesta.set_mensaje("Solo un emprendedor puede hacer esta operación")

        except Exception as e:
            print(e)
            respuesta.tipo_error()
            respuesta.set_mensaje(str(e))
        return respuesta.get_respuesta_http()

    def post(self, request, *args, **kwargs):
        respuesta = RespuestaHTTP()
        servicio = ServicioUsuarios()

        try:
            user = servicio.get_usuario_jwt(request)
            usuario, rol = servicio.get_rol(user)

            if (rol == 'emprendedor'):
                emprendedor = usuario
                json_request = request.POST.dict()

                try:
                    marca = Marcas.objects.get(emprendedor_id=emprendedor)
                except Marcas.DoesNotExist:
                    marca = Marcas()

                with transaction.atomic():
                    nombre = json_request.get('nombre', None)
                    frase = json_request.get('frase', None)
                    descripcion = json_request.get('descripcion', None)
                    logo = request.FILES.get('logo', None)

                    campos_validar = [nombre]

                    if validar_no_vacio(campos_validar):

                        marca.nombre = nombre
                        marca.frase = frase
                        marca.descripcion = descripcion
                        marca.emprendedor = emprendedor

                        if logo:
                            marca.logo = logo # Si el user no seleccionó nada, no la cambiamos
                        marca.save()
                        respuesta.tipo_ok()
                    else:
                        respuesta.tipo_error()
                        respuesta.set_mensaje("Faltan campos obligatorios")

            else:
                respuesta.tipo_error()
                respuesta.set_mensaje("Solo un emprendedor puede hacer esta operación")

        except Exception as exc:
            respuesta.tipo_error()
            respuesta.set_mensaje(str(exc))
            print(str(exc))

        return respuesta.get_respuesta_http()
