import json
from django.http import HttpResponse, QueryDict
from django.views import View
from rest_framework import viewsets
from django.conf import settings
from Emprered.utils import RespuestaHTTP, validar_no_vacio

from Emprered.server.models.materias_primas import MateriasPrimas, DatosComercialesMateriaPrima
from Emprered.server.models.tipificaciones import UnidadesMedida, AlicuotasIVA
from Emprered.server.models.emprendedores import Emprendedores
from Emprered.server.servicios.servicio_usuarios import ServicioUsuarios

from django.db.models.query import RawQuerySet
from django.db import transaction


class MateriasPrimasView(View):

    def get_materias_primas(self, id_emprendedor, filtros, orden):

        # Validar que el usuario logueado es un emprendedor y posee el id que intenta consultar
        materias_primas = MateriasPrimas.objects.all().filter(id_emprendedor=id_emprendedor).filter(activo=True) #(id_emprendedor=id_emprendedor)



        if filtros.get('nombre'):
            materias_primas = materias_primas.filter(nombre__icontains = filtros.get('nombre'))
        if filtros.get('observaciones'):
            materias_primas = materias_primas.filter(observaciones__icontains=filtros.get('observaciones'))
        if filtros.get('costo_desde'):
            materias_primas = materias_primas.filter(id_datos_comerciales__costo__gte=filtros.get('costo_desde'))
        if filtros.get('costo_hasta'):
            materias_primas = materias_primas.filter(id_datos_comerciales__costo__lte=filtros.get('costo_hasta'))
        if filtros.get('stock_desde'):
            materias_primas = materias_primas.filter(id_datos_comerciales__stock_actual__gte=filtros.get('stock_desde'))
        if filtros.get('stock_hasta'):
            materias_primas = materias_primas.filter(id_datos_comerciales__stock_actual__lte=filtros.get('stock_hasta'))

        if filtros.get('unidad_medida'):
            materias_primas = materias_primas.filter(id_unidad_medida=filtros.get('unidad_medida'))



        atributo = orden.get('atributo')

        if atributo == 'costo':
            atributo = 'id_datos_comerciales__costo'
        elif atributo == 'stock':
            atributo = 'id_datos_comerciales__stock_actual'

        materias_primas = materias_primas.order_by('{0}{1}'.format(orden.get('criterio'), atributo))


        materias_primas_dic = []
        for materia_prima in materias_primas:

            materia_prima_dic = {
                "id": materia_prima.id,
                "nombre": materia_prima.nombre,
                "unidad_medida": materia_prima.id_unidad_medida.descripcion,
                "unidad_medida_id": materia_prima.id_unidad_medida.id,
                "observaciones": materia_prima.observaciones if materia_prima.observaciones else " ",
                "costo": materia_prima.id_datos_comerciales.costo,
                "stock_actual": materia_prima.id_datos_comerciales.stock_actual,
                "stock_minimo": materia_prima.id_datos_comerciales.stock_minimo
            }

            if filtros.get('stock_bajo'):
                if filtros.get('stock_bajo') == '1':
                    if materia_prima.id_datos_comerciales.stock_actual < materia_prima.id_datos_comerciales.stock_minimo:
                        materias_primas_dic.append(materia_prima_dic)
                elif filtros.get('stock_bajo') == '0':
                    if materia_prima.id_datos_comerciales.stock_actual >= materia_prima.id_datos_comerciales.stock_minimo:
                        materias_primas_dic.append(materia_prima_dic)
            else:
                materias_primas_dic.append(materia_prima_dic)

        return materias_primas_dic

    def get_materia_prima(self, id_materia_prima):
        materia_prima = MateriasPrimas.objects.get(id=id_materia_prima)
        # TODO: Validar si la materia prima corresponde con el emprendedor que consulta
        materia_prima_dic = {
            "id": materia_prima.id,
            "nombre": materia_prima.nombre,
            "unidad_medida": materia_prima.id_unidad_medida.descripcion,
            "observaciones": materia_prima.observaciones if materia_prima.observaciones else " ",
            #"costo": materia_prima.id_datos_comerciales.costo,
            #"stock_actual": materia_prima.id_datos_comerciales.stock_actual,
            "stock_minimo": materia_prima.id_datos_comerciales.stock_minimo
        }
        return materia_prima_dic


    def get(self, request, *args, **kwargs):
        respuesta = RespuestaHTTP()
        servicio = ServicioUsuarios()

        try:

            user = servicio.get_usuario_jwt(request)
            usuario, rol = servicio.get_rol(user)

            if (rol == 'emprendedor'):
                emprendedor = usuario
                id_materia_prima = kwargs['id_materia_prima'] if 'id_materia_prima' in kwargs else None


                if not id_materia_prima:

                    orden = request.GET.get('orden', 'nombre')
                    criterio = request.GET.get('criterio', 'desc')

                    nombre = request.GET.get('nombre', "")
                    observaciones = request.GET.get('observaciones', "")
                    costo_desde = request.GET.get('costo_desde', "")
                    costo_hasta = request.GET.get('costo_hasta', "")
                    stock_desde = request.GET.get('stock_desde', "")
                    stock_hasta = request.GET.get('stock_hasta', "")
                    stock_bajo = request.GET.get('stock_bajo', "")
                    unidad_medida = request.GET.get('unidad_medida', "")

                    filtros = {
                        'nombre': nombre,
                        'observaciones': observaciones,
                        'costo_desde': costo_desde,
                        'costo_hasta': costo_hasta,
                        'stock_desde': stock_desde,
                        'stock_hasta': stock_hasta,
                        'stock_bajo': stock_bajo,
                        'unidad_medida': unidad_medida

                    }

                    criterio = '-' if criterio == 'desc' else ''

                    orden = {
                        'atributo': orden,
                        'criterio': criterio
                    }

                    respuesta.agregar_dato('materias_primas', self.get_materias_primas(emprendedor.id, filtros = filtros, orden = orden))
                else:
                    respuesta.agregar_dato('materia_prima', self.get_materia_prima(id_materia_prima))
                respuesta.tipo_ok()

            else:
                respuesta.tipo_error()
                respuesta.set_mensaje("Solo un emprendedor puede hacer esta operación")

        except Exception as e:
            print(e)
            respuesta.tipo_error()
            respuesta.set_mensaje(str(e))
        return respuesta.get_respuesta_http()


    def put(self, request, *args, **kwargs):
        respuesta = RespuestaHTTP()
        servicio = ServicioUsuarios()

        try:

            user = servicio.get_usuario_jwt(request)
            usuario, rol = servicio.get_rol(user)

            if (rol == 'emprendedor'):

                emprendedor = usuario
                json_request = json.loads(request.body.decode('utf-8'))
                materia_prima_a_modificar = None

                id_materia_prima_a_modificar = kwargs.get('id_materia_prima')

                if id_materia_prima_a_modificar is not None:
                    materia_prima = MateriasPrimas.objects.get(id=id_materia_prima_a_modificar)
                else:
                    materia_prima = MateriasPrimas()


                with transaction.atomic():
                    nombre = json_request.get('nombre', None)
                    id_unidad_medida = json_request.get('unidad_medida_id', None)
                    observaciones = json_request.get('observaciones', " ")
                    stock_minimo = json_request.get('stock_minimo', 0)
                    #costo = json_request.get('costo', None)


                    campos_validar = [nombre]

                    if validar_no_vacio(campos_validar):

                        datos_comerciales = None
                        try:
                            datos_comerciales = materia_prima.id_datos_comerciales
                        except:
                            pass

                        if not datos_comerciales:
                            datos_comerciales  = DatosComercialesMateriaPrima()
                            datos_comerciales.costo = 0
                            datos_comerciales.stock_actual = 0

                        datos_comerciales.stock_minimo = stock_minimo
                        datos_comerciales.save()


                        unidad_medida = UnidadesMedida.objects.get(id=id_unidad_medida)

                        materia_prima.id_emprendedor = emprendedor
                        materia_prima.nombre = nombre

                        materia_prima.id_unidad_medida = unidad_medida
                        materia_prima.observaciones = observaciones
                        materia_prima.id_datos_comerciales = datos_comerciales
                        materia_prima.save()

                        respuesta.tipo_ok()

                        if id_materia_prima_a_modificar is not None:
                            respuesta.set_mensaje("Se ha modificado correctamente la materia prima con id {}.".format(id_materia_prima_a_modificar))
                        else:
                            respuesta.set_mensaje("Se ha insertado la materia prima correctamente, con id {}.".format(materia_prima.id))
                    else:
                        respuesta.tipo_error()
                        print('faltan')
                        respuesta.set_mensaje("Faltan parámetros obligatorios")
            else:
                respuesta.tipo_error()
                respuesta.set_mensaje("Solo un emprendedor puede hacer esta operación")
        except Exception as exc:
            respuesta.tipo_error()
            respuesta.set_mensaje(str(exc))
            print(str(exc))

        return respuesta.get_respuesta_http()

    def delete(self, request, *args, **kwargs):
        respuesta = RespuestaHTTP()
        servicio = ServicioUsuarios()
        id_materia_prima_a_borrar = kwargs.get('id_materia_prima')

        try:
            user = servicio.get_usuario_jwt(request)
            usuario, rol = servicio.get_rol(user)

            if (rol == 'emprendedor'):
                emprendedor = usuario
                if id_materia_prima_a_borrar:
                    materia_prima = MateriasPrimas.objects.filter(id= id_materia_prima_a_borrar).filter(id_emprendedor=emprendedor)[0]
                    materia_prima.activo = False

                    respuesta.tipo_ok()
                    respuesta.set_mensaje("La materia prima se ha borrado correctamente")
                    materia_prima.save()
                else:
                    respuesta.tipo_error()
                    respuesta.set_mensaje("Faltan parámetros obligatorios: id_materia_prima")
            else:
                respuesta.tipo_error()
                respuesta.set_mensaje("Solo un emprendedor puede hacer esta operación")
        except Exception as exc:
            respuesta.tipo_error()
            respuesta.set_mensaje(str(exc))

        return respuesta.get_respuesta_http()
