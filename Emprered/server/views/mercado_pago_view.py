from decimal import Decimal

from django.db import transaction
from django.shortcuts import redirect
from django.views import View

from Emprered.server.models.catalogo import DetalleCatalogo, Catalogo
from Emprered.server.models.emprendedores import Emprendedores
from Emprered.server.models.usuarios import Perfil
from Emprered.server.models.venta import Venta, Pago, DetallesVenta, Tarjeta
from Emprered.server.servicios.servicio_mercado_pago import ServicioMercadoPago
from Emprered.utils import RespuestaHTTP
from Emprered.server.servicios.servicio_usuarios import ServicioUsuarios
import mercadopago
import json
import sys
from Emprered.settings import APP_ID_MERCADO_PAGO, SECRET_KEY_MERCADO_PAGO, DEBUG, WEB_PATH


class MercadoPagoView(View):

    def get(self, request, *args, **kwargs):
        respuesta = RespuestaHTTP()
        servicio = ServicioUsuarios()
        mercado_pago_servicio = ServicioMercadoPago()

        resultado_pago = kwargs.get('resultado_pago')

        try:
            if (resultado_pago == 'exito'):

                collection_id = request.GET.get('collection_id')
                collection_status = request.GET.get('collection_status')
                preference_id = request.GET.get('preference_id')
                payment_type = request.GET.get('payment_type')
                merchant_order_id = request.GET.get('merchant_order_id')

                with transaction.atomic():
                    pago = Pago.objects.all().get(id_preferencia=preference_id)

                    transaccion_mercado_pago = mercado_pago_servicio.consultar_transaccion(pago.venta)


                    pago.id_collection = collection_id
                    pago.estado_collection = collection_status
                    pago.id_orden_mercado_pago = merchant_order_id
                    pago.metodo_pago = payment_type
                    pago.estado = Pago.REALIZADO

                    pago.monto = transaccion_mercado_pago.get('monto_final')

                    tarjeta_data = transaccion_mercado_pago.get('tarjeta')

                    tarjeta = Tarjeta()

                    tarjeta.tipo = tarjeta_data['tipo']
                    tarjeta.ultimos_cuatro_digitos = tarjeta_data['ultimos_cuatro_digitos']

                    tarjeta.dni_titular = tarjeta_data['titular']['dni']
                    tarjeta.nombre_titular = tarjeta_data['titular']['nombre']

                    tarjeta.save()
                    pago.tarjeta = tarjeta
                    pago.save()

                    pago.venta.estado = Venta.CONFIRMADA
                    pago.venta.save()

                    detalle_venta = DetallesVenta.objects.filter(venta = pago.venta)

                    if (len(detalle_venta) > 0):
                        detalle_venta = detalle_venta[0]


                    url_exito = pago.url_exito + "?estado=ok&&id_orden=" + str(pago.id_orden_mercado_pago)+ "&id_detalle=" + str(detalle_venta.id) + "&pago_mobile=" + str(pago.pago_mobile)

                    if (url_exito):
                        return redirect(url_exito)
                    else:
                        respuesta.tipo_ok()
                        respuesta.set_mensaje("La venta y el pago se registraron con éxito")



            elif (resultado_pago == 'error'):
                preference_id = request.GET.get('preference_id')
                pago = Pago.objects.all().get(id_preferencia=preference_id)
                pago.venta.estado = Venta.CANCELADA
                pago.venta.save()
                url_error = pago.url_error + "?estado=error&mensaje=Ha ocurrido un error, la venta se ha cancelado&pago_mobile=" + str(pago.pago_mobile)
                if (url_error):
                    return redirect(url_error)
                else:
                    respuesta.tipo_error()
                    respuesta.set_mensaje("Ha ocurrido un error. La venta se ha cancelado")

                #redirect(url_error)

            else:
                print("error")
        except Exception as e:
            print(e)
            print('Error on line {}'.format(sys.exc_info()[-1].tb_lineno))
        return respuesta.get_respuesta_http()


    def post(self, request, *args, **kwargs):
        respuesta = RespuestaHTTP()
        servicio = ServicioUsuarios()
        return respuesta.get_respuesta_http()


    def put(self, request, *args, **kwargs):
        respuesta = RespuestaHTTP()
        servicio = ServicioUsuarios()
        return respuesta.get_respuesta_http()



    def delete(self, request, *args, **kwargs):
        respuesta = RespuestaHTTP()
        return respuesta.get_respuesta_http()



