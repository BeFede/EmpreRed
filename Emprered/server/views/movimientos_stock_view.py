import json
from django.http import HttpResponse, QueryDict
from django.views import View
from rest_framework import viewsets
from django.conf import settings
from Emprered.utils import RespuestaHTTP, validar_no_vacio, NotAuthorizedException
from Emprered.server.servicios.servicio_usuarios import ServicioUsuarios
from Emprered.server.models.movimientos_stock import MovimientosStock, registrar_movimiento_stock
from Emprered.server.models.productos import Productos
from Emprered.server.models.materias_primas import MateriasPrimas
from decimal import Decimal

from django.db.models.query import RawQuerySet
from django.db import transaction
import time

import io
from django.http import FileResponse
from reportlab.pdfgen import canvas


class MovimientosStockView(View):


    def get_movimiento_stock(self, id_movimiento_stock, emprendedor):

        movimiento_stock = MovimientosStock.objects.filter(id = id_movimiento_stock).filter(emprendedor=emprendedor)

        if movimiento_stock:
            movimiento_stock = movimiento_stock[0]
            movimiento_stock_dic = {
                "id": movimiento_stock.id,
                "fecha_hora": movimiento_stock.fecha_hora,
                "producto": movimiento_stock.producto.nombre if movimiento_stock.producto else None,
                "materia_prima": movimiento_stock.materia_prima.nombre if movimiento_stock.materia_prima else None,
                "cantidad": movimiento_stock.cantidad,
                "motivo": movimiento_stock.motivo,
                "tipo_movimiento": MovimientosStock.TIPOS_MOVIMIENTOS[movimiento_stock.tipo_movimiento-1][1],
                "unidad_medida": movimiento_stock.producto.id_unidad_medida.descripcion if movimiento_stock.producto else movimiento_stock.materia_prima.id_unidad_medida.descripcion
            }
            return movimiento_stock_dic
        return []

    def get_movimientos_stock(self, emprendedor, filtros, orden):

        movimientos_stock = MovimientosStock.objects.filter(emprendedor = emprendedor)

        if filtros.get('numero'):
            movimientos_stock = movimientos_stock.filter(id=filtros.get('numero'))
        if filtros.get('motivo'):
            movimientos_stock = movimientos_stock.filter(motivo__icontains=filtros.get('motivo'))
        if filtros.get('fecha_desde'):
            movimientos_stock = movimientos_stock.filter(fecha_hora__gte=filtros.get('fecha_desde'))
        if filtros.get('fecha_hasta'):
            movimientos_stock = movimientos_stock.filter(fecha_hora__lte=filtros.get('fecha_hasta'))
        if filtros.get('tipo_movimiento'):
            movimientos_stock = movimientos_stock.filter(tipo_movimiento=filtros.get('tipo_movimiento'))

        if filtros.get('producto'):
            movimiento_stock_temp = []
            for movimiento_stock in movimientos_stock:

                if movimiento_stock.producto and Decimal(movimiento_stock.producto.id) == Decimal(filtros.get('producto')):
                    movimiento_stock_temp.append(movimiento_stock)
                    
            movimientos_stock = movimientos_stock.filter(id__in=[mov.id for mov in movimiento_stock_temp])


        if filtros.get('materia_prima'):
            movimiento_stock_temp = []
            for movimiento_stock in movimientos_stock:

                if movimiento_stock.materia_prima and Decimal(movimiento_stock.materia_prima.id) == Decimal(filtros.get('materia_prima')):
                    movimiento_stock_temp.append(movimiento_stock)

            movimientos_stock = movimientos_stock.filter(id__in=[mov.id for mov in movimiento_stock_temp])

        movimientos_stock = movimientos_stock.order_by('{0}{1}'.format(orden.get('criterio'), orden.get('atributo')))

        movimientos_stock_list = []

        for movimiento_stock in movimientos_stock:
            movimiento_stock_dic = {
                "id": movimiento_stock.id,
                "fecha_hora": movimiento_stock.fecha_hora,
                "producto": movimiento_stock.producto.nombre if movimiento_stock.producto else None,
                "materia_prima": movimiento_stock.materia_prima.nombre if movimiento_stock.materia_prima else None,
                "cantidad": movimiento_stock.cantidad,
                "motivo": movimiento_stock.motivo,
                "tipo_movimiento": MovimientosStock.TIPOS_MOVIMIENTOS[movimiento_stock.tipo_movimiento-1][1],
                "unidad_medida": movimiento_stock.producto.id_unidad_medida.descripcion if movimiento_stock.producto else movimiento_stock.materia_prima.id_unidad_medida.descripcion
            }
            movimientos_stock_list.append(movimiento_stock_dic)

        return movimientos_stock_list


    def get(self, request, *args, **kwargs):
        respuesta = RespuestaHTTP()
        servicio = ServicioUsuarios()

        try:

            user = servicio.get_usuario_jwt(request)
            if user.is_anonymous:
                raise NotAuthorizedException()
            usuario, rol = servicio.get_rol(user)

            if (rol == 'emprendedor'):
                emprendedor = usuario
                id_movimiento_stock = kwargs['id_movimiento_stock'] if 'id_movimiento_stock' in kwargs else None

                if not id_movimiento_stock:

                    orden = request.GET.get('orden', 'id')
                    criterio = request.GET.get('criterio', 'desc')
                    numero = request.GET.get('numero', "")
                    motivo = request.GET.get('motivo', "")
                    producto = request.GET.get('producto', "")
                    tipo_movimiento = request.GET.get('tipo_movimiento', "")
                    materia_prima = request.GET.get('materia_prima', "")
                    fecha_desde = request.GET.get('fecha_desde', "")
                    fecha_hasta = request.GET.get('fecha_hasta', "")

                    filtros = {
                        'numero': numero,
                        'motivo': motivo,
                        'producto': producto,
                        'materia_prima': materia_prima,
                        'fecha_desde': fecha_desde,
                        'fecha_hasta': fecha_hasta,
                        'tipo_movimiento': tipo_movimiento
                    }

                    criterio = '-' if criterio == 'desc' else ''

                    orden = {
                        'atributo': orden,
                        'criterio': criterio
                    }

                    respuesta.agregar_dato('movimientos_stock', self.get_movimientos_stock(emprendedor, filtros = filtros, orden = orden))
                else:
                    respuesta.agregar_dato('movimiento_stock', self.get_movimiento_stock(id_movimiento_stock, emprendedor))
                respuesta.tipo_ok()

            else:
                respuesta.tipo_forbidden()
                respuesta.set_mensaje("Solo un emprendedor puede hacer esta operación")

        except Exception as e:
            print(e)
            respuesta.tipo_error()
            respuesta.set_mensaje(str(e))
        return respuesta.get_respuesta_http()


    def post(self, request, *args, **kwargs):
        respuesta = RespuestaHTTP()
        servicio = ServicioUsuarios()

        try:

            user = servicio.get_usuario_jwt(request)
            usuario, rol = servicio.get_rol(user)

            if (rol == 'emprendedor'):

                emprendedor = usuario
                json_request = json.loads(request.body.decode('utf-8'))

                with transaction.atomic():
                    producto = None
                    materia_prima = None

                    fecha_hora = time.strftime("%Y-%m-%d %H:%M:%S")
                    producto_id = json_request.get('producto_id', None)
                    materia_prima_id = json_request.get('materia_prima_id', None)
                    cantidad = json_request.get('cantidad', None)
                    tipo_movimiento = json_request.get('tipo_movimiento', None)
                    motivo = json_request.get('motivo', None)

                    # Si mandan el id buscamos el producto
                    producto = Productos.objects.filter(id=producto_id).filter(id_emprendedor = emprendedor) if producto_id else None
                    # si se encuentra el producto lo guardamos en la variable
                    if producto:
                        producto = producto[0]
                    else :
                        materia_prima = MateriasPrimas.objects.filter(id=materia_prima_id).filter(id_emprendedor=emprendedor) if materia_prima_id else None
                            # Si no se recibe el producto entonces se recibe una materia prima
                        if materia_prima:
                            materia_prima = materia_prima[0]
                        else:
                            respuesta.tipo_error()
                            respuesta.set_mensaje("Faltan parámetros obligatorios --")
                            return respuesta.get_respuesta_http()

                    if validar_no_vacio([cantidad, tipo_movimiento]):
                        cantidad = Decimal(cantidad)
                        # Esto no me gusta pero Nico insistió en pasar el string

                        for key, value in MovimientosStock.TIPOS_MOVIMIENTOS:
                            # Buscamos la clave que corresponde al valor pasado por parámetro
                            if (value == tipo_movimiento):
                                tipo_movimiento_key = key
                                break

                        if  not tipo_movimiento_key:
                            respuesta.tipo_error()
                            respuesta.set_mensaje("El tipo de movimiento no es válido")
                            return respuesta.get_respuesta_http()

                        """
                        movimiento_stock = MovimientosStock()
                        movimiento_stock.fecha_hora = fecha_hora
                        movimiento_stock.emprendedor = emprendedor
                        movimiento_stock.materia_prima = materia_prima
                        movimiento_stock.producto = producto
                        movimiento_stock.motivo = motivo
                        movimiento_stock.tipo_movimiento = tipo_movimiento_key

                        movimiento_stock.save()
                        """
                        exito, mensaje = registrar_movimiento_stock(emprendedor, fecha_hora, producto, materia_prima, cantidad, tipo_movimiento_key, motivo)

                        respuesta.tipo_ok() if exito else respuesta.tipo_error()
                        respuesta.set_mensaje(mensaje)

                    else:
                        respuesta.tipo_error()
                        respuesta.set_mensaje("Faltan parámetros obligatorios")
                        return respuesta.get_respuesta_http()


            else:
                respuesta.tipo_forbidden()
                respuesta.set_mensaje("Solo un emprendedor puede hacer esta operación")
        except Exception as exc:
            respuesta.tipo_error()
            respuesta.set_mensaje(str(exc))
            print(str(exc))

        return respuesta.get_respuesta_http()

    """
    def delete(self, request, *args, **kwargs):
        respuesta = RespuestaHTTP()
        servicio = ServicioUsuarios()
        id_compra_a_borrar = kwargs.get('id_compra', None)

        if id_compra_a_borrar:
            try:
                user = servicio.get_usuario_jwt(request)
                usuario, rol = servicio.get_rol(user)

                if (rol == 'emprendedor'):
                    emprendedor = usuario
                    compra = Compra.objects.filter(id=id_compra_a_borrar).filter(emprendedor=emprendedor)[0]
                    # No hace falta buscar los detalles porque se borrarán en cascada al borrar la compra.
                    compra.delete()
                    respuesta.tipo_ok()
                    respuesta.set_mensaje("Se ha borrado correctamente la compra con id {}".format(id_compra_a_borrar))
                else:
                    respuesta.tipo_error()
                    respuesta.set_mensaje("Solo un emprendedor puede hacer esta operación")
            except IndexError as ie:
                respuesta.tipo_error()
                respuesta.set_mensaje('No posee una compra con id {}'.format(id_compra_a_borrar))
            except Exception as exc:
                respuesta.tipo_error()
                respuesta.set_mensaje(str(exc))
        else:
            respuesta.tipo_error()
            respuesta.set_mensaje("Faltan parámetros obligatorios.")

        return respuesta.get_respuesta_http()
    """
