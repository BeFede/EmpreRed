import json
import time
import sys
from django.http import HttpResponse, QueryDict
from django.views import View
from rest_framework import viewsets
from django.conf import settings

from Emprered.server.models.notificaciones import Notificaciones
from Emprered.utils import RespuestaHTTP, validar_no_vacio, Paginador, NotAuthorizedException

from Emprered.server.servicios.servicio_usuarios import ServicioUsuarios

from django.db.models.query import RawQuerySet
from django.db import transaction
import datetime
from django.db.models import Sum
from django.utils.timezone import timedelta, now

class NotificacionesView(View):

    def get(self, request, *args, **kwargs):
        respuesta = RespuestaHTTP()
        servicio = ServicioUsuarios()
        try:
            user = servicio.get_usuario_jwt(request)
            if user.is_anonymous:
                raise NotAuthorizedException()

            usuario, rol = servicio.get_rol(user)

            if (rol == 'emprendedor'):
                emprendedor = usuario
                id_notificacion = kwargs.get('id_notificacion')

                if id_notificacion:
                    # respuesta.agregar_dato(
                    #     'notificaciones', self.get_notificacion(id_notificacion, emprendedor)
                    # )
                    respuesta.tipo_error()
                    respuesta.set_mensaje(u'Operación no soportada \U0001F937')
                else:
                    respuesta.agregar_dato(
                        'notificaciones', self.get_notificaciones(emprendedor)
                    )
                respuesta.tipo_ok()


            else:
                respuesta.tipo_error()
                respuesta.set_mensaje("Solo un emprendedor puede hacer esta operación")

        except IndexError as ie:
            print('Error on line {}'.format(sys.exc_info()[-1].tb_lineno))
            respuesta.tipo_error()
            respuesta.set_mensaje('Esta notificación no existe.')
        except Exception as e:
            print('Error on line {}'.format(sys.exc_info()[-1].tb_lineno))
            print(e)
            respuesta.tipo_error()
            respuesta.set_mensaje(str(e))
        return respuesta.get_respuesta_http()


    def get_notificacion(self, id_notificacion, emprendedor):
        return []


    def get_notificaciones(self, emprendedor):

        notificaciones = Notificaciones.objects.filter(
            emprendedor=emprendedor
        ).filter( # traemos todas las que aún no fueron vistas
            fecha_hora_vista__isnull=True)
        notificaciones_list = []

        for n in notificaciones:
            notificacion_dic = {
                "id": n.id,
                "titulo": n.titulo,
                "fecha_hora_creacion": n.fecha_hora_creacion,
                "fecha_hora_vista": n.fecha_hora_vista,
            }
            notificaciones_list.append(notificacion_dic)
        return notificaciones_list


    def post(self, request, *args, **kwargs):
        respuesta = RespuestaHTTP()
        servicio = ServicioUsuarios()

        try:
            user = servicio.get_usuario_jwt(request)
            if user.is_anonymous:
                raise NotAuthorizedException()
            usuario, rol = servicio.get_rol(user)

            if (rol == 'emprendedor'):
                emprendedor = usuario

                id_notificacion = kwargs.get("id_notificacion", -1)

                try:
                    notificacion = Notificaciones.objects.get(id=id_notificacion)
                except Notificaciones.DoesNotExist:
                    raise Exception("La notificación con id {} no existe.".format(id_notificacion))

                with transaction.atomic():
                    notificacion.fecha_hora_vista = now()
                    notificacion.save()

                respuesta.tipo_ok()
            else:
                respuesta.tipo_error()
                respuesta.set_mensaje("Solo un emprendedor puede hacer esta operación")

        except Exception as exc:
            respuesta.tipo_error()
            respuesta.set_mensaje(str(exc))
            print(str(exc))

        return respuesta.get_respuesta_http()


    def delete_old_notifications(days):
        delete_date = now() - timedelta(days=days)
        Notificaciones.objects.filter(fecha_hora_vista__lt=delete_date).delete()
        return True
