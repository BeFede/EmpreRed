import json
from django.http import HttpResponse, QueryDict
from django.views import View
from rest_framework import viewsets
from django.conf import settings
from Emprered.utils import RespuestaHTTP, validar_no_vacio, NotAuthorizedException
from Emprered.server.servicios.servicio_usuarios import ServicioUsuarios
from Emprered.server.models.planes_produccion import PlanProduccion, DetallePlanProduccion
from Emprered.server.models.productos import Productos
from Emprered.server.models.materias_primas import MateriasPrimas

from django.db.models.query import RawQuerySet
from django.db import transaction


class PlanProduccionView(View):


    def get_plan_produccion(self, producto):

        plan_produccion = PlanProduccion.objects.get(producto = producto)

        detalles_plan_produccion = DetallePlanProduccion.objects.filter(plan_produccion = plan_produccion)

        detalles_plan_produccion_dic = []

        for detalle_plan_produccion in detalles_plan_produccion:
            detalle_plan_produccion_dic = {
                "id": detalle_plan_produccion.id,
                "materia_prima": detalle_plan_produccion.materia_prima.nombre,
                "materia_prima_id": detalle_plan_produccion.materia_prima.id,
                "unidad_medida": detalle_plan_produccion.materia_prima.id_unidad_medida.descripcion,
                "unidad_medida_id": detalle_plan_produccion.materia_prima.id_unidad_medida.id,
                "cantidad": detalle_plan_produccion.cantidad,
                "costo_merma": detalle_plan_produccion.costo_merma
            }
            detalles_plan_produccion_dic.append(detalle_plan_produccion_dic)

        plan_produccion_dic = {
            "producto" : plan_produccion.producto.nombre,
            "costo_mano_obra" : plan_produccion.costo_mano_obra,
            "detalles" : detalles_plan_produccion_dic
        }

        return plan_produccion_dic


    def get(self, request, *args, **kwargs):
        respuesta = RespuestaHTTP()
        servicio = ServicioUsuarios()

        try:

            user = servicio.get_usuario_jwt(request)
            if user.is_anonymous:
                raise NotAuthorizedException()
            usuario, rol = servicio.get_rol(user)

            if (rol == 'emprendedor'):
                emprendedor = usuario
                producto = kwargs.get('id_producto')

                if producto:
                    respuesta.agregar_dato('plan_produccion', self.get_plan_produccion(producto))
                    respuesta.tipo_ok()
                else:
                    respuesta.tipo_error()
                    respuesta.set_mensaje("No se ha especificado el producto")

            else:
                respuesta.tipo_error()
                respuesta.set_mensaje("Solo un emprendedor puede hacer esta operación")

        except Exception as e:
            print(e)
            respuesta.tipo_error()
            respuesta.set_mensaje(str(e))
        return respuesta.get_respuesta_http()


    def post(self, request, *args, **kwargs):
        respuesta = RespuestaHTTP()
        servicio = ServicioUsuarios()
        try:
            user = servicio.get_usuario_jwt(request)
            if user.is_anonymous:
                raise NotAuthorizedException()
            usuario, rol = servicio.get_rol(user)

            if (rol == 'emprendedor'):

                emprendedor = usuario
                json_request = json.loads(request.body.decode('utf-8'))

                with transaction.atomic():
                    id_producto = json_request.get('id_producto', None)
                    costo_mano_obra = json_request.get('costo_mano_obra', None)
                    descripcion = json_request.get('descripcion', None)
                    detalles = json_request.get('detalles', None)

                    campos_validar = [id_producto]

                    if validar_no_vacio(campos_validar):
                        # Busco el producto con el id que nos pasan y valido de que pertenezca
                        # al emprendedor registrado
                        try:
                            producto = Productos.objects.filter(id = id_producto).filter(id_emprendedor = emprendedor)[0]
                        except Exception:
                            respuesta.tipo_error()
                            respuesta.set_mensaje("Error. El producto no existe")

                        # Si el producto ya tiene asignado un plan de producción lo editamos
                        # De lo contrairio se lo creamos
                        try:
                            plan_produccion_producto = PlanProduccion.objects.get(producto = producto)
                        except PlanProduccion.DoesNotExist:
                            plan_produccion_producto = PlanProduccion()

                        plan_produccion_producto.producto = producto
                        plan_produccion_producto.costo_mano_obra = costo_mano_obra
                        plan_produccion_producto.descripcion = descripcion
                        plan_produccion_producto.save()

                        DetallePlanProduccion.objects.filter(plan_produccion = plan_produccion_producto).delete()

                        for detalle in detalles:

                            id_materia_prima = detalle.get('materia_prima')
                            costo_merma = detalle.get('costo_merma')
                            cantidad = detalle.get('cantidad')

                            if validar_no_vacio([id_materia_prima, cantidad]):

                                detalle_plan_produccion = DetallePlanProduccion()
                                try:
                                    # Busco la materia prima con el id que nos pasan y valido de que pertenezca
                                    # al emprendedor registrado
                                    materia_prima = MateriasPrimas.objects.filter(id = id_materia_prima).filter(id_emprendedor=emprendedor)[0]
                                    detalle_plan_produccion.plan_produccion = plan_produccion_producto
                                    detalle_plan_produccion.materia_prima = materia_prima
                                    detalle_plan_produccion.cantidad = cantidad
                                    detalle_plan_produccion.costo_merma = costo_merma
                                    detalle_plan_produccion.save()

                                except Exception as e:
                                    respuesta.tipo_error()
                                    print(e)
                                    respuesta.set_mensaje("Error. La materia prima no existe")
                                respuesta.tipo_ok()
                                plan_produccion_producto.save()


                            else:
                                respuesta.tipo_error()
                                respuesta.set_mensaje("Faltan parámetros obligatorios")

                    else:
                        respuesta.tipo_error()
                        respuesta.set_mensaje("Faltan parámetros obligatorios")

            else:
                respuesta.tipo_error()
                respuesta.set_mensaje("Solo un emprendedor puede hacer esta operación")
        except Exception as exc:
            respuesta.tipo_error()
            respuesta.set_mensaje(str(exc))
            print(str(exc))

        return respuesta.get_respuesta_http()
