import json
import time
import sys
from decimal import Decimal
import re
from django.http import HttpResponse, QueryDict
from django.views import View
from rest_framework import viewsets
from django.conf import settings
from Emprered.utils import RespuestaHTTP, validar_no_vacio, NotAuthorizedException
from Emprered.server.servicios.servicio_usuarios import ServicioUsuarios
from Emprered.server.models.producciones import Produccion, DetalleProduccion
from Emprered.server.models.planes_produccion import PlanProduccion, DetallePlanProduccion
from Emprered.server.models.productos import Productos
from Emprered.server.models.movimientos_stock import MovimientosStock, registrar_movimiento_stock
from datetime import datetime
from django.db.models.query import RawQuerySet
from django.db import transaction


class ProduccionesView(View):

    def get_produccion(self, id_produccion, emprendedor):

        produccion = Produccion.objects.filter(id=id_produccion).filter(emprendedor=emprendedor)[0]

        detalles_produccion = DetalleProduccion.objects.filter(produccion=produccion)

        detalles_produccion_list = []

        for detalle_produccion in detalles_produccion:
            detalle_produccion_dic = {
                "id": detalle_produccion.id,
                "producto": detalle_produccion.producto.nombre,
                "producto_id": detalle_produccion.producto.id,
                "unidad_medida": detalle_produccion.producto.id_unidad_medida.descripcion,
                "cantidad": detalle_produccion.cantidad,
                #"costo_merma": detalle_produccion.costo_merma
            }
            detalles_produccion_list.append(detalle_produccion_dic)

        # Obtengo la lista de tuplas de (estado_id, estado_descripcion)
        estados = produccion._meta.get_field('estado').flatchoices
        # La convierto a un diccionario {estado_id: estado_descripcion}
        estados_dict = dict((key, value) for (key, value) in estados)

        produccion_dic = {
            "descripcion": produccion.descripcion,
            "fecha_hora": produccion.fecha_hora,
            "estado_id": produccion.estado or 1,
            "estado_descripcion": estados_dict.get(produccion.estado, 'Pendiente'),
            "hashtags": [hashtag.name for hashtag in produccion.hashtags.all()],
            "detalles": detalles_produccion_list
        }

        return produccion_dic

    def get_producciones(self, emprendedor, filtros = {}, orden = {}):

        producciones = Produccion.objects.filter(emprendedor = emprendedor)

        if filtros.get('numero'):
            producciones = producciones.filter(id=filtros.get('numero'))
        if filtros.get('descripcion'):
            producciones = producciones.filter(descripcion__icontains=filtros.get('descripcion'))
        if filtros.get('fecha_desde'):
            producciones = producciones.filter(fecha_hora__gte = filtros.get('fecha_desde'))
        if filtros.get('fecha_hasta'):
            producciones = producciones.filter(fecha_hora__lte=filtros.get('fecha_hasta'))
        if filtros.get('estado'):
            producciones = producciones.filter(estado=filtros.get('estado'))


        if filtros.get('producto'):
            producciones_temp = []
            for produccion in producciones:
                detalles = DetalleProduccion.objects.all().filter(produccion=produccion)

                for detalle in detalles:

                    if Decimal(detalle.producto.id) == Decimal(filtros.get('producto')):
                        producciones_temp.append(produccion)
                        break
            producciones = producciones.filter(id__in=[prod.id for prod in producciones_temp ])

        if filtros.get('materia_prima'):
            producciones_temp = []
            for produccion in producciones:
                detalles = DetalleProduccion.objects.all().filter(produccion=produccion)

                for detalle in detalles:
                    if detalle.producto.es_fabricado:
                        plan_produccion = PlanProduccion.objects.get(producto=detalle.producto)
                        detalles_plan_produccion = DetallePlanProduccion.objects.filter(plan_produccion=plan_produccion)

                        for detalle_pp in detalles_plan_produccion:
                            if Decimal(detalle_pp.materia_prima.id) == Decimal(filtros.get('materia_prima')):
                                producciones_temp.append(produccion)
                                break
            producciones = producciones.filter(id__in=[prod.id for prod in producciones_temp])


        producciones = producciones.order_by('{0}{1}'.format(orden.get('criterio'), orden.get('atributo')))

        produccion_list = []

        """Obtengo la lista de tuplas de (estado_id, estado_descripcion).
        Acá agarro la primera, total todas tienen el mismo modelo, pero uso
        el try por si no hay ninguna produccion, en cuyo caso habría IndexError.
        """
        try:
            estados = producciones[0]._meta.get_field('estado').flatchoices
            # La convierto a un diccionario {estado_id: estado_descripcion}
            estados_dict = dict((key, value) for (key, value) in estados)
        except Exception as e:
            pass

        for produccion in producciones:
            produccion_dic = {
                "id": produccion.id,
                "descripcion": produccion.descripcion,
                "estado_id": produccion.estado or 1,
                "estado_descripcion": estados_dict.get(produccion.estado, 'Pendiente'),
                "hashtags": [hashtag.name for hashtag in produccion.hashtags.all()],
                "fecha_hora": produccion.fecha_hora,
            }
            produccion_list.append(produccion_dic)

        return produccion_list

    def get(self, request, *args, **kwargs):
        respuesta = RespuestaHTTP()
        servicio = ServicioUsuarios()

        try:
            user = servicio.get_usuario_jwt(request)
            usuario, rol = servicio.get_rol(user)

            if (rol == 'emprendedor'):
                emprendedor = usuario
                id_produccion = kwargs.get('id_produccion', None)

                if not id_produccion:
                    orden = request.GET.get('orden', 'id')
                    criterio = request.GET.get('criterio', 'desc')
                    numero = request.GET.get('numero', "")
                    descripcion = request.GET.get('descripcion', "")
                    producto = request.GET.get('producto', "")
                    materia_prima = request.GET.get('materia_prima', "")
                    fecha_desde = request.GET.get('fecha_desde', "")
                    fecha_hasta = request.GET.get('fecha_hasta', "")
                    estado = request.GET.get('estado', "")

                    filtros = {
                        'numero': numero,
                        'descripcion': descripcion,
                        'producto': producto,
                        'materia_prima': materia_prima,
                        'fecha_desde': fecha_desde,
                        'fecha_hasta': fecha_hasta,
                        'estado': estado,
                    }

                    criterio = '-' if criterio == 'desc' else ''

                    orden = {
                        'atributo': orden,
                        'criterio': criterio
                    }

                    respuesta.agregar_dato('producciones', self.get_producciones(emprendedor, filtros= filtros, orden = orden))
                else:
                    respuesta.agregar_dato('produccion', self.get_produccion(id_produccion, emprendedor))
                respuesta.tipo_ok()

            else:
                respuesta.tipo_error()
                respuesta.set_mensaje("Solo un emprendedor puede hacer esta operación")

        except IndexError as ie:
            respuesta.tipo_error()
            respuesta.set_mensaje('No posee una producción con id {}'.format(id_produccion))
        except Exception as e:
            print(e)
            respuesta.tipo_error()
            respuesta.set_mensaje(str(e))
        return respuesta.get_respuesta_http()



    def post(self, request, *args, **kwargs):
        respuesta = RespuestaHTTP()
        servicio = ServicioUsuarios()
        try:
            user = servicio.get_usuario_jwt(request)
            if user.is_anonymous:
                raise NotAuthorizedException()

            usuario, rol = servicio.get_rol(user)

            if (rol == 'emprendedor'):

                emprendedor = usuario
                json_request = json.loads(request.body.decode('utf-8'))

                with transaction.atomic():

                    id_produccion = json_request.get('id_produccion', None)
                    fecha_hora = datetime.now() #time.strftime("%Y-%m-%d %H:%M:%S")

                    descripcion = json_request.get('descripcion', None)
                    nuevo_estado = json_request.get('estado', None)
                    detalles = json_request.get('detalles', [])
                    estado_anterior = ""

                    # Si hay id_produccion es porque hay que modificar una ya existente
                    if id_produccion:
                        try:
                            produccion = Produccion.objects.get(id = id_produccion)
                            estado_anterior = produccion.estado
                            # Verificamos si el cambio de estado es válido
                            cambio_estado, mensaje = produccion.set_estado(nuevo_estado)

                            if not cambio_estado:
                                respuesta.tipo_error()
                                respuesta.set_mensaje(mensaje)
                                return respuesta.get_respuesta_http()

                        except Produccion.DoesNotExist as e:
                            respuesta.tipo_error()
                            respuesta.set_mensaje("La producción con id {} no existe".format(id_produccion))
                            print('Error on line {}'.format(sys.exc_info()[-1].tb_lineno))
                            return respuesta.get_respuesta_http()


                    else: # Si no hay id, es una produccion nueva
                        # si es una producción nueva seteamos la fecha, si no
                        # dejamos la otra. De paso la inicializamos en pendiente
                        produccion = Produccion()
                        produccion.fecha_hora = fecha_hora
                        produccion.set_estado(Produccion.PENDIENTE)


                    produccion.emprendedor = emprendedor
                    produccion.descripcion = descripcion

                    produccion.save() # keep on reading
                    """¿Por qué llamo dos veces a save()?
                    Porque TaggableManager.set() requiere acceder a los tags del
                    objeto producción, y este objeto no está aún creado (la primera)
                    vez que genero la producción.
                    Por lo tanto, lo guardo con save() para que ya esté creado,
                    después le seteo los hashtags y finalmente lo guardo otra vez.
                    """
                    # Obtenemos los hashtags de la descripcion
                    hashtags = re.findall(r'#\w+', descripcion)
                    # set() borra los hashtags que estaban antes pero ahora no,
                    # y añade los que no estaban pero ahora sí. Hace un update piola, digamos.
                    produccion.hashtags.set(*hashtags)
                    produccion.save()


                    # A partir de acá están verificado los estados asi que solo analizamos el nuevo estado


                    # Si el nuevo estado es pendiente, entonces se registran/modifican los detalles
                    if (produccion.estado == Produccion.PENDIENTE or (produccion.estado == Produccion.EN_PROCESO and estado_anterior == Produccion.PENDIENTE)):
                        exito_detalles, mensaje = produccion.actualizar_detalles(detalles)

                        if not exito_detalles:
                            raise Exception(mensaje)


                    # Ya tenemos el estado y los detalles de la producción actualizados.
                    # Ahora llevamos a cabo los movimientos de stock correspondientes según el estado en cuestión


                    # En estado PENDIENTE no se realizan movimientos de stock
                    # Es estado EN PROCESO se da de baja la materia prima en cuestión
                    # En estado TERMINADA se da de alta los productos fabricados
                    # En estado ANULADA se hacen movimientos de stock para hacer rollback al mismo



                    if produccion.estado == Produccion.PENDIENTE:
                        # No hacemos movimientos de stock
                        respuesta.tipo_ok()

                    elif produccion.estado == Produccion.EN_PROCESO:
                        if (estado_anterior == Produccion.PENDIENTE):
                            # Tenemos que dar de baja la materia prima involucrada
                            # Validamos disponibilidad de materia prima
                            hay_stock, mensaje =  produccion.validar_disponibilidad_materia_prima()

                            if not hay_stock:
                                raise Exception(mensaje)


                            # Hay stock, procedemos a registrar los movimientos correspondientes
                            detalles_produccion = DetalleProduccion.objects.filter(produccion=produccion)

                            for dp in detalles_produccion:
                                producto = Productos.objects.filter(id=dp.producto.id).filter(id_emprendedor=emprendedor)[0]
                                plan_produccion = PlanProduccion.objects.get(producto=producto)
                                detalles_plan_produccion = DetallePlanProduccion.objects.filter(plan_produccion=plan_produccion)
                                for detalle in detalles_plan_produccion:
                                    exito, mensaje = registrar_movimiento_stock(emprendedor, fecha_hora, None, detalle.materia_prima, (dp.cantidad * detalle.cantidad), MovimientosStock.EGRESO, "Producción")
                                    if not exito:
                                        raise Exception(mensaje)
                            respuesta.tipo_ok()

                    elif produccion.estado == Produccion.TERMINADA:
                        # Si ya estaba terminada de antemano, no tenemos que mover stock, no quieren pinchilear
                        # Solo si la producción estaba en proceso y pasa a terminada, se hacen los mov
                        if estado_anterior == Produccion.EN_PROCESO:
                            detalles_produccion = DetalleProduccion.objects.filter(produccion=produccion)
                            for dp in detalles_produccion:
                                # Tenemos que dar de alta los productos fabricados
                                #producto = Productos.objects.filter(id=dp.producto.id).filter(id_emprendedor=emprendedor)[0]
                                exito, mensaje = registrar_movimiento_stock(emprendedor, fecha_hora, dp.producto, None, dp.cantidad, MovimientosStock.INGRESO, "Producción")
                                if not exito:
                                    raise Exception(mensaje)
                        respuesta.tipo_ok()

                    elif produccion.estado == Produccion.ANULADA:

                        if (estado_anterior == Produccion.PENDIENTE):
                            # No hago, pues no hice ningún mov de stock
                            respuesta.tipo_ok()
                        elif (estado_anterior == Produccion.ANULADA):
                            # No hago, pues la producción ya está anulada
                            respuesta.tipo_ok()
                        elif (estado_anterior == Produccion.EN_PROCESO):
                            # Ingresamos toda la materia prima que habíamos egresado
                            detalles_produccion = DetalleProduccion.objects.filter(produccion=produccion)

                            for dp in detalles_produccion:
                                producto = Productos.objects.filter(id=dp.producto.id).filter(id_emprendedor=emprendedor)[0]
                                plan_produccion = PlanProduccion.objects.get(producto=producto)
                                detalles_plan_produccion = DetallePlanProduccion.objects.filter(plan_produccion=plan_produccion)
                                for detalle in detalles_plan_produccion:
                                    exito, mensaje = registrar_movimiento_stock(emprendedor, fecha_hora, None, detalle.materia_prima, (dp.cantidad * detalle.cantidad), MovimientosStock.INGRESO, "Anulación de producción")
                                    if not exito:
                                        raise Exception(mensaje)
                            respuesta.tipo_ok()
                        else:
                            # Desde Terminada no se puede pasar a anulada
                            respuesta.tipo_error()

            else:
                respuesta.tipo_error()
                respuesta.set_mensaje("Solo un emprendedor puede hacer esta operación")
        except Exception as exc:
            respuesta.tipo_error()
            respuesta.set_mensaje(str(exc))
            print('Error on line {}'.format(sys.exc_info()[-1].tb_lineno))

        return respuesta.get_respuesta_http()

    def delete(self, request, *args, **kwargs):
        respuesta = RespuestaHTTP()
        servicio = ServicioUsuarios()
        id_produccion_a_borrar = kwargs.get('id_produccion', None)

        if id_produccion_a_borrar:
            try:
                user = servicio.get_usuario_jwt(request)
                if user.is_anonymous:
                    raise NotAuthorizedException()
                usuario, rol = servicio.get_rol(user)

                if (rol == 'emprendedor'):
                    emprendedor = usuario
                    produccion = Produccion.objects.filter(id=id_produccion_a_borrar).filter(emprendedor=emprendedor)[0]
                    # No hace falta buscar los detalles porque se borrarán en cascada al borrar la produccion.
                    produccion.delete()
                    respuesta.tipo_ok()
                    respuesta.set_mensaje("Se ha borrado correctamente la producción con id {}".format(id_produccion_a_borrar))
                else:
                    respuesta.tipo_error()
                    respuesta.set_mensaje("Solo un emprendedor puede hacer esta operación")
            except IndexError as ie:
                respuesta.tipo_error()
                respuesta.set_mensaje('No posee una producción con id {}'.format(id_produccion_a_borrar))
            except Exception as exc:
                respuesta.tipo_error()
                respuesta.set_mensaje(str(exc))

        else:
            respuesta.tipo_error()
            respuesta.set_mensaje("Faltan parámetros obligatorios")

        return respuesta.get_respuesta_http()
