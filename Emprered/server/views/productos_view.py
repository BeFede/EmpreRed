import json
import sys
from django.http import HttpResponse, QueryDict
from django.views import View
from rest_framework import viewsets
from django.conf import settings
from Emprered.utils import RespuestaHTTP, validar_no_vacio
from Emprered.server.models.productos import Productos, DatosComercialesProducto
from Emprered.server.models.tipificaciones import UnidadesMedida, AlicuotasIVA
from Emprered.server.models.emprendedores import Emprendedores
from Emprered.server.models.categorias import Categoria, Subcategoria
from Emprered.server.servicios.servicio_usuarios import ServicioUsuarios

from django.db.models.query import RawQuerySet
from django.db import transaction


class ProductosView(View):

    def get_productos(self, id_emprendedor, filtros, orden):
        # Validar que el usuario logueado es un emprendedor y posee el id que intenta consultar
        productos = Productos.objects.all().filter(id_emprendedor=id_emprendedor).filter(activo=True) #(id_emprendedor=id_emprendedor)

        if filtros.get('nombre'):
            productos = productos.filter(nombre__icontains = filtros.get('nombre'))

        if filtros.get('observaciones'):
            productos = productos.filter(observaciones__icontains=filtros.get('observaciones'))

        if filtros.get('costo_desde'):
            productos = productos.filter(id_datos_comerciales__costo__gte=filtros.get('costo_desde'))

        if filtros.get('costo_hasta'):
            productos = productos.filter(id_datos_comerciales__costo__lte=filtros.get('costo_hasta'))

        if filtros.get('stock_desde'):
            productos = productos.filter(id_datos_comerciales__stock_actual__gte=filtros.get('stock_desde'))

        if filtros.get('stock_hasta'):
            productos = productos.filter(id_datos_comerciales__stock_actual__lte=filtros.get('stock_hasta'))

        if filtros.get('unidad_medida'):
            productos = productos.filter(id_unidad_medida=filtros.get('unidad_medida'))

        if filtros.get('es_fabricado'):
            productos = productos.filter(es_fabricado=filtros.get('es_fabricado'))

        """1. Si me pasan subcategorias: filtro por esas subcategorias.
        2. Si me pasan categorias: filtro por las subcategorias de esas categorias.
        3. Si me pasan subcategorias AND categorias: filtro por la unión de 1 y 2."""
        subcategorias = []

        if filtros.get('subcategorias'):
            subcategorias = filtros.get('subcategorias', [])
        if filtros.get('categorias'):
            subcategorias_objs = Subcategoria.objects.filter(categoria__in=filtros.get('categorias', []))
            ids_subcategorias = [ subcategoria.id for subcategoria in subcategorias_objs ]
            subcategorias = list(set(subcategorias + ids_subcategorias))

        if subcategorias:
            productos = productos.filter(subcategoria__in=subcategorias)

        atributo = orden.get('atributo')

        if atributo == 'costo':
            atributo = 'id_datos_comerciales__costo'
        elif atributo == 'stock':
            atributo = 'id_datos_comerciales__stock_actual'

        productos = productos.order_by('{0}{1}'.format(orden.get('criterio'), atributo))

        productos_list = []
        for producto in productos:
            producto_dic = {
                "id": producto.id,
                "nombre": producto.nombre,
                "foto": producto.foto.url if producto.foto else "",
                "categoria_id": producto.id_categoria.id,
                "id_subcategoria": producto.subcategoria.id,
                "subcategoria": producto.subcategoria.descripcion,
                "id_categoria": producto.subcategoria.categoria.id,
                "categoria": producto.subcategoria.categoria.descripcion,
                "unidad_medida": producto.id_unidad_medida.descripcion,
                "unidad_medida_id": producto.id_unidad_medida.id,
                "cantidad_mayorista": producto.cantidad_mayorista,
                "es_fabricado": producto.es_fabricado,
                "observaciones": producto.observaciones if producto.observaciones else "",
                "costo": producto.id_datos_comerciales.costo,
                "stock_actual": producto.id_datos_comerciales.stock_actual,
                "stock_minimo": producto.id_datos_comerciales.stock_minimo,
                "precio": producto.id_datos_comerciales.precio,
                #"utilidad": producto.id_datos_comerciales.utilidad,
            }


            if filtros.get('stock_bajo'):
                if filtros.get('stock_bajo') == '1':
                    if producto.id_datos_comerciales.stock_actual < producto.id_datos_comerciales.stock_minimo:
                        productos_list.append(producto_dic)
                elif filtros.get('stock_bajo') == '0':
                    if producto.id_datos_comerciales.stock_actual >= producto.id_datos_comerciales.stock_minimo:
                        productos_list.append(producto_dic)
            else:
                productos_list.append(producto_dic)


        return productos_list


    def get_producto(self, id_producto):
        producto = Productos.objects.get(id=id_producto)
        producto_dic = {
            "id": producto.id,
            "nombre": producto.nombre,
            "foto": producto.foto.url if producto.foto else "",
            "id_subcategoria": producto.subcategoria.id,
            "subcategoria": producto.subcategoria.descripcion,
            "id_categoria": producto.subcategoria.categoria.id,
            "categoria": producto.subcategoria.categoria.descripcion,
            "unidad_medida": producto.id_unidad_medida.descripcion,
            "cantidad_mayorista": producto.cantidad_mayorista,
            "es_fabricado": producto.es_fabricado,
            "observaciones": producto.observaciones if producto.observaciones else "" ,
            #"costo": producto.id_datos_comerciales.costo,
            #"stock_actual": producto.id_datos_comerciales.stock_actual,
            "stock_minimo": producto.id_datos_comerciales.stock_minimo,
            "precio": producto.id_datos_comerciales.precio,
            #"utilidad": producto.id_datos_comerciales.utilidad,
        }
        return producto_dic


    def get(self, request, *args, **kwargs):

        respuesta = RespuestaHTTP()
        servicio = ServicioUsuarios()

        try:
            user = servicio.get_usuario_jwt(request)
            usuario, rol = servicio.get_rol(user)
            if (rol == 'emprendedor'):
                emprendedor = usuario
                id_producto = kwargs.get('id_producto')

                if not id_producto: # traigo todos

                    orden = request.GET.get('orden', 'nombre')
                    criterio = request.GET.get('criterio', '-')

                    nombre = request.GET.get('nombre', "")
                    observaciones = request.GET.get('observaciones', "")
                    costo_desde = request.GET.get('costo_desde', "")
                    costo_hasta = request.GET.get('costo_hasta', "")
                    stock_desde = request.GET.get('stock_desde', "")
                    stock_hasta = request.GET.get('stock_hasta', "")
                    stock_bajo = request.GET.get('stock_bajo', "")
                    unidad_medida = request.GET.get('unidad_medida', "")
                    es_fabricado = request.GET.get('es_fabricado', "")

                    subcategorias = request.GET.get('subcategorias', [])
                    if subcategorias:
                        subcategorias = subcategorias.split(",")

                    categorias = request.GET.get('categorias', [])
                    if categorias:
                        categorias = categorias.split(",")

                    filtros = {
                        'nombre': nombre,
                        'observaciones': observaciones,
                        'costo_desde': costo_desde,
                        'costo_hasta': costo_hasta,
                        'stock_desde': stock_desde,
                        'stock_hasta': stock_hasta,
                        'stock_bajo': stock_bajo,
                        'unidad_medida': unidad_medida,
                        'es_fabricado': es_fabricado,
                        'categorias': [ int(c) for c in categorias if c.isdigit() ],
                        'subcategorias': [ int(sc) for sc in subcategorias if sc.isdigit() ],
                    }


                    criterio = '-' if criterio == 'desc' else ''

                    orden = {
                        'atributo': orden,
                        'criterio': criterio
                    }

                    respuesta.agregar_dato('productos', self.get_productos(emprendedor.id, filtros=filtros, orden=orden))
                    respuesta.tipo_ok()
                else: # si hay un id definido, traigo solo ese
                    respuesta.agregar_dato('producto', self.get_producto(id_producto))
                    respuesta.tipo_ok()

            else:
                respuesta.tipo_error()
                respuesta.set_mensaje("Solo un emprendedor puede hacer esta operación")
        except Exception as e:
            print(e)
            print('Error on line {}'.format(sys.exc_info()[-1].tb_lineno))
            respuesta.tipo_error()
            respuesta.set_mensaje(str(e))
        return respuesta.get_respuesta_http()



    def post(self, request, *args, **kwargs):
        respuesta = RespuestaHTTP()
        servicio = ServicioUsuarios()

        try:
            user = servicio.get_usuario_jwt(request)
            usuario, rol = servicio.get_rol(user)

            if (rol == 'emprendedor'):
                emprendedor = usuario
                """ Uso request.POST y no request.body porque este último trae el archivo de foto,
                lo cual es un grave problema ya que no puede ser decodeado como el resto de los campos.
                En cambio, a la foto la sacamos de request.FILES.
                GOTCHA: en todos los valores booleanos vamos a tener que hacer lo siguiente para parsearlo
                de manera correcta:
                    foo = json_request.get('boolean_value', None) == 'true'
                De este modo si el boolean_value era 'true' {str}, foo será True {bool},
                y False {bool} de otro modo.
                """
                json_request = request.POST.dict()

                producto_a_modificar = None
                id_producto_a_modificar = kwargs.get('id_producto')

                # Dejo el "is not None" por el extraño caso en el que venga un id 0,
                # el cual es falsy. De todos modos mysql es 1-based con los ids.
                if id_producto_a_modificar is not None:
                    producto = Productos.objects.get(id=id_producto_a_modificar)
                else:
                    producto = Productos()

                with transaction.atomic():
                    nombre = json_request.get('nombre', None)
                    foto = request.FILES.get('foto', None)
                    # False siempre que haya foto, pero si no vino la foto, no siempre implica borrar.
                    borrar_foto = False if foto else json_request.get('borrar_foto', False)


                    # Dejo esto por las dudas luego de un merge
                    id_alicuota_IVA = json_request.get('alicuota_iva_id', 1)
                    id_categoria = json_request.get('categoria_id', None)


                    id_subcategoria = json_request.get('id_subcategoria', None)

                    id_unidad_medida = json_request.get('unidad_medida_id', None)
                    cantidad_mayorista = json_request.get('cantidad_mayorista', None)
                    # De esta forma le asignamos un valor booleano, ya que
                    # request.POST.dict() lo trae como 'false' o 'true', y el
                    # ORM de Django se queda esperando un True o False

                    es_fabricado = json_request.get('es_fabricado', None) == 'true'

                    observaciones = json_request.get('observaciones', None)
                    #costo = json_request.get('costo', None)
                    #stock_actual = json_request.get('stock_actual', None)
                    stock_minimo = json_request.get('stock_minimo', None)
                    precio = json_request.get('precio', None)

                    campos_validar = [nombre, id_unidad_medida, es_fabricado]

                    if validar_no_vacio(campos_validar):

                        datos_comerciales = None

                        try:
                            datos_comerciales = producto.id_datos_comerciales
                        except:
                            pass

                        if not datos_comerciales:
                            datos_comerciales = DatosComercialesProducto()

                        # Si es nuevo el producto seteasmo en 0 los datos comerciales
                        if not id_producto_a_modificar:
                            datos_comerciales.costo = 0
                            datos_comerciales.stock_actual = 0
                            datos_comerciales.utilidad = 0

                        datos_comerciales.precio= precio
                        datos_comerciales.stock_minimo = stock_minimo
                        datos_comerciales.save()

                        # Guarda en tabla Productos

                        unidad_medida = UnidadesMedida.objects.get(id=id_unidad_medida)
                        subcategoria = Subcategoria.objects.get(id=id_subcategoria)

                        producto.id_emprendedor = emprendedor
                        producto.nombre = nombre
                        if foto or borrar_foto:
                            # Si hay foto -> modificarla
                            # Si borrar_foto es True -> perfil.foto = None
                            # En ese caso foto es None, ergo se borra la foto
                            producto.foto = foto

                        producto.id_unidad_medida = unidad_medida
                        producto.subcategoria = subcategoria
                        producto.cantidad_mayorista = cantidad_mayorista
                        producto.es_fabricado = es_fabricado
                        producto.observaciones = observaciones
                        producto.id_datos_comerciales = datos_comerciales

                        producto.save()

                        respuesta.tipo_ok()

                        if id_producto_a_modificar is not None:
                            respuesta.set_mensaje("Se ha modificado correctamente el producto con id {}.".format(id_producto_a_modificar))
                        else:
                            respuesta.set_mensaje("Se ha insertado correctamente el nuevo producto, con id {}.".format(producto.id))

            else:
                respuesta.tipo_error()
                respuesta.set_mensaje("Solo un emprendedor puede hacer esta operación")

        except Exception as exc:
            print('Error on line {}'.format(sys.exc_info()[-1].tb_lineno))
            respuesta.tipo_error()
            respuesta.set_mensaje(str(exc))
            print(str(exc))

        return respuesta.get_respuesta_http()


    def put(self, request, *args, **kwargs):
        respuesta = RespuestaHTTP()
        servicio = ServicioUsuarios()

        try:
            user = servicio.get_usuario_jwt(request)
            usuario, rol = servicio.get_rol(user)

            if (rol == 'emprendedor'):
                emprendedor = usuario
                json_request = json.loads(request.body.decode('utf-8'))
                producto_a_modificar = None
                id_producto_a_modificar = kwargs['id_producto'] if 'id_producto' in kwargs else None

                # Dejo el "is not None" por el extraño caso en el que venga un id 0,
                # el cual es falsy. De todos modos mysql es 1-based con los ids.
                if id_producto_a_modificar is not None:
                    producto = Productos.objects.get(id=id_producto_a_modificar)
                else:
                    producto = Productos()

                with transaction.atomic():
                    nombre = json_request.get('nombre', None)
                    # foto = request.FILES.get('foto', None)

                    id_alicuota_IVA = json_request.get('alicuota_iva_id', 1)
                    id_categoria = json_request.get('categoria_id', None)


                    id_subcategoria = json_request.get('id_subcategoria', None)

                    id_unidad_medida = json_request.get('unidad_medida_id', None)
                    cantidad_mayorista = json_request.get('cantidad_mayorista', None)
                    es_fabricado = json_request.get('es_fabricado', None)
                    observaciones = json_request.get('observaciones', None)
                    # costo = json_request.get('costo', None)
                    # stock_actual = json_request.get('stock_actual', None)
                    stock_minimo = json_request.get('stock_minimo', None)
                    precio = json_request.get('precio', None)
                    # utilidad = json_request.get('utilidad', None)

                    campos_validar = [nombre, id_unidad_medida, es_fabricado]

                    if validar_no_vacio(campos_validar):

                        # Guarda en tabla DatosComercialesProducto
                        try:
                            datos_comerciales = DatosComercialesProducto.objects.filter(producto = producto)[id]
                        except Exception as e:
                            print(str(e))
                        print(datos_comerciales)
                        # Si es nuevo el producto seteasmo en 0 los datos comerciales
                        if not id_producto_a_modificar:
                            datos_comerciales = DatosComercialesProducto()
                            datos_comerciales.costo = 0
                            datos_comerciales.stock_actual = 0
                            datos_comerciales.utilidad = 0
                        datos_comerciales.precio= precio
                        datos_comerciales.stock_minimo = stock_minimo
                        datos_comerciales.save()

                        # Guarda en tabla Productos

                        unidad_medida = UnidadesMedida.objects.get(id=id_unidad_medida)

                        producto.id_emprendedor = emprendedor
                        producto.nombre = nombre
                        producto.foto = None

                        producto.id_unidad_medida = unidad_medida
                        producto.subcategoria = id_subcategoria
                        producto.cantidad_mayorista = cantidad_mayorista
                        producto.es_fabricado = es_fabricado
                        producto.observaciones = observaciones
                        producto.id_datos_comerciales = datos_comerciales

                        producto.save()

                        respuesta.tipo_ok()

                        if id_producto_a_modificar is not None:
                            respuesta.set_mensaje("Se ha modificado correctamente el producto con id {}.".format(id_producto_a_modificar))
                        else:
                            respuesta.set_mensaje("Se ha insertado correctamente el nuevo producto, con id {}.".format(producto.id))

            else:
                respuesta.tipo_error()
                respuesta.set_mensaje("Solo un emprendedor puede hacer esta operación")

        except Exception as exc:
            print('Error on line {}'.format(sys.exc_info()[-1].tb_lineno))
            respuesta.tipo_error()
            respuesta.set_mensaje(str(exc))
            print(str(exc))

        return respuesta.get_respuesta_http()

    def delete(self, request, *args, **kwargs):
        respuesta = RespuestaHTTP()
        servicio = ServicioUsuarios()
        id_producto_a_borrar = kwargs['id_producto'] if 'id_producto' in kwargs else None

        try:
            user = servicio.get_usuario_jwt(request)
            usuario, rol = servicio.get_rol(user)

            if (rol == 'emprendedor'):
                emprendedor = usuario
                if id_producto_a_borrar:
                    producto = Productos.objects.filter(id=id_producto_a_borrar).filter(id_emprendedor=emprendedor)[0]
                    producto.activo = False
                    producto.save()
                    respuesta.tipo_ok()
                    respuesta.set_mensaje("Se ha borrado correctamente el producto con id {}.".format(id_producto_a_borrar))
                else:
                    respuesta.tipo_error()
                    respuesta.set_mensaje("Faltan parámetros obligatorios: id_producto")
            else:
                respuesta.tipo_error()
                respuesta.set_mensaje("Solo un emprendedor puede hacer esta operación")
        except Exception as exc:
            print('Error on line {}'.format(sys.exc_info()[-1].tb_lineno))
            respuesta.tipo_error()
            respuesta.set_mensaje(str(exc))

        return respuesta.get_respuesta_http()
