import json
import sys

from django.db.models import Sum

from django.views import View

from Emprered.server.models.emprendedores import Emprendedores
from Emprered.utils import RespuestaHTTP, validar_no_vacio

from Emprered.server.models.proveedores import Proveedores
from Emprered.server.models.tipificaciones import TiposDocumento
from django.db import transaction
from Emprered.server.servicios.servicio_usuarios import ServicioUsuarios


class ProveedoresView(View):

    def get_proveedores(self, id_emprendedor):
        proveedores = Proveedores.objects.filter(emprendedor=id_emprendedor)
        proveedores_dic = []

        for proveedor in proveedores:

            proveedor_dic = {
                "id": proveedor.id,
                "nombre": proveedor.nombre,
                "apellido": proveedor.apellido,
                "tipo_documento": proveedor.id_tipo_documento.id,
                "numero_documento":proveedor.numero_documento,
                "telefono": proveedor.telefono,
                "direccion": proveedor.direccion
            }
            proveedores_dic.append(proveedor_dic)
        return proveedores_dic

    def get_proveedor(self, id_emprendedor, id_proveedor):
        proveedor = Proveedores.objects.get(id=id_proveedor)

        if (proveedor.emprendedor.id == id_emprendedor):

            proveedor_dic = {
                "id": proveedor.id,
                "nombre": proveedor.nombre,
                "apellido": proveedor.apellido,
                "tipo_documento": proveedor.id_tipo_documento.id,
                "numero_documento":proveedor.numero_documento,
                "telefono": proveedor.telefono,
                "direccion": proveedor.direccion
            }
        else:
            proveedor_dic = []
        return proveedor_dic


    def get(self, request, *args, **kwargs):
        respuesta = RespuestaHTTP()
        servicio = ServicioUsuarios()


        try:

            user = servicio.get_usuario_jwt(request)
            usuario, rol = servicio.get_rol(user)

            emprendedor = usuario

            id_proveedor = kwargs.get('id_proveedor')

            if (id_proveedor):
                respuesta.agregar_dato('proveedor', self.get_proveedor(emprendedor.id, id_proveedor))
            else:
                respuesta.agregar_dato('proveedores', self.get_proveedores(emprendedor.id))
            respuesta.tipo_ok()
        except Exception as e:
            print(e)
            respuesta.tipo_error()
            respuesta.set_mensaje(str(e))
        return respuesta.get_respuesta_http()


    def post(self, request, *args, **kwargs):
        respuesta = RespuestaHTTP()
        servicio = ServicioUsuarios()

        try:
            user = servicio.get_usuario_jwt(request)
            usuario, rol = servicio.get_rol(user)

            if (rol == 'emprendedor'):

                emprendedor = usuario


                json_request = json.loads(request.body.decode('utf-8'))

                id_proveedor = kwargs.get('id_proveedor')

                if (id_proveedor):
                    proveedor = Proveedores.objects.get(id=id_proveedor)
                else:
                    proveedor = Proveedores()
                    proveedor.emprendedor = emprendedor

                with transaction.atomic():


                    nombre = json_request.get('nombre', None)
                    apellido = json_request.get('apellido', None)
                    #id_tipo_documento = json_request.get('tipo_documento', None)
                    numero_documento = json_request.get('numero_documento', None)
                    telefono = json_request.get('telefono', None)
                    direccion = json_request.get('direccion', None)

                    campos_validar = [nombre, apellido, numero_documento]

                    if validar_no_vacio(campos_validar):

                        tipo_documento = TiposDocumento.objects.get(descripcion="CUIT")


                        proveedor.nombre = nombre
                        proveedor.apellido = apellido
                        proveedor.numero_documento = numero_documento
                        proveedor.id_tipo_documento = tipo_documento
                        proveedor.telefono = telefono
                        proveedor.direccion = direccion
                        proveedor.save()

                        respuesta.tipo_ok()

                        respuesta.set_mensaje("Se ha insertado correctamente el proveedor, con id {}.".format(proveedor.id))
            else:
                respuesta.tipo_error()
                respuesta.set_mensaje("Solo un emprendedor puede realizar esta operación")


        except Exception as exc:
            print('Error on line {}'.format(sys.exc_info()[-1].tb_lineno))
            respuesta.tipo_error()
            respuesta.set_mensaje(str(exc))

        return respuesta.get_respuesta_http()




    def delete(self, request, *args, **kwargs):
        respuesta = RespuestaHTTP()
        id_proveedor_a_borrar = kwargs['id_proveedor'] if 'id_proveedor' in kwargs else None
        servicio = ServicioUsuarios()

        try:

            user = servicio.get_usuario_jwt(request)
            usuario, rol = servicio.get_rol(user)

            emprendedor = usuario

            if id_proveedor_a_borrar:
                proveedor = Proveedores.objects.get(id=id_proveedor_a_borrar)

                if proveedor.emprendedor.id == emprendedor.id:
                    proveedor.delete()
                    respuesta.tipo_ok()
                    respuesta.set_mensaje("Se ha borrado correctamente el proveedor")
                else:
                    respuesta.tipo_error()
                    respuesta.set_mensaje("El proveedor no existe")
            else:
                respuesta.tipo_error()
                respuesta.set_mensaje("Faltan parámetros obligatorios: id_proveedor")
        except Exception as exc:
            respuesta.tipo_error()
            respuesta.set_mensaje(str(exc))

        return respuesta.get_respuesta_http()
