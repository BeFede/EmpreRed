import datetime
import hashlib
import json
import random
import sys

import jwt
from django.http import HttpResponse
from django.template import loader
from django.views import View
import pytz

from Emprered.utils import RespuestaHTTP, validar_no_vacio, get_datos_token_google
from Emprered.utils import CLAVE_GOOGLE, TIPO_AUTENTICACION_GOOGLE, TIPO_AUTENTICACION_EMPRERED
from django.contrib.auth.models import User
from Emprered.server.models.usuarios import Perfil
from django.contrib.auth import authenticate
from django.conf import settings
from Emprered.server.servicios.servicio_usuarios import ServicioUsuarios
import requests
from django.core.files.base import ContentFile
from Emprered.settings import WEB_PATH


class RegistroView(View):

    def post(self, request, *args, **kwargs):
        respuesta = RespuestaHTTP()
        json_request = json.loads(request.body.decode('utf-8'))
        servicio = ServicioUsuarios()

        try:
            email= ""
            nombre_usuario= ""
            clave = ""
            clave_repetida = ""

            tipo_autenticacion = json_request.get('tipo_autenticacion', None)

            if (tipo_autenticacion == TIPO_AUTENTICACION_EMPRERED):
                user = json_request.get('usuario', None)
                email = user.get('email', None)
                nombre_usuario = user.get('username', None)
                clave = user.get('password', None)
                clave_repetida = user.get('password_repetida', None)
                foto = None
                tipo_registro = 'emprered'

            else:
                if (tipo_autenticacion == TIPO_AUTENTICACION_GOOGLE):

                    token = request.META.get('HTTP_AUTHORIZATION', None)[7:] # why 7? por el Bearer

                    user = get_datos_token_google(token)
                    email = user.get('email', None)
                    nombre_usuario = user.get('email', None)
                    clave = CLAVE_GOOGLE
                    clave_repetida = CLAVE_GOOGLE
                    tipo_registro = 'google'


            campos_validar = [email, nombre_usuario, clave, clave_repetida]


            if (validar_no_vacio(campos_validar)):

                if (servicio.validar_existencia_username(nombre_usuario)):
                    respuesta.set_mensaje("El nombre de usuario ya está registrado")
                    return respuesta.get_respuesta_http()

                if not servicio.validar_formato_mail(email):
                    respuesta.set_mensaje("El formato del mail es incorrecto")
                    return respuesta.get_respuesta_http()

                if servicio.validar_existencia_mail(email):
                    respuesta.set_mensaje("El email ya está registrado")
                    return respuesta.get_respuesta_http()

                validar_clave, mensaje_error = servicio.validar_clave(clave, clave_repetida)

                if not validar_clave:
                    respuesta.set_mensaje(mensaje_error)
                    return respuesta.get_respuesta_http()

                usuario = User()
                perfil = Perfil()


                usuario.username = nombre_usuario
                usuario.email = email
                usuario.set_password(clave)
                usuario.save()

                perfil.user = usuario
                perfil.rol = Perfil.ANONIMO
                perfil.tipo_registro = tipo_registro

                perfil.es_activo = False
                perfil.es_email_confirmado = False

                if tipo_autenticacion == TIPO_AUTENTICACION_EMPRERED:
                    salt = hashlib.sha1(str(random.getrandbits(10)).encode('utf-8')).hexdigest()
                    to_hash = str(salt + email).encode('utf-8')
                    perfil.clave_activacion = hashlib.sha1(to_hash).hexdigest()
                    perfil.expiracion_clave = datetime.datetime.now() + datetime.timedelta(days = 2)
                    servicio.enviar_confirmacion_email(perfil)
                elif tipo_autenticacion == TIPO_AUTENTICACION_GOOGLE:
                    perfil.es_email_confirmado = True
                    servicio.enviar_bienvenida_email(perfil)

                perfil.save()





                respuesta.set_mensaje("El usuario se registró exitosamente")
                respuesta.tipo_ok()
                return respuesta.get_respuesta_http()
            else:
                respuesta.set_mensaje("Faltan campos obligatorios")
                return respuesta.get_respuesta_http()
        except Exception as e:
            print(e)
            respuesta.set_mensaje("Ha ocurrido un error")
            print('Error on line {}'.format(sys.exc_info()[-1].tb_lineno))
            return respuesta.get_respuesta_http()




class ConfirmarEmailView(View):

    def get(self, request, *args, **kwargs):
        username = kwargs.get('username', None)
        token_activacion = kwargs.get('token_activacion', None)

        campos_validar = [username, token_activacion]
        html = ""
        mensaje = ""

        if (validar_no_vacio(campos_validar)):
            try:
                perfil = Perfil.objects.all().get(user__username=username)

                if not perfil.es_email_confirmado:
                    now = datetime.datetime.now()

                    utc = pytz.UTC
                    now = now.replace(tzinfo=utc)
                    perfil.expiracion_clave = perfil.expiracion_clave.replace(tzinfo=utc)

                    if (now <= perfil.expiracion_clave):
                        if (token_activacion == perfil.clave_activacion):
                            perfil.es_email_confirmado = True
                            mensaje = "La cuenta ha sido verificada correctamente. Bienvenidos a Emprered"
                            perfil.save()

                        else:
                            mensaje = "Ha ocurrido un error. Presione <a href=WEB_PATH + 'renovar_confirmacion_cuenta/" + str(perfil.user.username) + "'>aquí</a> para repetir el proceso"
                    else:
                        mensaje = "El tiempo para confirmar la cuenta ha expirado. Presione <a href=WEB_PATH + 'server/renovar_confirmacion_cuenta/" + str(perfil.user.username) + "'>aquí</a> para repetir el proceso"

                else:
                    mensaje = "La cuenta ya ha sido confirmada"

            except Exception as e:
                mensaje = "Ha ocurrido un error"
        else:
            mensaje = "Ha ocurrido un error"

        html =  "<html><body><h2>Confirmación de cuenta</h2><h5>" + mensaje + "</h5></body></html>"

        return HttpResponse(html)

class RenovarConfirmacionEmailView(View):

    def get(self, request, *args, **kwargs):
        username = kwargs.get('username', None)

        campos_validar = [username]
        html = ""
        mensaje = ""

        if (validar_no_vacio(campos_validar)):
            try:
                perfil = Perfil.objects.all().get(user__username=username)
                servicio = ServicioUsuarios()
                if not perfil.es_email_confirmado:

                    salt = hashlib.sha1(str(random.getrandbits(10)).encode('utf-8')).hexdigest()

                    to_hash = str(salt + perfil.user.email).encode('utf-8')
                    perfil.clave_activacion = hashlib.sha1(to_hash).hexdigest()
                    perfil.expiracion_clave = datetime.datetime.now() + datetime.timedelta(days=2)

                    perfil.save()

                    servicio.enviar_confirmacion_email(perfil=perfil)

                    mensaje = "Se ha enviado un email a su casilla de correo para repetir el proceso de confirmación"
                else:
                    mensaje = "La cuenta ya ha sido confirmada"

            except Exception as e:
                mensaje = "Ha ocurrido un error"
        else:
            mensaje = "Ha ocurrido un error"

        html =  "<html><body><h2>Confirmación de cuenta</h2><h5>" + mensaje + "</h5></body></html>"

        return HttpResponse(html)



class OlvidoClaveView(View):

    def get(self, request, *args, **kwargs):
        respuesta = RespuestaHTTP()
        username = kwargs.get('username', None)
        servicio = ServicioUsuarios()

        campos_validar = [username]

        if validar_no_vacio(campos_validar):
            try:
                perfil = Perfil.objects.all().get(user__username=username)
                salt = hashlib.sha1(str(random.getrandbits(10)).encode('utf-8')).hexdigest()

                to_hash = str(salt + perfil.user.email).encode('utf-8')
                perfil.clave_recuperacion = hashlib.sha1(to_hash).hexdigest()
                perfil.expiracion_clave_recuperacion = datetime.datetime.now() + datetime.timedelta(days=2)

                perfil.save()

                servicio.enviar_email_recuperacion_clave(perfil)
                respuesta.tipo_ok()

                respuesta.set_mensaje("Se ha enviado un email con los pasos necesarios para recuperar su cuenta")


            except Exception as e:
                respuesta.tipo_error()
                respuesta.set_mensaje("Ha ocurrido un error")



        else:
            respuesta.tipo_error()
            respuesta.set_mensaje("Faltan datos obligatorios")

        return respuesta.get_respuesta_http()


class RecuperarClaveView(View):

    def get(self, request, *args, **kwargs):
        username = kwargs.get('username', None)
        token_recuperacion = kwargs.get('token_recuperacion', None)


        template = loader.get_template('../templates/recuperacion_clave.html')
        context = {
            'url': WEB_PATH + 'recuperar_clave/' + username + '/' + token_recuperacion,
            'username': username
        }
        return HttpResponse(template.render(context, request))

    def post(self, request, *args, **kwargs):


        username = kwargs.get('username', None)
        token_recuperacion = kwargs.get('token_recuperacion', None)

        mensaje = ""

        try:
            servicio = ServicioUsuarios()
            clave = request.POST.get('clave', None)
            clave_repetida = request.POST.get('clave_repetida', None)

            if clave and clave_repetida:
                exito, mensaje = servicio.validar_clave(clave, clave_repetida)

                if (exito):
                    perfil = Perfil.objects.all().get(user__username=username)
                    now = datetime.datetime.now()
                    utc = pytz.UTC
                    now = now.replace(tzinfo=utc)
                    perfil.expiracion_clave = perfil.expiracion_clave_recuperacion.replace(tzinfo=utc)

                    if (now <= perfil.expiracion_clave_recuperacion):
                        if (token_recuperacion == perfil.clave_recuperacion):
                            perfil.user.set_password(clave)
                            mensaje = "La clave ha sido actualizada correctamente"
                            perfil.user.save()
                            perfil.save()
                    else:
                        mensaje = "El tiempo para renovar las claves ha expirado. Comience el proceso de nuevo"
            else:
               mensaje = "Faltan datos obligatorios"

        except Exception:
            mensaje = "Ha ocurrido un error"

        return HttpResponse(mensaje)
