import json
from django.http import HttpResponse, QueryDict
from django.views import View
from rest_framework import viewsets
from django.conf import settings
from Emprered.utils import RespuestaHTTP, validar_no_vacio

from Emprered.server.servicios.servicio_usuarios import ServicioUsuarios
from Emprered.server.servicios.servicio_reportes import ServicioReportes
from Emprered.server.models.movimientos_stock import MovimientosStock
from Emprered.server.models.productos import Productos
from Emprered.server.models.materias_primas import MateriasPrimas
from decimal import Decimal
#from Emprered.utils import NotAuthorizedException
from django.db.models.query import RawQuerySet
from django.db import transaction
import time
from django.utils import timezone



class ReportesMovimientosStock(View):


    def reporte_movimientos_stock(self, emprendedor, fecha_desde, fecha_hasta, producto = None, materia_prima = None, inicio = None, fin = None):


        kwargs = {}
        kwargs['emprendedor'] = emprendedor
        if producto:
            kwargs['producto'] = producto
        if materia_prima:
            kwargs['materia_prima'] = materia_prima         

        movimientos_stock = MovimientosStock.objects.filter(**kwargs).order_by("-fecha_hora")[inicio:fin]

        """
        if fecha_desde:
            movimientos_stock = movimientos_stock.filter(fecha_hora >= fecha_desde)
        
        if fecha_hasta:
            movimientos_stock = movimientos_stock.filter(fecha_hora <= fecha_hasta)
        """
        

        movimientos_stock_list = []

        for movimiento_stock in movimientos_stock:
            movimiento_stock_dic = {
                "id": movimiento_stock.id,
                "fecha_hora": movimiento_stock.fecha_hora,
                "producto": movimiento_stock.producto.nombre if movimiento_stock.producto else None,
                "materia_prima": movimiento_stock.materia_prima.nombre if movimiento_stock.materia_prima else None,
                "producto_materia_prima":  movimiento_stock.producto.nombre if movimiento_stock.producto else movimiento_stock.materia_prima,
                "cantidad": movimiento_stock.cantidad,
                "motivo": movimiento_stock.motivo,
                "tipo_movimiento": MovimientosStock.TIPOS_MOVIMIENTOS[movimiento_stock.tipo_movimiento-1][1]
            }
            movimientos_stock_list.append(movimiento_stock_dic)

        return movimientos_stock_list


    def get(self, request, *args, **kwargs):
        respuesta = RespuestaHTTP()
        servicio = ServicioUsuarios()
        try:
            user = servicio.get_usuario_jwt(request)
            
            #if user.is_anonymous:
            #    raise NotAuthorizedException()

            usuario, rol = servicio.get_rol(user)
            if (rol == 'emprendedor'):
                emprendedor = usuario
                listado = request.GET.get('listado')

                if not listado:
                    respuesta.tipo_error()
                    respuesta.set_mensaje("No se especificó ningún listado")    

                if (listado == 'movimientos_stock'):
                    inicio = Decimal(request.GET.get('inicio'))  if request.GET.get('inicio') else None
                    fin = Decimal(request.GET.get('fin')) if request.GET.get('fin') else None
                    producto = request.GET.get('producto') if request.GET.get('producto') else None
                    materia_prima = request.GET.get('materia_prima') if request.GET.get('materia_prima') else None
                    fecha_desde = request.GET.get('fecha_desde') if request.GET.get('fecha_desde') else None
                    fecha_hasta = request.GET.get('fecha_hasta') if request.GET.get('fecha_hasta') else None
                    today = timezone.now()
                    params = {
                        'today': today,
                        'movimientos_stock': self.reporte_movimientos_stock(emprendedor, fecha_desde, fecha_hasta, producto, materia_prima, inicio, fin),
                        'autor': emprendedor
                    }
                    return ServicioReportes.reporte_pdf('../templates/listado_movimientos_stock.html', params, "movimientos_stock.pdf")
                else:
                    respuesta.tipo_error()
                    respuesta.set_mensaje("No se encontró el listado solicitado")    

            else:
                respuesta.tipo_error()
                respuesta.set_mensaje("Solo un emprendedor puede hacer esta operación")
        except Exception as e:
            print(e)
            respuesta.tipo_error()
            respuesta.set_mensaje(str(e))
            
        return respuesta.get_respuesta_http()
    