import json
from django.http import HttpResponse, QueryDict
from django.views import View
from rest_framework import viewsets
from django.conf import settings
from Emprered.utils import RespuestaHTTP, validar_no_vacio

from Emprered.server.servicios.servicio_usuarios import ServicioUsuarios
from Emprered.server.servicios.servicio_reportes import ServicioReportes
from Emprered.server.servicios.servicio_email import ServicioEmail


from Emprered.server.models.movimientos_stock import MovimientosStock
from Emprered.server.models.productos import Productos
from Emprered.server.models.materias_primas import MateriasPrimas
from Emprered.server.models.producciones import Produccion, DetalleProduccion
from decimal import Decimal
from Emprered.server.models.planes_produccion import PlanProduccion, DetallePlanProduccion

from django.db.models.query import RawQuerySet
from django.db import transaction
import time
from django.utils import timezone



class ReportesProduccion(View):


    def reporte_materia_prima_por_produccion(self, emprendedor, id_produccion):

        try:
            produccion = Produccion.objects.filter(emprendedor = emprendedor).filter(id = id_produccion)
            produccion = produccion[0]
        except:
            return []

        return produccion.resumir_produccion()


    def get(self, request, *args, **kwargs):
        respuesta = RespuestaHTTP()
        servicio = ServicioUsuarios()
        try:        
            user = servicio.get_usuario_jwt(request)
            usuario, rol = servicio.get_rol(user)

            if (rol == 'emprendedor'):
                emprendedor = usuario

                listado = request.GET.get('listado')

                if not listado:
                    respuesta.tipo_error()
                    respuesta.set_mensaje("No se especificó ningún listado")    

                if (listado == "reporte_materias_primas"):
                    id_produccion = Decimal(request.GET.get('id_produccion'))  if request.GET.get('id_produccion') else None
                    accion = request.GET.get('accion') if request.GET.get('accion') else 'descarga'
                    
                    
                    today = timezone.now()
                    params = {
                        'today': today,
                        'produccion': self.reporte_materia_prima_por_produccion(emprendedor, id_produccion),
                        'autor': emprendedor
                    }
                    #respuesta.agregar_dato('produccion', self.reporte_materia_prima_por_produccion(emprendedor, id_produccion))
                    #respuesta.tipo_ok()
                    #respuesta.set_mensaje("ss")
                    #return respuesta.get_respuesta_http()
                    #pdf = ServicioReportes.reporte_pdf('../templates/reporte_materia_prima_por_produccion.html', params, "produccion.pdf")

                    #return pdf
                    #elif accion == 'email':
                    if accion == 'descarga':
                       
                        
                        pdf, pdf_value = ServicioReportes.reporte_pdf_file('../templates/reporte_materia_prima_por_produccion.html', params, "produccion.pdf")
                        
                        try:
                            asunto = "Reporte de producción " + str(id_produccion)
                            cuerpo = "El reporte de producción que solicitó ya está disponible. Gracias por utilizar nuestros servicios"
                            destinatarios = [user.email]

                            archivo_pdf = {
                                "filename": "reporte_produccion_" + str(id_produccion),
                                "file": pdf_value
                            }

                            files = [archivo_pdf]
                            ServicioEmail.enviar_email(asunto, cuerpo, destinatarios, files)
                        except Exception:
                            pass
                        return pdf
                        #respuesta.tipo_ok()                        

                else:
                    respuesta.tipo_error()
                    respuesta.set_mensaje("No se encontró el listado solicitado")    


            else:
                respuesta.tipo_error()
                respuesta.set_mensaje("Solo un emprendedor puede hacer esta operación")
        except Exception as e:
            print(e)
            respuesta.tipo_error()
            respuesta.set_mensaje(str(e))
            
        return respuesta.get_respuesta_http()
    