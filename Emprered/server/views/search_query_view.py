import json
import sys
import re
from django.http import HttpResponse, QueryDict
from django.views import View
from rest_framework import viewsets
from django.conf import settings

from Emprered.utils import RespuestaHTTP

from Emprered.server.servicios.servicio_usuarios import ServicioUsuarios
from Emprered.server.models.busquedas import Busquedas
from Emprered.server.models.catalogo import Catalogo, DetalleCatalogo, Comentario
from Emprered.server.views.tienda_view import TiendaView
from Emprered.server.models.emprendedores import Emprendedores, Marcas
from Emprered.server.views.emprendedor_view import EmprendedorView

from django.db.models.query import RawQuerySet
from django.db import transaction
import datetime
from django.db.models import Sum

from haystack.query import SearchQuerySet


class SearchQueryView(View):

    def get(self, request, *args, **kwargs):
        respuesta = RespuestaHTTP()
        servicio = ServicioUsuarios()
        tienda = TiendaView()
        emprendedores = EmprendedorView()

        # Los modelos en los que puede buscarse
        models_dic = {
            # Le decimos productos porque en realidad son eso,
            # pero nunca vamos a estar buscando en esa tabla
            "productos": [DetalleCatalogo],
            "emprendedores": [Emprendedores],
            "marcas": [Marcas],
            "elementos": [DetalleCatalogo, Emprendedores, Marcas]
        }

        try:
            user = servicio.get_usuario_jwt(request)
            if user.is_anonymous:
                user, _ = servicio.get_rol(user)

            # Vemos en qué modelo (tabla) vamos a buscar

            """
            Con el model name pasa algo raro y por eso no puedo hacer:
                model_name = kwargs.get('model', 'elementos')
            Pasa que hay un par {clave: valor} en el kwargs que es {'model': None},
            por lo que model_name termina valiendo None y no 'elementos'.
            Sacado de StackOverflow:
                "...with a default fallback [, .get()] will still return None
                if the key is explicitly mapped to None in the dict."
            """
            model_name = kwargs.get('model')

            if not model_name:
                model_name = "elementos"

            model = models_dic.get(model_name, DetalleCatalogo)

            query = kwargs.get('query')
            if not query:
                raise Exception("No se ingresó ninguna búsqueda")
            boost = kwargs.get('boost')

            # Pueden también mandarnos subcategorias por las cuales filtrar.
            # Si no está especificado, mandamos todas
            subcategorias = request.GET.get('subcategorias', [])
            if subcategorias:
                subcategorias = subcategorias.split(",")
            # Pasémoslo a int porque así está en la BD
            subcategorias = [ int(sc) for sc in subcategorias if sc.isdigit() ]
            # Guardar la búsqueda
            with transaction.atomic():
                busqueda = Busquedas()
                busqueda.query = query
                busqueda.boost = boost
                busqueda.model_name = model_name
                busqueda.id_usuario = user
                busqueda.save()

            if boost:
                """
                De la doc:
                Boosts a certain term of the query. You provide the term to be
                boosted and the value is the amount to boost it by. Boost
                amounts may be either an integer or a float.
                """
                raw_results = SearchQuerySet().models(*model).filter(content=query).boost(boost, 2)
            else:
                raw_results = SearchQuerySet().models(*model).filter(content=query)

            resultados = []

            for r in raw_results:
                obj = r.object
                if isinstance(obj, DetalleCatalogo):
                    if subcategorias and obj.producto.subcategoria.id not in subcategorias:
                        # Si no está en las subcategorias que queremos
                        continue
                    res = tienda.get_detalle_catalogo(obj.id)
                elif isinstance(obj, Emprendedores):
                    res = emprendedores.get_emprendedor(obj.id)
                elif isinstance(obj, Marcas):
                    res = {
                        'descripcion': obj.descripcion,
                        'nombre': obj.nombre,
                        'frase': obj.frase,
                        'logo': obj.logo.url if obj.logo else '',
                        'nombre_emprendedor': obj.emprendedor.nombre + ' ' + obj.emprendedor.apellido
                    }
                    
                resultados.append(res)

            if resultados:
                if len(resultados) == 1:
                    model_name_singular = re.sub('e?s', '', model_name)
                    genero_articulo = 'a' if model_name_singular[-1] == 'a' else ''
                    respuesta.set_mensaje('¡Encontramos un{} {} que coincide!'.format(genero_articulo, model_name_singular))
                else:
                    respuesta.set_mensaje('¡Encontramos {} {} que coinciden!'.format(len(resultados), model_name))
                respuesta.agregar_dato('resultados', resultados)
            else:
                respuesta.set_mensaje('Lo sentimos, no encontramos {} que coincidieran con tu búsqueda.'.format(model_name))
            respuesta.tipo_ok()

        except Exception as e:
            print('Error on line {}'.format(sys.exc_info()[-1].tb_lineno))
            print(e)
            respuesta.tipo_error()
            respuesta.set_mensaje(str(e))

        return respuesta.get_respuesta_http()
