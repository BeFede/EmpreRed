import json
from django.http import HttpResponse, QueryDict
from django.views import View
from rest_framework import viewsets
from django.conf import settings

from Emprered.server.models import catalogo
from Emprered.server.models.emprendedores import Marcas, Emprendedores
from Emprered.server.models.usuarios import Perfil
from Emprered.settings import IMAGEN_PERFIL_DEFECTO
from Emprered.utils import RespuestaHTTP, validar_no_vacio, Paginador

from Emprered.server.servicios.servicio_usuarios import ServicioUsuarios
from Emprered.server.models.productos import Productos
from Emprered.server.models.catalogo import Catalogo, DetalleCatalogo, Comentario, ImagenesDetalleCatalogo
from Emprered.server.models.categorias import Categoria, Subcategoria

from django.db.models.query import RawQuerySet
from django.db import transaction
import datetime
from django.db.models import Sum



class TiendaView(View):


    def get_catalogo(self, id_catalogo, pagina_actual = 1, tamano_pagina = 10):


        catalogo = Catalogo.objects.filter(id = id_catalogo)[0]
        catalogo_dic = []

        if catalogo.estado == Catalogo.VISIBLE:

            detalles_catalogo = DetalleCatalogo.objects.filter(catalogo = catalogo)

            detalles_catalogo_list = []

            for detalle_catalogo in detalles_catalogo:

                if (detalle_catalogo.estado == DetalleCatalogo.VISIBLE):

                    producto = {
                        "id": detalle_catalogo.producto.id,
                        "nombre": detalle_catalogo.producto.nombre,
                        "foto": detalle_catalogo.producto.foto.url if detalle_catalogo.producto.foto else "",
                        "categoria": detalle_catalogo.producto.subcategoria.categoria.descripcion,
                        "subcategoria": detalle_catalogo.producto.subcategoria.descripcion
                    }

                    detalle_catalogo_dic = {
                        "id": detalle_catalogo.id,
                        "titulo": detalle_catalogo.titulo,
                        "descripcion": detalle_catalogo.descripcion,
                        "producto": producto,
                        "precio_venta": detalle_catalogo.precio,
                        "valoracion": detalle_catalogo.valoracion_promedio,
                        "visitas": detalle_catalogo.visitas,
                        "etiqueta": detalle_catalogo.etiqueta.descripcion if detalle_catalogo.etiqueta else ""

                    }
                    detalles_catalogo_list.append(detalle_catalogo_dic)

            detalles_paginados = Paginador(detalles_catalogo_list, tamano_pagina)

            catalogo_dic = {
                "id_catalogo" : catalogo.id,
                "fecha_hora_publicacion" : catalogo.fecha_hora_publicacion,
                "detalles_catalogo" : detalles_paginados.pagina(pagina_actual),
                "cantidad_detalles": len(detalles_catalogo),
                "cantidad_visitas": detalles_catalogo.aggregate(Sum('visitas'))
            }

        return catalogo_dic



    def get_catalogo_minorista(self, emprendedor, pagina_actual = 1, tamano_pagina = 10):

        catalogos = Catalogo.objects.filter(emprendedor__id=emprendedor)

        catalogo = None  # catálogo minorista

        for cat in catalogos:
            if cat.tipo_catalogo == Catalogo.MINORISTA:
                catalogo = cat
                break




        # catalogo = Catalogo.objects.filter(id = id_catalogo)[0]
        catalogo_dic = []

        if catalogo.estado == Catalogo.VISIBLE:

            detalles_catalogo = DetalleCatalogo.objects.filter(catalogo = catalogo)

            detalles_catalogo_list = []

            for detalle_catalogo in detalles_catalogo:

                imagenes = ImagenesDetalleCatalogo.objects.filter(detalle_catalogo=detalle_catalogo)
                foto_producto = settings.IMAGEN_ARTICULO_DEFECTO

                if (len(imagenes) > 0):
                    foto_producto = imagenes[0].imagen.url


                if (detalle_catalogo.estado == DetalleCatalogo.VISIBLE):
                    producto = {
                        "id": detalle_catalogo.producto.id,
                        "nombre": detalle_catalogo.producto.nombre,
                        "foto": detalle_catalogo.producto.foto.url if detalle_catalogo.producto.foto else foto_producto,
                        "categoria": detalle_catalogo.producto.subcategoria.categoria.descripcion,
                        "subcategoria": detalle_catalogo.producto.subcategoria.descripcion
                    }

                    detalle_catalogo_dic = {
                        "id": detalle_catalogo.id,
                        "titulo": detalle_catalogo.titulo,
                        "descripcion": detalle_catalogo.descripcion,
                        "producto": producto,
                        "precio_venta": detalle_catalogo.precio,
                        "valoracion": detalle_catalogo.valoracion_promedio,
                        "visitas": detalle_catalogo.visitas,
                        "etiqueta": detalle_catalogo.etiqueta.descripcion if detalle_catalogo.etiqueta else ""

                    }
                    detalles_catalogo_list.append(detalle_catalogo_dic)

            detalles_paginados = Paginador(detalles_catalogo_list, tamano_pagina)

            catalogo_dic = {
                "id_catalogo" : catalogo.id,
                "fecha_hora_publicacion" : catalogo.fecha_hora_publicacion,
                "detalles_catalogo" : detalles_paginados.pagina(pagina_actual),
                "cantidad_detalles": len(detalles_catalogo),
                "cantidad_visitas": detalles_catalogo.aggregate(Sum('visitas'))
            }

        return catalogo_dic


    def get_catalogos(self, pagina_actual =1, tamano_pagina = 10):

        catalogos = Catalogo.objects.order_by("-fecha_hora_publicacion")

        catalogos_list = []

        for catalogo in catalogos:

            emprendedor = Emprendedores.objects.get(id=catalogo.emprendedor.id)
            perfil = Perfil.objects.get(user__id=emprendedor.id_usuario.id)
            marca = None
            try:
                marca = Marcas.objects.filter(id_emprendedor=emprendedor)[0]
            except:
                pass

            emprendedor_dic = {
                "id": emprendedor.id,
                "nombre": emprendedor.nombre,
                "apellido": emprendedor.apellido,
                "foto": perfil.foto.url if perfil.foto else ""
            }

            marca_dic = {
                "id": marca.id if marca else "",
                "nombre": marca.nombre if marca else "",
                "logo": marca.logo.url if (marca and marca.logo) else ""
            }

            if (catalogo.estado == Catalogo.VISIBLE):
                detalles_catalogo = DetalleCatalogo.objects.filter(catalogo = catalogo)
                emprendedor = Emprendedores.objects.get(id = catalogo.emprendedor.id)
                marca = None

                try:
                    marca = Marcas.objects.filter(id_emprendedor = emprendedor)[0]
                except:
                    pass

                catalogo_dic = {
                    "id":catalogo.id,
                    "cantidad_detalles": len(detalles_catalogo),
                    "emprendedor": emprendedor_dic,
                    "marca":marca_dic
                }
                catalogos_list.append(catalogo_dic)

        catalogos_paginados = Paginador(catalogos_list, tamano_pagina)
        return catalogos_paginados.pagina(pagina_actual)


    def get_detalle_catalogo(self, id_detalle_catalogo):
        # Traemos el detalle que nos pasan
        try:
            detalle_catalogo = DetalleCatalogo.objects.get(id = id_detalle_catalogo)
        except:
            return []
        emprendedor = Emprendedores.objects.get(id=detalle_catalogo.catalogo.emprendedor.id)
        perfil = Perfil.objects.get(user__id=emprendedor.id_usuario.id)
        marca = None
        try:
            marca = Marcas.objects.filter(id_emprendedor=emprendedor)[0]
        except:
            pass

        catalogos_emprendedor = Catalogo.objects.filter(emprendedor=emprendedor)

        catalogos_dic = []

        for catalogo in catalogos_emprendedor:
            catalogos_dic.append(
                {
                    "id": catalogo.id,
                    "tipo": catalogo.get_tipo_catalogo(),
                    "estado": catalogo.get_estado()
                }
            )

        emprendedor_dic = {
            "id": emprendedor.id,
            "nombre": emprendedor.nombre,
            "apellido": emprendedor.apellido,
            "foto": perfil.foto.url if perfil.foto else "",
            "telefono": emprendedor.telefono,
            "catalogos": catalogos_dic
        }

        marca_dic = {
            "id": marca.id if marca else "",
            "nombre": marca.nombre if marca else "",
            "logo": marca.logo.url if (marca and marca.logo) else ""
        }

        comentarios = Comentario.objects.filter(detalle_catalogo = detalle_catalogo).order_by("-fecha_hora_publicacion")

        comentarios_list = []

        for comentario in comentarios:
            comentario_dic = {
                "creador": comentario.perfil_creador.user.username,
                "foto": comentario.perfil_creador.foto.url if comentario.perfil_creador.foto else IMAGEN_PERFIL_DEFECTO,
                "titulo": comentario.titulo,
                "descripcion": comentario.descripcion,
                "fecha_hora": comentario.fecha_hora_publicacion
            }
            comentarios_list.append(comentario_dic)
        imagenes = ImagenesDetalleCatalogo.objects.filter(detalle_catalogo = detalle_catalogo)

        imagenes_list = []
        if detalle_catalogo.producto.foto:
            imagenes_list.append(
                {
                    "foto": detalle_catalogo.producto.foto.url
                }
            )
        for imagen in imagenes:
            imagen_dic = {
                "foto": imagen.imagen.url
            }
            imagenes_list.append(imagen_dic)


        producto = {
            "id": detalle_catalogo.producto.id,
            "nombre": detalle_catalogo.producto.nombre,
            "foto": detalle_catalogo.producto.foto.url if detalle_catalogo.producto.foto else "",
            "categoria": detalle_catalogo.producto.subcategoria.categoria.descripcion,
            "id_categoria": detalle_catalogo.producto.subcategoria.categoria.id,
            "subcategoria": detalle_catalogo.producto.subcategoria.descripcion
        }

        detalle_catalogo_dic = {
            "id": detalle_catalogo.id,
            "emprendedor": emprendedor_dic,
            "marca": marca_dic,
            "titulo": detalle_catalogo.titulo,
            "descripcion": detalle_catalogo.descripcion,
            "producto": producto,
            "precio_venta": detalle_catalogo.precio,
            "valoracion": detalle_catalogo.valoracion_promedio,
            "visitas": detalle_catalogo.visitas,
            "estado": detalle_catalogo.get_estado(),
            "etiqueta": detalle_catalogo.etiqueta.descripcion if detalle_catalogo.etiqueta else "",
            "etiqueta_id": detalle_catalogo.etiqueta.id if detalle_catalogo.etiqueta else "",
            "comentarios": comentarios_list,
            "imagenes": imagenes_list,
        }

        return detalle_catalogo_dic




    def get_todos_los_detalles(self):

        detalles_catalogo = DetalleCatalogo.objects.all()


        detalles_catalogo_list = []

        for detalle_catalogo in detalles_catalogo:

            if (detalle_catalogo.estado == DetalleCatalogo.VISIBLE):


                detalle_catalogo_dic = {
                    "id": detalle_catalogo.id,
                    "titulo": detalle_catalogo.titulo,

                }
                detalles_catalogo_list.append(detalle_catalogo_dic)


        return detalles_catalogo_list


    def get_productos(self, orden, filtros = {}, pagina_actual =1, tamano_pagina = 10):


        detalles_catalogo = DetalleCatalogo.objects.all()

        if filtros.get('nombre'):
            detalles_catalogo = detalles_catalogo.filter(titulo__icontains = filtros.get('nombre'))
        if filtros.get('emprendedor'):
            detalles_catalogo = detalles_catalogo.filter(catalogo__emprendedor_id = filtros.get('emprendedor'))
        if filtros.get('marca'):
            detalles_catalogo = detalles_catalogo.filter(catalogo__emprendedor_marca_id = filtros.get('marca'))
        if filtros.get('precio_desde'):
            detalles_catalogo = detalles_catalogo.filter(precio__gte = filtros.get('precio_desde'))
        if filtros.get('precio_hasta'):
            detalles_catalogo = detalles_catalogo.filter(precio__lte = filtros.get('precio_hasta'))

        """1. Si me pasan subcategorias: filtro por esas subcategorias.
        2. Si me pasan categorias: filtro por las subcategorias de esas categorias.
        3. Si me pasan subcategorias AND categorias: filtro por la unión de 1 y 2."""
        subcategorias = []

        if filtros.get('subcategorias'):
            subcategorias = filtros.get('subcategorias', [])
        if filtros.get('categorias'):
            subcategorias_objs = Subcategoria.objects.filter(categoria__in=filtros.get('categorias', []))
            ids_subcategorias = [ subcategoria.id for subcategoria in subcategorias_objs ]
            subcategorias = list(set(subcategorias + ids_subcategorias))

        if subcategorias:
            # Vamos a tener que bajar un poco más porque producto es quien tiene una subcategoria.
            productos = Productos.objects.filter(subcategoria__in=subcategorias)
            ids_productos = [ p.id for p in productos ]
            detalles_catalogo = detalles_catalogo.filter(producto__in=ids_productos)


        detalles_catalogo = detalles_catalogo.order_by('{0}{1}'.format(orden.get('criterio'), orden.get('atributo')))


        detalles_catalogo_list = []

        for detalle_catalogo in detalles_catalogo:

            if (detalle_catalogo.estado == DetalleCatalogo.VISIBLE):

                emprendedor = Emprendedores.objects.get(id=detalle_catalogo.catalogo.emprendedor.id)
                perfil = Perfil.objects.get(user__id=emprendedor.id_usuario.id)
                catalogos_emprendedor = Catalogo.objects.filter(emprendedor = emprendedor)
                marca = None
                try:

                    marca = Marcas.objects.get(emprendedor=emprendedor)

                except Exception as e:
                    print(e)

                catalogos_dic = []

                imagenes = ImagenesDetalleCatalogo.objects.filter(detalle_catalogo=detalle_catalogo)
                foto_producto = settings.IMAGEN_ARTICULO_DEFECTO

                if (len(imagenes) > 0):
                    foto_producto = imagenes[0].imagen.url



                for catalogo in catalogos_emprendedor:
                    catalogos_dic.append(
                        {
                            "id": catalogo.id,
                            "tipo": catalogo.get_tipo_catalogo(),
                            "estado":catalogo.get_estado()
                        }
                    )

                emprendedor_dic = {
                    "id": emprendedor.id,
                    "nombre": emprendedor.nombre,
                    "apellido": emprendedor.apellido,
                    "telefono": emprendedor.telefono,
                    "foto": perfil.foto.url if perfil.foto else "",
                    "catalogos": catalogos_dic
                }


                marca_dic = {

                    "nombre": marca.nombre if marca else "",
                    "descripcion": marca.descripcion if marca else "",
                    "logo": marca.logo.url if (marca and marca.logo) else ""
                }

                producto = {
                    "id": detalle_catalogo.producto.id,
                    "nombre": detalle_catalogo.producto.nombre,
                    "foto": detalle_catalogo.producto.foto.url if detalle_catalogo.producto.foto else foto_producto,
                    "categoria": detalle_catalogo.producto.subcategoria.categoria.descripcion,
                    "id_categoria": detalle_catalogo.producto.subcategoria.categoria.id,
                    "subcategoria": detalle_catalogo.producto.subcategoria.descripcion,
                    "id_subcategoria": detalle_catalogo.producto.subcategoria.id,
                }

                detalle_catalogo_dic = {
                    "id": detalle_catalogo.id,
                    "titulo": detalle_catalogo.titulo,
                    "descripcion": detalle_catalogo.descripcion,
                    "producto": producto,
                    "precio_venta": detalle_catalogo.precio,
                    "valoracion": detalle_catalogo.valoracion_promedio,
                    "visitas": detalle_catalogo.visitas,
                    "etiqueta": detalle_catalogo.etiqueta.descripcion if detalle_catalogo.etiqueta else "",
                    "marca": marca_dic,
                    "emprendedor": emprendedor_dic,

                }
                detalles_catalogo_list.append(detalle_catalogo_dic)

        detalles_paginados = Paginador(detalles_catalogo_list, tamano_pagina)

        return detalles_paginados.pagina(pagina_actual)



    def get(self, request, *args, **kwargs):
        respuesta = RespuestaHTTP()
        servicio = ServicioUsuarios()

        try:

            #user = servicio.get_usuario_jwt(request)

            recurso = kwargs['recurso'] if 'recurso' in kwargs else None
            id = request.GET.get('id', None)

            tamano_pagina = request.GET.get('tamano_pagina', 10)
            pagina_actual = request.GET.get('pagina_actual', 1)


            if (recurso == 'catalogos'):
                if not id:
                    respuesta.agregar_dato('catalogos', self.get_catalogos(pagina_actual, tamano_pagina))
                else:
                    # El id representa el id del emprendedor
                    respuesta.agregar_dato('catalogo', self.get_catalogo_minorista(id, pagina_actual, tamano_pagina))

            elif (recurso == 'detalles_catalogo'):
                if id:
                    # El id representa el id del detalle.
                    # Actualizamos acá las visitas, y no en get_detalle_catalogo.
                    detalle_catalogo = DetalleCatalogo.objects.get(id = id)
                    with transaction.atomic():
                        detalle_catalogo.visitas += 1
                        detalle_catalogo.save()
                    respuesta.agregar_dato('detalle_catalogo', self.get_detalle_catalogo(id))
                else:
                    respuesta.agregar_dato('detalles_catalogo', self.get_todos_los_detalles())

            elif (recurso == 'productos'):
                orden = request.GET.get('orden', 'valoracion_promedio')
                criterio = request.GET.get('criterio', 'desc')
                nombre = request.GET.get('nombre', "")
                precio_desde = request.GET.get('precio_desde', "")
                precio_hasta = request.GET.get('precio_hasta', "")
                emprendedor = request.GET.get('emprendedor', "")
                marca = request.GET.get('marca', "")

                subcategorias = request.GET.get('subcategorias', [])
                if subcategorias:
                    subcategorias = subcategorias.split(",")

                categorias = request.GET.get('categorias', [])
                if categorias:
                    categorias = categorias.split(",")

                filtros = {
                    'nombre': nombre,
                    'precio_desde': precio_desde,
                    'precio_hasta': precio_hasta,
                    'emprendedor': emprendedor,
                    'marca': marca,
                    'categorias': [ int(c) for c in categorias if c.isdigit() ],
                    'subcategorias': [ int(sc) for sc in subcategorias if sc.isdigit() ],
                }

                criterio = '-' if criterio == 'desc' else ''

                orden = {
                    'atributo': orden,
                    'criterio': criterio
                }



                respuesta.agregar_dato('productos', self.get_productos(orden = orden, filtros = filtros, pagina_actual = pagina_actual, tamano_pagina = tamano_pagina))



            respuesta.tipo_ok()


        except Exception as e:
            print(e)
            respuesta.tipo_error()
            respuesta.set_mensaje(str(e))
        return respuesta.get_respuesta_http()
