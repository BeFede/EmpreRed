
from django.http import HttpResponse, QueryDict
from django.views import View
from rest_framework import viewsets
from django.conf import settings
from Emprered.utils import RespuestaHTTP
from Emprered.server.models.tipificaciones import TiposDocumento, AlicuotasIVA, CondicionesFrenteIVA, UnidadesMedida
from Emprered.server.models.catalogo import Etiqueta
from django.db.models.query import RawQuerySet



class TiposDocumentoView(View):

    def get_tipos_documento(self):
        tipos_documento = TiposDocumento.objects.all()
        tipos_documento_dic = []
        for tipo_documento in tipos_documento:
            tipo_documento_dic = {
                "id": tipo_documento.id,
                "descripcion": tipo_documento.descripcion
            }
            tipos_documento_dic.append(tipo_documento_dic)
        return tipos_documento_dic


    def get(self, request, *args, **kwargs):
        respuesta = RespuestaHTTP()
        try:
            respuesta.agregar_dato('tipos_documento', self.get_tipos_documento())
            respuesta.tipo_ok()
        except Exception as e:
            print(e)
            respuesta.tipo_error()
            respuesta.set_mensaje(str(e))
        return respuesta.get_respuesta_http()



class AlicuotasIvaView(View):

    def get_alicuotas_iva(self):
        alicuotas_iva = AlicuotasIVA.objects.all()
        alicuotas_iva_dic = []
        for alicuota_iva in alicuotas_iva:
            alicuota_iva_dic = {
                "id": alicuota_iva.id,
                "descripcion": alicuota_iva.porcentaje_IVA
            }
            alicuotas_iva_dic.append(alicuota_iva_dic)
        return alicuotas_iva_dic


    def get(self, request, *args, **kwargs):
        respuesta = RespuestaHTTP()
        try:
            respuesta.agregar_dato('alicuotas_iva', self.get_alicuotas_iva())
            respuesta.tipo_ok()
        except Exception as e:
            print(e)
            respuesta.tipo_error()
            respuesta.set_mensaje(str(e))
        return respuesta.get_respuesta_http()




class CondicionesFrenteIVAView(View):

    def get_condiciones_frente_iva(self):
        condiciones_frente_iva = CondicionesFrenteIVA.objects.all()
        condiciones_frente_iva_dic = []
        for condicion_frente_iva in condiciones_frente_iva:
            condicion_frente_iva_dic = {
                "id": condicion_frente_iva.id,
                "descripcion": condicion_frente_iva.descripcion
            }
            condiciones_frente_iva_dic.append(condicion_frente_iva_dic)
        return condiciones_frente_iva_dic


    def get(self, request, *args, **kwargs):
        respuesta = RespuestaHTTP()
        try:
            respuesta.agregar_dato('condiciones_frente_iva', self.get_condiciones_frente_iva())
            respuesta.tipo_ok()
        except Exception as e:
            print(e)
            respuesta.tipo_error()
            respuesta.set_mensaje(str(e))
        return respuesta.get_respuesta_http()




class UnidadesMedidaView(View):

    def get_unidades_medida(self):
        unidades_medida = UnidadesMedida.objects.all()
        unidades_medida_dic = []
        for unidad_medida in unidades_medida:
            unidad_medida_dic = {
                "id": unidad_medida.id,
                "descripcion": unidad_medida.descripcion
            }
            unidades_medida_dic.append(unidad_medida_dic)
        return unidades_medida_dic


    def get(self, request, *args, **kwargs):
        respuesta = RespuestaHTTP()
        try:
            respuesta.agregar_dato('unidades_medida', self.get_unidades_medida())
            respuesta.tipo_ok()
        except Exception as e:
            print(e)
            respuesta.tipo_error()
            respuesta.set_mensaje(str(e))
        return respuesta.get_respuesta_http()


class EtiquetasView(View):

    def get_etiquetas(self):
        etiquetas = Etiqueta.objects.all()
        etiquetas_dic = []
        for etiqueta in etiquetas:
            etiqueta_dic = {
                "id": etiqueta.id,
                "descripcion": etiqueta.descripcion
            }
            etiquetas_dic.append(etiqueta_dic)
        return etiquetas_dic


    def get(self, request, *args, **kwargs):
        respuesta = RespuestaHTTP()
        try:
            respuesta.agregar_dato('etiquetas', self.get_etiquetas())
            respuesta.tipo_ok()
        except Exception as e:
            print(e)
            respuesta.tipo_error()
            respuesta.set_mensaje(str(e))
        return respuesta.get_respuesta_http()
