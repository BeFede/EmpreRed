from decimal import Decimal

from django.db import transaction
from django.db.models import Sum, Count
from django.views import View

from Emprered.server.models import venta
from Emprered.server.models.catalogo import DetalleCatalogo, Catalogo, Valoracion
from Emprered.server.models.emprendedores import Emprendedores
from Emprered.server.models.usuarios import Perfil
from Emprered.server.models.venta import Venta, Pago, DetallesVenta
from Emprered.server.servicios.servicio_mercado_pago import ServicioMercadoPago
from Emprered.server.views.mercado_pago_view import MercadoPagoView
from Emprered.utils import RespuestaHTTP
from Emprered.server.servicios.servicio_usuarios import ServicioUsuarios
from Emprered.utils import  Paginador
import mercadopago
import json
import sys
from Emprered.settings import APP_ID_MERCADO_PAGO, SECRET_KEY_MERCADO_PAGO, DEBUG


class ValoracionView(View):





    def get(self, request, *args, **kwargs):
        respuesta = RespuestaHTTP()

        servicio = ServicioUsuarios()


        try:
            user = servicio.get_usuario_jwt(request)
            usuario, rol = servicio.get_rol(user)

            if (rol != 'anonimo'):



                id_detalle_catalogo = request.GET.get('id_detalle_catalogo', "")
                perfil = Perfil.objects.get(user=user)

                if (id_detalle_catalogo):

                    valoracion_existente = Valoracion.objects.filter(detalle_catalogo=id_detalle_catalogo).filter(perfil_creador=perfil)

                    response_datos = {
                        "es_valorada": False,
                        "puntaje": ""
                    }

                    if (len(valoracion_existente) > 0):
                        response_datos['es_valorada'] = True
                        response_datos['puntaje'] = valoracion_existente[0].puntaje

                    respuesta.agregar_dato("valoracion", response_datos)
                    respuesta.tipo_ok()

                else:
                    respuesta.set_mensaje("Faltan datos obligatorios")
            else:
                respuesta.set_mensaje("Debe estar registrado para comprar un producto")

        except Exception as e:
            print(e)
            print('Error on line {}'.format(sys.exc_info()[-1].tb_lineno))

        return respuesta.get_respuesta_http()

        return respuesta.get_respuesta_http()


    def post(self, request, *args, **kwargs):
        respuesta = RespuestaHTTP()
        servicio = ServicioUsuarios()


        try:
            user = servicio.get_usuario_jwt(request)
            usuario, rol = servicio.get_rol(user)

            if (rol != 'anonimo'):

                json_request = json.loads(request.body.decode('utf-8'))

                id_detalle_catalogo = json_request.get('id_detalle_catalogo', "")
                puntaje = json_request.get('puntaje', "")
                perfil = Perfil.objects.get(user=user)
                if (id_detalle_catalogo and puntaje):

                    valoracion_existente = Valoracion.objects.filter(detalle_catalogo=id_detalle_catalogo).filter(perfil_creador=perfil)





                    if (Decimal(puntaje) >= 0 and Decimal(puntaje) <= 5):
                        try:
                            detalle = DetalleCatalogo.objects.get(id = id_detalle_catalogo)
                        except:
                            respuesta.tipo_error()
                            respuesta.set_mensaje("La publicación no existe")
                            return respuesta.get_respuesta_http()

                        if (len(valoracion_existente) == 0):
                            valoracion = Valoracion()
                            valoracion.detalle_catalogo = detalle
                            valoracion.perfil_creador = perfil
                            valoracion.puntaje = Decimal(puntaje)
                            valoracion.save()

                            valoraciones = Valoracion.objects.filter(detalle_catalogo=detalle)

                            suma_valoraciones = (valoraciones.aggregate(Sum('puntaje'))['puntaje__sum'])  # [0].puntaje
                            cantidad = len(valoraciones)  # [0].cantidad

                            detalle.valoracion_promedio = suma_valoraciones / cantidad
                            detalle.save()
                            respuesta.tipo_ok()
                        else:
                            respuesta.set_mensaje("Ya ha valorado esta publicación")



                    else:
                        respuesta.set_mensaje("El puntaje debe estar entre 0 y 5")




                else:
                    respuesta.set_mensaje("Faltan datos obligatorios")
            else:
                respuesta.set_mensaje("Debe estar registrado para comprar un producto")

        except Exception as e:
            print(e)
            print('Error on line {}'.format(sys.exc_info()[-1].tb_lineno))

        return respuesta.get_respuesta_http()




    def put(self, request, *args, **kwargs):
        respuesta = RespuestaHTTP()
        servicio = ServicioUsuarios()
        return respuesta.get_respuesta_http()



    def delete(self, request, *args, **kwargs):
        respuesta = RespuestaHTTP()
        return respuesta.get_respuesta_http()



