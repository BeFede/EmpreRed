from decimal import Decimal

from django.db import transaction
from django.db.models import Q
from django.views import View

from Emprered.server.models import venta
from Emprered.server.models.catalogo import DetalleCatalogo, Catalogo
from Emprered.server.models.emprendedores import Emprendedores
from Emprered.server.models.usuarios import Perfil
from Emprered.server.models.venta import Venta, Pago, DetallesVenta
from Emprered.server.servicios.servicio_mercado_pago import ServicioMercadoPago
from Emprered.server.views.mercado_pago_view import MercadoPagoView
from Emprered.utils import RespuestaHTTP
from Emprered.server.servicios.servicio_usuarios import ServicioUsuarios
from Emprered.utils import  Paginador
import mercadopago
import json
import sys
from Emprered.settings import APP_ID_MERCADO_PAGO, SECRET_KEY_MERCADO_PAGO, DEBUG


class VentasView(View):


    def get_ventas(self, orden, filtros, pagina_actual, tamano_pagina):

        servicio_mercado_pago = ServicioMercadoPago()

        ventas = Venta.objects.all()



        ventas_list = []

        if filtros.get('emprendedor_id'):
            ventas = ventas.filter(emprendedor=filtros.get('emprendedor_id'))
        if filtros.get('comprador_id'):
            ventas = ventas.filter(comprador__user = filtros.get('comprador_id'))
        if filtros.get('emprendedor_nombre'):
            ventas = ventas.filter(emprendedor__nombre__icontains=filtros.get('emprendedor_nommbre'))
        if filtros.get('emprendedor_apellido'):
            ventas = ventas.filter(emprendedor__apellido__icontains = filtros.get('emprendedor_apellido'))
        if filtros.get('comprador_nombre'):
            ventas = ventas.filter(comprador__user__username__icontains = filtros.get('comprador_nombre'))
        if filtros.get('estado'):
            ventas = ventas.filter(estado = filtros.get('estado'))
        if filtros.get('fecha_desde'):
            ventas = ventas.filter(fecha_hora_registro__gte = filtros.get('fecha_desde'))
        if filtros.get('fecha_hasta'):
            ventas = ventas.filter(fecha_hora_registro__lte = filtros.get('fecha_hasta'))
        if (filtros.get('ventas_id')):
            ventas = ventas.filter(id = filtros.get('ventas_id'))



        for venta in ventas:
            agregar = True


            pagos = Pago.objects.filter(venta = venta)
            detalles = DetallesVenta.objects.filter(venta = venta)



            productos_dic = []
            detalles_dic = []
            for detalle in detalles:

                if filtros.get('producto') and not (int(filtros.get('producto')) == detalle.producto.id):
                    agregar = False

                detalles_dic.append({
                    "id": detalle.id,
                    "id_publicacion": detalle.id_publicacion.id if detalle.id_publicacion else "",
                    "producto_id": detalle.producto.id,
                    "producto_nombre": detalle.producto.nombre,
                    "cantidad": detalle.cantidad,
                    "precio_individual": detalle.precio_individual
                })


            pagos_dic = []
            for pago in pagos:

                if filtros.get('monto_desde') and pago.monto < Decimal(filtros.get('monto_desde')):
                    agregar = False
                if filtros.get('monto_hasta') and pago.monto > Decimal(filtros.get('monto_hasta')):
                    agregar = False
                if filtros.get('metodo_pago') and pago.metodo_pago != filtros.get('metodo_pago'):
                    agregar = False


                tarjeta_dic = {
                    "tipo": "",
                    "nombre_titular": "",
                    "dni_titular": "",
                    "ultimos_cuatro_digitos": ""
                }

                if pago.tarjeta:

                    tarjeta_dic ={
                        "tipo": pago.tarjeta.tipo if pago.tarjeta else "",
                        "nombre_titular": pago.tarjeta.nombre_titular if pago.tarjeta else "",
                        "dni_titular": pago.tarjeta.dni_titular if pago.tarjeta else "",
                        "ultimos_cuatro_digitos": pago.tarjeta.ultimos_cuatro_digitos if pago.tarjeta else ""
                    }

                pago_dic = {
                    "id": pago.id,
                    "monto": pago.monto,
                    "metodo_pago": pago.metodo_pago,
                    "estado_id": pago.estado,
                    "estado": pago.get_estado(),
                    "tarjeta": tarjeta_dic
                }

                pagos_dic.append(pago_dic)


            comprador_dic = {
                "id": venta.comprador.id,
                "email": venta.comprador.user.email,
                "username": venta.comprador.user.username
            }

            emprendedor_dic = {
                "id": venta.emprendedor.id,
                "nombre": venta.emprendedor.nombre + " " + venta.emprendedor.apellido
            }

            venta_dic = {
                "id": venta.id,
                "fecha_hora": venta.fecha_hora_registro,
                "comprador": comprador_dic,
                "emprendedor": emprendedor_dic,
                "estado_id": venta.estado,
                "estado": venta.get_estado(),
                #"productos": productos_dic,
                "pagos": pago_dic,
                "monto": venta.monto,
                "detalles": detalles_dic
            }

            if (agregar):
                ventas_list.append(venta_dic)

        ventas_paginadas = Paginador(ventas_list, tamano_pagina)

        return ventas_paginadas.pagina(pagina_actual)



    def get(self, request, *args, **kwargs):
        respuesta = RespuestaHTTP()
        servicio = ServicioUsuarios()

        try:

            id = request.GET.get('id', None)

            user = servicio.get_usuario_jwt(request)
            usuario, rol = servicio.get_rol(user)

            consultar_como = kwargs.get('rol')

            if (rol != 'anonimo'):

                #emprendedor = usuario

                if not id:
                    orden = request.GET.get('orden', 'valoracion')
                    criterio = request.GET.get('criterio', 'desc')


                    tamano_pagina = request.GET.get('tamano_pagina', 10)
                    pagina_actual = request.GET.get('pagina_actual', 1)

                    fecha_desde = request.GET.get('fecha_desde', "")
                    fecha_hasta = request.GET.get('fecha_hasta', "")
                    estado = request.GET.get('estado', "")

                    monto_desde = request.GET.get('monto_desde', "")
                    monto_hasta = request.GET.get('monto_hasta', "")
                    metodo_pago = request.GET.get('metodo_pago', "")
                    producto = request.GET.get('producto', "")

                    comprador = request.GET.get('comprador_id', "")
                    emprendedor = request.GET.get('emprendedor_id', "")
                    comprador_nombre = request.GET.get('comprador_nombre', "")
                    emprendedor_nombre = request.GET.get('emprendedor_nombre', "")
                    emprendedor_apellido = request.GET.get('emprendedor_apellido', "")
                    venta_id = request.GET.get('venta_id', "")


                    if (consultar_como == 'vendedor' and rol == 'emprendedor'):
                        emprendedor = usuario
                    elif (consultar_como == 'comprador'):
                        comprador = user
                    else:
                        respuesta.set_mensaje("Puede consultar como vendedor o como comprador")
                        raise Exception("Puede consultar como vendedor o como comprador")

                    filtros = {

                        'emprendedor_id': emprendedor,
                        'comprador_id': comprador,
                        'comprador_nombre': comprador_nombre,
                        'emprendedor_nombre': emprendedor_nombre,
                        'emprendedor_apellido': emprendedor_apellido,
                        'estado': estado,
                        'fecha_desde': fecha_desde,
                        'fecha_hasta': fecha_hasta,
                        'monto_desde': monto_desde,
                        'monto_hasta': monto_hasta,
                        'metodo_pago': metodo_pago,
                        'producto': producto,
                        'venta_id': venta_id

                    }

                    criterio = '-' if criterio == 'desc' else ''

                    orden = {
                        'atributo': orden,
                        'criterio': criterio
                    }


                    respuesta.agregar_dato('ventas', self.get_ventas(orden=orden, filtros=filtros, pagina_actual=pagina_actual,
                                                              tamano_pagina=tamano_pagina))
                    respuesta.tipo_ok()
                else:
                    print("consultar más cosas de la venta")

            else:
                respuesta.set_mensaje("Debe estar registrado para realizar esta operación")

        except Exception as e:
            print(e)
            print('Error on line {}'.format(sys.exc_info()[-1].tb_lineno))


        try:
            respuesta = respuesta.get_respuesta_http()
            return respuesta
        except Exception as e:
            print(e)
            print('Error on line {}'.format(sys.exc_info()[-1].tb_lineno))



    def post(self, request, *args, **kwargs):
        respuesta = RespuestaHTTP()
        servicio = ServicioUsuarios()
        mercado_pago_servicio = ServicioMercadoPago()

        try:
            user = servicio.get_usuario_jwt(request)
            usuario, rol = servicio.get_rol(user)

            if (rol != 'anonimo'):

                json_request = json.loads(request.body.decode('utf-8'))

                id_detalle_catalogo = json_request.get('id_detalle_catalogo', "")
                cantidad = json_request.get('cantidad', "")
                # si no se manda url_exito entonces se asume que la petición es mobile
                url_exito = json_request.get('url_exito', "")
                url_error = json_request.get('url_error', "")

                pago_mobile = json_request.get('pago_mobile', 0)



                if (id_detalle_catalogo and cantidad and cantidad > 0):


                    perfil = Perfil.objects.all().get(user=user)

                    detalle_catalogo = DetalleCatalogo.objects.all().get(id=id_detalle_catalogo)
                    catalogo = Catalogo.objects.get(id=detalle_catalogo.catalogo.id)

                    emprendedor = Emprendedores.objects.all().get(id=catalogo.emprendedor.id)

                    access_token = emprendedor.access_token_mercado_pago

                    with transaction.atomic():
                        venta = Venta()
                        venta.comprador = perfil
                        venta.emprendedor = emprendedor
                        venta.estado = venta.PENDIENTE_DE_PAGO

                        venta.monto = detalle_catalogo.precio * cantidad
                        venta.save()
                        status, response_mercado_pago = mercado_pago_servicio.generar_link_pago(access_token, detalle_catalogo, cantidad, venta)

                        if (status == 200 or status == 201):


                            pago = Pago()
                            pago.monto = detalle_catalogo.precio * cantidad

                            pago.id_preferencia = response_mercado_pago['id']
                            pago.venta = venta
                            pago.url_exito = url_exito
                            pago.url_error = url_error

                            pago.pago_mobile = pago_mobile

                            pago.save()

                            detalle_venta = DetallesVenta()

                            detalle_venta.id_publicacion = detalle_catalogo
                            detalle_venta.producto = detalle_catalogo.producto
                            detalle_venta.cantidad = cantidad
                            detalle_venta.precio_individual = detalle_catalogo.precio
                            detalle_venta.venta = venta
                            detalle_venta.save()

                            if (DEBUG):
                                link_mercado_pago = response_mercado_pago['sandbox_init_point']
                            else:
                                link_mercado_pago = response_mercado_pago['init_point']

                            respuesta.tipo_ok()
                            respuesta.agregar_dato("link_de_pago", (link_mercado_pago))
                            respuesta.agregar_dato("id_venta", venta.id)
                        else:

                            print(response_mercado_pago)
                            respuesta.tipo_error()
                            respuesta.set_mensaje("Ha ocurrido un error al intentar conectarse con mercado pago")
                else:
                    respuesta.set_mensaje("Faltan datos obligatorios")
            else:
                respuesta.set_mensaje("Debe estar registrado para comprar un producto")

        except Exception as e:
            print(e)
            print('Error on line {}'.format(sys.exc_info()[-1].tb_lineno))


        return respuesta.get_respuesta_http()




    def put(self, request, *args, **kwargs):
        respuesta = RespuestaHTTP()
        servicio = ServicioUsuarios()
        return respuesta.get_respuesta_http()



    def delete(self, request, *args, **kwargs):
        respuesta = RespuestaHTTP()
        return respuesta.get_respuesta_http()



