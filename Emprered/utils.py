"""
En este documento irán todas aquellas clases y funciones comunes a todo el
sistema
"""
from django.http import HttpResponse
from django.views import View
from django.core.serializers.json import DjangoJSONEncoder
import requests
import json
import math



CLAVE_GOOGLE = "$3wef2324+.-423dfsd**asd#"

TIPO_AUTENTICACION_EMPRERED = 'emprered'
TIPO_AUTENTICACION_GOOGLE = 'google'


class RespuestaHTTP:
    ESTADO_ERROR = 'NO'
    ESTADO_FORBIDDEN = 'FORBIDDEN'
    ESTADO_OK = 'OK'

    def __init__(self, estado=ESTADO_ERROR, mensaje=''):
        self.estado = estado
        self.mensaje = mensaje
        self.datos = {}

    def set_estado(self, estado):
        self.estado = estado

    def set_mensaje(self, mensaje):
        self.mensaje = mensaje

    def tipo_error(self):
        self.set_estado(RespuestaHTTP.ESTADO_ERROR)

    def tipo_ok(self):
        self.set_estado(RespuestaHTTP.ESTADO_OK)

    def tipo_forbidden(self):
        self.set_estado(RespuestaHTTP.ESTADO_FORBIDDEN)

    def agregar_dato(self, clave, valor):
        self.datos[clave] = valor

    def get_respuesta_http(self):
        dic = {
            'estado': self.estado if self.estado is not RespuestaHTTP.ESTADO_FORBIDDEN else RespuestaHTTP.ESTADO_ERROR,
            'mensaje': self.mensaje,
            'datos': self.datos
        }
        status_code = 200
        if self.estado == RespuestaHTTP.ESTADO_ERROR:
            status_code = 500
        elif self.estado == RespuestaHTTP.ESTADO_FORBIDDEN:
            status_code = 403
        return HttpResponse(json.dumps(dic, cls=DjangoJSONEncoder), content_type='application/json',
                            status=status_code)


def validar_no_vacio(lista):
    """
    Recorre la lista y valida que ningún elemento sea None o ''
    :param lista: lista de variables
    :return: True si no hay elementos None o '', False de otro modo
    """
    for l in lista:
        if l is None or l is '':
            return False
    return True
    

def get_datos_token_google(token):
    if token is not None:
        r = requests.get('https://www.googleapis.com/oauth2/v3/tokeninfo?id_token=%s' % (token))
        if r.status_code == 200:
            json = r.json()
            email = json.get("email")
            nombre = json.get("given_name")
            apellido = json.get("family_name")
            foto = json.get("picture")
            return {
                'email': email,
                'nombre': nombre,
                'apellido': apellido,
                'foto':foto
            }
    return None


class NotAuthorizedException(Exception):
    """Para manejar el error de la falta de token. Forma de uso:
        servicio = ServicioUsuarios()
        user = servicio.get_usuario_jwt(request)
        if user.is_anonymous:
            raise NotAuthorizedException()
    """

    def __init__(self):
        self.message = "No está autorizado. Su sesión puede haber caducado."

    def __str__(self):
        return self.message


class Paginador():

    def __init__(self, data, tamano_pagina):
        self.datos = data
        self.tamano_pagina = int(tamano_pagina)

    def pagina(self, pagina):

        pagina = int(pagina)
        if (pagina <= 0): return []
        if (self.tamano_pagina  <= 0): return []

        cantidad_paginas = math.ceil(len(self.datos) / int(self.tamano_pagina))

        if (pagina > cantidad_paginas): return []

        indice_inicial = int(int(pagina) - 1) * int(self.tamano_pagina)

        datos_pagina = self.datos[indice_inicial:(indice_inicial + int(self.tamano_pagina))]

        respuesta = {
            "cant_pagina": cantidad_paginas,
            "pagina_actual": pagina,
            "tamano_pagina": self.tamano_pagina,
            "cantidad_resultados_totales": len(self.datos),
            "data": datos_pagina
        }
        return respuesta
