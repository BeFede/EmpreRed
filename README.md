# Proyecto Final Ing en Sistema - La Revancha

## Pasos para configurar y correr el Backend:

0. En un virtualenv, ejecutar:

```
pip install -r requirements.txt
```

Al hacerlo quizás aparezca un error como: `python setup.py egg_info" failed with error code 1 in /tmp/pip-install-7fbiilzs/mysqlclient`.
**En ese caso, lo lamento, no vas a poder correr el backend.**




Just kidding. Para solucionarlo usá la #Magia (TM):


Si estás en Linux:
```
sudo apt-get install python3-dev libmysqlclient-dev
```

Si estás en macOS:
```
brew install mysql-client
echo 'export PATH="/usr/local/opt/mysql-client/bin:$PATH"' >> ~/.bash_profile
echo 'export LIBRARY_PATH="$LIBRARY_PATH:/usr/local/opt/openssl/lib/"' >> ~/.bash_profile
source ~/.bash_profile
pip3 install mysqlclient
```

And *voilá*, you're all set. Intentá instalar el requirements.txt nuevamente.


1. Copiar en el archivo Emprered/Emprered/settings.py sus credenciales de la BD en donde dice DATABASES


2. Crear la base de datos:
```
$ mysql -u root -p # Enter for no password
MariaDB [(none)]> source script_bd; # Esto crea la base de datos
MariaDB [(none)]> use emprered_db; # Para usar la base de datos. Paso no necesario ahora.
```

Al correr `mysql -u root -p` puede salir el siguiente error:

`ERROR 2002 (HY000): Can't connect to local MySQL server through socket '/var/run/mysqld/mysqld.sock'`

Eso es porque faltan algunas cosas por instalar. Si estás en arch:

```
sudo mysql_install_db --user=mysql --basedir=/usr --datadir=/var/lib/mysql;
sudo systemctl start mysqld.service;
```

En Ubuntu:

```
sudo apt install mysql-server
```

En macOS sólo falta levantar el servicio:
```
brew services start mysql
```

Luego de crear la base de datos, **realizar makemigrations y migrate** (ver punto 6)

3. Para correr cualquier comando se ubican en /Emprered y usan `python manage.py comando`
*Nota: si su path de python está configurado a python2, asegúrense de usar python3 en vez de python.*
*Nota 2: si aparece un error con celery, instalarlo mediante pip..*

4. Crear nuevo superusuario: `python manage.py createsuperuser`. ¿Problemas con esto? Ver punto 6.

5. Correr el server `python manage.py runserver`

6. Si hacen modelos, para migrarlos a la BD tienen que hacer 2 pasos:

    ```
    python manage.py makemigrations
    python manage.py migrate
    ```


## Instalar y configurar Celery y RabbitMQ

Una vez instalados Celery (mediante pip) y RabbitMQ (mediante pacman, apt o brew), además de la configuración que ya está en settings.py, hay que agregar manualmente esta configuración en la máquina que los vaya a correr:

```
# En Linux
$ sudo rabbitmq-server
# En macOS
$ echo 'export PATH=$PATH:/usr/local/opt/rabbitmq/sbin' >> ~/.bash_profile
$ source ~/.bash_profile
$ sudo rabbitmq-server
```
Acá vas a tener que abrir otra consola, a menos que corras el comando anterior con la opción `-detached`
```
$ sudo rabbitmqctl add_user <user> <password>
$ sudo rabbitmqctl add_vhost <vhost>
$ sudo rabbitmqctl set_user_tags <user> <tag>
$ sudo rabbitmqctl set_permissions -p <vhost> <user> ".*" ".*" ".*"
```

> Sustituir los valores de arriba, idealmente así:
  user = root;
  password = root;
  vhost = emprered_host;
  tag = root_tag;

Ya con esto debería ser suficiente. Dejá el server de rabbitmq corriendo y ejecutá—del mismo modo que ejecutarías el runserver de Django—lo siguiente (mind your pwd):

```
$ celery -A Emprered worker -l info
```

También hay que correr el beat de Celery, que puede correrse junto con el comando anterior pero no es recomendable. El beat ejecuta las tareas que se ejecutan cada cierto tiempo, por ejemplo, la que verifica los eventos próximos.

```
$ celery -A Emprered worker -B
```

#### Importante

TL;DR: siempre pero siempre levantá celery y rabbitmq cuando vayas a usar EmpreRed.

> Explicación: Sin ejecutarlos cada vez que levantes el server de django, algunos modelos de la base de datos podrían no actualizarse nunca. Esto ocurre porque cuando los modelos indexados por haystack se actualizan, hay que reconstruir el índice del motor de búsqueda, y esto es realizado por celery en segundo plano. La falta de celery hace que el server se quede esperando infinitamente hasta poder mandarle la task de reconstrucción del índice.


## Pasos para configurar y correr la UI

1. Primero que nada se ubican en Emprered/Emprered/client y hacen `npm install`

Les van a saltar un par de errores con expo y alguna otra pero no les den bola

2. Para correr la App Web, debajo de esa ubicación usan `npm start`

3. Para correr la App Mobile, debajo de esa ubicación usan `react-native run-android`

Ahí les va a pedir que pongan en consola 2 comandos "sudo sysctl...", háganlo.



### Integrantes:
* [Benito Federico] - 66921
* [Brond Matías] - 67201
* [Filardo Juan] - 67627
* [Silva Federico] - 67948
* [Valentinuzzi Nicolás] - 67625

[//]:# (Links. This won't be seen after it's interpreted.)
[Benito Federico]: <https://t.me/BeFede>
[Brond Matías]: <https://t.me/MatiBrond>
[Filardo Juan]: <https://t.me/JuaniFilardo>
[Valentinuzzi Nicolás]: <https://t.me/NicoValentinuzzi>
[Silva Federico]: <https://t.me/FedeSilva>
